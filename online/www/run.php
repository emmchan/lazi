<?php 
session_start();
include "config.php";
$start_xml=<<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<commands>
<compute shortcuts-level="2" compute-args="true" profile="false">
<formula format="sources">

EOF
;
$end_xml=<<<EOF
</formula>
</compute>
</commands>
EOF
;
if(isset($_POST['text'])) 
  {
    $id=session_id();
    // check if there is no hack (the id is used inside commands).
    if(!preg_match("/^[a-z0-9]+$/",$id)) exit("bad session id"); 
    // paths
    $src_base="users_files/" . $id;
    $src_file= "$src_base.lazi";
    $input_lazi_file= "$src_base-input-lazi.xml";
    $input_xml_file= "$src_base-input-xml.xml";
    $output_lazi_file= "$src_base-output-lazi.xml";
    $output_xml_file= "$src_base-output-xml.xml";
    $output_html_file="$src_base-output.html";
    $output_url="$usr_base/users_files/$id-output.html";
    
    // Save pure source and source rapped with xml.
    file_put_contents($src_file,$_POST['text']);
    file_put_contents($input_lazi_file,$start_xml . $_POST['text'] . $end_xml);
    
    // First command -> translate the formula in xml, ready to be computed.
    $command="LANG=en_US.UTF-8;$lazi_path   --pathexec $lazi_bin --pathres $lazi_res_path --nonInteractive --simplify --xmlinput $input_lazi_file --output $input_xml_file --inputs0 \"$lazi_libs/02-tools-to-prove/\" --format xml 2>&1";
    $output=null;
    $retval=null;
    if(exec($command, $output, $retval)!==false)
    {
      if($retval!=0)
      {
        $error_msg="Syntax error ";
        // There is a syntax error.
        $line_start=substr($output[0],strpos($output[0],"(")+6);
        $line=substr($line_start,0,strpos($line_start,","));
        $error_msg="Syntax error line " . ($line-4) . " " . substr($line_start,strpos($line_start,","),-2);
        $error_msg .= substr($output[1],strpos($output[1],":"));
        echo<<<EOF
<html lang="en">
<head>
<title>Lazi reports</title>
<meta HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<style>
    h1,h2 {
        text-align:center;
    }
    div.errorBox {
      margin-left: 25%;
      margin-right: 25%;
      border: 1px solid red ;
      padding:5px;
      box-shadow: 4px 4px 2px 1px grey;
      text-align: center;
    }
</style>
</head>
<body>
<h1>Lazi, syntax error</h1> 
<div class="errorBox">$error_msg</div>
</body>
</html>
       
EOF;
      }
      else // If there is no syntax error.
      {
        // Second command : computation, limit to 1Go of memory (to stop loops)
        $command="$compute_path -c $input_xml_file  -d lazi_defs.xml -o $output_xml_file 2>&1";
        $output=null;
        $retval=null;
        if(exec($command, $output, $retval)!==false && $retval==0)
        { 
          // Third command : tranlate the result of the computation in an html page. It use xsltproc which need to know it's UTF-8.
          $command="LANG=en_US.UTF-8;$lazi_path  --inputs0 \"$lazi_libs/02-tools-to-prove/\" --format html  --pathexec $lazi_bin --pathres $lazi_res_path --xmlinput  $output_xml_file  --output $output_html_file 2>&1";
          $output=null;
          $retval=null;
          if(exec($command, $output, $retval)!==false)
          { 
            // Redirect to the result page.
            header("Location: $output_url");
          }
        } else {
        $add_detail = $debug ? "<br>Error message : " . implode('<br>', $output) : "";
        echo<<<EOF
<html lang="en">
<head>
<title>Lazi reports</title>
<meta HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<style>
    h1,h2 {
        text-align:center;
    }
    div.errorBox {
      margin-left: 25%;
      margin-right: 25%;
      border: 1px solid red ;
      padding:5px;
      box-shadow: 4px 4px 2px 1px grey;
      text-align: center;
    }
</style>
</head>
<body>
<h1>Lazi, computation error</h1> 
<div class="errorBox">Error in computation or out of memory.<br>Probably a loop.$add_detail</div>
</body>
</html>
       
EOF;
        }
      }
    }
    else
    {
      die($output);
    }
  }
?>
