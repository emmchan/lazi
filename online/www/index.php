<?php 
  include "config.php";
  session_cache_expire(14400); // minutes, 14400=10 days
  session_set_cookie_params(864000,$session_path); // 10 jours, path of sessions files
  session_start();
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lazi's sandbox </title>
<style type="text/css" media="screen">
    #editor { 
        position: absolute;
        top: 50px;
        right: 0;
        bottom: 0;
        left: 0;
    }
    span.ace_ponctuation {
      color : blue;
      font-weight: bold;
    }
    span.ace_dollKeyword {
      color : #B00;
    }
    span.ace_longComment,span.ace_comment {
      color : green;
    }
    button.editor {
      color: blue;
    }
    button.run {
      color: green;
    }
    button.clear {
      color: red;
    }
    a.doclink {
      margin-left: 3em;
    }
</style>
</head>
<body>

<div id="editor"><?php 
$id=session_id();
if(!preg_match("/^[a-z0-9]+$/",$id)) exit("bad session id"); // check if there is no hack
$src_file="users_files/" . $id . ".lazi";
// If called by this page to save
if(isset($_POST['save']))
{
  file_put_contents($src_file,$_POST['text']);
  echo($_POST['text']);
} else {
  // Else, if the text exists in a file.
  if(file_exists($src_file)) {
    echo(file_get_contents($src_file));
  }
}
?></div>
<script src="ace/ace.js" type="text/javascript" charset="utf-8"></script><script>
    window.name = 'lazi_sandbox';
    var editor = ace.edit("editor");
    var UndoManager = require("ace/undomanager").UndoManager;
    var undoManager = new UndoManager();
    editor.session.setMode("ace/mode/lazi");
    // Undo manager initialisation
    editor.getSession().setUndoManager(this.undoManager);
    this._bindKeys = function(){
             this.editor.commands.addCommands([
                 {
                     name : 'undo',
                     bindKey : 'Ctrl-Z',
                     exec : function(editor){
                         editor.session.getUndoManager().undo();
                     }
                 },
                 {
                     name : 'redo',
                     bindKey : 'Ctrl-Y',
                     exec : function(editor){
                         editor.session.getUndoManager().redo();
                     }
                 }
             ]);
         };

    function setTextAndPost() {
      document.getElementById("formText").value=editor.getValue();
      document.getElementById('runForm').submit();
    }
    function save() {
      document.getElementById("formSaveText").value=editor.getValue();
      document.getElementById('saveForm').submit();
    }
    function clearContent() {
      editor.setValue("");
    }
</script>
<form style="display: inline" id="runForm" action="run.php" target="result" method="POST" >
  <input id="formText" name="text" type="hidden" value="?">
  <button class="editor" type="submit" form="run" value="run" onclick="editor.session.getUndoManager().undo()">Undo</button>
  <button class="editor" type="submit" form="run" value="run" onclick="editor.session.getUndoManager().redo()">Redo</button>
  <button class="run" type="submit" form="run" value="run" onclick="setTextAndPost()">Save and Run</button>
</form>
<form style="display: inline" id="saveForm" action="index.php" method="POST" >
  <input id="formSaveText" name="text" type="hidden" value="?">
  <input name="save" type="hidden" value="1">
  <button class="run" type="submit" form="run" value="run" onclick="save()">Save</button>
  <button class="clear" type="submit" form="run" value="run" onclick="clearContent()">Clear</button>
</form>
<?php 
if(isset($_POST['save'])) 
  echo<<<EOF
<span id="tempMessage" style="color: green;display: inline;border: 1px solid black ;padding:5px;box-shadow: 4px 4px 2px 1px grey">File saved</span>
<script>
function hideMessage(){
 document.getElementById("tempMessage").style.display = "none";
}
setInterval(hideMessage,3000);
</script>
EOF;
?>
<a class="doclink" href="/wiki/Pr%C3%A9sentation/Lazi_Tutorial.html" target="lazi_tutorial">Tutorial</a>
</body>
</html>
