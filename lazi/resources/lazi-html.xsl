<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="sorttable.js"/>
<!-- ******************* Common headers ************************ -->

<xsl:variable name="header">
<meta HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8"/>
<style>
    h1,h2 {
        text-align:center;
    }
    
    h1.subtitle {
        text-align:center;
        color: #555;
        background: #88F;
    }
    
    h2 {
        background: #803;
        color: white
    }
    span.arrow {
    
    }
    span.arrow.success {
    background: #efe;
    }
    span.arrow.failure {
    background: #fee;
    }

    .result-success {
        background: #cfc;
        color: #303;
    }
    
    .result-failure {
        background: #fcc;
    }
    
    div.traces h3 {
        background: #fcc;
    }
    
    span.entitled {
    font-weight: bold;
    display:inline-block;
    width: 8em;
    }

    div.formula {
    font-family: monospace;
    white-space: pre;    
    margin: 0.2em;
    padding: 0.2em;
    background: #f8f8ff;
    } 
    
    table.profile  {
        border-collapse: collapse;
        border: 1px solid black;
        margin: 1em;
    }

    table.profile th {
        background-color: #ccc;
    }

    table.profile td,th {
        padding-left : 1em;
        padding-right : 1em;
        text-align: right;
        border: 1px solid grey;
        text-wrap: normal;
        word-wrap: break-word;
    }

    div.status0 {
    background: white
    }

    div.status1 {
    background: #ffeeee
    }

    div.status2 {
    background: #ffaaaa
    }

    div.content {
        padding-left: 1em;
    }

    div.clear {
    clear:both;
    }
</style>
</xsl:variable>


<!-- ******************* Common template ************************ -->

<xsl:template match="formula">
<div class="formula"><xsl:value-of select="."/></div>
</xsl:template>

<xsl:template match="title">
    <h1 class="subtitle"><xsl:value-of select="."/></h1>
</xsl:template>

<!-- ******************* REPORTS ************************ -->

<xsl:template match="report">
  <xsl:for-each select="compute">
      <h2>Compute command n°<xsl:value-of select="../@number"/></h2>
      <p class="command-options">Options: 
      <xsl:if test="@times != 'infinite'">times=<xsl:value-of select="@times"/></xsl:if>
      <xsl:if test="@shortcuts-level"> shortcuts-level=<xsl:value-of select="@shortcuts-level"/></xsl:if>
      <xsl:if test="@compute-args"> compute-args=<xsl:value-of select="@compute-args"/></xsl:if>
      </p>
      
      <xsl:for-each select="comment">
      <h3>Comment</h3>
      <p><xsl:value-of select="."/></p>
      </xsl:for-each>

      <h3>To compute</h3>
      <div class="formula"><xsl:apply-templates select="formula"/></div>
      <xsl:for-each select="check">
          <h3>Check, must match</h3>
          <div class="formula">
              <xsl:apply-templates select="formula"/>
          </div>
      </xsl:for-each>
  
  </xsl:for-each>

  <xsl:for-each select="result">
      <div class="result">
          <h3>
              
                  <xsl:choose>
                      <xsl:when test="@passed = 'true' or (not(@passed) and @done = 'true')">
                        <xsl:attribute name="class">result-success</xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:attribute name="id">bottom</xsl:attribute>
                        <xsl:attribute name="class">result-failure</xsl:attribute>
                      </xsl:otherwise>
                  </xsl:choose>
              
              Result
          </h3>
          <xsl:choose>
              <xsl:when test="@passed">
                  <xsl:choose>
                      <xsl:when test="@passed = 'true' or (not(@passed) and @done = 'true')"><span class="result-success">Passed</span></xsl:when>
                      <xsl:otherwise>
                        <span class="result-failure">Failure, result is :</span>
                        <xsl:apply-templates select="formula"/>
                      </xsl:otherwise>
                  </xsl:choose>
              </xsl:when>
              <xsl:otherwise><xsl:apply-templates select="formula"/></xsl:otherwise>
          </xsl:choose>
      </div>
  </xsl:for-each>
  
  <xsl:for-each select="profiling">
      <div class="profiling"><h3>Profiling</h3>
          <xsl:for-each select="time">
              <p>Total computation time : <xsl:value-of select="."/> <br/><small>All durations are in <a href="https://fr.wikipedia.org/wiki/High_Precision_Event_Timer">HPET</a> unit (computer dependant and smaller than 1^-10 sec).</small></p>
          </xsl:for-each>
          <xsl:for-each select="calls">
              <p><i>Click on the header to sort.</i></p>
              <table class="profile sortable"><tr><th>Name &amp; guessed arity</th><th>Interpreter calls</th><th>Internal time</th><th>...in %</th><th>External time</th><th>Can shortcut</th><th>Shortcut calls</th></tr> 
              <xsl:for-each select="dvar">
                  <tr>
                      <td class="defined-name"><xsl:value-of select="@name"/></td>
                      <td><xsl:value-of select="@count"/></td>
                      <td><xsl:value-of select="@internal-time"/></td>
                      <td><xsl:value-of select="@internal-time-percent"/>%</td>
                      <td><xsl:value-of select="@external-time"/></td>
                      <td><xsl:value-of select="@is-shortcut"/></td>
                      <td>
                          <xsl:if test="@shortcut-count"><xsl:value-of select="@shortcut-count"/></xsl:if>
                      </td>
                  </tr>
              </xsl:for-each>
              </table>
          </xsl:for-each>
      </div>
  </xsl:for-each>
  
  <xsl:for-each select="traces">
      <div class="traces"><h3>Traces</h3>
        <ul class="traces">
          <xsl:for-each select="trace">
            <li  class="traces">
              <p class="trace-label"><xsl:value-of select="@label"/> :</p>
              <xsl:apply-templates select="formula"/>
            </li>
          </xsl:for-each>   
        </ul>
      </div>
  </xsl:for-each>   
</xsl:template>

<xsl:template match="/reports">
<html lang="en">
<head>
<title>Lazi reports</title>
<xsl:copy-of select="$header" />
<script type="text/javascript">
<xsl:value-of select="$sortable" />
</script>
</head>
<body>
    <h1>Lazi, computation reports</h1>
    
    <xsl:for-each select=".">
       <xsl:apply-templates/>
    </xsl:for-each>
    <span id="bottom"/><!--There can be a "bottom" above if there is an error, then the navigator choose the first one: on the error. This is to avoid to the user to search from the bottom of a very long formula.-->
</body>
</html>
</xsl:template>

<!-- ******************* Definitions ************************ -->

<xsl:template match="/definitions">
<html lang="en">
<head>
<title>Lazi, definitions</title>
<xsl:copy-of select="$header" />
</head>
<body>
    <h1>Définitions</h1>
    
    <xsl:for-each select="definition">
    
      <b>$<xsl:value-of select="@name" /></b> = 
      <div class="formula"><xsl:value-of select="."/></div>
    
    </xsl:for-each>
</body>
</html>
</xsl:template>


<!-- ******************* Commands ************************ -->

<xsl:template match="/commands">
<html lang="en">
<head>
<title>Lazi, commands</title>
<xsl:copy-of select="$header" />
</head>
<body>
    <h1>Commands</h1>
    
    <xsl:for-each select="compute">
    
    <b>Compute <xsl:value-of select="@times" /> times :</b>
    <div class="formula"><xsl:value-of select="."/></div>

    </xsl:for-each>

<span id="bottom"></span>
</body>
</html>
</xsl:template>


</xsl:stylesheet>
