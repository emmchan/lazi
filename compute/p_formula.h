#pragma once
// Smart pointer to a formula.

class Formula;
extern Formula *p_null_formula;

class PFormula {
public:
  // The nullFormula has the same functionality as the NULL pointer without the overload.
  inline PFormula(Formula *_p=p_null_formula);

  // Copy constructor.
  inline PFormula( const PFormula& sp);
  
  inline ~PFormula();

// This pragma is for PFormula *dict_init in formula/word/shortcut/dict_apply.cpp
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
  inline Formula* operator-> () { return p; };

  inline const Formula* operator-> () const { return static_cast<const Formula*>(p); };
  
  inline Formula& operator* () { return *p; };

  inline const Formula& operator* () const { return *static_cast<const Formula*>(p); };

  // Overload of assignment. Race case x=x works.
  inline PFormula& operator= (const PFormula& sp);

  // Formulas equality and inequality
  inline bool operator==(const PFormula& sq) const {return p==sq.p;};

  inline bool operator!=(const PFormula& sq) const {return p!=sq.p;};

  inline bool operator==(const Formula * q) const  {return p==q;};

  inline bool operator!=(const Formula * q) const  {return p!=q;};
  
  inline Formula * get_p() {return p;}
  
  inline bool is_not_null() const {return p!=p_null_formula;};
  
  inline bool is_null() const {return p==p_null_formula;};

private:
  Formula *p;
};
