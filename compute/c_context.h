// Computation context class. Store context informations for a computation.
#pragma once

#include <list>
#include <string>
#include <utility>

#include "p_formula.h"

using namespace std;

class CContext
{
public:
  CContext(unsigned int _shortcut_level,bool _profile, bool _store_callstack, int _max_callstack, int _max_callstack_trace) :  to_continue(true),profile(_profile), store_callstack(_store_callstack), max_callstack(_max_callstack), max_callstack_trace(_max_callstack_trace), max_callstack_reached(false), stop_at_nbr(false), shortcut_level(_shortcut_level) {
    watch=profile||store_callstack;
  };
  
  // Store a trace information.
  void take_trace(string label, PFormula value) {
    trace.push_back(make_pair(label,value));
  };
  
  // Return a Pformula (a list) corresponding to the stack
  PFormula callstack_to_list();
  bool to_continue; // De we have to continue the computation ? (Set to false when computation can't continue).
  bool profile; // see command.h
  bool store_callstack; // see command.h
  int max_callstack;// see command.h
  int max_callstack_trace; // see command.h
  bool max_callstack_reached; // If the callstack reached the maxumum: we must stop.
  // Is there a time consuming flag (this group store_callstack and do_profile to avoid time consuming checks).
  bool watch; // Do we have to profile or store call trace ?
  unsigned int computation_nbr; // Number of computations already done.
  bool stop_at_nbr; // Do we have to stop computation if reached the limit of computation number ?
  // Maximum usable shortcut level.
  unsigned int shortcut_level; 
  // For tracing values
  list<pair<string,PFormula>> trace;
  // The actual call stack
  list<PFormula> callstack;
};


