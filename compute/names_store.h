#pragma once
// Store (using a map) a set of object of parent type NameFormula. This is used to have just one formula for a name. For exemple there is just one instance of the Keyword "if", so all "if" in formulas share the same object. This is good for the memory footprint but also to compare Keyword objects because we just have to compare the address.
// We need a template because "get" create the object.


#include <string>
#include <map>

#include "p_formula.h"

using namespace std;

template <class T> class NamesStore
{
public:
  // Test if an object exists
  bool check(string name);
  
  // Add an object.
  void add(PFormula obj);

  // Get an object and create it if it doesn't exists.
  PFormula get(string name);

  // Return the store
  map<string,PFormula> &get_store() {return store;};
  
//========== data
private:
  map<string,PFormula> store;
};

#include "names_store.cpp" // template trick

