#pragma once
// MatchAnything is used to math anything.

#include "match_no_apply.h"

class MatchAnything : public Match
{
public:
  MatchAnything() {};
  bool match(CContext &/*C*/, PFormula &/*f*/) const { return true; };
};

// This class has no parameter, so one global object is enough.
extern MatchAnything match_anything;
