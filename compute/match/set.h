#pragma once
// MatchSet use a Match object argument (called "child") to match the formula, but it also set the value of the formula in a reference of a formula (var).

#include "match.h"

class MatchSet : public Match
{
public:
  MatchSet(PFormula & _var, const Match &_child) : var(_var), child(_child) {};
  
  bool match(CContext &C, PFormula &f) const 
  {
    if(child.match(C,f))
    {
      var=f->to_sharer(); // The value can be computed, so we share the computation.
      return true;
    }
    return false;
  }
protected:
  PFormula &var ; // Where to put the pointer on the PFormula containing the result.
  const Match &child; // The match condition for the formula.
};
