#pragma once
// MatchEqual is used to test syntaxic equality.

#include "match_no_apply.h"
#include "../formula/formula.h"

class MatchEqual : public MatchNoApply
{
public:
  MatchEqual(PFormula _value) : value(_value) {};
  
  bool match_no_compute(PFormula &f) const {
    return value->synt_equal(f);
  };
protected:
  const PFormula value; // The value to be compared.
};
