#include "apply.h"

bool MatchApply::match(CContext &C, PFormula &f) const 
{ 
  Apply *p=f->get_Apply();
  if(p!=nullptr) // If it's an apply
  {
    if(mf.match(C,p->f)) // If the function part matches.
      return ma.match(C,p->a); // We miss rares cases when "f x = f y" and we try to match "f y". The matching system is used for shortcuts, we can miss some cases, the result will be a non-shortcut computation. And in practice we don't use matching with a recursive f, hence computations are faster this way.
    else
    {
      // Else we compute f and we try to match.
      PFormula g=f->compute(C); // debug!
      f=g;
      p=f->get_Apply();
      return p!=nullptr && mf.match(C,p->f) && ma.match(C,p->a);
    }
  }
  // Other cases: it does not match.
  return false;
}

