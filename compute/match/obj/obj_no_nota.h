#pragma once
/*
MatchObjNoNota match a function with a Dict as body. It return the variable and the dictionary and convert the formula to an Obj notation. Contrary to dict matching, we don't need to return an iterator because Obj matching doesn't have to convert to Dict.
*/

#include "../match_no_apply.h"

#include "../../formula/function.h"
#include "../../formula/obj.h"

class MatchObjNoNota : public MatchNoApply
{
public:
  // "var" is the variable name of the function.
  MatchObjNoNota(WordLVar * &_var, Dict * &_dict) : var(_var), dict(_dict) {};
    
  bool match_no_compute(PFormula &f) const { 
    Function *func=f->get_Function();
    if(func!=nullptr)
    {
      var=func->var;
      dict=func->body->get_Dict();
      if(dict!=nullptr)
      {
        f=new Obj(f);
        return true;
      }
    }
    return false;
  }
  
protected:
  WordLVar * & var;
  Dict * & dict;
};

