#pragma once
/*
MatchObj match an Obj notation, like MatchObjNota (it's a typedef). We don't try to recognize a non notation structure, because a dictionary insine an object can't have (Pre)Sharer at any place. Then it's simpler to work on shortcuts to have very few Obj::simplify.
It return the variable and the dictionary. Contrary to dict matching, we don't need to return an iterator because Obj matching doesn't have to convert to Dict.
*/

#include "../match.h"
#include "obj_no_nota.h"
#include "obj_nota.h"
#include "../or.h"

#define MatchObj MatchObjNota 

/*
class MatchObj : public Match
{
public:
  MatchObj(WordLVar * & _var, Dict * & _dict) : var(_var), dict(_dict), m_nota(_var,_dict),m_no_nota(_var,_dict),m(m_nota,m_no_nota) {};
    
  bool match(CContext &C, PFormula &f) const { return m.match(C,f); };
  
protected:
  WordLVar * & var;
  Dict * & dict;
  
  MatchObjNota m_nota;
  MatchObjNoNota m_no_nota;
  OrMatch m;
};*/
