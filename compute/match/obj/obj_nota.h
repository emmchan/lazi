#pragma once
/*
MatchObjNota match an Obj. It return the variable and the dictionary. Contrary to dict matching, we don't need to return an iterator because Obj matching doesn't have to convert to Dict.
*/

#include "../match_no_apply.h"

#include "../../formula/obj.h"

class MatchObjNota : public MatchNoApply
{
public:
  // "var" is the variable name of the function.
  MatchObjNota(WordLVar * &_var, Dict * &_dict) : var(_var), dict(_dict) {};
    
  bool match_no_compute(PFormula &f) const { 
    Obj *p=f->get_Obj();
    if(p!=nullptr)
    {
      Function *func=p->get_f()->get_Function();
      var=func->var;
      dict=func->body->get_Dict();
      assert(dict!=nullptr);
      return true;
    }
    return false;
  }
  
protected:
  WordLVar * & var;
  Dict * & dict;
};

