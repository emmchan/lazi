#pragma once
// MatchNoApply is the base class of Match classes when we don't match an apply. In this case, we compute the formula if it's an apply, else we just check if it's match without computation (a non apply always compute to itself). These classes have to overload match_no_compute.
#include <assert.h>

#include "match.h"
#include "../formula/apply.h"

class MatchNoApply : public Match
{
public:
  // Try to match with f.
  bool match(CContext &C, PFormula &f) const {
    // If the formula is an apply, no need to try to match.
    if(Apply::is_me(f))
    {
      f=f->compute(C); // When an application is computed (see Apply::compute_internal() ) the argument is put in a sharer, so the computation of f is stored.
      return match_no_compute(f);
    }
    else
    {
      if(match_no_compute(f))
        return true;
      f=f->compute(C);
      return match_no_compute(f);
    }
  };
  
  // Same as match, but do not try to compute, just to match.
  virtual bool match_no_compute(PFormula &/*f*/) const=0;
};
