#pragma once
/*
MatchApply try to match an apply and to recurse the matching using the given two Match sub-objects.MatchApply does not own its children (see why in Notes.txt). 
Note : if we get an apply, the continuation of the matching can change the value of the apply, because the computation modifies its formulas.
We have to make care of what we compute: we must compute something that for sure would be computed the normal way.
The algorithm is:
- Compute the formula if it's not an Apply.
- Try to match the left part, without computing a function of an apply (but we can compute the overall apply). See Match::match_apply comment.
- If success, match the right part.
*/

#include "match.h"
#include "../formula/apply.h"

class MatchApply : public Match
{
public:
  MatchApply(const Match &_mf, const Match &_ma) : mf(_mf),ma(_ma) {};
  
  bool match(CContext &C, PFormula &f) const;
protected:
  const Match &mf; // The match condition for the function.
  const Match &ma; // The match condition for the argument.
};
