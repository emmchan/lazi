#pragma once
/*
OrMatch have 3 arguments:
- the_first : a boolean
- match1 : a Match object
- match2 : a Match object

It returns true if one of the two match matches (match1 is tested, if failure match2 is tested). If one of the pair matches, then "the_first" is true if it's the first one.
*/
#include "match.h"

class OrMatch : public Match
{
public:
  OrMatch(bool &_the_first, const Match &_m1, const Match &_m2) : the_first(_the_first), m1(_m1), m2(_m2) {};
  OrMatch(const Match &_m1, const Match &_m2) : the_first(dummy), m1(_m1), m2(_m2) {};
  
  bool match(CContext &C, PFormula &f) const
  {
    if(m1.match(C,f))
    {
      the_first=true;
      return true;
    }
    else
    {
      if(m2.match(C,f))
      {
        the_first=false;
        return true;
      }
    }
    return false;
  };
protected:
  bool &the_first; // If m1 or m2 match, then the_first is true if it's m1.
  bool dummy; // For the constructor without the "the_first"
  const Match &m1; // The match condition n°1.
  const Match &m2; // The match condition n°2.
};
