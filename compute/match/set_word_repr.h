#pragma once
// MatchSetWordRepr is like MatchSet but check that the formula is a WordRepr and return a pointer to a WordRepr.

#include "match_no_apply.h"

class MatchSetWordRepr : public MatchNoApply
{
public:
  MatchSetWordRepr(WordRepr * & _var) : var(_var) {};
  
  bool match_no_compute(PFormula &f) const 
  {
    WordRepr *p=f->get_WordRepr();
    if(p!=nullptr)
    {
      var=p;
      return true;
    }
    return false;
  }
protected:
  WordRepr * &var ; // Where to put the result.
};

