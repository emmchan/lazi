#pragma once
// Match is the base class of all Match classes.
#include <assert.h>

#include "../p_formula.h"
#include "../formula/formula.h"
#include "../c_context.h"

class Match
{
public:
  // Try to match with f.
  virtual bool match(CContext &C, PFormula &f) const =0;
};
