#pragma once
/*
MatchNode recognise a dictionary node, a notation or not. 
*/

#include "../or.h"
#include "node_no_nota.h"
#include "node_nota.h"


class MatchDictNode : public Match
{
public:
  MatchDictNode(PFormula &l_init,WordRepr * &key, PFormula &value) : 
    m1(l_init,key,value), m2(l_init,key,value), m(m1,m2) {};
  
  bool match(CContext &C, PFormula &f) const { return m.match(C,f); };
protected:
  MatchNodeNota m1;
  MatchNodeNoNota m2;
  OrMatch m;
};

