#pragma once
/*
A dictionary iterator when the dictionary can be notations or not.
*/

#include "dict_iterator.h"
#include "dict_nota_iterator.h"
#include "../node.h"

class DictNoNotaIterator : public DictIterator
{
public:
  // "valid" is used to know if the match workds for the first node.
  DictNoNotaIterator(bool &valid, DictIterator * & _ref_this, CContext &_C, PFormula &_dict, WordRepr * &_key, PFormula &_value):
    ref_this(_ref_this),C(_C), dict(_dict), key(_key),value(_value),m(l_init,_key,_value), m_0l(dvar_0l), converted(false)
  {
    ref_this=static_cast<DictIterator *>(this);
    valid=m.match(C,dict);
    parent_node=dict;
    assert(!valid || DictNode::is_me(parent_node));
  };
  ~DictNoNotaIterator() {};
  
  bool next() { 
    PFormula *p=&(parent_node->get_DictNode()->l_init);
    if(m.match(C,*p)) // The created DictNode will be written in l_init of it's parent.
    {
      parent_node=*p; // The new parent.
      assert(DictNode::is_me(parent_node));
      return true;
    }
    return false;
  };
  void rewind() { 
    if(converted) {
      ref_this = new DictNotaIterator(dict->get_Dict(),key,value); // Update the reference to this.
      delete this; // Generaly dangerous but valid here.
    } 
    else
    {
      m.match(C,dict);
      parent_node=dict;
      assert(DictNode::is_me(parent_node));
    }
  };
  bool is_end() { 
    bool is_end=m_0l.match(C,parent_node->get_DictNode()->l_init); 
    // If we reach the end, it means all nodes and the 0l are DictNode and dvar_0l. So we can convert to a dict.
    if(is_end)
    {
      dict = new Dict(dict); // No risk to loop, argument passed by value
      converted=true;
    }
    return is_end;
  };
  
  bool is_nota_dict() const  { return false; };
  
  
  
protected:
  DictIterator * & ref_this; // The pointer to this, so we can change it when the iterator is converted.
  CContext &C;
  PFormula &dict;
  WordRepr * & key; // Just to store the argument to be able to create a DictNotaIterator
  PFormula & value; // Same as above.
  // The node can be a notation or not.
  MatchDictNode m;
  MatchEqual m_0l;
  PFormula parent_node; // The node owner of the node we focus on.
  bool converted; // true if the overall dictionary formula is converted to a Dict.
};
