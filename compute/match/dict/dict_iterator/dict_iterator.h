#pragma once
/*
Base virtual class for a dictionary iterator when the dictionary.
*/

class DictIterator
{
public:
  virtual ~DictIterator() {};
  
  // Go to the next element.
  virtual bool next() = 0;
  // Return to the start of the list.
  virtual void rewind()= 0;
  // Is the iterator on the end (øl) of the list.
  virtual bool is_end() = 0;
  // Is it an iterator for a Dict ?
  virtual bool is_nota_dict() const =0;
  // Return the l_init value for the matched node.
  PFormula get_l_init() { return l_init; };
protected:
  PFormula l_init; // The l_init of the actuel node.

};
