#pragma once
/*
A dictionary iterator when we are sure (else it's a bug) that the dictionary is a notation (for Obj).
*/

#include "dict_iterator.h"
#include "../../../formula/dict.h"
#include "../../../formula/dict_node.h"
#include "../node_nota.h"

class DictNotaIterator : public DictIterator
{
public:
  DictNotaIterator(Dict *p, WordRepr * &key, PFormula &value):
    dict(p), m(l_init,key,value) 
  {
    m.match_no_compute(p->dn);
  };
  ~DictNotaIterator() {};
  bool next() { v_is_end=!m.match_no_compute(l_init); assert(!v_is_end || l_init->synt_equal(dvar_0l)); return !v_is_end; };
  void rewind() { v_is_end=false; m.match_no_compute(dict->dn);};
  bool is_end() { return v_is_end; };
  bool is_nota_dict() const  { return true; };
protected:
  Dict *dict;
  MatchNodeNota m;
  bool v_is_end; // Value returned by "is_end()", in a Dict, if the node is not a DictNode, then it's the end.
};
