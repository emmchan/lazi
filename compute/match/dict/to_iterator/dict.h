#pragma once
/*
MatchToIterDict match a dictionary of any shape. It use a "OrMatch" of MatchDictNota and MatchDictNoNota.
The matching function set a node iterator (used, guess what, to interate on nodes).
The iterator is owned (and then created and deleted) by the MatchDict object. The iterator automatically replace what it reads by notations, so we can end by a Dict if all the dictionary is read. In this case, rewind change the iterator to a dict_nota iterator.
If the match function of a MatchDict object is called, the eventually old iterator is deleted, then, for one MatchDict objet, we can only iterate on the matched doctionary (artificial limitation to spimplify the basic smart pointer thing).
*/

#include "dict_nota.h"
#include "dict_no_nota.h"
#include "../dict_iterator/dict_iterator.h"
#include "../../or.h"

#include "../../../formula/dict.h"

class MatchToIterDict : public Match
{
public:
  MatchToIterDict(DictIterator * &_ni, WordRepr * &_key, PFormula &_value) : 
    ni(_ni), key(_key), value(_value),
    m_nota(_ni,_key,_value),m_no_nota(_ni,_key,_value),m(m_nota,m_no_nota) {};
    
  bool match(CContext &C, PFormula &f) const { return m.match(C,f); };
  
private:
  DictIterator * & ni;
  WordRepr * & key;
  PFormula & value;
  
  MatchToIterDictNota m_nota;
  MatchToIterDictNoNota m_no_nota;
  OrMatch m;
};
