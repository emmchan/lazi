#pragma once
/*
MatchToIterDictNota matches a "Dict(...)". 
The matching function set a node iterator.
The iterator is owned (and then created and deleted) by the MatchDictNota object.
*/

#include "../dict_iterator/dict_nota_iterator.h"

#include "../../match_no_apply.h"

#include "../../../formula/dict.h"

class MatchToIterDictNota : public MatchNoApply
{
public:
  MatchToIterDictNota(DictIterator * &_ni, WordRepr * &_key, PFormula &_value) : 
    ni(_ni), key(_key), value(_value) {ni=nullptr;};

  ~MatchToIterDictNota() { if(ni != nullptr) { delete (ni); ni=nullptr; } };

  bool match_no_compute(PFormula &f) const { 
    Dict *p=f->get_Dict();
    if(p!=nullptr)
    {
      if(ni!=nullptr) // If match is used several times, we can have just one iterator so we delete the old used one.
        delete ni; 
      ni=new DictNotaIterator(p,key,value);
      return true;
    }
    return false;
  }

private:
  DictIterator * & ni;
  WordRepr * & key;
  PFormula & value;
};
