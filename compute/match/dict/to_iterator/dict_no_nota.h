#pragma once
/*
MatchToIterDictNoNota match a dictionary when it's not a Dict (matches the start of a dictionnary node list, in a notation form or not).
Note that, because of the way list are constructed, what we see at start is the end of the list.
The matching function set initializes a node iterator.
The iterator is owned (hence created and deleted) by the MatchDictNota object. The iterator can be changed by rewind, but this is transparent.
*/

#include "../dict_iterator/dict_no_nota_iterator.h"

#include "../../../formula/dict.h"

class MatchToIterDictNoNota : public Match
{
public:
  MatchToIterDictNoNota(DictIterator * &_ni, WordRepr * &_key, PFormula &_value) : 
    ni(_ni), key(_key), value(_value) {ni=nullptr;};
    
  ~MatchToIterDictNoNota() { if(ni != nullptr) { delete (ni); ni=nullptr; } };

  bool match(CContext &C, PFormula &f) const { 
    bool valid; // Match works for the first node ?
    // First we delete the old iterator if he exists (this object could be use for several iterations on several dictionaries.
    if(ni!=nullptr)
    {
      delete ni;
      ni=nullptr;
    }
    new DictNoNotaIterator(valid,ni,C,f,key,value); // The object manage his own reference (*ni will be updated in all cases). 
    if(!valid)
    {
      delete ni;
      ni=nullptr;
    }
    return valid;
  }
  
private:
  DictIterator * & ni;
  WordRepr * & key;
  PFormula & value;
};
