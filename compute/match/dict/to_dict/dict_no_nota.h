#pragma once
/*
MatchToDictDictNoNota matches a formula convertible to Dict and not already a Dict, and do the conversion.
The matching function set nothing, but the argument is now a Dict if match() return true.
*/

#include "../to_iterator/dict_no_nota.h"

#include "../../../formula/dict.h"

class MatchToDictDictNoNota : public Match
{
public:
  MatchToDictDictNoNota() {};
    
  // To convert the formula to a dictionary, we iterate on the overall dictionary, which convert it.
  bool match(CContext &C, PFormula &f) const { 
    DictIterator *it; // note: it must be constructed before m, because when m is deleted it writes into it.
    WordRepr *key; // read key
    PFormula val; // read value
    MatchToIterDictNoNota m(it,key,val);
    
    for(bool cont=m.match(C,f);cont;cont=it->next());
    // If we started and went up to the end, then it's converted, else it's not.
    return it!=nullptr && it->is_end();
  }
};
