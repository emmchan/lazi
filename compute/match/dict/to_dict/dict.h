#pragma once
/*
MatchToDictDict matches a "Dict(...)" or a formula convertible to a Dict. 
The matching function set nothing, but the argument is now a Dict if match() return true.
*/

#include "../../or.h"
#include "dict_nota.h"
#include "dict_no_nota.h"

#include "../../../formula/dict.h"

class MatchToDictDict : public Match
{
public:
  MatchToDictDict() : m_nota(),m_no_nota(),m(m_nota,m_no_nota) {};
        
  bool match(CContext &C, PFormula &f) const { return m.match(C,f); };
  
private:
  MatchToDictDictNota m_nota;
  MatchToDictDictNoNota m_no_nota;
  OrMatch m;
};


