#pragma once
/*
MatchToDictDictNota matches a "Dict(...)". 
The matching function set nothing, but the argument is now a Dict if match() return true.
*/

#include "../../property.h"

#include "../../../formula/dict.h"

class MatchToDictDictNota : public MatchProperty
{
public:
  MatchToDictDictNota() : MatchProperty(Dict::is_me) {};
};
