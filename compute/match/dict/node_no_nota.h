#pragma once
/*
MatchNodeNoNoda match a dictionary node when it's not a notation ( matches a "toListBwAdd l_init $T[k,v]"). 
3 formulas are returned: l_init, key, value.
*/

#include "../apply.h"
#include "../equal.h"
#include "../property.h"
#include "../set.h"
#include "../set_word_repr.h"
#include "../anything.h"
#include "../or.h"
#include "../../formula/word_repr.h"
#include "../../formula/dict_node.h"
#include "../../global_context.h"

class MatchNodeNoNota : public Match
{
public:
  MatchNodeNoNota(PFormula &_l_init,WordRepr * &_wkey, PFormula &_value) : l_init(_l_init), wkey(_wkey), value(_value),
            m_pair_value(_value,match_anything),
          m_pair_word(wkey),
          m_dvar_pair(dvar_pair),
        m_a2(m_dvar_pair,m_pair_word),
      m_a3(m_a2,m_pair_value),
        m_dict_init(_l_init,match_anything),
        m_dvar_toListBwAdd(dvar_toListBwAdd),
      m_a1(m_dvar_toListBwAdd,m_dict_init),
    m(m_a1,m_a3)
    {};
  
  bool match(CContext &C, PFormula &f) const { 
    if(m.match(C,f))
    {
      // We can convert the formula to a notation.
      f=new DictNode(l_init,wkey,value);
      return true;
    }
    return false;
  };
  
private:
  PFormula & l_init;
  WordRepr * & wkey;
  PFormula & value;
  MatchSet m_pair_value;
  MatchSetWordRepr m_pair_word;
  MatchEqual m_dvar_pair;
  MatchApply m_a2;
  MatchApply m_a3;
  MatchSet m_dict_init;
  MatchEqual m_dvar_toListBwAdd;
  MatchApply m_a1;
  MatchApply m;
};
