#pragma once
/*
MatchToDictNota matches a formula convertible to a Dict, return nothing but if match the argument is now a Dict.
*/

#include "dict_iterator/dict_nota_iterator.h"

#include "../match_no_apply.h"

#include "../../formula/dict.h"

class MatchToDictNota : public Match
{
public:
  MatchToDictNota()  {};


  bool match(PFormula &f) const { 
    
    Dict *p=f->get_Dict();
    if(p!=nullptr)
    {
      if(ni!=nullptr) // If match is used several times, we can have just one iterator so we delete the old used one.
        delete ni; 
      ni=new DictNotaIterator(p,key,value);
      return true;
    }
    return false;
  }

private:
  DictIterator * & ni;
  WordRepr * & key;
  PFormula & value;
};
