#pragma once
/*
MatchNodeNoda match a dictionary node when it's a notation ( matches a "DictNode(...)"). 
3 formulas are returned: l_init, key, value.
*/

#include "../match_no_apply.h"

#include "../../formula/dict_node.h"

class MatchNodeNota : public MatchNoApply
{
public:
  MatchNodeNota(PFormula &_l_init,WordRepr * &_key, PFormula &_value) : l_init(_l_init), key(_key), value(_value) {};
  
  // This is ot a problem if f and l_init are the same reference (if &f == &l_init).
  bool match_no_compute(PFormula &f) const { 
    DictNode *p;
    p=f->get_DictNode(); // It can be also dvar_0l, then p==nullptr
    if(p!=nullptr)
    {
      key=p->name;
      value=p->value;
      l_init=p->l_init;
      return true;
    }
    return false;
  }
  
private:
  PFormula & l_init;
  WordRepr * & key;
  PFormula & value;
 
};
