#pragma once
/*
MatchDictToSet match a dictionary, like MatchDict, but instead of returning an iterator, return the set (unordered_set) of keys. This is usefull when we use this set multiple times (like in dictMerge).
*/


#include <unordered_set>

#include "to_iterator/dict.h"
#include "../../formula/dict.h"

class MatchDictToSet : public Match
{
public:
  MatchDictToSet(unordered_set<WordRepr *> &_keys) : keys(_keys) {};
    
  bool match(CContext &C, PFormula &f) const {
    WordRepr * key;
    PFormula value;
    DictIterator *it;
    MatchToIterDict m(it,key,value);
    
    for(bool cont=m.match(C,f);cont;cont=it->next())
      keys.insert(key);
    return it!=nullptr && it->is_end();
  };
  
private:
  unordered_set<WordRepr *> &keys;
};
