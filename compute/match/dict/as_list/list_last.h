#pragma once
/*
MatchDictAsListLast read the last element (a pair) of a dictionnary seen as a list.
*/


#include "../node.h"

#include "../../../formula/dict.h"

class MatchDictAsListLast : public Match
{
public:
  MatchDictAsListLast(PFormula &_list_last) : list_last(_list_last) {};
    
  bool match(CContext &C, PFormula &f) const { 
    Dict *p=f->get_Dict();
    if(p==nullptr)
    {
      // We need to compute f to know if it's a Dict or directly a list.
      f=f->compute(C);
      p=f->get_Dict();
    }
    if(p==nullptr)
      return match_node(C,f);
    else
      return match_node(C,p->dn);
  };
  
  bool match_node(CContext &C, PFormula &f) const { 
    PFormula l_init;
    WordRepr *key;
    PFormula value;
    
    if(MatchNodeNota(l_init,key,value).match(C,f))
    {
      list_last=new Apply(new Apply(dvar_pair,key),value);
      return true;
    }
    return false;
  };
  
private:
  PFormula & list_last;
};
