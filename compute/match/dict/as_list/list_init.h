#pragma once
/*
MatchDictAsListLInit read the l_init value the node of a dictionary. The node of a dictionary is the last element of the list, so the returned l_init is the element before the last one.
*/


#include "../node.h"

#include "../../../formula/dict.h"

class MatchDictAsListLInit : public Match
{
public:
  MatchDictAsListLInit(PFormula &_l_init) : l_init(_l_init) {};
    
  bool match(CContext &C, PFormula &f) const { 
  Dict *p=f->get_Dict();
  if(p==nullptr) // Perhaps we need to compute the value to recognize it
  {
    f=f->compute(C);
    p=f->get_Dict();
  }
  return match_node(C,p==nullptr ? f : p->dn);
  };
  
  bool match_node(CContext &C, PFormula &f) const { 
    WordRepr *key;
    PFormula value;
    
    return MatchNodeNota(l_init,key,value).match(C,f);
  };
  
private:
  PFormula & l_init;
};
