#pragma once
/*
MatchDictAsListNonEmpty match a dictionnary notation (which is always non empty).
*/


#include "../node.h"

#include "../../../formula/dict.h"

class MatchDictAsListNonEmpty : public MatchNoApply
{
public:
  bool match_no_compute(PFormula &f) const { 
    return Dict::is_me(f)||DictNode::is_me(f);
  };
};

