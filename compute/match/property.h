#pragma once
// MatchProperty use a function (a property) to check the matching. In practice we will use the static "is_me" functions in some formula classes (like WordRepr). The property (prop) must not select an apply formula.

#include "match_no_apply.h"

class MatchProperty : public MatchNoApply
{
public:
  MatchProperty(bool (*_prop)(PFormula)) : prop(_prop) {};
  
  bool match_no_compute(PFormula &f) const { return prop(f); }
protected:
  bool (* const prop)(PFormula);
};
