#include <iterator>

#include "c_context.h"

#include "formula/apply.h"
#include "global_context.h"

PFormula CContext::callstack_to_list()
{
  PFormula res=dvar_0l;
  list<PFormula>::iterator it;
  if(max_callstack_trace>0)
  {
    it=callstack.end();
    advance(it,-max_callstack_trace);
  }
  else
    it=callstack.begin();
  for(; it!=callstack.end(); it++ )
    res=PnewApply( PnewApply( dvar_toListBwAdd , res ) , *it );
  return res;
}
