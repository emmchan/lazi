#include <pugixml.hpp>
#include <tgmath.h> 

using namespace pugi;

#include "formula/formula.h"
#include "formula/word/dvar.h"
#include "formula/word/shortcut/shortcut.h"
#include "command.h"
#include "global_context.h"

Command::Command(xml_node node)
{
  max_depth=node.attribute("max-depth").as_int(-1);
  shortcuts_level=node.attribute("shortcuts-level").as_int(2);
  compute_args=node.attribute("compute-args").as_bool(false);
//   xml_node node_trace=node.child("trace");
//   trace=node_trace ? Trace::xml_to_trace(node_trace) : nullptr;
  xml_node node_check=node.child("check");
  if(node_check)
    requested=Formula::xml_global_to_formula(node_check.child("formula"));
  to_compute=Formula::xml_global_to_formula(node.child("formula"));
  comment=node.child_value("comment");
  do_profile=node.attribute("profile").as_bool(false);
  max_callstack=node.attribute("max-callstack").as_int(-1);
  max_callstack_trace=node.attribute("max-callstack-trace").as_int(-1);
  store_callstack=max_callstack>=0 || GC.stack_trace_used;
}

// Little utility
unsigned long long duration_to(durationT d)
{
  //auto int_ms = std::chrono::duration_cast<std::chrono::milliseconds>(d);
  unsigned long long ms=d.count();
  return ms;
}

bool Command::execute(xml_node parent, int n)
{
  // -------- Compute "to_compute", result will be in "computed"
  Args args;
  chronT start_time;
  durationT duration;
  CContext c(shortcuts_level,do_profile,store_callstack,max_callstack,max_callstack_trace);
  bool continu=true; // The return value
  
  // -------- Generate the report.
  xml_node report_node=parent.append_child("report");
  report_node.append_attribute("number") = n;
  xml_node command_node=report_node.append_child("compute");
  if(max_depth>=0)
    command_node.append_attribute("max-depth")=max_depth;
  if(shortcuts_level<Formula::max_shortcuts_level)
    command_node.append_attribute("shortcuts-level")=shortcuts_level;
  if(compute_args)
    command_node.append_attribute("compute-args")=compute_args;
  if(!comment.empty())
    command_node.append_child("comment").append_child(pugi::node_pcdata).set_value(comment.c_str());
  to_compute->overall_to_xml(command_node,max_depth);
  xml_node result_node=report_node.append_child("result");
  // -------- Do the computation.
  if(do_profile)
    start_time=chrono::high_resolution_clock::now();
  PFormula computed_f = to_compute->new_compute_on_args(c,args);
  if(compute_args && !c.max_callstack_reached)
    Formula::compute_args(c,args);
  PFormula computed=computed_f->apply_args(args);
  if(do_profile)
    duration=chrono::high_resolution_clock::now() - start_time;
  // -------- Continue the report.
  computed->overall_to_xml(result_node,max_depth);
  if(requested!=p_null_formula)
  {
    requested->overall_to_xml(command_node.append_child("check"),max_depth);
    bool passed=requested->synt_equal(computed);
    result_node.append_attribute("passed")=passed;
    if(!passed)
      continu=false;
  }
  if(do_profile)
  {
    xml_node profiling_node=report_node.append_child("profiling");
    auto duration_conv=duration_to(duration);
    profiling_node.append_child("time").text().set(duration_conv);
    xml_node calls_node=profiling_node.append_child("calls");
    xml_node call_node;
    WordDVar *dvar; // The dvar we report.
    bool is_shortcut;
    Shortcut *shortcut;
    char buff[32]; // For printing time %
    for(auto it : GC.dvar_store.get_store() )
    {
      dvar=static_cast<WordDVar *>(&*(it.second));
      is_shortcut=dvar->is_shortcut();
      if(is_shortcut)
        shortcut=static_cast<Shortcut *>(dvar);
      if(dvar->call_count>0 || (is_shortcut && shortcut->shortcut_count>0))
      {
        call_node=calls_node.append_child("dvar");
        call_node.append_attribute("name")=(dvar->get_name() + "(" + std::to_string(dvar->get_arity()) +")").c_str();
        call_node.append_attribute("count")=static_cast<unsigned int>(dvar->call_count);
        if(dvar->call_count>0)
        {
          sprintf(buff, "%.1f", 100*static_cast<double>(duration_to(dvar->internal_time))/static_cast<double>(duration_conv));
          call_node.append_attribute("internal-time-percent")=buff;
          call_node.append_attribute("internal-time")=duration_to(dvar->internal_time);
          call_node.append_attribute("external-time")=duration_to(dvar->external_time);
        }
        else
        {
          call_node.append_attribute("internal-time-percent")="NA";
          call_node.append_attribute("internal-time")="NA";
          call_node.append_attribute("external-time")="NA";
        }
        call_node.append_attribute("is-shortcut")=is_shortcut;
        if(dvar->is_shortcut())
          call_node.append_attribute("shortcut-count")=static_cast<unsigned int>(shortcut->shortcut_count);
      }
    }
  }
  if(!c.trace.empty())
  {
    xml_node traces_node=report_node.append_child("traces");
    for(auto pa: c.trace)
    {
      xml_node trace_node=traces_node.append_child("trace");
      trace_node.append_attribute("label")=pa.first.c_str();
      (pa.second)->overall_to_xml(trace_node,max_depth);
    }
  }
  return continu;
}

