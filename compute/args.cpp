#include <stdlib.h>

#include "args.h"
#include "p_formula.h"
#include "formula/formula.h"


unsigned Args::start_grow_by=16; 
unsigned Args::grow_factor_ten=20;


void Args::init_PFormula_area(PFormula *p,unsigned n)
{
  Formula **q=static_cast<Formula **>(static_cast<void *>(p));
  for(;n>0;n--,q++)
  {
    *q=p_null_formula;
    p_null_formula->new_parent();
  }
}

Args::~Args()
{
  if(base_mem==nullptr)
    return;
  Formula **q=static_cast<Formula **>(static_cast<void *>(base_mem));
  for(unsigned n=0;n<size_mem;n++,q++)
    Formula::lost_parent(*q);
  free(base_mem);
}

void Args::push(PFormula f)
{
  // base_mem can be 0, works also with this case.
  unsigned diff_base=base-base_mem;
  if(diff_base+size >= size_mem) // If we have no free PFormula : we allocate more memory
  {
#pragma GCC diagnostic ignored "-Wclass-memaccess"
    base_mem=static_cast<PFormula *>(realloc(base_mem,(size_mem+grow_by)*sizeof(PFormula)));
    if(base_mem==nullptr) throw std::bad_alloc();
    init_PFormula_area(base_mem+size_mem,grow_by);
    size_mem+=grow_by;
    base=base_mem+diff_base;
    grow_by=(grow_factor_ten*grow_by)/10;
  }
  base[size]=f;
  size++;
}

void Args::consume(unsigned n) 
{ 
  assert(n<=size); 
  PFormula *p=base+size-1;
  size-=n; 
  for(;n>0;n--,p--)
    *p=p_null_formula;
}

unsigned Args::set_visibles(unsigned n)
{
  assert(size>=n);
  unsigned shift=size-n;
  base=base+shift;
  size=n;
  return shift;
}

void Args::restore_visibles(unsigned shift)
{
  size=size+shift;
  base=base-shift;
}






