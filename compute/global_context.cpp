#include <map>
#include <lazypool/multi_pooled.h>

using namespace std;

#include "global_context.h"
#include "formula/null_formula.h"
#include "formula/apply.h"
#include "formula/function.h"
#include "formula/word_repr.h"
#include "formula/word/key_if.h"
#include "formula/word/dvar.h"
#include "formula/word/key_distribute.h"
#include "formula/word/constructor.h"
#include "command.h"
#include "formula/word/shortcut/maybe/is_thing.h"
#include "formula/word/shortcut/maybe/get_thing.h"
#include "formula/word/shortcut/word/word_equal.h"
#include "formula/word/shortcut/word/is_word.h"
#include "formula/word/shortcut/dict/dict_apply.h"
#include "formula/word/shortcut/dict/dict_apply_maybe.h"
#include "formula/word/shortcut/dict/dict_merge.h"
#include "formula/word/shortcut/dict/dict_mod.h"
#include "formula/word/shortcut/dict/is_dict_entry.h"
#include "formula/word/shortcut/obj/obj_apply.h"
#include "formula/word/shortcut/obj/obj_apply_maybe.h"
#include "formula/word/shortcut/obj/obj_merge.h"
#include "formula/word/shortcut/obj/obj_dict_merge.h"
#include "formula/word/shortcut/obj/obj_dyn_mod.h"
#include "formula/word/shortcut/obj/obj_dyn_add.h"
#include "formula/word/shortcut/list/is_empty_list.h"
#include "formula/word/shortcut/list/list_init.h"
#include "formula/word/shortcut/list/list_last.h"
#include "formula/word/shortcut/identF/identF.h"
#include "formula/word/shortcut/pair/pair_first.h"
#include "formula/word/shortcut/pair/pair_second.h"
#include "formula/word/shortcut/recurse/recurse.h"
#include "formula/word/shortcut/debug/trace.h"
#include "formula/share/sharer.h"
#include "formula/share/pre_sharer_big.h"
#include "formula/share/pre_sharer_small.h"
#include "match/anything.h"


// All global variables are instanciated here.
// Order matters (dependencies)

// Pooling (Lazipool tool) global variables (see Apply for example)
POOLED_GLOBS(1)
POOLED_GLOBS(2)
POOLED_GLOBS(3)
POOLED_GLOBS(4)
POOLED_GLOBS(5)
POOLED_GLOBS(6)
POOLED_GLOBS(7)
POOLED_GLOBS(8)

// see formula/null_formula.h
Formula *p_null_formula=new NullFormula();

// Glob / keywords
PFormula kw_if;
PFormula kw_0b;
PFormula kw_1b;
PFormula kw_distribute;
PFormula kw_equal;

// Glob / constructor
PFormula dvar_pair;
PFormula dvar_just;
PFormula dvar_nothing;
PFormula dvar_left;
PFormula dvar_right;
PFormula dvar_wordToFormula;
PFormula dvar_formulaApply;
PFormula dvar_0l;
PFormula dvar_toListBwAdd;
PFormula dvar_recurse;

// Glob / miscelanous
PFormula glob_recurse;
PFormula wordRepr__callstack;
PFormula wordRepr_callstack_limit_reached;
PFormula key0Repr;
PFormula gvarRepr;
PFormula dvarRepr;
PFormula lvarRepr;
PFormula wordRepr;
PFormula functionRepr;
PFormula wordReprRepr;

// Global object containing global data.
GlobalContext GC;

// This matching object (see match directory) can be unique because it doesn't depend on parameters.
MatchAnything match_anything;

GlobalContext::GlobalContext()
{
  stack_trace_used=false;
  //---------------------------------------------------------
  // Initialize constants
  
  // Keywords
  kw_equal=key_store.get("equal");
  kw_distribute=PFormula(new WordKey0Distribute()); key_store.add(kw_distribute);
  kw_1b=PFormula(key_store.get("1b"));
  kw_0b=PFormula(key_store.get("0b"));
  kw_if=PFormula(new WordKeyIf()); key_store.add(kw_if);

  // Constructors
  dvar_pair = PFormula(new Constructor("pair", 2)); dvar_store.add(dvar_pair);
  dvar_just = PFormula(new Constructor("just", 1)); dvar_store.add(dvar_just);
  dvar_nothing = PFormula(new Constructor("nothing", 0)); dvar_store.add(dvar_nothing);
  dvar_left = PFormula(new Constructor("left", 1)); dvar_store.add(dvar_left);
  dvar_right = PFormula(new Constructor("right", 1)); dvar_store.add(dvar_right);
  dvar_wordToFormula = PFormula(new Constructor("wordToFormula", 1)); dvar_store.add(dvar_wordToFormula);
  dvar_formulaApply = PFormula(new Constructor("formulaApply", 2)); dvar_store.add(dvar_formulaApply);
  dvar_0l = PFormula(new Constructor("∅l", 0)); dvar_store.add(dvar_0l);
  dvar_toListBwAdd = PFormula(new Constructor("toListBwAdd", 3)); dvar_store.add(dvar_toListBwAdd);
  
  // Shortcuts
  dvar_store.add(PFormula(new IsEmptyList())); 
  dvar_store.add(PFormula(new ListInit()));
  dvar_store.add(PFormula(new ListLast()));
  dvar_store.add(PFormula(new IsThing()));
  dvar_store.add(PFormula(new GetThing()));
  dvar_store.add(PFormula(new IsWord()));
  dvar_store.add(PFormula(new WordEqual()));
  dvar_store.add(PFormula(new DictApply()));
  dvar_store.add(PFormula(new DictApplyMaybe()));
  dvar_store.add(PFormula(new DictMerge()));
  dvar_store.add(PFormula(new DictMod()));
  dvar_store.add(PFormula(new IsDictEntry()));
  dvar_store.add(PFormula(new ObjApply()));
  dvar_store.add(PFormula(new ObjApplyMaybe()));
  dvar_store.add(PFormula(new ObjMerge()));
  dvar_store.add(PFormula(new ObjDictMerge()));
  dvar_store.add(PFormula(new ObjDynMod()));
  dvar_store.add(PFormula(new ObjDynAdd()));
  dvar_store.add(PFormula(new IdentF()));
  dvar_store.add(PFormula(new PairFirst()));
  dvar_store.add(PFormula(new PairSecond()));
  dvar_store.add(PFormula(new Trace()));
  dvar_recurse = PFormula(new Recurse()); dvar_store.add(dvar_recurse);
  
  // miscelanous
  wordRepr__callstack = repr_store.get("_callstack");
  wordRepr_callstack_limit_reached = repr_store.get("callstack_limit_reached");
  key0Repr = repr_store.get("key0");
  gvarRepr = repr_store.get("gvar");
  dvarRepr = repr_store.get("dvar");
  lvarRepr = repr_store.get("lvar");
  wordRepr = repr_store.get("word");
  functionRepr = repr_store.get("function");
  wordReprRepr = repr_store.get("wordRepr");
}

GlobalContext::~GlobalContext()
{
}

void GlobalContext::reset()
{
  // Reset the values of dvar words.
  for (auto it : dvar_store.get_store())
    (static_cast<WordDVar &>(*(it.second))).reset();
}
