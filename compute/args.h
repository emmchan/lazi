#pragma once
// Stack of arguments (of PFormula).
// We can get references on PFormula in the stack (to change their values), but in this case we must not use "push" because it can reallocate and then invalidate the references.
// Args start with an empty stack because some computations abort without using the stack.

#include <new>
#include <assert.h> 
#include <lazypool/pooled.h>

#include "p_formula.h"

using namespace std;

class Args
{
public:
  
  Args() : grow_by(start_grow_by),base_mem(nullptr),size_mem(0),base(nullptr),size(0) {};
  ~Args();
  
  PFormula & operator [](unsigned i) { assert(i<size); return *(base + (size -1 -i));};

  void push(PFormula f); // Add one element.
  // Remove n first entries.
  void consume(unsigned n=1);
  // Return the size of the stack.
  unsigned get_size() const{ return size; };
  // Is the stack empty ?
  bool empty() const { return size==0; };
  
  // Change the number of visible elements to n. Return a number which will be used to restore the stack to the original.
  unsigned set_visibles(unsigned n=0);
  
  // After a set_visibles, restore the stack.
  void restore_visibles(unsigned shift);
  
  // Get n elements
  void get2(PFormula &i0, PFormula &i1) { 
    PFormula *p=base + size -1; i0=*p--; i1=*p; };
  void get3(PFormula &i0, PFormula &i1, PFormula &i2) { 
    PFormula *p=base + size -1; i0=*p--; i1=*p--; i2=*p;};
  void get4(PFormula &i0, PFormula &i1, PFormula &i2, PFormula &i3) { 
    PFormula *p=base + size -1; i0=*p--; i1=*p--; i2=*p--; i3=*p;};
  void get5(PFormula &i0, PFormula &i1, PFormula &i2, PFormula &i3, PFormula &i4) { 
    PFormula *p=base + size -1; i0=*p--; i1=*p--; i2=*p--; i3=*p--; i4=*p;};
  void get6(PFormula &i0, PFormula &i1, PFormula &i2, PFormula &i3, PFormula &i4, PFormula &i5) { 
    PFormula *p=base + size -1; i0=*p--; i1=*p--; i2=*p--; i3=*p--; i4=*p--; i5=*p;};
 
protected:
  // Initialize n objects starting at p. We can do this because PFormula has no vtable.
  void init_PFormula_area(PFormula *p,unsigned n);
  
  // How many elements this grow at start.
  static unsigned start_grow_by;
  // The grow factor x 10.
  static unsigned grow_factor_ten;
  // Number of elements we add to grow or at start.
  unsigned grow_by; 
  // base of the stack
  PFormula * base_mem; 
  // Number of allocated PFormula *.
  unsigned size_mem; 
  // Virtual base of the stack. Because sometimes we want to ignore some elements in the base. See set_visibles() & restore_visibles().
  PFormula * base; 
  // Number of the stack from the virtual base.
  unsigned size; 
};
