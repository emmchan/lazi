#include <assert.h>

#include "flagged_var_list.h"

CompareRes FlaggedVarList::compare(const FlaggedVarList &l) const
{
  CompareRes res=CMP_EQUAL;
  fvlist::const_iterator itl = l.begin();
  for(fvlist::const_iterator it=begin(); it != end() && res!=CMP_DIFF; ++it, itl++)
  {
    assert(itl != l.end());
    if(itl->second!=it->second)
    {
      if(itl->second>it->second)
      {
        if(res==CMP_INCLUDED)
          res=CMP_DIFF;
        else if(res==CMP_EQUAL)
          res=CMP_REV_INCLUDED;
      }
      else
      {
        if(res==CMP_REV_INCLUDED)
          res=CMP_DIFF;
        else if(res==CMP_EQUAL)
          res=CMP_INCLUDED;
      }
    }
  }
  assert(res==CMP_DIFF || itl == l.end());
  return res;
}

void FlaggedVarList::saw(const WordLVar *wv)
{
  for (fvlist::reverse_iterator it=rbegin(); ; ++it)
  {
    assert(it!=rend());
    if(it->first==wv)
    {
      it->second=true;
      return;
    }
  }
}

void FlaggedVarList::set_union(const FlaggedVarList &l)
{
  fvlist::const_iterator itl = l.begin();
  for(fvlist::iterator it=begin(); it != end(); ++it, itl++)
  {
    assert(itl != l.end());
    it->second = it->second || itl->second;
  }
  assert(itl == l.end());
}

bool FlaggedVarList::set_empty() const
{
  for(fvlist::const_iterator it=begin(); it != end(); ++it)
    if(it->second)
      return false;
  return true;
}
