#include <iostream>
#include <pugixml.hpp>
#include <list>
#include <string>
#include <sstream> 
#include <unistd.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <errno.h>
#include <stdexcept>
#include <string.h>
#include <csignal>


#include "formula/word/dvar.h"
#include "global_context.h"
#include "command.h"
#include "c_context.h"

using namespace pugi;

void read_definitions(list<const char *> xml_files);
void run_commands(const char * commands_file, char * output_file);
void limit_memory(size_t max);
void limit_stack(size_t max);
void signal_handler(int signal, siginfo_t *si, void *arg);
// We use global variables for the report title and the command number, because the signal handle can use it to say where we abort.
unsigned int report_nbr;
string report_title;
// Size of the stack for signal handler (to print the test title & number when compute crash).
#define HANDLER_STACK_SIZE 1024*1024

void show_usage(string name);
void* pugixml_custom_allocate(size_t size);
void pugixml_custom_deallocate(void* ptr);

int main(int argc, char **argv) {
  list<const char *> definitions_files;
  char * commands_file=NULL;
  char * output_file=NULL;
  size_t  memory_limit=0;
  struct rlimit rl;
  
  try 
  {
    // --------------- Read program arguments
    if (argc < 5) {
    show_usage(argv[0]);
    return 1;
    }
    for (int i = 1; i < argc; ++i) 
    {
      string arg = argv[i];
      if ((arg == "-h") || (arg == "--help")) {
          show_usage(argv[0]);
          return 0;
      } else if (arg == "-d") { 
          if (i + 1 < argc) {
          definitions_files.push_back(argv[++i]);
          } else {
          cerr << "-d option requires one argument." << endl;
          return 1;
          }  
      } else if (arg == "-c") {
          if (i + 1 < argc) {
          commands_file = argv[++i];
          } else {
          cerr << "-c option requires one argument." << endl;
          return 1;
          }  
      } else if (arg == "-o") {
          if (i + 1 < argc) {
          output_file = argv[++i];
          } else {
          cerr << "-o option requires one argument." << endl;
          return 1;
          }  
      } else if (arg == "-l") {
          if (i + 1 < argc) {
            try {
              rl.rlim_max = rl.rlim_cur = stoi(argv[++i])*1024*1024; // From Mib to bytes
              setrlimit(RLIMIT_AS,&rl);
            }
            catch(std::invalid_argument& e) {
              cerr << "-l option requires an integer (the memory limit in MiB)." << endl;
              return 1;
            }
          } else {
          cerr << "-o option requires one argument." << endl;
          return 1;
          }  
      } else {
          cerr << "Error: unknown option \"" << arg << '"' << endl;
          show_usage(argv[0]);
          return 1;
      }
    }
    if (definitions_files.empty() || commands_file==NULL) {
      cerr << "Error: option -d and -c must be set." << endl;
      show_usage(argv[0]);
      return 1;
    }
    // ----------- Add a signal handler to print the title and the number when there is an interuption. For example if there is a loop and a stack overflow.
    struct sigaction action;
    bzero(&action, sizeof(action));
    action.sa_sigaction = signal_handler;
    action.sa_flags   = SA_SIGINFO|SA_STACK;
    sigaction(SIGSEGV, &action, NULL);
    sigaction(SIGPIPE, &action, NULL);
    sigaction(SIGXCPU, &action, NULL);
    sigaction(SIGXFSZ, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGQUIT, &action, NULL);
    sigaction(SIGHUP, &action, NULL);
    // Add a special stack to make sigsegv for stack overflow works, see https://rethinkdb.com/blog/handling-stack-overflow-on-custom-stacks/
    stack_t segv_stack;
    segv_stack.ss_sp = valloc(HANDLER_STACK_SIZE);
    segv_stack.ss_flags = 0;
    segv_stack.ss_size = HANDLER_STACK_SIZE;
    sigaltstack(&segv_stack, NULL);
    
    // We modifie the memory management function of pugixml to throw exception in case of error.
    pugi::set_memory_management_functions(pugixml_custom_allocate, pugixml_custom_deallocate);
    // ------------- Execution.
    
    if(memory_limit>0)
      limit_memory(memory_limit*1024*1024); // Mb to bytes
#ifndef NDEBUG
    limit_stack(512*1024); // In bytes. We reduce the stack size to avoid too big stack trace when the bug is an infinite recursion.
#endif
    read_definitions(definitions_files);
    run_commands(commands_file,output_file);
  } 
  catch (const std::bad_alloc&) {
    cerr << argv[0] << " : fatal error : memory shortage";
    if(!report_title.empty())
      cerr << ", at title \"" << report_title << "\"";
    cerr << ", test number " << report_nbr <<".\n";
    return 1;
  }
  catch(std::runtime_error& e)
  {
    cout << argv[0] << " : fatal error : " << e.what() << "\n";
    return 1;
  }

}

void show_usage(string name)
{
    cerr << "Usage: " << name << " [-h] -d DEFINITIONS_FILE -d ... -c COMMAND_FILE [ -l LIMIT ] [ -o FILE] \n\n"
              << "Options:\n"
              << "\t-h,--help\t\tShow this help message\n"
              << "\t-d DEFINITIONS_FILE\tRead all the definitions from these xml file(s).\n"
              << "\t-c COMMAND_FILE\tExecute command(s) from this xml file.\n"
              << "\t-l LIMIT\tLimit memory usage to LIMIT MiB (to stop infinites loops).\n"
              << "\t-o FILE\tOutput report file, or STDOUT if not spécified.\n"
              << endl;
}

// Read definitions files and initialize corresponding objects.
// Throw an exception if there is an error.
void read_definitions(list<const char *> xml_files)
{
  string def_name;
  // For each file name.
  for (auto file_name : xml_files)
  {
    xml_document doc;
    xml_parse_result result = doc.load_file(file_name);
    if(!result)
    {
      string mess = string("XML [") + file_name + "] parsed with errors.\n" +
        "Error description: " + result.description() + "\n" +
        "Error offset: " + to_string(result.offset) + "\n\n";
      throw std::runtime_error(mess);
    }
    // For each definition node
    for(auto def_node : doc.child("definitions").children("definition") )
    {
      def_name=def_node.attribute("name").value();
      // cerr << "definition :" << def_name << "\n"; // debug!
      try
      {
        PFormula value(Formula::xml_global_to_formula(def_node.first_child()));
        PFormula dvar=GC.dvar_store.get(def_name);
        (static_cast<WordDVar &>(*dvar)).set(value);
//         cerr << "= " << value->to_txt() << "\n-------------\n"; // debug!
      }
      catch (const runtime_error& error)
      {
        throw ( runtime_error(string("In the definition of \"") + def_name + "\" : " + error.what()));
      }
    }
  }
}

void signal_handler(int signal, siginfo_t */*info*/, void */*arg*/)
{
  cerr << "Lazi-compute crashed by signal \"" << strsignal(signal) << "\"";
  if(!report_title.empty())
    cerr << ", at title \"" << report_title << "\"";
  cerr << ", test number " << report_nbr <<".";
  if(signal == SIGSEGV)
    cerr << " This is probably due to a loop in your code which leads to a stack overflow.";
  cerr << "\n";
  exit(128+signal);
}

// Read the commands file and initialize the command list in GC.
// Throw an exception if there is an error.
void run_commands(const char * commands_file, char * output_file)
{
  xml_document doc;
  xml_parse_result result = doc.load_file(commands_file);
  if(!result)
  {
    string mess = string("XML [") + commands_file + "] parsed with errors.\n" +
      "Error description: " + result.description() + "\n" +
      "Error offset: " + to_string(result.offset) + "\n\n";
    throw std::runtime_error(mess);
  }
  xml_document report_doc;
  xml_node reports_node=report_doc.append_child("reports");
  // For each command node
  report_nbr=0;
  for (pugi::xml_node node = doc.child("commands").first_child(); node; node = node.next_sibling())
  {
    report_nbr++; // We must increment just before start of the next report, else we can report a bad number in signal handling.
    if(strcmp(node.name(),"title")==0)
    {
      report_nbr=0;
      xml_node title_node=reports_node.append_child("title");
      report_title=node.text().get();
      title_node.text().set(node.text().get());
    } else if(strcmp(node.name(),"compute")==0)
    {
      Command c(node);
      if(c.execute(reports_node,report_nbr))
      {
        GC.reset();
      }
      else
        break; // If there is an error, we stop.
    } else
    {
      string mess = string("XML [") + commands_file + "] parsed with errors.\n" +
      "Error description: encountered a tag in commands which is different from \"compute\" or \"title\" : \"" + node.name() + "\"\n";
      throw std::runtime_error(mess);
    }
  }
  // Save the report
  if(output_file==nullptr)
    report_doc.save(std::cout);
  else
  {
    if(!report_doc.save_file(output_file))
    {
      throw std::runtime_error(string("Error while saving the report to the file \"") + output_file + "\".");
    }
  }
  
}

void limit_memory(size_t max)
{
  struct rlimit r;

  r.rlim_cur = max;
  r.rlim_max = max;

  if (::setrlimit (RLIMIT_AS, &r) != 0)
    throw std::runtime_error(string("Error : setrlimit, try without limiting memory."));
}

void limit_stack(size_t max)
{
  struct rlimit r;

  r.rlim_cur = max;
  r.rlim_max = max;

  if (::setrlimit (RLIMIT_STACK, &r) != 0)
    throw std::runtime_error(string("Error : setrlimit on stack."));
}

// We change the allocator of pugixml to throw exception in case of error.
void* pugixml_custom_allocate(size_t size)
{
    return new char[size];
}

void pugixml_custom_deallocate(void* ptr)
{
    delete[] static_cast<char*>(ptr);
}
