#pragma once
#include <list>

#include "p_formula.h"

// Command as read from the xml.
class Command
{
public:
  Command(xml_node node);
  
  // Run the computation, return false if an error occured. Write the result in the xml_node. N is the number of the command (used for the output).
  bool execute(xml_node parent, int n);

public:
  // -------------------------------------------
  // All data of a command, they are not modified by "execute".
  
  // Configuration of the level of shortcuts. 
  // 0 : no shortcuts
  // 1 : function
  // 2 : isFormula, isKeyword, listLast, listInit etc
  unsigned int shortcuts_level;
  // Do we compute arguments of the result ? (To have a more readable result.)
  bool compute_args;
  // Maximum depth of formula when output. -1 = unlimited.
  int max_depth;
  // ------------ Profile & Call stack things
  // Do we have to generate profiling data ?
  bool do_profile;
  // Maximum call stack (the lazi one). -1 = unlimited. Used to stop computation before SIGSEV when there is a computation loop. When the maximum is reached a trace of the call stack is produced.
  int max_callstack;
  // Maximum calls (from the begening) traced when a call stack trace is produced. -1 = unlimited.
  int max_callstack_trace;
  // Do we have to store the call-stack ? This takes time so a flag control this.
  bool store_callstack;

  // The requested result (optional). If no requested result then resquested = p_null_formula
  PFormula requested;
  // The formula to compute.
  PFormula to_compute; 
  // The comment from the xml command
  string comment;
};

