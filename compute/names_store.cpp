#ifndef NAMES_STORE_CPP
#define NAMES_STORE_CPP
#include "names_store.h"
#include "formula/word/word.h"
  
template <class T> bool NamesStore<T>::check(string name)
{
  return store.find(name)!=store.end();
}

template <class T> void NamesStore<T>::add(PFormula obj)
{
  store.insert( std::pair<string,PFormula>((static_cast<Word &>(*obj)).get_name(),obj) );
}

template <class T> PFormula NamesStore<T>::get(string name)
{
  auto it=store.find(name);
  
  if(it==store.end())
  {
    PFormula pf=PFormula(new T(name));
    add(pf);
    return pf;
  }
  else
  {
    return it->second;
  }
}

#endif
