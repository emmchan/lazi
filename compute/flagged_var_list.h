#pragma once
// A pair of (variable,flag) list, used to compute the PreSharer objects. The flag means "the variable is used in the formula".
// We can also see a FlaggedVarList as a set: the set of used variables.

#include <list>
#include <utility>

using namespace std;

class WordLVar;

typedef list<pair<WordLVar *,bool>> fvlist;

typedef enum { CMP_EQUAL, CMP_INCLUDED, CMP_REV_INCLUDED, CMP_DIFF} CompareRes;


class FlaggedVarList : public fvlist
{
public:
  FlaggedVarList(WordLVar *wv) { add_var(wv); };
  FlaggedVarList(const FlaggedVarList &fvl) : fvlist(fvl) {};
  
  // Add a new variable
  void add_var(WordLVar *wv) { push_back(make_pair(wv,false)); };
  // Remove the last added variable.
  void rem_var() {pop_back();};
  // Mark a variable as seed (flag=true).
  void saw(const WordLVar *wv);
  // Get the flag of the last variable
  bool get_last_flag() const { return back().second; };
  
  // Compare 2 lists with same variables in the same order, we compare them as sets: the set of variable flagged "true": 
  // - CMP_INCLUDED : the list in argument is included in this list, 
  // - CMP_EQUAL : the lists are equals, 
  // - CMP_REV_INCLUDED : this list is included in the argument.
  // - CMP_DIFF : different sets
  CompareRes compare(const FlaggedVarList &l) const;
  
  // Union with the list in argument.
  void set_union(const FlaggedVarList &fvl);
  
  // Is the set empty ?
  bool set_empty() const;
};
