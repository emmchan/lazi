#pragma once
// Define the global context. This is the only global data.
#include "names_store.h"
#include "p_formula.h"


class WordLVar;
class WordKey0;
class WordDVar;
class WordGVar;
class WordRepr;
class NullFormula;
class Command;

class GlobalContext
{
public:
  GlobalContext();
  ~GlobalContext();
  
  // Prepare for a new computation after one is done.
  void reset(); 
public:
  NamesStore<WordLVar> lvar_store;
  NamesStore<WordKey0> key_store;
  NamesStore<WordGVar> gvar1_store;
  NamesStore<WordDVar> dvar_store;
  NamesStore<WordRepr> repr_store;
  bool stack_trace_used; // true if the global word "traceStack" is used anywere.
};

extern GlobalContext GC;

// Constants

extern Formula *p_null_formula;

extern PFormula kw_if;
extern PFormula kw_0b;
extern PFormula kw_1b;
extern PFormula kw_distribute;
extern PFormula kw_equal;

extern PFormula dvar_pair;
extern PFormula dvar_just;
extern PFormula dvar_nothing;
extern PFormula dvar_left;
extern PFormula dvar_right;
extern PFormula dvar_wordToFormula;
extern PFormula dvar_formulaApply;
extern PFormula dvar_0l;
extern PFormula dvar_toListBwAdd;
extern PFormula dvar_recurse;

extern PFormula wordRepr__callstack;
extern PFormula wordRepr_callstack_limit_reached;
extern PFormula key0Repr;
extern PFormula gvarRepr;
extern PFormula dvarRepr;
extern PFormula lvarRepr;
extern PFormula wordRepr;
extern PFormula functionRepr;
extern PFormula wordReprRepr;
