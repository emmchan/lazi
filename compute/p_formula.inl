#pragma once
// Inline functions for PFormula class.


PFormula::PFormula(Formula *_p) : p(_p) 
{
  assert(p!=NULL);
  p->new_parent();
}

PFormula::PFormula( const PFormula& sp) 
{
  p=sp.p;
  p->new_parent();
}

PFormula::~PFormula() 
{ 
  Formula::lost_parent(p);
}

// Overload of assignment. Race case x=x works.
PFormula& PFormula::operator= (const PFormula& sp) {
    sp.p->new_parent();
    Formula::lost_parent(p);
// This pragma is for PFormula *dict_init in formula/word/shortcut/dict_apply.cpp
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
    p=sp.p;
    return *this;
}

