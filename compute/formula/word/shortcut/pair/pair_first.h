#pragma once
#include "../shortcut.h"
// Shortcut pairFirst

class PairFirst : public Shortcut
{
public:
  PairFirst() : Shortcut("pairFirst") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
