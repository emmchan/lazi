#include "pair_second.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula PairSecond::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula pair_second;
      MatchSet m_second(pair_second,match_anything);
        MatchEqual m_dvar_pair(dvar_pair) ;
      MatchApply m_a1(m_dvar_pair,match_anything);
    MatchApply m(m_a1,m_second);
    if(m.match(C,args[0]))
    { 
      args.consume(1);
      if(C.profile) shortcut_count++;
      return pair_second;
    }
    // If there is 1 arguments but we can't recognize it.
    return WordDVar::compute_internal(C,args); 
  }
  return compute_stop(C);
}
  
  

