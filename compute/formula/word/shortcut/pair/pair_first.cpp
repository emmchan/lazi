#include "pair_first.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula PairFirst::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula pair_first;
        MatchSet m_first(pair_first,match_anything);
        MatchEqual m_dvar_pair(dvar_pair) ;
      MatchApply m_a1(m_dvar_pair,m_first);
    MatchApply m(m_a1,match_anything);
    if(m.match(C,args[0]))
    { 
      args.consume(1);
      if(C.profile) shortcut_count++;
      return pair_first;
    }
    // If there is 1 argument but we can't recognize it.
    return WordDVar::compute_internal(C,args); 
  }
  return compute_stop(C);
}
  
  

