#pragma once
#include "../shortcut.h"
// Shortcut pairSecond

class PairSecond : public Shortcut
{
public:
  PairSecond() : Shortcut("pairSecond") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
