#pragma once
#include "../shortcut.h"
// Shortcut "recurse f"   -->  f \ recurse f" 

class Recurse : public Shortcut
{
public:
  Recurse() : Shortcut("recurse") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

