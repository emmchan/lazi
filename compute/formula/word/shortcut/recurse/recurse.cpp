#include "recurse.h"
#include "../../../apply.h"
#include "../../../../global_context.h"

PFormula Recurse::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula res=new Apply(args[0],new Apply(dvar_recurse,args[0]));
    args.consume(1);
    if(C.profile) shortcut_count++;
    return res;
  }
  // If there is not 1 argument, do nothing
  return compute_stop(C);
}

