#include "is_dict_entry.h"
#include "../../../dict.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"


PFormula IsDictEntry::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=2)
  {
    PFormula &arg_word=args[0];
    PFormula &arg_dict=args[1];

    // --- Try to get the word.
    WordRepr *seek=WordRepr::get_comp(C,arg_word);
    if(seek!=nullptr) // If we have the word.
    {
      DictIterator *it; // note: it must be constructed before m, because when m is deleted it writes into it.
      WordRepr *key; // read key
      PFormula val; // read value
      MatchToIterDict m(it,key,val);
      
      for(bool cont=m.match(C,arg_dict);cont;cont=it->next())
      {
        if(key==seek)
        {
          args.consume(2);
          if(C.profile) shortcut_count++;
          return kw_1b; 
        }
      }  
      // If we recognised a dictApply but didn't found the sought key, return 0b
      if(it!=nullptr && it->is_end())
      {
        args.consume(2);
        if(C.profile) shortcut_count++;
        return kw_0b; 
      }
    }
    // The shortcut failed but we have 2 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 2 arguments, do nothing
  return compute_stop(C);
}
