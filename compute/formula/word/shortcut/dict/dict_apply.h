#pragma once
#include "../shortcut.h"
// Shortcut dictApply

class DictApply : public Shortcut
{
public:
  DictApply() : Shortcut("dictApply") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
