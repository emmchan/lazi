#pragma once
#include "../shortcut.h"
// Shortcut dictMerge

class DictMerge : public Shortcut
{
public:
  DictMerge() : Shortcut("dictMerge") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

