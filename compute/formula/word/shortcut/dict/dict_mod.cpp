#include "dict_mod.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"


PFormula DictMod::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=3)
  {
    PFormula &arg_dict=args[0];
    PFormula &arg_word=args[1];
    PFormula &arg_f=args[2]; // The function to apply to the value

    // --- Try to get the word.
    WordRepr *seek=WordRepr::get_comp(C,arg_word);
    if(seek!=nullptr) // If we have the word.
    {
      DictIterator *it; // note: it must be constructed before m, because when m is deleted it writes into it.
      WordRepr *key; // read key
      PFormula val; // read value
      MatchToIterDict m(it,key,val);
      PFormula node_list; // The result
      PFormula *p_l_init=&node_list; // Pointer where to write the l_init value.
      
      for(bool cont=m.match(C,arg_dict);cont;cont=it->next())
      {
        // We can use == for "key" and "seek" because the Match... can't return a Sharer for key.
        if(key==seek)
        {
          *p_l_init=new DictNode(it->get_l_init(),key,key==seek ? new Apply(arg_f,val) : val);
          args.consume(3);
          if(C.profile) shortcut_count++;
          // If we iterate on a Dict, we can return a Dict, else we don't know if the rest of the list is well formed for a Dict.
          return it->is_nota_dict() ? new Dict(node_list) : node_list;
        }
        *p_l_init=new DictNode(PFormula(),key,val);
        p_l_init=&((*p_l_init)->get_DictNode()->l_init);
      }
      if(it!=nullptr && it->is_end()) // If success with key not found (but the overall list searched).
      {
        PFormula dict=arg_dict; // Save the dict, which is the result (nothing changed). dict is now a Dict because all the dictionary is read.
        args.consume(3);
        if(C.profile) shortcut_count++;
        return dict;
      }
    }
    // The shortcut failed but we have 3 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 3 arguments, do nothing
  return compute_stop(C);
}

          
          
          
          
          
