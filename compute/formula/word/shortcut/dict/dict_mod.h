#pragma once
#include "../shortcut.h"
// Shortcut dictMod

class DictMod : public Shortcut
{
public:
  DictMod() : Shortcut("dictMod") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
