#include "dict_apply.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"


PFormula DictApply::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=2)
  {
    PFormula &arg_dict=args[0];
    PFormula &arg_word=args[1];

    // --- Try to get the word.
    WordRepr *seek=WordRepr::get_comp(C,arg_word);
    if(seek!=nullptr) // If we have the word.
    {
      DictIterator *it; // note: it must be constructed before m, because when m is deleted it writes into it.
      WordRepr *key; // read key
      PFormula val; // read value
      MatchToIterDict m(it,key,val);
      
      for(bool cont=m.match(C,arg_dict);cont;cont=it->next())
      {
        if(key==seek)
        {
          args.consume(2);
          if(C.profile) shortcut_count++;
          // Continue computation on the result. If the dictionary is a shared value, then the computation we made on it is kept.
          return val; 
        }
      }  
      // If we recognised a dictApply but didn't found the sought key, it leeds to a "trash" value, very probably a bug.
      if(it!=nullptr && it->is_end())
        cerr << "dictApply with unfound key \"" << seek->get_name() << "\".\n";
    }
    // The shortcut failed but we have 2 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 2 arguments, do nothing
  return compute_stop(C);
}

          
          
          
          
          
