#include "dict_merge.h"
#include "../../../dict.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"


PFormula DictMerge::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=2)
  {
    PFormula &arg_dictf=args[0];
    PFormula &arg_dictg=args[1];
    unordered_set<WordRepr *> keysg; // Keys of the dictionary g
    MatchDictToSet mg(keysg); // To get the set of keys of dictg
    DictIterator * itf; // Iterator on f.
    // A (key,value) of f
    WordRepr *key;
    PFormula val;
    MatchToIterDict mf(itf,key,val); // To iterate on f
    PFormula new_list; // The new list to construct.

    // First we get the keys of g.
    if(mg.match(C,arg_dictg))
    {
      // dictg is completely read, so it is now a Dict.
      assert(Dict::is_me(arg_dictg));
      new_list=arg_dictg->get_Dict()->get_node();
      for(bool cont=mf.match(C,arg_dictf);cont;cont=itf->next())
        if(keysg.count(key)==0) // If the key is not in arg_dictg we add the pair
          new_list=new DictNode(new_list,key,val);
      // It's a success if we went up to the end.
      if(itf!=nullptr && itf->is_end())
      {
        args.consume(2);
        if(C.profile) shortcut_count++;
        return new Dict(new_list); // Because all is read, we are sure the list is only with DictNodes and dvar_0l.
      }
    }
    // The shortcut failed but we have 2 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 2 arguments, do nothing
  return compute_stop(C);
}

          
          
          
          
          

