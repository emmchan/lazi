#pragma once
#include "../shortcut.h"
// Shortcut dictApply

class DictApplyMaybe : public Shortcut
{
public:
  DictApplyMaybe() : Shortcut("dictApplyMaybe") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

