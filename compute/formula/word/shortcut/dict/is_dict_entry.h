#pragma once
#include "../shortcut.h"
// Shortcut isDictEntry

class IsDictEntry : public Shortcut
{
public:
  IsDictEntry() : Shortcut("isDictEntry") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

