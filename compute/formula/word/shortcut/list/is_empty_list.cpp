#include "is_empty_list.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula IsEmptyList::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula &arg_x=args[0];
    
      // We test the false answer first because it's more frequent.
          MatchEqual m_toListBwAdd(dvar_toListBwAdd);
        MatchApply m_1(m_toListBwAdd,match_anything);
      MatchApply m_no_nota(m_1,match_anything);
      MatchDictAsListNonEmpty m_nota;
    OrMatch m(m_nota,m_no_nota);
    if(m.match(C,arg_x))
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return kw_0b;
    }
    else if(dvar_0l->synt_equal(arg_x)) // Here arg_x is already computed.
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return kw_1b;
    }
    else
      return WordDVar::compute_internal(C,args); 
  }
  return compute_stop(C);
}
  

