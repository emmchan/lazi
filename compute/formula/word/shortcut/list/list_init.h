#pragma once
#include "../shortcut.h"
// Shortcut listInit

class ListInit : public Shortcut
{
public:
  ListInit() : Shortcut("listInit") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

