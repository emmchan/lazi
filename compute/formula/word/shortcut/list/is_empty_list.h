#pragma once
#include "../shortcut.h"
// Shortcut isEmptyList

class IsEmptyList : public Shortcut
{
public:
  IsEmptyList() : Shortcut("isEmptyList") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

