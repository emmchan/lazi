#pragma once
#include "../shortcut.h"
// Shortcut listLast

class ListLast : public Shortcut
{
public:
  ListLast() : Shortcut("listLast") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

