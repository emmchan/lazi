#include "list_last.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula ListLast::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula &arg_x=args[0];
    PFormula last;
    
          MatchSet m_last(last,match_anything);
          MatchEqual m_toListBwAdd(dvar_toListBwAdd);
        MatchApply m_1(m_toListBwAdd,match_anything);
      MatchApply m_no_nota(m_1,m_last);
      MatchDictAsListLast m_nota(last);
    OrMatch m(m_nota,m_no_nota);
    if(m.match(C,arg_x))
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return last; 
    }
    else
      return WordDVar::compute_internal(C,args); 
  }
  return compute_stop(C);
}
  



