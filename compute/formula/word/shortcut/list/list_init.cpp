#include "list_init.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula ListInit::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula &arg_x=args[0];
    PFormula init;
    
          MatchSet m_init(init,match_anything);
          MatchEqual m_toListBwAdd(dvar_toListBwAdd);
        MatchApply m_1(m_toListBwAdd,m_init);
      MatchApply m_no_nota(m_1,match_anything);
      MatchDictAsListLInit m_nota(init);
      OrMatch m(m_nota,m_no_nota);
    if(m.match(C,arg_x))
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return init; 
    }
    else
      return WordDVar::compute_internal(C,args); 
  }
  return compute_stop(C);
}
  


