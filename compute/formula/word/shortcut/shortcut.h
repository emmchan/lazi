#pragma once
#include "../dvar.h"
// Shortcut , parent class of all shortcuts.

class Command;

class Shortcut : public WordDVar
{
public:
  Shortcut(string _name, unsigned int _level=2) : WordDVar(_name), level(_level) {};

  void reset() {shortcut_count=0;WordDVar::reset();};
  bool is_shortcut() { return true; }
  // Do the computation starting by trying the shortcut way, and if it's not possible then do the normal way.
  virtual PFormula compute_shortcut(CContext &C, Args &args)=0;

  // Run the shortcut if the level allow it, else do the normal way.
  PFormula compute_internal(CContext &C, Args &args) {
    if(C.shortcut_level>=level)
      return compute_shortcut(C,args);
    else
      return WordDVar::compute_internal(C,args);
  };
protected:
  unsigned int level; // The level of the shortcut
  // Profiling :
  callCountT shortcut_count; // Number of successfull shortcut usage.
  
friend Command;
};
