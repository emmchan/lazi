#pragma once
#include "../shortcut.h"
// Shortcut objDynAdd

class ObjDynAdd : public Shortcut
{
public:
  ObjDynAdd() : Shortcut("objDynAdd") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};


