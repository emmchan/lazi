#pragma once
#include "../shortcut.h"
// Shortcut objDictMerge
// Like ObjMerge but the second argument is a dictionary. We just match a notation for the dictionary because it's generaly the case.


class ObjDictMerge : public Shortcut
{
public:
  ObjDictMerge() : Shortcut("objDictMerge") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

