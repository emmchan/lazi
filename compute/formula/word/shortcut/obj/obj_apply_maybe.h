#pragma once
#include "../shortcut.h"
// Shortcut dictApply

class ObjApplyMaybe : public Shortcut
{
public:
  ObjApplyMaybe() : Shortcut("objApplyMaybe") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

