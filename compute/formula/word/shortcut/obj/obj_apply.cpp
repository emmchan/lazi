#include "obj_apply.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"


PFormula ObjApply::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=2)
  {
    PFormula &arg_obj=args[0];
    PFormula &arg_word=args[1];

    // --- Try to get the word.
    WordRepr *seek=WordRepr::get_comp(C,arg_word);
    if(seek!=nullptr) // If we have the word.
    {
      WordLVar *var;
      Dict *dict;
      MatchObj m(var,dict);
      if(m.match(C,arg_obj))
      {
        DictNode *dn; // Current DictNode on f
        PFormula node; // Current node on f
        for(node=dict->get_node(); DictNode::is_me(node);node=dn->get_l_init())
        {
          dn=node->get_DictNode();
          if(dn->get_key()==seek)
          {
            PFormula obj=arg_obj; // args.consume(3) could delete the object
            args.consume(2); // "node" make "dn" not deleted.
            if(C.profile) shortcut_count++;
            // We can do this construction because the structure of a dictionary of an Obj doesn't contain (Pre)Sharer
            return new Apply(new Function(var,dn->get_value()),obj);
          }
        }
        // If we didn't find the key
        cerr << "objApply with unfound key \"" << seek->get_name() << "\".\n";
      }
    }
    // The shortcut failed but we have 2 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 2 arguments, do nothing
  return compute_stop(C);
}

       
          
          
          
          
          

