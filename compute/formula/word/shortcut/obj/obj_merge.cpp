#include "obj_merge.h"
#include "../../../obj.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"


PFormula ObjMerge::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=2)
  {
    PFormula &arg_objf=args[0];
    PFormula &arg_objg=args[1];
    // Variable and dictionary of f & b
    WordLVar *var_f;
    Dict *dict_f;
    WordLVar *var_g;
    Dict *dict_g;
    MatchObj mf(var_f,dict_f);
    MatchObj mg(var_g,dict_g);
    unordered_set<WordRepr *> keys_g; // Keys of the dictionary g
    // A (key,value) of f
    WordRepr *key;
    PFormula new_list; // The new list to construct.

    // To do the shortcut we need to have 2 Obj with same variable name.
    if(mf.match(C,arg_objf) && mg.match(C,arg_objg))
    {
      PFormula new_dict_g;
      if(var_f!=var_g) // If variables are different, we rename the variable of g in its dictionary.
      {
        try{ new_dict_g=dict_g->rename_var(var_g,var_f); }
        catch(Collision &inter){// If there is a variable collision then we can't rename, then we can't use the shortcut.
          return WordDVar::compute_internal(C,args); 
        }
        // Update data about g
        var_g=var_f;
        dict_g=new_dict_g->get_Dict();
      }
      dict_g->get_keys(keys_g); // Read the set of keys from g
      new_list=dict_g->get_node();
      // We iterate on f nodes
      DictNode *dn_f; // Current DictNode on f
      PFormula node_f; // Current node on f
      for(node_f=dict_f->get_node(); DictNode::is_me(node_f);node_f=dn_f->get_l_init())
      {
        dn_f=node_f->get_DictNode();
        key=dn_f->get_key();
        if(keys_g.count(key)==0) // If the key is not in arg_dictg we add the pair
          new_list=new DictNode(new_list,key,dn_f->get_value());
      }
      args.consume(2);
      if(C.profile) shortcut_count++;
      // We can construct an Obj because we take values from 2 Obj, then we are sure there was no (Pre)Sharers in the structure.
      return new Obj(new Function(var_f,new Dict(new_list)));
    }

    // The shortcut failed but we have 2 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 2 arguments, do nothing
  return compute_stop(C);
}

          

