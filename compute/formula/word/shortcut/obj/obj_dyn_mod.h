#pragma once
#include "../shortcut.h"
// Shortcut objDynMod

class ObjDynMod : public Shortcut
{
public:
  ObjDynMod() : Shortcut("objDynMod") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

