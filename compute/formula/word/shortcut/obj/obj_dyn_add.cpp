#include "obj_dyn_add.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"
#include "../../lvar.h"


PFormula ObjDynAdd::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=3)
  {
    PFormula &arg_obj=args[0];
    PFormula &arg_word=args[1];
    PFormula &arg_f=args[2]; // The function f such that "f this" is the value to add.

    // --- Try to get the word.
    WordRepr *key=WordRepr::get_comp(C,arg_word);
    if(key!=nullptr) // If we have the word.
    {
      WordLVar *var;
      Dict *dict;
      MatchObj m(var,dict);
      if(m.match(C,arg_obj))
      {
        PFormula first_node=dict->get_node();
        PFormula f=arg_f;
        args.consume(3);
        if(C.profile) shortcut_count++;
        return new Obj(new Function(var,new Dict(new DictNode(first_node,key,new Apply(f,var)))));
      }
    }
    // The shortcut failed but we have 3 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 3 arguments, do nothing
  return compute_stop(C);
}
