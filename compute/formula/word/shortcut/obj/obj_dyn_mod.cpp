
#include "obj_dyn_mod.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"
#include "../../lvar.h"


PFormula ObjDynMod::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=3)
  {
    PFormula &arg_obj=args[0];
    PFormula &arg_word=args[1];
    PFormula &arg_f=args[2]; // The function to apply to the value

    // --- Try to get the word.
    WordRepr *seek=WordRepr::get_comp(C,arg_word);
    if(seek!=nullptr) // If we have the word.
    {
      WordLVar *var;
      Dict *dict;
      MatchObj m(var,dict);
      if(m.match(C,arg_obj))
      {
        DictNode *dn; // Current DictNode on f
        PFormula node; // Current node on f
        WordRepr *key;
        PFormula new_list; // The new list to construct.
        PFormula *p_l_init=&new_list; // Pointer where to write the l_init value. We construct the result from the outside to the inside (from end of list to the start). Then we respect order.
        DictNode *new_dn; // The new DictNode to construct the result.
        for(node=dict->get_node(); DictNode::is_me(node);node=dn->get_l_init())
        {
          dn=node->get_DictNode();
          key=dn->get_key();
          if(key==seek)
          {
            // We attach the new node to the rest of the (unchanged) list.
            *p_l_init=new DictNode(dn->get_l_init(),key, new Apply(new Apply(arg_f,var),dn->get_value()));
            args.consume(3);
            if(C.profile) shortcut_count++;
            // We can construct an Obj because we take values from an Obj, then we are sure there was no (Pre)Sharers in the structure.
            return new Obj(new Function(var,new Dict(new_list)));
          }
          *p_l_init=new_dn=new DictNode(PFormula(),key,dn->get_value());
          p_l_init=&(new_dn->l_init);
        }
        // If the key is not found, then the object is returned unchanged, and "new_list" is unused.
        {
        PFormula obj=arg_obj; // Save the object.
        args.consume(3);
        if(C.profile) shortcut_count++;
        return obj;
        }
      }
    }
    // The shortcut failed but we have 3 arguments, we compute it without the shortcut.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 3 arguments, do nothing
  return compute_stop(C);
}
