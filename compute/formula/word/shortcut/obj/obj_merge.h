
#pragma once
#include "../shortcut.h"
// Shortcut dictMerge

class ObjMerge : public Shortcut
{
public:
  ObjMerge() : Shortcut("objMerge") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
