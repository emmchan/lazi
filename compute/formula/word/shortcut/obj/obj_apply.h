#pragma once
#include "../shortcut.h"
// Shortcut dictApply

class ObjApply : public Shortcut
{
public:
  ObjApply() : Shortcut("objApply") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
