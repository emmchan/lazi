#include "get_thing.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula GetThing::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula x; // The result (the x in "just x").
    MatchEqual m_dvar_just(dvar_just);
    MatchSet m_x(x,match_anything);
    MatchApply m_a1(m_dvar_just,m_x);

    if(m_a1.match(C,args[0]))
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return x; 
    }
    else 
    {
      // If thre is 1 argument but we can't recognize it : we use the normal way.
      return WordDVar::compute_internal(C,args); 
    }
  }
  // If there is not 1 argument, do nothing
  return compute_stop(C);
}
