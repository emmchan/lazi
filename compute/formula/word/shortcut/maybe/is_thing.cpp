#include "is_thing.h"
#include "../../../../global_context.h"
#include "../../../../match/all.h"

PFormula IsThing::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula &x=args[0];
    
    MatchEqual m_dvar_just(dvar_just);
    MatchApply m_a1(m_dvar_just,match_anything);

    if(m_a1.match(C,x))
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return kw_1b;
    }
    else if(dvar_nothing->synt_equal(x)) // x is already computed, we can just compare.
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return kw_0b;
    }
    else 
    {
      // If thre is 1 argument but we can't recognize it : we use the normal way.
      return WordDVar::compute_internal(C,args); 
    }
  }
  // If there is not 1 argument, do nothing
  return compute_stop(C);
}
