#pragma once
#include "../shortcut.h"
// Shortcut isThing

class IsThing : public Shortcut
{
public:
  IsThing() : Shortcut("isThing") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
