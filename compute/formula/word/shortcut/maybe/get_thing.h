#pragma once
#include "../shortcut.h"
// Shortcut getThing

class GetThing : public Shortcut
{
public:
  GetThing() : Shortcut("getThing") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
