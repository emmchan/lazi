#include "word_equal.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"

PFormula WordEqual::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=2)
  {
    PFormula &arg_x=args[0];
    PFormula &arg_y=args[1];
    WordRepr *pwx=WordRepr::get_comp(C,arg_x);
    if(pwx!=nullptr)
    {
      WordRepr *pwy=WordRepr::get_comp(C,arg_y);
      if(pwy!=nullptr)
      {
        PFormula res=pwx->synt_equal(arg_y) ? kw_1b : kw_0b; // consume below can destroy arg_x & arg_y
        args.consume(2);
        if(C.profile) shortcut_count++;
        return res;
      }
    }
    // If thre is 2 arguments but we can't have WordRepr for both : we use the normal way.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 2 arguments, do nothing
  return compute_stop(C);
}
  
