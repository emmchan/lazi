#include "is_word.h"
#include "../../../word_repr.h"
#include "../../../../global_context.h"

PFormula IsWord::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
    PFormula &arg_x=args[0];
    if(WordRepr::get_comp(C,arg_x)!=nullptr)
    {
      args.consume(1);
      if(C.profile) shortcut_count++;
      return kw_1b;
    }
    // If thre is 1 argument but we can't have WordRepr : we use the normal way.
    return WordDVar::compute_internal(C,args); 
  }
  // If there is not 1 arguments, do nothing
  return compute_stop(C);
}
  

