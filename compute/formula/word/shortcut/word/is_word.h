#pragma once
#include "../shortcut.h"
// Shortcut isWord

class IsWord : public Shortcut
{
public:
  IsWord() : Shortcut("isWord") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

