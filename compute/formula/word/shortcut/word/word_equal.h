#pragma once
#include "../shortcut.h"
// Shortcut wordEqual

class WordEqual : public Shortcut
{
public:
  WordEqual() : Shortcut("wordEqual") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
