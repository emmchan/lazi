#pragma once
#include "../shortcut.h"
// Shortcut getThing

class IdentF : public Shortcut
{
public:
  IdentF() : Shortcut("identF") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};

