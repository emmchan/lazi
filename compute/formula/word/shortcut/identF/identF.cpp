#include "identF.h"
#include "../../../../global_context.h"


PFormula IdentF::compute_shortcut(CContext &C, Args &args)
{
  if(!args.empty())
  {
      PFormula res=args[0]; // Else consume below can destroy the PFormula 
      args.consume(1);
      if(C.profile) shortcut_count++;
      return res; 
  }
  // If there is not 1 argument, do nothing
  return compute_stop(C);
}
