#include <string>
#include <iostream>

#include "trace.h"
#include "../../../word_repr.h"
#include "../../../apply.h"
#include "../../../../c_context.h"
#include "../../../../global_context.h"


PFormula Trace::compute_shortcut(CContext &C, Args &args)
{
  if(args.get_size()>=6)
  {
    PFormula arg_flags,arg_label,arg_test,arg_test_comp,arg_print,arg_result;
    
    args.get6(arg_flags,arg_label,arg_test,arg_test_comp,arg_print,arg_result);
    WordRepr *p_flags=WordRepr::get_comp(C,arg_flags);
    WordRepr *p_label=WordRepr::get_comp(C,arg_label);
    if(p_flags!=nullptr && p_label!=nullptr)
    {
      // --- Get option in the word
      string opt=p_flags->get_name();
      const char *pc_opt=opt.c_str();
      size_t len=opt.length();
      bool to_stderr=false;
      bool to_compute=false;
      bool compute_args=false;
      if(len>0)
      {
        for(size_t n=0; n<len; n++)
        {
          if(pc_opt[n]=='p') to_stderr=true;
          if(pc_opt[n]=='c') to_compute=true;
          if(pc_opt[n]=='a') compute_args=true;
        }
      }
      // Compute test and check if we have to trace
      PFormula test_res = arg_test->compute(C);
      if(arg_test_comp->synt_equal(test_res))
      {
        // If we have to print '_callstack, then we replace the value to print by the callstack list.
        if(arg_print->synt_equal(wordRepr__callstack))
          arg_print=C.callstack_to_list();
        else
        {
          // --- Eventualy, compute the formula to print
          if(to_compute)
          {
            Args args2;
            arg_print=arg_print->new_compute_on_args(C,args2);
            if(compute_args)
              arg_print->compute_args(C,args2);
            arg_print=arg_print->apply_args(args2);
          }
        }
        
        // --- Print the formula, in stderr or in GC
        if(to_stderr)
          cerr << "--- Trace : " << p_label->get_name() << " : " << arg_print->to_txt() << "\n";
        else
          C.take_trace(p_label->get_name(),arg_print);
      }
    }
    // If we have the good arguments number, return the result.
    args.consume(6);
    if(C.profile) shortcut_count++;
    return arg_result; 
  }
  // If we don't have lesser argument, do nothing.
  return compute_stop(C);
}
  
  
