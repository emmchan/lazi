#pragma once
// Add a trace, like the Haskell Debug.Trace.
// The function is "trace 'flags 'label test test_comp to_print result", where:
// - 'flag : a word which contains flags (a flag is a letter):
//     - p : print in stderr instead of the normal xml output
//     - c : compute to_print before output
//     - a : if the flag c is on : also compute the arguments in to_print
// - 'label : a label to identifie the output
// - test : A value to compute (see test_comp) to decide if the trace will occur.
// - test_comp : The trace occurs if test_comp is syntactically equal to test.
// - to_print : The value to trace. If the value is "'_callstack", then instead of printed this word the callstack is printed.
// - result : The returned value.

#include "../shortcut.h"
// Shortcut trace

class Trace : public Shortcut
{
public:
  Trace() : Shortcut("trace") {};
  
  PFormula compute_shortcut(CContext &C, Args &args);
};
