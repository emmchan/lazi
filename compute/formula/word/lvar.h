#pragma once
// Lazi class for local variables.

#include "word.h"
#include "../../flagged_var_list.h"

extern PFormula lvarRepr;

class WordLVar : public Word
{
public:
  WordLVar(string _name) :  Word(_name) {};
  WordLVar(const WordLVar &) :  Word(string("copy_is_bad")) { assert(false); } // copy is bad
  

  //----------------------------------------
  // Syntax functions.

  // It's part of a notation, but not a notation by itself, and can't be simplified alone.
  bool is_notation() const { return false;};
  bool find_var(PFormula g) {return this==g.get_p();};
  PFormula replace_var(Formula *from, Formula *to) {return this==from ? to : this;};
  PFormula rename_var(WordLVar *from, WordLVar *to) {return this==from ? to : this;};
  // We can't simplify, should never be called.
  PFormula simplify() {assert(false); return this;};
  const char * word_s_type() const { return "lvar";};
  PFormula getTypeRepr() { return lvarRepr; };
  
  
  //----------------------------------------
  // Sharers
  
  void insert_pre_sharers(FlaggedVarList & vl) { vl.saw(this); };
};
