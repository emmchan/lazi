#pragma once
// Parent of all the classes for the word type in the Lazi syntax.
#include "../formula.h"

class Word : public Formula
{
public:
  Word(string _name);
  string get_name() const { return name; };
  // Type of the word as a string (can be "dvar", lvar, "gvar" or "key0")
  virtual const char * word_s_type() const=0;
  Word * get_Word() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_Word()!=nullptr; };
  bool is_notation() const { return false;}
  // Return repr (see below).
  PFormula getRepr() { return repr; };
  // Return the word representation of the type of the word (key0 or gvar).
  virtual PFormula getTypeRepr()=0;
  
  //----------------------------------------
  // Convertion to/from texte.
  
  void to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
protected:
  string name;
  PFormula repr; // The WordRepr of the name (used when we simplify a formula representation).
};
