#include "key_if.h"
#include "../../global_context.h"

PFormula WordKeyIf::compute_internal(CContext &C, Args &args)
{
  if(args.get_size()>=3)
  {
    PFormula x,y,z;
    args.get3(x,y,z);
    x = x->compute(C); // Fast if it's already 0b or 1b
    if(x==kw_1b) 
    {
      args.consume(3);
      return y;
    }
    if(x==kw_0b) 
    {
      args.consume(3);
      return z;
    }
    // if we can't compute the if, we set the computed condition.
    args[0]=x;
  }
  // If we fail to compute
  return compute_stop(C);
}
