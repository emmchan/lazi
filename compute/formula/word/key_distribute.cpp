#include "key_distribute.h"
#include "../../global_context.h"
#include "../apply.h"

// rule distribute x y z = (x z) (y z)
PFormula WordKey0Distribute::compute_internal(CContext &C, Args &args)
{
  if(args.get_size()>=3)
  {
    PFormula x,y,z;
    args.get3(x,y,z);
    args.consume(3);
    return new Apply( new Apply(x,z) , new Apply(y,z) );
  }
  // If we fail to compute the distribute.
  return compute_stop(C);
}

