#pragma once
// Lazi class for global variables.

#include "word.h"
#include "../../flagged_var_list.h"

extern PFormula gvarRepr;

class WordGVar : public Word
{
public:
  WordGVar(string _name) :  Word(_name) {};
  WordGVar(const WordGVar &) :  Word(string("copy_is_bad")) { assert(false); } // copy is bad
  
  const char * word_s_type() const { return "gvar";};
  PFormula getTypeRepr() { return gvarRepr; };
};
