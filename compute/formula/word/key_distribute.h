#pragma once
// Class for the "distribute" keyword.

#include "key0.h"

class WordKey0Distribute : public WordKey0
{
public:
  WordKey0Distribute() :  WordKey0("distribute") {};
  PFormula compute_internal(CContext &C, Args &args);
  unsigned int get_arity() const {return 3;}
};
