#pragma once
// Class for the "if" keyword.

#include "key0.h"

class WordKeyIf : public WordKey0
{
public:
  WordKeyIf() :  WordKey0("if") {};
  PFormula compute_internal(CContext &C, Args &args);
  unsigned int get_arity() const {return 3;}
};
