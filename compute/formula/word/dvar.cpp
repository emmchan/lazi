#include <algorithm> 

#include "dvar.h"
#include "../function.h"
#include "../../global_context.h"
#include "../word_repr.h"

PFormula WordDVar::compute_internal(CContext &C, Args &args)
{
  if(!C.watch) // The most common case
  {
    if(computed)
      return value;
    else
    {
      PFormula res; /* The formula used for the overall computation.*/
      /* We start to compute_internal the internal value alone, so we can share the result, after we will do the overall computation.*/
      unsigned save_stack=args.set_visibles(0);
      res=value->compute_on_args(C,args);
      value=res->apply_args(args); /* Store the sub-computation.*/
      computed=true;
      args.restore_visibles(save_stack);
      return res;
    }
  }
  else
  {
    PFormula res; /* The formula used for the overall computation.*/
    chronT start_time;
    
    // If we already readched the maximum callstack, then we should end quickly.
    if(C.max_callstack_reached) 
      return compute_stop(C);
    if(C.profile)
    {
      start_time=start_timer();
      call_count++;
    }
    if(C.store_callstack)
    {
      // If maximum callstack is reached : we stop and we trace the callstack
      if(C.max_callstack>=0 &&  C.max_callstack <= static_cast<int>(C.callstack.size()))
      {
        C.take_trace("callstack_limit_reached",C.callstack_to_list());
        C.max_callstack_reached=true;
        return compute_stop(C);
      }
      C.callstack.push_back(apply_args(args));
      // Reduce the stack (to minimize memory usage) if possible.
      if( C.max_callstack_trace!=-1 && static_cast<int>(C.callstack.size()) > max(C.max_callstack,C.max_callstack_trace) )
        C.callstack.pop_front();
    }
    if(computed)
      res=value;
    else
    {
      /* We start to compute_internal the internal value alone, so we can share the result, after we will do the overall computation.*/
      unsigned save_stack=args.set_visibles(0);
      res=value->compute_on_args(C,args);
      value=res->apply_args(args); /* Store the sub-computation.*/
      computed=true;
      args.restore_visibles(save_stack);
    }
    if(C.profile)
    {
      // Note: if the arity is >0 and if "computed" was false, then the previous computation did nothing because the value is a function without argument.
      if(arity>0 && !args.empty()) 
      {
        // Here we compute only on the arguments corresponding to the arity.
        unsigned save_stack=args.set_visibles(min(arity,args.get_size()));
        res=res->compute_on_args(C,args);
        args.restore_visibles(save_stack);
      }  
      internal_time += stop_timer(start_time); // Stop the timer for the internal computation
      // --- External computation
      start_time=start_timer();
      /* The overall computation.*/
      res=res->compute_on_args(C,args);
      external_time += stop_timer(start_time);
      C.to_continue=false; // No more computation to do
    }
    return res;
  }
}
  

//----------------------------------------
// Timer functions. (for profiling).

chronT WordDVar::start_timer()
{
  timer_nest++;
    return chrono::high_resolution_clock::now();
}

durationT WordDVar::stop_timer(chronT start)
{
  assert(timer_nest>0);
  timer_nest--;
  if(timer_nest==0)
    return chrono::high_resolution_clock::now() - start;
  else
    return chrono::high_resolution_clock::duration::zero();
}

