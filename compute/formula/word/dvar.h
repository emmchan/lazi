#pragma once
// Lazi keyword class for defined variables.
#include <assert.h>
#include <chrono>

#include "word.h"

typedef std::chrono::duration<long, std::ratio<1, 1000000000> > durationT;
typedef unsigned long int callCountT;
typedef chrono::time_point<chrono::high_resolution_clock>  chronT;
class Command;

extern PFormula dvarRepr;

class WordDVar : public Word
{
public:
  //----------------------------------------
  // Object management.
  
  WordDVar(string _name) :  Word(_name),computed(false),timer_nest(0) {};
  WordDVar(const WordDVar &) :  Word(string("copy_is_bad")) { assert(false); }; // copy is bad
  virtual void set(PFormula _value) { 
    original_value=_value; 
    arity=original_value->get_arity();
    reset();
  };
  bool is_set() const { return value!=p_null_formula; };
   // Called before starting an other computation.
  virtual void reset() { value=original_value;internal_time=durationT::zero();external_time=durationT::zero();call_count=0; assert(timer_nest==0);};
  virtual bool is_shortcut() { return false; }
  //----------------------------------------
  // Computation
  
  PFormula compute_internal(CContext &C, Args &args);

  //----------------------------------------
  // Syntax functions.

  const char * word_s_type() const { return "dvar";};
  PFormula getTypeRepr() { return dvarRepr; };
  PFormula simplify() { return value; };
  unsigned int get_arity() const { return arity; }

  //----------------------------------------
  // Timer functions. (for profiling).
  // We must avoid to count the time twice (recursivity).
  
  chronT start_timer(); // Return now time.
  durationT stop_timer(chronT start); // Compute the duration from "start", return zero if we are nested in the same dvar

protected:
  PFormula value; // The value of the global const.
  PFormula original_value; // Backup of the original value
  bool computed; // Do we have already compted the value ?
  unsigned int arity;
  // -------------- Profiling :
  durationT internal_time;
  durationT external_time;
  callCountT call_count;
  unsigned int timer_nest; // The nesting level of start_timer call
  
friend Command;
};


