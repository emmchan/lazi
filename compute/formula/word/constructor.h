#pragma once
// A constructor is a WordGlob who is not computed when the number of the arguments is equal or less the normal arity. For example "pair". The idea is : there is few function which use a constructor (with its argument) as argument, and all these function have a shortcut. These shortcuts need to see the constructor. 
// If a shortcut fails, then the normal way to compute it is used, which end with the constructor applied with more arguments, so it is computed.
// We can see constructors as function which don't need to be computed more if they have fewer arguments than a certain value.

#include "dvar.h"

class Constructor : public WordDVar
{
public:
  Constructor(string _name,unsigned int _arity) :  WordDVar(_name) {arity=_arity;};
  Constructor(const Constructor &) :  WordDVar(string("copy_is_bad")) { assert(false); } // copy is bad
  void set(PFormula _value) { original_value=_value; reset();};
  Constructor * get_Constructor() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_Constructor()!=nullptr; };

  PFormula compute_internal(CContext &C, Args &args);
};
