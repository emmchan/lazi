#pragma once
// Lazi keyword class.

#include "word.h"

extern PFormula key0Repr;

class WordKey0 : public Word
{
public:
  WordKey0(string _name) :  Word(_name) {};
  WordKey0(const WordKey0 &) :  Word(string("copy_is_bad")) { assert(false); } // copy is bad
  
  WordKey0 * get_WordKey0() {return this;};
  
  PFormula compute(CContext &) { return this; };
  const char * word_s_type() const { return "key0";};
  PFormula getTypeRepr() { return key0Repr; };
};
