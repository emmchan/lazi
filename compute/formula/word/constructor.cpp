#include "constructor.h"

PFormula Constructor::compute_internal(CContext &C, Args &args)
{
  // We compute only if there is more arguments than the arity.
  if(args.get_size()>arity) 
    return WordDVar::compute_internal(C,args);
  else
    return compute_stop(C);
}
