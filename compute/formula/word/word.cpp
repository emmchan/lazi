#include <string.h>

#include "word.h"
#include "dvar.h"
#include "lvar.h"
#include "key0.h"
#include "gvar.h"
#include "../word_repr.h"
#include "../../global_context.h"


Word::Word(string _name) : name(_name), repr(GC.repr_store.get(_name))  
{}

PFormula Word::xml_to_formula(xml_node node, bool inRepr)
{
  const char *name;
  const char *type; // type of the name

  name=node.attribute("name").value();
  type=node.attribute("type").value();
  if(strcmp(type, "lvar")==0)
    return GC.lvar_store.get(name);
  if(strcmp(type, "key0")==0)
    return GC.key_store.get(name);
  if(strcmp(type, "gvar")==0)
  {
    if(!inRepr)
      throw runtime_error(string("Error : use of a gvar word \"") + name + "\" outside a formula representation.");
    else
      return GC.gvar1_store.get(name);
  }
  if(strcmp(type, "dvar")==0)
  {
    // First we check that the dvar is already initialized.
    PFormula g=GC.dvar_store.get(name);
    if(!inRepr && !(static_cast<WordDVar &>(*g)).is_set())
      throw runtime_error(string("Error : use of undefined dvar word \"") + name + "\".");
    else
      return g;
  }
  else
    throw runtime_error(string("Error : bad word type : ") + type);
}

void Word::to_xml(xml_node parent,int) const
{
  xml_node node=parent.append_child("word");
  node.append_attribute("type")=word_s_type();
  node.append_attribute("name")=get_name().c_str();
}

string Word::to_txt() const
{
  return /*string(":") + word_s_type() + ":" +*/ get_name();
}

  
