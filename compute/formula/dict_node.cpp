#include <list>

#include "dict_node.h"
#include "../global_context.h"
#include "share/pre_sharer_big.h"
#include "word_repr.h"
#include "apply.h"
#include "../names_store.h"


//----------------------------------------
// Object management.

DictNode::DictNode(PFormula _l_init, WordRepr *_name, PFormula _value) : l_init(_l_init),name(_name),value(_value) 
{
}

PFormula DictNode::to_pre_sharer(FlaggedVarList &vl)
{
  if(vl.set_empty())
    return to_sharer();
  return new PreSharerBig(this,vl);
} 
 
    
//----------------------------------------
// Convertion to/from texte.

void DictNode::to_xml(xml_node parent,int max_depth) const
{
  const_cast<DictNode *>(this)->simplify()->to_xml(parent,max_depth);
}

void DictNode::entries_to_xml(xml_node parent,int max_depth) const
{
  string s_name; // The name of the key, as a string.
  if(max_depth==0)
    to_xml_max_depth(parent);
  else
  {
    // The first "to_xml" on a DictNode is on the last node of the list. We need to put elements from start to end. So we get the list of elements and then create the xml dict.
    list<DictNode *> l;
    for(PFormula n(const_cast<DictNode *>(this)); !n->synt_equal(dvar_0l); n = n->get_DictNode()->get_l_init())
    {
      assert(n->get_DictNode()!=0);
      l.push_front(n->get_DictNode());
    }
    for(DictNode *p:l)
    {
      xml_node entry_xml_node=parent.append_child("entry");
      s_name=p->get_key()->get_name();
      entry_xml_node.append_attribute("name") = s_name.c_str();
      p->get_value()->to_xml(entry_xml_node,max_depth-1);
    }  
  }
}

string DictNode::to_txt() const
{
  string res;
  res="( DictNode name=\"" + name->get_name() + "\"\nvalue=( " + value->to_txt() + " )\nl_init = ( " + l_init->to_txt() + " )\n)";
  return res;
}

PFormula DictNode::xml_to_formula(xml_node node, bool inRepr)
{
  PFormula l_init;
  const char *cname; // name of the entry, as a char *
  PFormula name; // name of the entry, as a PFormula
  PFormula value; // The value of the entry
  PFormula old_res; // The old result
  PFormula new_res(dvar_0l); // The new result, constructed from the old one.
  for (pugi::xml_node entry_node = node.first_child(); entry_node; entry_node = entry_node.next_sibling())
  {
    old_res=new_res; // The new result become old.
    cname=entry_node.attribute("name").value();
    name=GC.repr_store.get(cname);
    value=Formula::xml_to_formula(entry_node.first_child(),inRepr);
    new_res=PFormula(new DictNode(old_res,name->get_WordRepr(),value));
  }
  assert(!new_res->synt_equal(dvar_0l)); // A dict can't be empty
  return new_res;
}

 
//----------------------------------------
// Computation

PFormula DictNode::compute_internal(CContext &C, Args &args)
{
  if(args.empty())
    return compute_stop(C); //As a constructor, we don't compute it without arguments.
  return simplify();
}

//----------------------------------------
// Syntax functions.

PFormula DictNode::simplify()
{
  // DictNode l_init name value = toListBwAdd l_init \ pair name value
  return 
    PnewApply(
      PnewApply(
        dvar_toListBwAdd,
        l_init
      )
    ,
      PnewApply(
        PnewApply(
          dvar_pair,
          PFormula(get_key())
        ),
        get_value()
      )
    );
}

bool DictNode::synt_equal(Formula * g) const
{
  DictNode *p=g->get_DictNode();
  if(p==nullptr)
    return false;
  return p->get_key()==name && 
    value->synt_equal(p->get_value()) && 
    l_init->synt_equal(p->get_l_init());
}

bool DictNode::find_var(PFormula g)
{
  return value->find_var(g) || l_init->find_var(g);
}

//----------------------------------------
// Sharers


PFormula DictNode::init_pre_sharers(list<bool> &l)
{
  list<bool> cl(l); // l can be modified, so we use a copy for f
  l_init=l_init->init_pre_sharers(cl);
  value=value->init_pre_sharers(l);
  return this;
}


