#include <string.h>

#include "function.h"
#include "share/sharer.h"
#include "apply.h"
#include "word/lvar.h"
#include "../global_context.h"
#include "share/pre_sharer_big.h"

//----------------------------------------
// Object management.

PFormula Function::to_pre_sharer(FlaggedVarList &vl)
{
  if(vl.set_empty())
    return to_sharer();
  return new PreSharerBig(this,vl);
} 

//----------------------------------------
// Computation

PFormula Function::compute_internal(CContext &C, Args &args)
{
  // Without argument, we do nothing.
  if(args.empty())
    return compute_stop(C);
  else
  {
    PFormula to(args[0]->to_sharer()); // We need to store the Formula the time of the replacement.
    args.consume(1);
    return body->replace_var(var,to.get_p());
  }
}

//----------------------------------------
// Convertion to/from texte.

void Function::to_xml(xml_node parent,int max_depth) const
{
  if(max_depth==0)
    to_xml_max_depth(parent);
  else
  {
    xml_node node=parent.append_child("function");
    node.append_attribute("var") = (static_cast<const WordLVar &>(*var)).get_name().c_str();
    body->to_xml(node,max_depth-1);
  }
}


string Function::to_txt() const
{
  return "( $F " + (static_cast<const WordLVar &>(*var)).get_name() + " -> " + body->to_txt() + " )";
}


PFormula Function::xml_to_formula(xml_node node, bool inRepr)
{
  const char *var_name; // name of the variable

  var_name=node.attribute("var").value();
  PFormula var=GC.lvar_store.get(var_name);
  PFormula body=Formula::xml_to_formula(node.first_child(), inRepr);
  return PFormula( new Function( static_cast<WordLVar *>(var.get_p()), body ) );  
}


    
//----------------------------------------
// Syntax functions.
  
bool Function::synt_equal(Formula * g) const
{
  Function *g_func=g->get_Function();
  
  if(g_func==nullptr) // If g is not a function.
    return false;
  else
    return var==g_func->var && body->synt_equal(g_func->body);
}

PFormula Function::simplify()
{
  // If the function is a constant function.
  if(!body->find_var(var))
  {
    return PnewApply(PnewApply(kw_if, kw_1b), body);
  }
  // If the function is the identity function.
  if(body->synt_equal(var))
  {
    return PnewApply(PnewApply(kw_if, kw_0b), kw_0b);
  }
  // Get an apply from the body. Because it's not a constant, the variable is in the body. Because the case where the variable is the body is already handled, we are sure the body is or can be simplified to an apply.
  Apply *apply=body->get_Apply();
  PFormula app;
  if(apply==nullptr)
  {
    app=body->simplify_notations();
    apply=app->get_Apply();
    assert(apply!=nullptr);
  }
  return 
    PnewApply 
    ( 
      PnewApply 
      ( 
        kw_distribute, 
        PFormula (new Function(var, apply->get_f() )) 
      ),
      PFormula (new Function(var, apply->get_a() ))
    );
}

bool Function::find_var(PFormula g)
{
  // If g is the variable, then it's not in the body (g is an external variable with the same name).
  if(var->synt_equal(g))
    return false;
  else
    return body->find_var(g);
}

PFormula Function::replace_var(Formula *from, Formula *to)
{
  if(from==var)  // "from" is a lvar, without a sharer arround, so we can use pure equality
    return this;
  else
    return new Function(var, body->replace_var(from,to));
}

PFormula Function::rename_var(WordLVar * from, WordLVar * to)
{
  if(var==from) // No free variable "from" inside.
    return this;
  if(var==to) // If body contains "from", then we can have a collision if "from" is a free variable in body.
  {
    if(body->find_var(from))
      throw(Collision());
    else
      return this;
  }
  // Ordinary case.
  return new Function(var,body->rename_var(from,to));
}

unsigned int Function::get_arity() const
{
  return 1+body->get_arity();
}

void Function::prepareBodies()
{
  FlaggedVarList vl(var);
  body->insert_pre_sharers(vl);
  list<bool> l;
  l.push_front(true); // The variable of the function is not hidded by a PreSharers.
  body->init_pre_sharers(l);
}

void Function::insert_pre_sharers(FlaggedVarList & vl)
{
  vl.add_var(var);
  body->insert_pre_sharers(vl);
  // Sharer and PreSharer objects wrapp a formula when there is a change in free variables set. By default a variable of a function is used in the body, hence this is the case if the body does not use the variable of the function.
  if(!vl.get_last_flag())
    body=body->to_pre_sharer(vl);
  vl.rem_var(); // Remove the added variable.
}

PFormula Function::init_pre_sharers(list<bool> & l)
{
  l.push_back(true); // Add the flag for the variable of the function.
  body=body->init_pre_sharers(l);
  l.pop_back(); // Remove the added flag (we need this, see Apply::init_pre_sharers(list<bool> & l)
  return this;
}


