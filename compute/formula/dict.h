#pragma once
///////////////////////////////////////////////////
// Dictionnary formula. A notation for a dictionnary formula: a DictNode where all l_init nodes are DictNode or dvar_0l

#include <lazypool/pooled.h>
#include <unordered_set>
#include <assert.h>


#include "formula.h"
#include "function.h"
#include "share/sharer.h"
#include "dict_node.h"

class DictNotaIterator;
class MatchDictAsListLInit;
class MatchDictAsListLast;

class Dict : public Formula, public lazypool::Pooled<8>
{
public:
  //----------------------------------------
  // Object management.
  
  Dict(PFormula _dn) : dn(_dn) {assert(check_validity());};

  Dict * get_Dict() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_Dict()!=nullptr; };
  // We can't compute a function, thus no need to share computation. But we can use a sharer for a function to store the information that there is no free variable.
  PFormula to_sharer() {return new Sharer(this,true);};
  PFormula to_pre_sharer(FlaggedVarList &vl);
  PFormula get_node() { return dn; };
  // Check if all nodes are well formed. For debug.
  bool check_validity();
   
  //----------------------------------------
  // Convertion 
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
  void to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;
  // Return the set of keys.
  void get_keys(unordered_set<WordRepr *> &keys);

 
  //----------------------------------------
  // Computation
  
  PFormula compute_internal(CContext &C, Args &args);
  // Compute the values of the dict, using compute_rec_args (used by compute_args used to convert the result to a human readable form).
  void compute_internal_args(CContext &C);
 
  //----------------------------------------
  // Syntax functions.

  PFormula simplify();
  bool synt_equal(Formula * g) const;
  bool find_var(PFormula g);
  PFormula replace_var(Formula *from, Formula *to) { return new Dict(dn->replace_var(from,to)); };
  PFormula rename_var(WordLVar *from, WordLVar *to) { return new Dict(dn->rename_var(from,to)); };
  //----------------------------------------
  // Sharers
  
  void prepareBodies() {dn->prepareBodies();};
  void insert_pre_sharers(FlaggedVarList & vl) { dn->insert_pre_sharers(vl); };
  PFormula init_pre_sharers(list<bool> &l) {dn=dn->init_pre_sharers(l); return this;};

protected:
  PFormula dn; // The DictNode

friend DictNotaIterator;
friend MatchDictAsListLInit;
friend MatchDictAsListLast;
};

