#pragma once
//  FormulaRepr : a formula representation.
#include "formula.h"

class FormulaRepr : public Formula
{
public:
  FormulaRepr(PFormula _f) : f(_f) {};
  
  FormulaRepr * get_FormulaRepr() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_FormulaRepr()!=nullptr; };
  

  //----------------------------------------
  // Computation

  PFormula compute_internal(CContext &, Args &);
  // If f is of this class, then return the pointer to the object. Else f is computed (in the hope of getting the good type). The result of the computation is set in f (it's a reference). If failure return nullptr, else return the object address.
  static FormulaRepr * get_comp(CContext &C, PFormula &f);
   
  //----------------------------------------
  // Convertion to/from texte.
  
  void to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
    
  //----------------------------------------
  // Syntax functions.
  
  bool synt_equal(Formula * g) const;
  PFormula simplify();
 
protected:
  PFormula f;
};

