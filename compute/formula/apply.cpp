///////////////////////////////////////////////////
// Apply formula

#include "apply.h"
#include "../global_context.h"
#include "share/pre_sharer_big.h"

//----------------------------------------
// Object management.
 
PFormula Apply::to_pre_sharer(FlaggedVarList &vl)
{
  if(vl.set_empty())
    return to_sharer();
  return new PreSharerBig(this,vl);
} 


//----------------------------------------
// Convertion to/from texte.

void Apply::to_xml(xml_node parent,int max_depth) const
{
  if(max_depth==0)
    to_xml_max_depth(parent);
  else
  {
    xml_node node=parent.append_child("apply");
    f->to_xml(node.append_child("f"),max_depth-1);
    a->to_xml(node.append_child("a"),max_depth-1);
  }
}

string Apply::to_txt() const
{
  string res,s;

  res= f->to_txt() + " ";
  if(Apply::is_me(a))
    res+= "( " + a->to_txt() + " )";
  else
    res+= a->to_txt();
  return res;
}

PFormula Apply::xml_to_formula(xml_node node, bool inRepr)
{
  return PnewApply(
          Formula::xml_to_formula(node.child("f").first_child(),inRepr),
          Formula::xml_to_formula(node.child("a").first_child(),inRepr)
         );
}

//----------------------------------------
// Computation

PFormula Apply::compute_internal(CContext &/*C*/, Args &args)
{
  // to_sharer because if the argument is computed, then the computation will be reported in the apply. For the function part, it's not worth it.
  a=a->to_sharer(); 
  args.push(a);
  return f;
}
 

//----------------------------------------
// Syntax functions.

bool Apply::synt_equal(Formula * g) const
{
  Apply *apply2=g->get_Apply();
  
  if(apply2==nullptr) // If g is not an Apply.
    return false;
  else
    return f->synt_equal(apply2->f) && a->synt_equal(apply2->a);
}

bool Apply::find_var(PFormula g)
{
  return f->find_var(g) || a->find_var(g);
}


unsigned int Apply::get_arity() const
{
  // Special case for recurse
  if(f->synt_equal(dvar_recurse))
  {
    unsigned int aar = a->get_arity();
    return aar == 0 ? 0 : aar - 1;
  }
  else
  {
    unsigned int far = f->get_arity();
    return far == 0 ? 0 : far - 1;
  }
}

PFormula Apply::init_pre_sharers(list<bool> &l)
{
  list<bool> cl(l); // l can be modified, so we use a copy for f
  f=f->init_pre_sharers(cl);
  a=a->init_pre_sharers(l);
  return this;
}

