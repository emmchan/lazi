#include <string.h>

#include "share/sharer.h"
#include "apply.h"
#include "formula.h"
#include "function.h"
#include "word/lvar.h"
#include "dict_node.h"
#include "../global_context.h"
#include "share/pre_sharer_big.h"
#include "dict.h"
#include "obj.h"


//----------------------------------------
// Object management.

PFormula Obj::to_pre_sharer(FlaggedVarList &vl)
{
  if(vl.set_empty())
    return to_sharer();
  return new PreSharerBig(this,vl);
} 


//----------------------------------------
// Convertion to/from texte.


void Obj::to_xml(xml_node parent,int max_depth) const
{
  if(max_depth==0)
    to_xml_max_depth(parent);
  else
  {
    Function *func=(const_cast<Obj *>(this))->f->get_Function();
    xml_node obj_xml_node=parent.append_child("obj");
    obj_xml_node.append_attribute("var") = func->get_var()->get_name().c_str();
    func->get_body()->get_Dict()->get_node()->get_DictNode()->entries_to_xml(obj_xml_node,max_depth-1);
  }
}

string Obj::to_txt() const
{
  Function *func=(const_cast<Obj *>(this))->f->get_Function();
  string res;
  res="( Obj var=\"" + func->get_var()->get_name() + "\"\ndict=( " + func->get_body()->to_txt() + " )\n)";
  return res;
}


// Convertion to/from texte.
PFormula Obj::xml_to_formula(xml_node node, bool inRepr)
{
  PFormula l_init;
  const char *cvar; // name of the variable, as a char *
  WordLVar *var;
  PFormula body;

  cvar=node.attribute("var").value();
  var=static_cast<WordLVar *>(GC.lvar_store.get(cvar).get_p());
  body=new Dict(DictNode::xml_to_formula(node,inRepr));
  return(new Obj(new Function(var,body)));
}


//----------------------------------------
// Computation

PFormula Obj::compute_internal(CContext &C, Args &args)
{
  if(args.empty())
    return compute_stop(C); //As a constructor, we don't compute it without arguments.
  return simplify();
}

//----------------------------------------
// Syntax functions.

bool Obj::synt_equal(Formula * g) const
{
  Obj *p=g->get_Obj();
  if(p==nullptr)
    return false;
  return f->synt_equal(p->f);
}

bool Obj::find_var(PFormula g)
{
  return f->find_var(g);
}

//----------------------------------------
// Sharers

void Obj::insert_pre_sharers(FlaggedVarList & vl)
{
  // There can't be a modification of the free variable list on the function, then we can directly iterete on nodes.
  Function *func = f->get_Function();
  WordLVar *var=func->get_var();
  
  vl.add_var(func->get_var());
  assert(func->get_body()->get_Dict()!=nullptr);
  insert_pre_sharers_on_obj_node(func->get_body()->get_Dict()->get_node(),vl,var);
  assert(func->get_body()->get_Dict()!=nullptr);
  vl.rem_var(); // Remove the added variable.
}
 
// Do the Obj::insert_pre_sharers job on a node. var is the variable of the function of the Obj.
void Obj::insert_pre_sharers_on_obj_node(PFormula l_init,FlaggedVarList & vl,WordLVar *var)
{
  DictNode *node=l_init->get_DictNode();
  FlaggedVarList vlf(vl);
  FlaggedVarList vla(vl);
  
  if(!node->l_init->synt_equal(dvar_0l)) // If it's the end of the list, vlf is not changed.
    insert_pre_sharers_on_obj_node(node->l_init,vlf,var);
  vlf.saw(var); // We do as if we saw var in l_init, see .h of Obj::insert_pre_sharers.
  node->value->insert_pre_sharers(vla);
  insert_pre_sharers_on_two_with_vlfa(vl,node->l_init,node->value,vlf,vla);
}
