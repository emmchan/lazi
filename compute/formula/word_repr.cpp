#include <list>
#include <string.h>

#include "word_repr.h"
#include "../global_context.h"
#include "word/key0.h"
#include "apply.h"
#include "../c_context.h"

PFormula WordRepr::compute_internal(CContext &C, Args &args)
{
  if(args.empty() || C.max_callstack_reached)
    return compute_stop(C);
  return simplify();
}

WordRepr * WordRepr::get_comp(CContext &C, PFormula &f)
{
  WordRepr *p=f->get_WordRepr();
  // If it's not a the good type, compute it, change the argument with the result.
  if(p==nullptr)
  {
    f=f->compute(C);
    return f->get_WordRepr();
  }
  else
    return p;
}

void WordRepr::to_xml(xml_node parent,int) const
{
  xml_node node=parent.append_child("nameRepr");
  node.append_attribute("name") = name.c_str();
}

PFormula WordRepr::xml_to_formula(xml_node node, bool /*inRepr*/)
{
  const char* name;
  
  name=node.attribute("name").value();
  // See if a debug shortcut which need to trace the Lazi callstack is used.
  if(strcmp(name,"_callstack")==0)
    GC.stack_trace_used=true;
  return GC.repr_store.get(name);
}


string WordRepr::to_txt() const
{
  return string("'") + get_name();
}


// Utility function for WordRepr::simplify : convert a c++ boolean list to a Lazi binary list.
PFormula bool_cpp_to_lazi(list<bool>::iterator start, list<bool>::iterator end) 
{
  PFormula result=dvar_0l;
  list<bool>::iterator it=start;

  if(it!=end)
  {
    do
    {
      result=PnewApply(PnewApply(dvar_toListBwAdd, result), *it ? kw_1b : kw_0b);
      it++;
    } while(it!=end);
  }
  return result;
}

PFormula WordRepr::simplify()
{
  // We must translate the word into a boolean list.
  const char * pchar;
  char c;
  list<bool> lb;
  
  // Convert to a C++ bool list then call bool_cpp_to_lazi
  
  // for each byte in the array (the array is null terminated)
  for(pchar=get_name().c_str(); *pchar != 0; pchar++)
  {
    c=*pchar;
    // For each bit of the character
    for(int n=0; n<8; n++)
    {
      lb.push_back(c & 128 ? true : false);
      c = c << 1; // shift to left
    }
  }
  return bool_cpp_to_lazi(lb.begin(),lb.end());
}


PFormula WordRepr::keyWordToWordRepr(WordKey0 *w)
{
  return GC.repr_store.get(w->get_name());
}
