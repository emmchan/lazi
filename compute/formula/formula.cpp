#include <string.h>
#include <list>
#include <pugixml.hpp>

#include "formula.h"
#include "apply.h"
#include "word/word.h"
#include "word_repr.h"
#include "formula_repr.h"
#include "function.h"
#include "dict_node.h"
#include "dict.h"
#include "obj.h"
#include "word/constructor.h"

void Formula::overall_to_xml(xml_node parent,int max_depth) const
{
  xml_node formula_node=parent.append_child("formula");
  formula_node.append_attribute("format") = "xml";
  to_xml(formula_node,max_depth);
}

void Formula::to_xml_max_depth(xml_node parent) const
{
  xml_node node=parent.append_child("word");
  node.append_attribute("type") = "dvar";
  node.append_attribute("name") = "MAX_DEPTH_REACHED";
}

PFormula  Formula::xml_global_to_formula(xml_node node)
{
  string format;
  
  format=node.attribute("format").value();
  if(format!="xml")
  {
    throw runtime_error(string("Error : I found a formula with an other format than xml : \"") + format + "\"");
  }
  PFormula f(xml_to_formula(node.first_child(),false));
  f->prepareBodies();
  return f;
}

PFormula  Formula::xml_to_formula(xml_node node, bool inRepr)
{
  const char *name=node.name();
  if(strcmp(name, "apply")==0)                  { return Apply::xml_to_formula(node, inRepr); }
  else if(strcmp(name, "word")==0)        { return Word::xml_to_formula(node, inRepr); } 
  else if(strcmp(name, "nameRepr")==0)       { return WordRepr::xml_to_formula(node, inRepr); } 
  else if(strcmp(name, "formulaRepr")==0) { return FormulaRepr::xml_to_formula(node, inRepr); }
  else if(strcmp(name, "function")==0)       { return Function::xml_to_formula(node, inRepr); }
  else if(strcmp(name, "dict")==0)       { return Dict::xml_to_formula(node, inRepr); }
  else if(strcmp(name, "obj")==0)       { return Obj::xml_to_formula(node, inRepr); }
  else{ throw runtime_error(string("Bad xml node name : ") + string(name)); }
}

PFormula Formula::apply_args(Args &args)
{
  PFormula res = PFormula(this);
  unsigned size=args.get_size();
  for ( unsigned n=0; n<size; n++ )
    res=new Apply(res,args[n]);
  return res;
}

PFormula Formula::compute_rec_args(CContext &C)
{
  Args args; // New empty args list for each computation.
  PFormula res=new_compute_on_args(C,args);
  // If res is a constructor, we compute its args too.
  if(Constructor::is_me(res))
    compute_args(C,args);
  // If it's an objet constructor with internal argument.
  if(Dict::is_me(res)) 
    res->get_Dict()->compute_internal_args(C);
  if(Obj::is_me(res)) 
    res->get_Obj()->get_f()->get_Function()->get_body()->get_Dict()->compute_internal_args(C);
 return res->apply_args(args); // Write the result.
  
}

void Formula::compute_args(CContext &C, Args &args)
{
  unsigned size=args.get_size();
  for ( unsigned n=0; n<size; n++ )
    args[n]=args[n]->compute_rec_args(C);
}

PFormula Formula::compute(CContext &C)
{
  Args args; // Arguments
  PFormula res=new_compute_on_args(C,args);
  return res->apply_args(args); // Add arguments to get the result.
}

PFormula Formula::new_compute_on_args(CContext &C, Args &args)
{
  if(C.store_callstack)
  {
    auto call_size=C.callstack.size();
    PFormula res=compute_on_args(C,args);
    // The computation end, we restore the stack to the original size.
    C.callstack.resize(call_size);
    return res;
  }
  else
    return compute_on_args(C,args);
}


PFormula Formula::compute_on_args(CContext &C, Args &args){
  PFormula res(this);
  C.to_continue=true;
  if(C.shortcut_level==0)
  {
    do
    {
      if(res->is_notation())
        res=res->simplify_to_FuncKeyword(args);
      res=res->compute_internal(C,args); 
    } while (C.to_continue);
  }
  else
  {
    do
    {
      res=res->compute_internal(C,args); 
    } while (C.to_continue);
  }
  // Restore C.to_continue, because compute_on_args can be called in a compute_internal.
  C.to_continue=true;
  return res;
}

PFormula Formula::simplify_notations()
{
  PFormula f(this);
  while(f->is_notation())
    f=f->simplify();
  return f;
}

PFormula Formula::simplify_to_FuncKeyword(Args &args)
{
  PFormula f(this);

  while(true)
  {
    f=f->simplify_notations();
    Apply *pa = f->get_Apply();
    if(pa==nullptr) 
      return f; // f is a keyword
    args.push(pa->get_a());
    f=pa->get_f();
  }
}

//----------------------------------------
// Sharers

void Formula::insert_pre_sharers_on_two(FlaggedVarList &vl, PFormula & f,PFormula & a)
{
  FlaggedVarList vlf(vl);
  FlaggedVarList vla(vl);
  
  f->insert_pre_sharers(vlf);
  a->insert_pre_sharers(vla);
  insert_pre_sharers_on_two_with_vlfa(vl,f,a,vlf,vla);
}

void Formula::insert_pre_sharers_on_two_with_vlfa(FlaggedVarList &vl,PFormula & f,PFormula & a,FlaggedVarList &vlf,FlaggedVarList &vla)
{
  CompareRes cmp;
  cmp=vlf.compare(vla);
  switch(cmp)
  {
    case CMP_EQUAL: // They have the same set of free variable, let parents create a Presharer if needed.
      vl.set_union(vlf);
      return;
    case CMP_INCLUDED: // "a" has less free variables than "f": we create a PreSharer for "a" to protect him from useless replacements.
      a=a->to_pre_sharer(vla); // Can be a Sharer too if the set is empty.
      vl.set_union(vlf);
      return;
    case CMP_REV_INCLUDED: // Reverse case of CMP_INCLUDED, see above.
      f=f->to_pre_sharer(vlf);
      vl.set_union(vla);
      return;
    case CMP_DIFF: // "a" and "f" have different free variables sets : we create a PreSharer for both to protect them from useless replacements.
      a=a->to_pre_sharer(vla);
      f=f->to_pre_sharer(vlf);
      vl.set_union(vlf);
      vl.set_union(vla);
      return;
    default:
      assert(false);
  }
}
