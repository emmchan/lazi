#include <string.h>

#include "share/sharer.h"
#include "apply.h"
#include "formula.h"
#include "word/lvar.h"
#include "dict_node.h"
#include "../global_context.h"
#include "share/pre_sharer_big.h"
#include "dict.h"


//----------------------------------------
// Object management.

PFormula Dict::to_pre_sharer(FlaggedVarList &vl)
{
  if(vl.set_empty())
    return to_sharer();
  return new PreSharerBig(this,vl);
} 

bool Dict::check_validity()
{
  PFormula node;
  for(node=dn;DictNode::is_me(node);node=node->get_DictNode()->get_l_init());
  return node->synt_equal(dvar_0l);
}
//----------------------------------------
// Convertion to/from texte.


void Dict::to_xml(xml_node parent,int max_depth) const
{
  if(max_depth==0)
    to_xml_max_depth(parent);
  else
  {
    DictNode *pdn=(const_cast<Dict *>(this))->dn->get_DictNode();
    xml_node dict_xml_node=parent.append_child("dict");
    pdn->entries_to_xml(dict_xml_node,max_depth-1);
  }
}

string Dict::to_txt() const
{
  DictNode *pdn=(const_cast<Dict *>(this))->dn->get_DictNode();
  string res;
  res="Dict(" + pdn->to_txt() + ")";
  return res;
}


// Convertion to/from texte.
PFormula Dict::xml_to_formula(xml_node node, bool inRepr)
{
  PFormula dn;

  dn=DictNode::xml_to_formula(node,inRepr);
  return(new Dict(dn));
}

void Dict::get_keys(unordered_set<WordRepr *> &keys)
{
  PFormula node=dn;
  DictNode *pdn;
  
  do
  {
    pdn=node->get_DictNode();
    keys.insert(pdn->get_key());
    node=pdn->get_l_init();
  } while(DictNode::is_me(node));
}

//----------------------------------------
// Computation

PFormula Dict::compute_internal(CContext &C, Args &args)
{
  if(args.empty())
    return compute_stop(C); //As a constructor, we don't compute it without arguments.
  return simplify();
}

void Dict::compute_internal_args(CContext &C)
{
  PFormula node=dn;
  DictNode *pdn;
  
  do
  {
    pdn=node->get_DictNode();
    pdn->value=pdn->value->compute_rec_args(C);
    node=pdn->get_l_init();
  } while(DictNode::is_me(node));
}

//----------------------------------------
// Syntax functions.

PFormula Dict::simplify()
{
  return dn->simplify();
}

bool Dict::synt_equal(Formula * g) const
{
  Dict *p=g->get_Dict();
  if(p==nullptr)
    return false;
  return dn->synt_equal(p->dn);
}

bool Dict::find_var(PFormula g)
{
  return dn->find_var(g);
}


