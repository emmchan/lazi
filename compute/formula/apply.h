#pragma once
///////////////////////////////////////////////////
// Apply formula
#include <lazypool/pooled.h>


#include "formula.h"
#include "share/sharer.h"

class MatchApply;

class Apply : public Formula, public lazypool::Pooled<1>
{
public:
  //----------------------------------------
  // Object management.
  
  Apply(PFormula _f, PFormula _a) : f(_f), a(_a) {};
  Apply * get_Apply() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_Apply()!=nullptr; };
  PFormula to_sharer() {return new Sharer(this);};
  PFormula to_pre_sharer(FlaggedVarList &vl);
  
  //----------------------------------------
  // Convertion to/from texte.
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
  void to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;

 
  //----------------------------------------
  // Computation
  
  PFormula compute_internal(CContext &C, Args &args);

  //----------------------------------------
  // Syntax functions.

  bool is_notation() const { return false;};
  bool synt_equal(Formula * g) const;
  bool find_var(PFormula g);
  // More than 10% of CPU
  PFormula replace_var(Formula *from, Formula *to) { return new Apply(f->replace_var(from,to),a->replace_var(from,to)); };
  PFormula get_f() {return f;}
  PFormula get_a() {return a;}
  // We can't simplify, should never be called.
  PFormula simplify() {assert(false); return this;};
  unsigned int get_arity() const;
  PFormula rename_var(WordLVar * from, WordLVar * to) { return new Apply(f->rename_var(from,to),a->rename_var(from,to)); };
  
  //----------------------------------------
  // Sharers
  
  void prepareBodies() {f->prepareBodies(); a->prepareBodies();};
  void insert_pre_sharers(FlaggedVarList & vl) { insert_pre_sharers_on_two(vl,f,a); };
  PFormula init_pre_sharers(list<bool> &l);

protected:
  PFormula f; // The function part of the apply,
  PFormula a; // and the argument part.

friend MatchApply;
};

inline PFormula PnewApply(PFormula f,PFormula a) { return new Apply(f,a);}

extern PFormula dvar_pair;
inline PFormula PnewPair(PFormula a,PFormula b) { return new Apply(new Apply(dvar_pair, a),b);}







