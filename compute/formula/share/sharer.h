#pragma once
// A container to share computation of a formula appearing in sereval places. We have formula duplication because of the distribute rule. This is mostly occuring while applying a function argument. A unique Sharer object can appear several times in a formula (like a Keyword for example), but when the sharer is computed, the value inside is modified, so the computation is shared. See http://url.bobu.eu/bV7

#include <lazypool/pooled.h>

#include "formula_shell.h"

class Sharer : public FormulaShell, public lazypool::Pooled<4>
{
public:
  //----------------------------------------
  // Object management.
  
  Sharer(PFormula _value,bool _computed=false) : FormulaShell(_value),computed(_computed) {};

  //----------------------------------------
  // Computation
  
  PFormula compute_internal(CContext &C, Args &args);
  // There is no variable in a sharer, so the default function find_var and replace_var are good.

  //----------------------------------------
  // Misc
  const char * class_name() const {return "Sharer";};
  PFormula init_pre_sharers(list<bool> &l);
   
protected:
  bool computed; // Do we have already compted the value ?
};




