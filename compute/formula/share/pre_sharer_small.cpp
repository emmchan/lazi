#include <list>

#include "pre_sharer_small.h"

PreSharerSmall::PreSharerSmall(PFormula _value, const list<bool> &l) : PreSharer(_value), free_vars(0)
{
  size=l.size();
  // We create the list of flags in free_vars, we put the list in reverse order, thus it is faster to read
  unsigned short shift=0;
  for(list<bool>::const_iterator it=l.begin(); it != l.end(); it++,shift++)
    free_vars |= static_cast<BitField>(*it)<<shift;
}

PFormula PreSharerSmall::rename_var(WordLVar * from, WordLVar * to)
{
  return new PreSharerSmall(value->rename_var(from,to),free_vars,size);
}
