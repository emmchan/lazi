#include "sharer.h"



PFormula Sharer::compute_internal(CContext &C, Args &args)
{
  PFormula res; /* The formula used for the overall computation.*/
  
  if(computed)
    return value;
  else 
  {
    /* We start to compute_internal the internal value alone, so we can share the result, after we will do the overall computation.*/
    unsigned save_stack=args.set_visibles(0);
    res=value->compute_on_args(C,args);
    value=res->apply_args(args); /* Store the sub-computation.*/
    computed=true;
    args.restore_visibles(save_stack);
    return res;
  }
}

PFormula Sharer::init_pre_sharers(list<bool> &l)
{
  // Like PreSharerBig::init_pre_sharers, but when there is no free variable
  for(list<bool>::iterator it=l.begin(); it != l.end(); ++it)
    *it=false;
  value->init_pre_sharers(l); // PreSharers can be inside functions.
  return this;
}


