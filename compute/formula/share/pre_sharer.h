#pragma once
// PreSharer virtual Class. These classes are used to store the information of what free variables are in the sub-formula. This is used to replace the PreSharer by a Sharer as soon as there is no free variable, and also to duplicate only if a replacement occured. We identifie variables by their order in the replacement, so we just need to store booleans. See http://url.bobu.eu/bV7

#include "formula_shell.h"

class PreSharer : public FormulaShell
{
public:
  PreSharer(PFormula _value) : FormulaShell(_value) {};
  
  PFormula replace_var(Formula *from, Formula *to);
  virtual bool next_flag()=0; // Retrieve the next flag.
  virtual bool last_flag() const=0; // All flags are used ?
  virtual PreSharer *duplicate_and_pop(PFormula _value)=0; // Duplicate this and pop a flag
  const char * class_name() const {return "PreSharer";};
};


