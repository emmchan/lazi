#include "pre_sharer.h"
#include "sharer.h"

PFormula PreSharer::replace_var(Formula *from, Formula *to)
{
  if(last_flag()) // If there is no more replacement then we need to convert to a Sharer. The last flag is always true (false trailing flags are eliminated in init_pre_sharers.
    // No risk of infinite recursivity, because the sharing is between separated formulas. The Sharer has 2 purposes:
    // 1) Sharing the computation. This is the case when the last variable is not a free variable of the inside formula, because in this case the Sharer apear before the end of the last replacement, thus it is copied.
    // 2) Protecting from replacements which must be hidden : the inside formula can contain functions and PreSharers. PreSharers are based on replacement sequences (with their flags list). Last replacements are eliminated (see init_pre_sharers) if the corresponding variable is not there, but inside PreSharer need to not see these replacements.
    return new Sharer(value->replace_var(from,to),false); 
  return duplicate_and_pop(next_flag() ? value->replace_var(from,to) : value);
}
