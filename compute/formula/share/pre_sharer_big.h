#pragma once
// See PreSherer first. PreSharerBig is a PreSharer with unlimited number of variables, he is also used to prepare the PreSharer initialization.
#include <list>
#include <lazypool/pooled.h>

#include "pre_sharer.h"

class PreSharerBig : public PreSharer, public lazypool::Pooled<3>
{
public:
  PreSharerBig(PFormula _value,const FlaggedVarList &vl);
  PreSharerBig(PFormula _value,const list<bool> &l) : PreSharer(_value), free_vars(l) {};
  
  PFormula init_pre_sharers(list<bool> &l);
  bool next_flag() { return free_vars.front();};
  bool last_flag() const { return free_vars.size()==1;};
  PreSharer *duplicate_and_pop(PFormula _value)
  {
    PreSharerBig *ps=new PreSharerBig(_value,free_vars);
    ps->free_vars.pop_front();
    return ps;
  };
  const char * class_name() const {return "PreSharerBig";};
  string to_txt() const;
  PFormula rename_var(WordLVar * from, WordLVar * to);
protected:
  list<bool> free_vars;
};
