#pragma once
// See PreSherer first. PreSharerBig is a PreSharer with limited in list size, and only used after preparation.
#include <list>
#include <lazypool/pooled.h>

#include "pre_sharer.h"

class PreSharerSmall : public PreSharer, public lazypool::Pooled<5>
{
public:
  typedef unsigned long long int BitField;
  
  PreSharerSmall(PFormula _value, const list<bool> & l);
  PreSharerSmall(PFormula _value,BitField n, unsigned short s) : PreSharer(_value), free_vars(n), size(s) {};
  
  
  bool next_flag() { return free_vars & 1;};
  bool last_flag() const { assert(size!=0); return size==1;};
  PreSharer *duplicate_and_pop(PFormula _value)
  {
    assert(size>1);
    return new PreSharerSmall(_value,free_vars>>1,size-1);
  };
  const char * class_name() const {return "PreSharerSmall";};
  PFormula rename_var(WordLVar * from, WordLVar * to);
protected:
  BitField free_vars;
  unsigned short size; // The size of the list.
};
