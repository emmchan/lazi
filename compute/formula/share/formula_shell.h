#pragma once
// Root class for all sharers. A sharer contains transparently a formula inside.
#include "../formula.h"

class FormulaShell : public Formula
{
public:
  FormulaShell(PFormula _value) : value(_value) {};
  
  //----------------------------------------
  // Type cast
  // Sharer is transparent with our type-cast.
  
  Apply * get_Apply() {return value->get_Apply();};
  FormulaRepr * get_FormulaRepr() {return value->get_FormulaRepr();};
  WordKey0 * get_WordKey0() {return value->get_WordKey0();};
  Word * get_Word() {return value->get_Word();};
  WordRepr * get_WordRepr() {return value->get_WordRepr();};
  Function * get_Function() {return value->get_Function();};
  Constructor * get_Constructor() {return value->get_Constructor();};
  DictNode * get_DictNode() {return value->get_DictNode();};
  Obj * get_Obj() {return value->get_Obj();};
  Dict * get_Dict() {return value->get_Dict();};

  Formula * get_real_formula() {return value->get_real_formula();}
  
  //----------------------------------------
  // Convertion to/from texte.
  void to_xml(xml_node parent,int max_depth = -1) const {value->to_xml(parent,max_depth);};
  string to_txt() const;
  virtual const char * class_name() const {return "FormulaShell";}; // For debug
  //----------------------------------------
  // Syntax functions.

  bool is_notation() const { return value->is_notation();}
  bool synt_equal(Formula * g)  const {  return value->synt_equal(g);};
  PFormula simplify() {  return value->simplify();};
  bool find_var(PFormula v) { return value->find_var(v);};
  unsigned int get_arity() const {return  value->get_arity();};

  //----------------------------------------
  // Sharers
  
  void prepareBodies() {assert(false);}; // A sharer should not exists if this function is called.

protected:
  PFormula value; // The shared formula.
};
