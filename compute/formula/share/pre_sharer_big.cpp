#include <climits>   // CHAR_BIT
#include "pre_sharer_big.h"
#include "pre_sharer_small.h"


#include <iostream> // debug!
#include "../word/lvar.h" // debug!

PreSharerBig::PreSharerBig(PFormula _value,const FlaggedVarList &vl) : PreSharer(_value)
{
  for(fvlist::const_iterator it=vl.begin(); it != vl.end(); ++it)
    free_vars.push_back(it->second);
  // debug!
//   cerr << "PreSharerBig\n  " << value->to_txt() << "\n  ";
//   for(fvlist::const_iterator it=vl.begin(); it != vl.end(); ++it)
//     cerr << it->first->get_name() << ":" << (it->second ? "T" : "F") << " ,";
//   cerr << "\n\n";
}

string PreSharerBig::to_txt() const
{
// #ifdef NDEBUG
  return value->to_txt();
// #else
//   string bits;
//   for(list<bool>::const_iterator it=free_vars.begin(); it != free_vars.end(); ++it)
//     bits += *it ? "T" : "F";
//   return string("$") + class_name() + ":" + bits + "(" + value->to_txt() + ")";
// #endif
}

PFormula PreSharerBig::init_pre_sharers(list<bool> &l)
{
  // l, as a set (see FlaggedVarList), can't be empty (it means all flags are "false" equals all variables are unused), because in this case we create a Sharer and we would not be here.
  list<bool> free_vars_orig; 
  free_vars.swap(free_vars_orig); // Now free_vars is empty.
  list<bool>::const_iterator itf=free_vars_orig.begin();
  for(list<bool>::iterator it=l.begin(); it != l.end(); ++it,itf++)
  {
    if(*it) // If we see the variable replacement ...
    {
      free_vars.push_back(*itf); //  ... then we copy the presence flag from the old free_vars.
      // Modifie l to reflect new unused variables.
      if(!*itf)
        *it=false;
    }
    assert(!(!*it && *itf)); // If "*this" see a free variable indise, then it's alse seen above.
  }
  assert(itf==free_vars_orig.end());
  // free_vars_orig contains flag worthing "true" else to_pre_sharer() would have created a Sharer and not a PreSharer. Does free_vars contain flag worthing "true" ? Yes because a free variable in free_vars_orig is a free varible in l (see the assert in the loop above), and then also in free_vars.
  // The last flags worthing "false" are eliminated because the formula is without free variable after the last true flag.
  assert(!free_vars.empty());
  while(!free_vars.back())
  {
    free_vars.pop_back();
    assert(!free_vars.empty()); 
  }
  value=value->init_pre_sharers(l); // Recurse
  // If we can use a PreSharerSmall // debug! à installer après
  if(free_vars.size()<sizeof(PreSharerSmall::BitField)*CHAR_BIT)
    return new PreSharerSmall(value,free_vars);
  return this;
}

PFormula PreSharerBig::rename_var(WordLVar * from, WordLVar * to)
{
  return new PreSharerBig(value->rename_var(from,to),free_vars);
}


