#pragma once
///////////////////////////////////////////////////
// DictNode formula. A notation for a "toListBwAdd l_init (pair name value)".
// * l_init is the initial part of the list, l_init must be a DictNode or dvar_0l
// * name is the key, it must be a WordRepr, 
// * value is the value of the node.
#include <lazypool/pooled.h>


#include "formula.h"
#include "share/sharer.h"

class MatchNodeNota;
class DictNoNotaIterator;
class DictMod;
class ObjDynMod;
class Obj;
class Dict;
extern PFormula dvar_0l;


class DictNode : public Formula, public lazypool::Pooled<6>
{
public:
  //----------------------------------------
  // Object management.
  
  // We don't need to use smart pointers for WordRepr because they have a store.
  DictNode(PFormula _l_init, WordRepr *_name, PFormula _value);
  DictNode * get_DictNode() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_DictNode()!=nullptr; };
  PFormula to_sharer() {return new Sharer(this,true);};
  PFormula to_pre_sharer(FlaggedVarList &vl);
  
    
  //----------------------------------------
  // Convertion to/from texte.
  
  // There is no representation of a DictNode in xml, only entries in a <dict> or <obj>. This function translate a list of entries to a DictNode.
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
  // There is no representation of a DictNode in xml, so we simplify before convertion (see entries_to_xml below).
  void to_xml(xml_node parent,int max_depth = -1) const;
  // There is no representation of a DictNode in xml, only entries in a <dict> or <obj>. This function translate a DictNode to a list of entries. It is called by the "to_xml" of Obj or Dict.
  void entries_to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;

 
  //----------------------------------------
  // Computation
  
  PFormula compute_internal(CContext &C, Args &args);

  //----------------------------------------
  // Syntax functions.

  PFormula simplify();
  bool synt_equal(Formula * g) const;
  bool find_var(PFormula g);
  PFormula replace_var(Formula *from, Formula *to) { return new DictNode(l_init->replace_var(from,to),name,value->replace_var(from,to)); };
  PFormula rename_var(WordLVar *from, WordLVar *to) { return new DictNode(l_init->rename_var(from,to),name,value->rename_var(from,to)); };
  PFormula get_l_init() {return l_init;}
  WordRepr * get_key() {return name;}
  PFormula get_value() {return value;}
  // Is it the first element of the list (is l_init the empty-list).
  bool has_prev() const {return !l_init->synt_equal(dvar_0l);};
  
  //----------------------------------------
  // Sharers
  
  void prepareBodies() {value->prepareBodies(); l_init->prepareBodies();};
  void insert_pre_sharers(FlaggedVarList & vl) { insert_pre_sharers_on_two(vl,l_init,value); };
  PFormula init_pre_sharers(list<bool> &l);

protected:
  PFormula l_init; // The initial part of the list, can be a DictNode or dvar_0l
  WordRepr *name; // The key
  PFormula value; // The value for this key

friend MatchNodeNota;
friend DictNoNotaIterator;
friend DictMod;
friend ObjDynMod;
friend Obj;
friend Dict;
};

  

