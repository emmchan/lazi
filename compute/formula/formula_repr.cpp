#include "formula_repr.h"
#include "apply.h"
#include "function.h"
#include "word/word.h"
#include "word/lvar.h"
#include "word_repr.h"
#include "../global_context.h"



PFormula FormulaRepr::compute_internal(CContext &C, Args &args)
{
  if(args.empty())
    return compute_stop(C);
  return simplify();
}


FormulaRepr * FormulaRepr::get_comp(CContext &C, PFormula &f)
{
  FormulaRepr *p=f->get_FormulaRepr();
  // If it's not a the good type, compute it, change the argument with the result.
  if(p==nullptr)
  {
    f=f->compute(C);
    return f->get_FormulaRepr();
  }
  else
    return p;
}
//----------------------------------------
// Convertion to/from texte.

void FormulaRepr::to_xml(xml_node parent,int max_depth) const
{
  if(max_depth==0)
    to_xml_max_depth(parent);
  else
  {
    xml_node node=parent.append_child("formulaRepr");
    f->to_xml(node,max_depth-1);
  }
}

string FormulaRepr::to_txt() const
{
  return "$F[" + f->to_txt() + "]";
}

PFormula FormulaRepr::xml_to_formula(xml_node node, bool /*inRepr*/)
{
  return PFormula(new FormulaRepr(Formula::xml_to_formula(node.first_child(),true)));
}


//----------------------------------------
// Syntax functions.

bool FormulaRepr::synt_equal(Formula *g) const
{
  FormulaRepr *gfr=g->get_FormulaRepr();
  return gfr==nullptr ? false : f->synt_equal(gfr->f);
}


PFormula FormulaRepr::simplify()
{
  PFormula fs; // The same formula but eventually translated, so that we can translate the formula representation.
  // Compute fs
  for(fs=f; !(Word::is_me(fs) || Apply::is_me(fs) || Function::is_me(fs) || WordRepr::is_me(fs)); fs=fs->simplify());
  // Word case.
  Word *pw=fs->get_Word();
  if(pw!=nullptr) // A key0 or a gvar
    return PnewApply (PnewApply( dvar_wordToFormula, pw->getTypeRepr()),pw->getRepr());
  // Apply case.
  Apply *pa = fs->get_Apply();
  if(pa!=nullptr)
    return PnewApply ( 
              PnewApply ( dvar_formulaApply, PFormula (new FormulaRepr(pa->get_f())) ), 
              PFormula (new FormulaRepr(pa->get_a()))
            );
  // Function case
  Function *pf = fs->get_Function();
  if(pf!=nullptr)
    return PnewPair( functionRepr , PnewPair( GC.repr_store.get(pf->get_var()->get_name()), new FormulaRepr(pf->get_body())));
  // WordRepr case
  WordRepr *pwr = fs->get_WordRepr();
  assert(pwr!=nullptr);
    return PnewPair( wordReprRepr, pwr);
}



