#pragma once
//  WordRepr : a word representation
#include "formula.h"

class WordKey0;

class WordRepr : public Formula
{
public:
  WordRepr(string _name) : name(_name) {};
  
  WordRepr * get_WordRepr() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_WordRepr()!=nullptr; };
  
  const string & get_name() const { return name; };
  

  //----------------------------------------
  // Computation
  PFormula compute_internal(CContext &, Args &);
  // If f is of this class, then return the pointer to the object. Else f is computed (in the hope of getting the good type). The result of the computation is set in f (it's a reference). If failure return nullptr, else return the object address.
  static WordRepr * get_comp(CContext &C, PFormula &f);
 
  //----------------------------------------
  // Convertion to/from texte.

  void to_xml(xml_node parent,int max_depth = -1) const;
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
  string to_txt() const;
    
  //----------------------------------------
  // Syntax functions.

  PFormula simplify();
  // Return a WordRepr with the same name as the name in the WordKey0 w. (Used in the simplification of a FormulaRepr).
  static PFormula keyWordToWordRepr(WordKey0 *w);
  
protected:
  string name;
};
