#pragma once
// A NullFormula has the same functionality as a null pointer. Used in PFormula.

#include "formula.h"

using namespace std;

class NullFormula : public Formula
{
public:
  NullFormula() {ref_count=1;}; // Avoid destruction, so the object must be explicitaly destroyed.
  virtual ~NullFormula() {};
  // Print the formula as xml, stop if max_depth = 0, <0 = infinite
  void to_xml(xml_node parent,int /*max_depth*/ = -1) const
    { to_xml_max_depth(parent); };
  string to_txt() const
    {return string(":key:BUG_IN_LAZI_COMPUTE_NullFormula_IS_A_GHOST");};
  // We can't simplify, should never be called.
  PFormula simplify() {assert(false); return this;};
};


