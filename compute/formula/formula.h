#pragma once
// Root class for all formula. 

#include <string>
#include <assert.h>  
#include <pugixml.hpp>
#include <vector>
#include <iostream> // debug!

#include "../p_formula.h"
#include "../c_context.h"
#include "../args.h"
#include "../flagged_var_list.h"


using namespace std;
using namespace pugi;

class Apply;
class FormulaRepr;
class Word;
class WordKey0;
class WordRepr;
class Function;
class Constructor;
class DictNode;
class Obj;
class Dict;

//EXception when we have a variables collision (when renaming a variable).
class Collision {};

class Formula
{
public:
  //----------------------------------------
  // Object management.
  
  Formula() : ref_count(0) {};
  virtual ~Formula() {};
  
  // For the smart-pointer PFormula.
  inline void new_parent() {ref_count++;};
  // For the smart-pointer PFormula.
  inline static void lost_parent(Formula *p);

  //----------------------------------------
  // Conversion
  
  // These functions are used to convert a Formula to a particular Formula class. We can't just use of dynamic_cast because Sharer must be transparent. They must all be also defined in formula_shell.h
  virtual Apply * get_Apply() {return nullptr;};
  virtual FormulaRepr * get_FormulaRepr() {return nullptr;};
  virtual Word * get_Word() {return nullptr;};
  virtual WordKey0 * get_WordKey0() {return nullptr;};
  virtual WordRepr * get_WordRepr() {return nullptr;};
  virtual Function * get_Function() {return nullptr;};
  virtual Constructor * get_Constructor() {return nullptr;};
  virtual DictNode * get_DictNode() {return nullptr;};
  virtual Obj * get_Obj() {return nullptr;};
  virtual Dict * get_Dict() {return nullptr;};
  
  // Sometimes we need to access the object without a possible Sharer (see default synt_equal).
  virtual Formula * get_real_formula() {return this;};
  
  // Return possibly a Sharer to wrap "this" in a Sharer object. dont_share_compute = false if we want to share the computation.
  // It's useless to share something that can't be computed, and just apply and dvar can be computed, and dvar has his own cache, so only apply is converted to a sharer.
  virtual PFormula to_sharer() {return this;};
  // Return possibly a PreSharer or a Sharer (if no free variable) to wrap "this".
  virtual PFormula to_pre_sharer(FlaggedVarList &/*vl*/) {return this;};
  
  //----------------------------------------
  // Convertion to/from texte.
  
  // Generate the <formula format="xml">...</formula> xml.
  void overall_to_xml(xml_node parent,int max_depth = -1) const;
  // Write the formula as xml, stop if max_depth = 0, <0 = infinite
  virtual void to_xml(xml_node parent,int max_depth = -1) const=0;
  // Generate the xml when max depth is reached.
  void to_xml_max_depth(xml_node parent) const;
  // Print the formula as texte (to debug)
  virtual string to_txt() const=0;
  // Return a formula from a xml node.
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
  // Get the global node "<formula format="xml">...</formula>" and return the Formula
  static PFormula xml_global_to_formula(xml_node node);

  //----------------------------------------
  // Computation
  
  // Compute without args.
  virtual PFormula compute(CContext &C);
  // Compute the formula applied to a list (args) of arguments and return the result. The list of arguments is a reference and is modified to return the  new arguments of the returned formula. For example if the formula is "if" and the arguments are "1b,distribute,equal,0b", then the result is the keyword "distribute" and args is the singleton list "0b". C is the computation context. This function must set C.to_continue to false if the computation can't continue.
  virtual PFormula compute_internal(CContext &C, Args &) {return compute_stop(C);};
  // Result when we can't compute.
  PFormula compute_stop(CContext &C) {C.to_continue=false;return this;};
  // This function loop on compute_internal until the computation is done. It also manage the computation when we have "shortcut=0".
  PFormula compute_on_args(CContext &C, Args &args);
  // When we start a new computation with arguments, we have to use new_compute_on_args (and not compute_on_args) because  new_compute_on_args do callstack trace job.
  PFormula new_compute_on_args(CContext &C, Args &args);
  // Reconstruct the formula (using Apply) from the object and the arguments.
  PFormula apply_args(Args &args);
  // Compute each arguments of args, the result are in args.
  static void compute_args(CContext &C,Args &args);
  // Compute the formula, and it's arguments if the result is a constructor (Constructor, or Obj or Dir).
  PFormula compute_rec_args(CContext &C);

    
  //----------------------------------------
  // Syntax functions.

  // Is the formula a notation ? yes if it can be simplified.
  virtual bool is_notation() const { return true;}
  // Syntaxic equality: the two formulas must be equals syntaxicaly, in the lazi-extension syntax (not as C++ object).
  virtual bool synt_equal(Formula * g) const { return this == g->get_real_formula();};
  bool synt_equal(PFormula g) const { return synt_equal(g.get_p());};
  // Return true if the variable "g" is a free in the formula.
  virtual bool find_var(PFormula ) {return false;}
  // Replace "from" by "to" anywere in the formula and return the result. "from" is a variable.
  virtual PFormula replace_var(Formula */*from*/, Formula */*to*/) {return this;};
  // Rename a variable. This function is used by objMerge shortcut on a body with just one free variable, the one to replace. The function can fail because a collision is possible (if from is in  a body of a function of variable "to"), in this case the exception int COLLISION is throw.
  virtual PFormula rename_var(WordLVar * /*from*/, WordLVar * /*to*/) { return this; };
  // Simplify a notation (translate the minimum we can to a formula closer to a basic Lazi formula), never called for Keyword or Apply. The result is a Lazi-1 formula (apply,key0,gvar).
  virtual PFormula simplify() {assert(false); return this;}; // Default for class which are not notation.
  // Repeat "simplify" until the formula type is a Lazi-1 formula. Except for faster algorithme this function should not need to be overloaded.
  PFormula  simplify_notations();
  // Do simplify_to_ApplyKeyword and if the result is an apply, then put the argument in args and continue on the function until it's a keyword.
  PFormula simplify_to_FuncKeyword(Args &args);
  // Return the arity of the formula. The arity computation is a guess, just use as hint for profile informations.
  virtual unsigned int get_arity() const {return 0;};

  //----------------------------------------
  // Sharers
  
  // Find first functions from the root formula node, and convert their bodies to use (Pre)Sharers.
  virtual void prepareBodies() {};
  // Add PreSharer where needed in the body of a function. vl is the list of pair(variable, used-flag) (see flagged_var_list.h"). vl has one variable per function where "*this" is (a name can appear several times). When the function is called all flags worth false, on return the flags must be set to true if the variable is used.
  virtual void insert_pre_sharers(FlaggedVarList &/*vl*/) {};
  // Specialized insert_pre_sharers when the formula has 2 independant sub-formulas (like Apply or DictNode). The 2 arguments are references to the 2 sub-formulas.
  static void insert_pre_sharers_on_two(FlaggedVarList &vl,PFormula & f,PFormula & a);
  // Specialized insert_pre_sharers when the formula has 2 independant sub-formulas (like Apply or DictNode) when we have already do the insert_pre_sharers on the 2 sub-formulas, we give the 2 variables list of the 2 sub-formulas.
  static void insert_pre_sharers_on_two_with_vlfa(FlaggedVarList &vl,PFormula & f,PFormula & a,FlaggedVarList &vlf,FlaggedVarList &vla);
  // Finish the initialization of PreSharer objects in the body of a function. We need this function because a PreSharer can be inside an other PreSharer, thus the PreSharer outside can hide variables to the inside one. So we need to eliminate, in the list of flags of the PreSharer inside, the flags corresponding to hidden variables. l is the list of flags of variables of parent functions, the flag is true if the variable is used (then the PreSharer inside see it).
  virtual PFormula init_pre_sharers(list<bool> &/*l*/) {return this;};
  
  //----------------------------------------
  // Data
public:
  static const unsigned int max_shortcuts_level=2;
protected:
  unsigned int ref_count; // For smart pointer.
};

void Formula::lost_parent(Formula *p)
{
  assert(p!=0);
  assert(p->ref_count>0);
  p->ref_count--;
  if(p->ref_count==0)
    delete p;
}

// A trick to have inlines for very often used short functions in PFormula.
#include "../p_formula.inl"

