#pragma once
///////////////////////////////////////////////////
// Object formula. A notation for an object formula: a function where the body is a DictNode.

#include <lazypool/pooled.h>
#include <assert.h>


#include "formula.h"
#include "function.h"
#include "share/sharer.h"
#include "dict.h"

class MatchObj;
class MatchApply;

class Obj : public Formula, public lazypool::Pooled<7>
{
public:
  //----------------------------------------
  // Object management.
  
  // We don't need to use smart pointers for WordRepr because they have a store.
  Obj(PFormula _f) : f(_f) {assert(Function::is_me(f) && Dict::is_me(f->get_Function()->get_body()) );};

  Obj * get_Obj() {return this;};
  // f is a formula of my class ?
  static bool is_me(PFormula f) { return f->get_Obj()!=nullptr; };
  // We can't compute a function, thus no need to share computation. But we can use a sharer for a function to store the information that there is no free variable.
  PFormula to_sharer() {return new Sharer(this,true);};
  PFormula to_pre_sharer(FlaggedVarList &vl);
  
   
  //----------------------------------------
  // Convertion to/from texte.
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);
  void to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;

 
  //----------------------------------------
  // Computation
  
  PFormula compute_internal(CContext &C, Args &args);
 
  //----------------------------------------
  // Syntax functions.

  PFormula simplify() {  return f; }; // Obj f = f

  bool synt_equal(Formula * g) const;
  bool find_var(PFormula g);
  PFormula replace_var(Formula *from, Formula *to) { return new Obj(f->replace_var(from,to)); };
  PFormula rename_var(WordLVar *from, WordLVar *to) { return new Obj(f->rename_var(from,to)); };
  PFormula get_f() {return f;}
  
  //----------------------------------------
  // Sharers
  
  void prepareBodies() {f->prepareBodies();};
  // Obj shortcuts work with formulas inside the function. We need to deal with PreSharer and Sharer objects. 
  // If we do nothing special we would have a bug: if we do an "objApply" we get the value and then replace the variable of the function of the object (generally "this"), but the value could be inside a Sharer which could be on the structure (for example on a l_init). In this case this replacement which should not occur could hit the PreSharers inside sub-functions of the value and consume a flag by error.
  // To avoid this, we first need to understand that, WHEN we execute the "objApply" shortcut, structural PreSharer can only contain one variable which is the variable of the function of the object. Then to avoid structural PreSharer while we iterate on the dictionnary list, we artificialy set the "used" flag of the variable of the function  for the list_init formulas. By this modification we ensure that no Sharer or PreSharer stay in the structure (on list_init formulas) at the shortcut time (when all other variables are replaced).
  void insert_pre_sharers(FlaggedVarList & vl);
  // Do the Obj::insert_pre_sharers job on a node. var is the variable of the function of the Obj.
  static void insert_pre_sharers_on_obj_node(PFormula l_init,FlaggedVarList & vl,WordLVar *var);
  PFormula init_pre_sharers(list<bool> &l) {
    f=f->init_pre_sharers(l);
    assert(f->get_Function()->get_body()->get_Dict()!=nullptr);
    return this;
  };

protected:
  PFormula f; // The function which contains the dictionnary (a Dict).

friend MatchApply;
friend MatchObj;
};
