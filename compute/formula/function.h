#pragma once
// Class for the function notation.
#include <lazypool/pooled.h>

#include "share/sharer.h"

class MatchObjNota;
class MatchObjNoNota;

class Function : public Formula, public lazypool::Pooled<2>
{
public:
  //----------------------------------------
  // Object management.
  
  Function(WordLVar * _var, PFormula _body) : var(_var), body(_body) {};

  Function * get_Function() {return this;};
  static bool is_me(PFormula f) { return f->get_Function()!=nullptr; };
  WordLVar *get_var() const {return var;};
  PFormula get_body() const {return body;}
  // We can't compute a function, thus no need to share computation. But we can use a sharer for a function to store the information that there is no free variable.
  PFormula to_sharer() {return new Sharer(this,true);};  // A function alone can't be computed. We wrap by a sharer just to give the information that there is no free variable.
  PFormula to_pre_sharer(FlaggedVarList &vl);

  //----------------------------------------
  // Convertion to/from texte.
  
  void to_xml(xml_node parent,int max_depth = -1) const;
  string to_txt() const;
  static PFormula xml_to_formula(xml_node node, bool inRepr=false);

  //----------------------------------------
  // Computation

  PFormula compute_internal(CContext &C, Args &args);
    
  //----------------------------------------
  // Syntax functions.
  
  bool synt_equal(Formula * g) const;
  PFormula simplify();
  bool find_var(PFormula g);
  PFormula replace_var(Formula *from, Formula *to);
  PFormula rename_var(WordLVar * from, WordLVar * to);
  unsigned int get_arity() const;
  
  //----------------------------------------
  // Sharers
  
  void prepareBodies();
  void insert_pre_sharers(FlaggedVarList & vl);
  PFormula init_pre_sharers(list<bool> & l);
  
protected:
  // The variable of the function, it must be a Var.
  WordLVar *var; 
  // The body of the function.
  PFormula  body; 
  
friend MatchObjNota;
friend MatchObjNoNota;
};
