\documentclass[a4paper,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{csquotes}
\usepackage{array}
\usepackage{makeidx}
\usepackage{hyperref}
\usepackage{geometry}
\usepackage{pdfpages}
\usepackage{graphicx}


% For definitions of terms
\makeindex

\MakeOuterQuote{"}
\parskip=1mm
\partopsep=1mm
\marginparwidth 1 cm
\geometry{vmargin=45pt}
\newcommand{\code}[1]{\texttt{#1}}

\begin{document}
\title{Lazi : introduction pratique au langage informatique}
\author{Emmanuel Chantréau}
\maketitle
\tableofcontents


\section{Introduction}

Le but de ce texte est de présenter le langage informatique Lazi de la manière la plus pratique possible, de sorte que le lecteur puisse avoir une idée du "look and feel" de la programmation en Lazi. La contrepartie de ce type de présentation est le manque d'éclairage sur les principes de base, qui ont en Lazi un rôle primordial. C'est pourquoi nous ne présentons ici que quelques éléments du langage, par exemple nous omettons les types.

\section{Ressemblance avec Haskell}

Lazi et Haskell on des points communs importants, et un "look and feel" de base proches. La syntaxe Lazi reprend le côté léger de celle d'Haskell mais d'une autre manière, en particulier le positionnement (nombre d'espaces au début des lignes) n'a pas de conséquence sur l'analyse syntaxique.
Les personnes venant de langages impératifs pourront utiliser les ressources d'Haskell pour se familiariser avec la programmation fonctionnelle pure.

\section{Exemple de la définition d'une fonction}

Pour les premiers éléments de langage nous allons utiliser cet exemple de définition d'une fonction:

{
	\setlength{\fboxsep}{0pt}
	\setlength{\fboxrule}{1pt}
	\fbox{\includegraphics[height=4cm]{exemple-intro-inList.pdf}}
}

\section{Les commentaires}

C'est la même syntaxe qu'en C++: "\code{//}" pour un commentaire d'une ligne et "\code{/* ... */}" pour les commentaires multi-lignes.

Comme le montre l'exemple, nous utiliser un / de plus, pour déclencher une coloration syntaxique spéciale (configuration pour kate).

\section{\color{blue}{\code{\$Def}}}

\code{\$Def} permet de définir un nom (ici \code{inList}). En C++ on distingue la définition des noms suivant qu'il s'agit d'une fonction, d'une donnée ou d'un type. En haskell, les données et les fonctions sont unifiées et on ne distingue que la définition des types et des données/fonctions. En Lazi tout est unifié et il n'y a qu'une sorte de définition. 

On peut remarquer que la définition de inList n'est pas explicitement délimitée (par exemple par des accolades comme en C++). En Lazi, la définition continue jusqu'à la prochaine notation globale (comme "\code{\$Def}" ou "\code{\$Nota}"). Ce choix a pour but d'alléger le code source.

\section{\color{blue}{\code{\$F}}}

"\code{\$F}" permet d'exprimer une fonction, suit la liste des variables, "\code{->}" puis le corps de la fonction.

\section{Fonctions et arguments}

L'application des arguments en Lazi fonctionne comme en Haskell (du point de vue théorique, c'est un langage fonctionnel pure basé sur le lambda calcul réduit aux applications).

En Lazi les données et les fonctions sont unifiées. Si vous avez deux choses x et y, vous pouvez appliquer y à x pour former la chose "x y" (x a le rôle de la fonction et y de l'argument). 

Par exemple si vous avez une fonction "sum" et deux entiers x et y que vous voulez additionner grâce à sum :\\
Vous appliquez x à sum, cela constitue la chose "sum x". On peut voir "sum x" comme une fonction qui prend un argument n et retourne x+n. Vous pouvez maintenant appliquer y à "sum x", le résultat sera "(sum x) y" que l'on note aussi "sum x y".
 
Cette forme d'application des arguments aux fonctions devient vite naturelle et pratique. Par exemple si vous avez une fonction d'égalité "eq", et une fonction prenant en argument une propriété, si vous voulez passer en argument la propriété "être égal à x", il vous suffit de passer "eq x".

\section{Récursivité}

Pour utiliser la récursivité il vous suffit d'utiliser le nom défini dans sa définition. Par exemple pour définir une fonction retournant la liste infinie remplie par x :

\code{\$Def infiniteList = \$F x -> infiniteList x :l x}

\code{:l} ajoute l'élément x à la fin de la liste "\code{infiniteList x}".

Remarquons que ce code ne fonctionnerait pas en langage impératif car la fonction bouclerait. Mais Lazi est un langage à évaluation paresseuse (comme Haskell), c'est à dire qu'il ne calcul que ce qu'il a besoin. 

Par exemple pour prendre le dernier élément de la liste \code{infiniteList a}, il calcul jusqu'à obtenir \code{infiniteList a :l a}, hors prendre le dernier élément de \code{infiniteList a :l a} ne nécéssite pas de calculer toute la liste (de même que si je vous demande "Quel est le dernier élément d'une liste se terminant par 0 ?" vous n'avez pas besoin de me demander de quoi est constituée toute la liste pour répondre).

N'oublions pas que les données et les fonctions sont unifiées, par exemple pour définir la liste infinie remplie de zéro :
\code{\$Def infiniteListZero = infiniteListZero :l 0}

Et nous pouvons tout aussi bien définir des types récursifs ou encore paramétrés, mais nous n'irons pas jusque là dans cette documentation.

\section{Simplifier le parenthèsage}

Les langages fonctionnels sont réputés nécessiter trop de parenthèses, par exemple pour appliquer  x y puis z t à f il faut écrire \code{f (x y) (z t) }, rappelons que c'est la même chose que \mbox{ \code{(f (x y)) (z t) }}. En C++ on écrirait \code{f( x(y) , z(t) )}.

Nous avons deux syntaxes en Lazi aidant à l'utilisation minimale des parenthèses. Je vous conseille de prendre l'habitude d'utiliser ces deux aides syntaxiques car elles permettent un code plus agréable à lire.

\subsection{Les parenthèses à virgule}

En Lazi, une notation transforme une expression \\
 \code{( x1 , x2 , ... , xn )} en \code{( x1 ) ( x2 ) ... ( xn )}\\
où  x1 ... xn sont des expressions quelconques.\\
 Donc nous pouvons écrire notre expression précédente "\code{f(x y, z t)}" mais aussi "\code{(f ,x y, z t)}" voir même "\code{(f, x y) (z t)}"


\subsection{L'application à priorité minimale}

La construction "f x" est l'application de x sur f. La priorité de l'application dépasse tout opérateur, par exemple \code{infiniteList x :l x} se lit \code{(infiniteList x) :l x} car l'application entre infiniteList et x a une priorité (ou précédence) plus grande que l'opérateur \code{:l}.

Il existe en Lazi un opérateur noté "\code{.}" qui représente l'application  (donc \code{(x y) = (x . y)}) 
, qui est associatif à droite et avec une priorité strictement minimale (rien n'a une priorité égale ou inférieure).

Donc, par exemple, on peut remplacer \code{f ( a b ( c d ( e f ) ) )} par \mbox{\code{f . a b . c d . e f}}

Vous pouvez utiliser la règle suivante pour utiliser cet opérateur : tout ce qui est à droite d'un point est une expression (même si elle utilise aussi cet opérateur). Donc par exemple  \mbox{\code{f . a b . c d . e f}} se traduit en \mbox{\code{f ( a b . c d . e f)}} . Cette notation est donc très pratique quand on applique une fonction à une grosse expression, car allors on peut remplacer de grande parenthèses par un point qui signifie "application avec tout ce qui est à droite".

\section{Correspondance par motif}

Imaginons que nous voulions définir une fonction qui inverse les deux éléments d'une paire. Sachant que la notation \code{\$P[x,y]} permet de construire une paire nous pourrions écrire :

\code{\$Def reversePair = \$F p -> \$P[ pairSecond p , pairFirst p ]} 

Mais nous pouvons simplifier cette définition par : 

\code{\$Def reversePair = \$F \$P[x,y] -> \$P[y,x]} 

La correspondance par motif n'a pratiquement pas de limite, on peut l'imbriquer ou encore nommer (en utilisant \code{@)}) l'argument entier. Par exemple : 


\code{\$Def flat = \$F \$P[x,p@\$P[y,z]] -> \$L[x,p,y,z]} 


\section{Définition d'un opérateur}

Dans l'exemple ci-dessous nous définissons une fonction de composition de deux fonctions à un argument (la fonction mathématique "$\circ$") et un opérateur associé à cette fonction.

{
	\setlength{\fboxsep}{0pt}
	\setlength{\fboxrule}{1pt}
	\fbox{\includegraphics[height=4cm]{exemple-intro-nota.pdf}}
}

Remarque : peut être que la définition strictement équivalente suivante vous est plus parlante :
\code{\$Def functionCompose = \$F f,g -> \$F x -> f . g x}

.o devient donc un opérateur infix, associatif à gauche de priorité 10 et tel que \mbox{\code{x .o y = functionCompose x y}}

\section{Le style de syntaxe Lazi}

\subsection{Place à la nouveauté}

Lazi a un style de syntaxe qui découle de sa nature spéciale. L'idée est de laisser toujours de l'espace pour de nouvelles notations. Un minimum de symboles sont réservés, par exemple Haskell définit une liste par \code{[ x , y ... ]} alors qu'en Lazi c'est \code{\$L[ x, y ... ]}. Quand Lazi sera mature, même des notations complexes comme "\code{\$F ->}" seront définissables en Lazi, il est donc important de ne pas s'accaparer les symboles pour laisser de la place à la nouveauté. C'est pourquoi en Lazi "x.y" est syntaxiquement invalide : il faut laisser un espace entre les opérateurs et les noms, de la sorte on peut définir l'opérateur ".o" sans risquer de confusion avec l'opérateur ".".

Les symboles qui peuvent être collés aux expressions sont donc rares: la virgule et les symboles de délimitations pour la partie intérieure ( parnthèses, crochets etc).

Certains symboles ont un rôle fixés:
\begin{itemize}
	\item les parenthèses : servent toujours à délimiter une expression
	\item la virgule : sert de séparateur
	\item \$Texte[ ... ] : Délimite une syntaxe spéciale (par exemple \code{\$L[ x,y,z]} est la notation pour la liste d'éléments x, y et z).
\end{itemize}



\subsection{Place à la clarté}

Le programmeur n'a pas besoin de se soucier des optimisations basiques, car elles sont faites par les programmes de traduction et d'interprétation. Il peut donc se concentrer sur la clarté de son code.

Exemples de formules ayant un temps de calcul strictement identiques :\newline
\begin{tabular}{|c|c|l|}
	\hline \code{(\$F x,y -> f a b x y)} & \code{f a b} &  Car \code{\$F x -> g x} est égal à \code{g}\\ 
	\hline  \code{\$Let x = y z, f x} & f . y z  & Car x n’apparaît qu'une fois \\ 
	\hline  \code{f (g . x y, h . x y)} &  \code{\$Let a = x y, f (g a, h a)} & "\code{x y}" est  factorisé\\ 
	\hline 
\end{tabular} 

Remarque : la factorisation d'expressions identiques se fait complètement intra-formule (mêmes avec les variables), mais pas entre formules différentes (donc pas entre les valeurs des noms définis).

\section{Un exemple plus gros}
{
	\setlength{\fboxsep}{0pt}
	\setlength{\fboxrule}{1pt}
	\fbox{\includegraphics[height=5cm]{exemple-intro-fold.pdf}}
}

Sans chercher à comprendre le but de cette fonction, on peut noter :
\begin{itemize}
	\item L'utilisation d'une lambda fonction récursive (\code{recurse \$F lf, ...}), \code{lf} est la variable représentant la lambda fonction récursive.
	\item L'utilisation du classique "if then else". Lazi ne fournit pas de notation spéciale car je trouve la syntaxe générale suffisante pour rendre le code source clair.
\end{itemize}
 
 \newpage
 

\end{document}

