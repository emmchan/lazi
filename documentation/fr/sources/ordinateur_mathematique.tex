\documentclass[a4paper,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{csquotes}
\usepackage{array}
\usepackage{makeidx}
\usepackage{hyperref}
\usepackage{geometry}
\usepackage{pdfpages}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\usepackage{framed}

% code listing configuration
\definecolor{mygreen}{rgb}{0,0.4,0}
\lstset{
	language=Haskell,
	commentstyle=\color{mygreen}
}

% For definitions of terms
\makeindex

\MakeOuterQuote{"}
\parskip=1mm
\partopsep=1mm
\marginparwidth 1 cm
\geometry{vmargin=45pt}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\translateTo}{{\color{cyan}$\longmapsto$} }

\begin{document}
\title{Lazi / informatique : L'ordinateur mathématique}
\author{Emmanuel Chantréau}
\maketitle
\tableofcontents


\section{Introduction}

Nous allons imaginer en quoi les langages informatiques pourraient être améliorés et voir en quoi un ordinateur complètement basé sur les mathématiques Lazi  (langage machine, mémoire etc) correspond aux qualités trouvées.
Cet ordinateur n'a pas besoin d'être construit pour être utile : il permet de donner un cadre logique et des outils à l'informatique actuelle.

\subsection{De l'informatique aux mathématique}

Les ordinateurs ont été construits de sorte qu'il soit possible de tout faire facilement, on peut par exemple écrire des données puis faire une "CALL" pour les exécuter en tant que code exécutable.

L'évolution des langages nous montre que nous avons besoin de restreindre les possibilités à des règles vertueuses (comme la vérification des types).

Nous pouvons remarquer que plus les langages évoluent et plus ils se rapprochent des mathématiques, par exemple la vérification des types est très proche de la vérification de propriétés mathématiques, ou encore Haskell est basé sur le lambda calcul. 

L'informatique est la science du traitement des informations formelles et les mathématiques la science des vérités formelles. La notion de vérité étant le paradigme premier et universel du monde des idées, plus on voit l'informatique sous ce paradigme et plus on peut lui faire bénéficier de la puissance des mathématiques. 

Nous allons définir ici un principe de fonctionnement d'un ordinateur mathématique et voir en quoi il peut être utilisé en pratique.

\subsection{Des mathématiques à l'informatique}

\subsubsection{La mathématique}

Les mathématiques ou la mathématique ? Il n'est pas commun que nous ayons une multitude de représentations pour un même domaine. Par exemple il paraîtrait fou de se passer du concept d'organe en biologie des mammifères. Hors en mathématiques il n'existe pas de représentation canonique \footnote{au sens mathématique du terme} de la science la plus formelle qui soit. Ce fait peut être expliqué par deux hypothèses opposées :
\begin{itemize}
	\item La nature des mathématiques place cette science hors du champs des représentations canoniques (c'est l'idée la plus courante).
	\item La nature des mathématiques n'est pas complètement connue. En conséquence il nous manque une vision plus adéquate pour arriver à une représentation canonique (à isomorphisme près).
\end{itemize}

L'histoire des sciences nous montre que les vérités sur les mécanismes de notre vie convergent, par exemple les vérité sur la forme de la terre ou encore sur la nature de la lumière. La question "Qu'est-ce que la vérité ?" nous a poussé par la pratique vers les systèmes logiques actuels. Même si cette question n'est souvent pas formulée explicitement tellement elle est naturelle par les multiples nécessitées auxquelles elle se rattache (besoin de calculer par exemple), elle se trouve en filigrane de beaucoup de pratiques de l'esprit.  Nous sommes donc naturellement poussés par notre environnement (au sens large) vers une/des certaine(s) conception(s) de la vérité. Ainsi une logique pouvant démontrer "faux" sera immédiatement rejetée. 
Nous voyons donc que la question "Qu'est-ce que la vérité ?" fait parti de notre environnement naturel, comme l'est la question sur la forme de la terre. 

On pourrait aussi penser que  l'existence de propositions indécidables induit l'absence de système canonique.
Mais cela montre plutôt que notre vision innocente d'un système canonique était absurde.

Enfin, un dernier point de blocage est le très fortement encré paradigme d'univers fermé, qui, comme celui de terre au centre de l'univers, nous offre un confort mental ne reposant sur aucune démonstration. En vertu de ce paradigme nous distinguons des frontières parfaitement hermétiques entre les concepts physiques et ceux mathématiques. Dans ce cadre de pensée il parait normal et souhaitable que la question "Qu'est-ce que l'électron ?" converge mais pas la question "Qu'est-ce que la vérité ?". À contrario, une vision non fermée\footnote{Plus précisément "fermée dans une certaine fondation mathématique", ce qui est déjà bien plus large que la fermeture à un univers physique.} impose que toute forme d’existence soit basée sur la notion mathématique d'existence, par exemple l'existence physique d'une chose x de notre univers est alors une existence mathématique vérifiant des  propriétés supplémentaire sur x (propriété en relation avec l'objet mathématique qu'est notre univers).

Il me parait donc plus raisonnable de chercher une réponse unique à la question "Qu'est-ce que la vérité ?" plutôt qu'une justification de la multiplicité des réponses actuelles. 

\subsubsection{Qu'est-ce que la mathématique ?}

Il nous faut donc préciser la réponse courante à la question "Qu'est-ce que la vérité ?". Pour cela nous allons prolonger nos habitudes de représentation du monde physique au monde mathématique :
\begin{itemize}
	\item Les entités du monde physique sont composées de \textbf{constituants plus simples}; par exemple les atomes sont constitués de particules plus petites.
	\item Nous allons retenir aussi la propriété de \textbf{calculabilité} du monde physique (nous n'utiliserons donc pas la notion d'infini).
\end{itemize}

D'autre part nous savons que la mathématique n'est pas figée, il peut être nécessaire d'utiliser de nouvelles notations ou même des axiomes ou règles supplémentaires ou différentes (voir \href{https://fr.wikipedia.org/wiki/Analyse_non_standard}{l'analyse non standard)}. C'est pourquoi la fondation que nous définirons devra être extensible, c'est à dire qu'elle doit pouvoir intégrer des vérités provenant d'un autre système de déduction.

\subsubsection{Les constituants de base}

Il nous faut donc trouver les notions minimales nécessaire à la construction d'une fondation mathématique:

\paragraph{Des entités.} Les idées sont représentées par des entités (mots, symboles etc).

\paragraph{Des testes d'égalité.} Nous avons besoin de vérifier si deux entités sont égale ou non.

\paragraph{Des structures.} Les preuves et les représentations des idées sont formées d'entités assemblées par des structures.

\paragraph{Des fonctions.} Nous avons besoin de représenter des méthodes générales s'appliquant à diverses choses, et pour cela nous avons besoin de la notion de fonction.

\paragraph{La récursivité.} Les structures peuvent être imbriqués un nombre quelconque (mais fini) de fois (par exemple une liste de vérités), il nous faut donc une forme de récursivité pour définir des fonctions manipulant des structures.

La contrainte que nous allons ajouter ici est que les fondations permettent de définir un ordinateur mathématique, c'est à dire un ordinateur fonctionnant par les principes suivants :
\begin{itemize}
	\item La mémoire de données est constituée d'une liste finie de vérités.
	\item La mémoire ne peut être lue que par l'opération de tester si une donnée est une vérité.
	\item Le programme est constitué d'une liste finie d'instructions (aussi appelées "déductions")
	\item 
\end{itemize}

Nous entendons par "simplement" les contraintes : 
\begin{itemize}
	\item de n'utiliser que des listes finies
\end{itemize}

\subsubsection{L'extensibilité}

Nous allons voir ici que la contrainte de l'extensibilité oriente structure fortement la fondation mathématique. Nous devons définir une fondation Lazi-0 telle que si F est une fondation et si p est une preuve pour F vérifiant certaines propriétés (toutes les preuves ne correspondent pas forcément à des vérités dans la fondation de base) alors une traduction de la conclusion de p soit déduite dans Lazi. En effet, ainsi on peut définir toute sorte d'extension à Lazi et les utiliser.

Cette propriété d'extensibilité implique une fonc

\section{L'ordinateur mathématique}

\subsection{données = vérités}

Prenons une donnée dans un ordinateur (par exemple la donnée d'un texte édité par un utilisateur) et changeons là aléatoirement. On crée alors un dysfonctionnement car la vérité "le texte que l'utilisateur veut travailler" n'est plus respectée. Pour toutes les données d'un ordinateur il existe une vérité (plus ou moins implicite mais toujours là) correspondante.

Si on veut une informatique de haut niveau, il faut que l'ordinateur ait accès au maximum à la structure des données, et cela correspond donc à avoir les vérités correspondantes à ces données. Nous allons donc utiliser le principe suivant: ne mémoriser que des vérités.

Cet ordinateur aura comme mémoire de données une liste de vérités. Les seules opérations possibles seront l'ajout d'une vérité et tester si une vérité est dans la liste.

\subsection{instruction = déduction}

Puisque la mémoire ne contient que des vérités, ajouter des données revient à faire une déduction. Les seules instructions que nous auront seront donc des déductions. Une suite d'instructions sera donc une preuve. Par la suite nous utiliserons indifféremment  le terme de preuve ou de programme. 

\subsection{L'ordinateur Lazi-0}

\subsubsection{Les données}

Les données (que ce soit les vérités ou les arguments des instructions) sont constituées de 5 mots (if equal zero one distribute) et de l'assemblage de deux données. Par exemple "if zero" ou "(if zero) one" ou "(((if zero) (if one)) distribute)". On retrouve donc le même langage que la partie "application" du lambda calcul réduit à 5 mots.

Nous appellerons "formule" ou "expression" ce type de donnée. 

\subsubsection{Les instructions}

Le langage machine de cet ordinateur mathématique comprend 6 instructions. Chacune est paramétrée par au plus 3 arguments. Une instruction peut vérifier si une certaine vérité (ou deux pour l'une des 6 instructions) s'y trouve. Ces instructions sont très simples, par exemple (voir la définition de Lazi pour la liste complète):
\begin{itemize}
	\item Une instruction à deux paramètres x et y et produisant la vérité "(if 1 x y) = x" sans vérifier de vérité.
	\item Une instruction à deux paramètres x et y et produisant la vérité "y" en vérifiant les vérité "x" et "x=y".
\end{itemize}

Ce langage peut paraître trop simple, mais nous verrons que l'on peut définir des fonctions à variables, des fonctions récursives, des types, des types paramétrés etc.

De plus nous verrons que les expressions peuvent être calculées grâce aux instructions, c'est à dire que ces 6 déductions permettent de produire une vérité de la forme "x = y" où y est le résultat du calcul de x. D'ailleurs le logiciel (réel) "compute" permet dors et déjà de calculer des expressions Lazi. Une expression Lazi peut être sophistiquée: par exemple calculer la traduction d'une représentation de preuve d'un langage mathématique en un autre.

\subsection{L'ordinateur Lazi-1}

Lazi-0 est parfaitement utilisable, mais c'est un ordinateur mathématique ne produisant que des vérités concrètes. Par exemple on peut déduire que 3=3, mais pas que $\forall x; x=x$.

C'est pourquoi nous utilisons Lazi-0 pour définir l'ordinateur mathématique Lazi-1 qui lui, pourra manipuler des vérités génériques. Il fonctionne sur le même principe que Lazi-0 et possède les 6 instructions de base, mais étend les expressions et fournit 3 instructions supplémentaires pour gérer ces expressions génériques.

Ces instructions sont :

\begin{description}
	\item[Un if générique.] Le "if" traditionnel informatique ne peut gérer que les données non génériques, par exemple "if (isOdd x) then ... else ..." ne fonctionne que si x est connu. Hors il se peut que l'on arrive à un traitement (ici = une production de vérité) identique peut importe la branche choisie. C'est le rôle de ce "if" qui, si la condition est un booléen, explore les deux cas et déduit les conclusions des deux preuves si elles sont égales. C'est l'équivalent du tiers exclu en mathématiques.
	
	\item[Une boucle "for".] Il s'agit de répéter une suite d'instructions (=une preuve) tant qu'une condition d'arrêt n'est pas vraie. La vérité ajoutée sera la dernière vérité produite. C'est l'équivalent du raisonnement par récurrence finie en mathématique.
	
   \item[Une fonction de preuve.] Une fonction de preuve Lazi-1 est l'équivalent de l'appel à une fonction typé de l'informatique traditionnelle. Une fonction de preuve Lazi-1 comporte des arguments nommés (comme en C), n'est exécutée que si  certaines conditions sur ces arguments sont vérifiés et a pour effet d'ajouter une vérité (une expression où l'on aura remplacé les variables de la fonction par leurs valeurs). Cette instruction possède en plus en paramètre la preuve générique (sans remplacer les variables) de la vérité à déduire en supposant les conditions vraies. C'est l'équivalent en mathématique du quantificateur universel et de l'implication.  
   
   Par exemple l'expression mathématique "$\forall x; isOdd(x) \Rightarrow isEven(x+1)$" et sa preuve P est vue comme une fonction de preuve de variable x, permettant d'ajouter la vérité "$ isEven(x+1)$" (où x est remplacé par la valeur de x) quand la condition "$isOdd(x)$" (où x est remplacé par la valeur de x) est vrai et si la preuve P, avec l'hypothèse de départ  "$isOdd(x)$", est valide.

\end{description}


\subsection{Lazi-1 calculé en Lazi-0}

La définition d'une preuve valide Lazi-1 est faite en Lazi-0. Que ce soit d'un point de vue mathématique ou informatique, il est toujours bon d'avoir une base petite et fiable. C'est pourquoi il est logique d'utiliser Lazi-0 pour vérifier les preuves Lazi-1. Nous verrons même ci-dessous que c'est indispensable pour obtenir une certaine propriété des fondations.

\subsection{Optimisation}

On pourrait penser que toute ces belles constructions mènent à des programmes d'une lenteur extraordinaire. Ça serait vrai si on appliquait les définitions telles qu'elles. Mais nous n'avons aucune raison de le faire. Ce qui importe c'est que l'on puisse \emph{à terme} prouver que le résultat final est égal à celui obtenu par la définition théorique.

\subsection{Ajouter l'indispensable virtualisation}

En informatique il est indispensable de pouvoir utiliser d'autres langages que le langage machine. Les ordinateurs traditionnels le permettent facilement. Comment obtenir la même fonctionnalité pour un ordinateur mathématique ? 

Nous n'avons pas d'argument en entrée (comme un code source à interprété) et il nous faut prouver une vérité. Nous pourrions prouver que pour s un code source, donc vérifiant une propriété "être un code source" p, la traduction de la conclusion de l'exécution de s est une vérité. Donc on aurait une vérité de la forme : pour tout s, si p s, alors f s.

Pour prendre un exemple simple, imaginons que p est la propriété "représenter une preuve Lazi-0" (pour une certaine forme de représentation). f serait alors la fonction qui, pour une représentation de preuve Lazi-0 s, traduirait sa conclusion en une vérité. Par exemple si la preuve conclue "1 = 1", alors on aurait \code{f s = ( 1 = 1 )}. Mais pour prouver  f s il nous faut avoir une vérité sur ce qu'est une preuve valide. Hors Lazi-0 ou Lazi-1 ne contiennent aucune règle à ce sujet. C'est pourquoi on ajoute à Lazi-0 (qui devient Lazi-0-1) une septième instruction qui signifie :

\begin{framed}
	\textit{Si s est une représentation de preuve Lazi-1 ayant en conclusion une représentation e d'expression Lazi-0, alors on déduit l'interprétation de e.}
\end{framed}

Remarque: nous avons une infinité de manières de représenter une preuve Lazi-1, une certaine manière est utilisée pour cette règle.

Remarquons que Lazi-1 ne comporte pas d'instruction d'interprétation. Comment alors interpréter un autre langage E en Lazi-1 ? Soit p la propriété "être une représentation de preuve en E ayant en conclusion une vérité traduisible en Lazi-1". Soit t une fonction qui, pour un s tel que p s, retourne la traduction de s en représentation de preuve Lazi-1. Nous pouvons montrer en Lazi-1 que si s vérifie p s alors t s est une représentation de preuve Lazi-1. 


\subsection{Ajouter l'indispensable virtualisation}

Un programme d'un tel ordinateur doit prouver une certaine vérité, il ne peut se contenter d'interpréter. Par exemple, imaginons que nous ayons une représentation d'un programme Lazi-0 et que nous voulions l'exécuter. 

Hors pour un ordinateur mathématique un programme valide doit vérifier certaines contraintes, par exemple si une déduction n'a pas ses conditions de remplies alors le programme est en erreur. Alors qu'un interpréteur traditionnel peut se contenter d'exécuter le programme, il nous faut ici prouver que le programme à interpréter.


Mais pour vérifier ces conditions, il faudrait que l'ordinateur mathématique puisse vérifier qu'une représentation de preuve est valide.

Pour un ordinateur mathématique aucun effet de bord n'est possible puisque seules des vérités peuvent être ajoutées. Un ordinateur mathématique B peut avoir une représentation d'un ordinateur mathématique C car un ordinateur mathématique est défini par ce qu'est une assertion et ce qu'est une déduction. B peut aussi calculer le résultat de l'exécution d'une preuve de C. Mais comment pourrait-il intégrer en tant que vérité native le résultat de l'exécution d'une preuve de C ?

B peut avoir une représentation de lui-même et de C et prouver que pour un certain ensemble de preuves de C, les conclusions de ces preuves sont des \emph{représentations} de vérités de B. Mais il manque un élément pour en déduire une vérité de B, une sorte d'instruction signifiant "Je suis B".

Si l'ordinateur mathématique A possède une instruction pour exécuter les preuves de B dont la conclusion est une représentation d'une vérité de A, alors A peut intégrer en vérité native certaines conclusions des preuves de B. Comme A peut utiliser certaines conclusions de B, il peut utiliser la conclusion provenant de B qu'une certaine représentation de vérité de C  représente une vérité de B. Donc si A calcul des vérités pour l'ordinateur B, il peut ajouter une vérité provenant d'une preuve de C quand B prouve que cette vérité est transposable dans B. On voit qu'il nous suffit de cette seule instruction 


\subsection{Ajouter l'indispensable interprétation}


En informatique nous ne nous contentons pas de programmer en langage machine, la possibilité d'utiliser de nouveaux langages est indispensable.

En terme d'ordinateur mathématique cela se traduit par la capacité à récupérer le résultat de l'exécution d'une suite d'instructions d'un langage interprété pour en faire une vérité native.

Mais comment déduire que la conclusion d'une preuve interprétée est une vérité native ? On peut déduire que c'est une vérité pour un certain système de déduction (comme Lazi-0), mais on n'a pas de moyen de savoir que Lazi-0 est le système de déduction en vigueur. On ne peut donc pas effectuer le raisonnement suivant :"Représentons nous une preuve, cette preuve est valide, donc nous pouvons faire nôtre sa conclusion".

Pour permettre l'interprétation dans les ordinateurs mathématiques il est donc nécessaire d'ajouter une instruction qui ajoute une vérité à partir d'une représentation d'une preuve. C'est pourquoi on ajoute à Lazi-0 (qui devient Lazi-01) une septième instruction qui signifie :

\begin{framed}
	\textit{Si p est une représentation de preuve Lazi-1 ayant en conclusion une représentation e d'expression Lazi-0, alors on déduit l'interprétation de e.}
\end{framed}

Lazi-1 est très loin de fournir un langage mathématique parfait, il définit plutôt un langage parfaitement extensible (propriété qui n'existe pas dans les autres fondations). Nous pourrons alors développer des extensions permettant de le perfectionner. Ces extensions peuvent être aussi bien des notations que des nouvelles formes de déductions.

Remarquons que Lazi-1 ne comporte pas d'instruction d'interprétation. Comment alors interpréter un autre langage E en Lazi-1 ? Nous pouvons prouver en Lazi-1 que les conclusions d'un ensemble de preuves de E sont interprétables en vérités Lazi-1. Si e est la conclusion d'une de ces preuves en E et que l'on a prouvé par une preuve p en Lazi-1 que son interprétation ei est une vérité Lazi-1 alors, par la règle d'interprétation de Lazi-0, Lazi-0 déduit aussi que ei est une vérité de Lazi-1. Hors Lazi-1 est interprété en Lazi-0, déduire une vérité Lazi-1 consiste à déduire en Lazi-0 qu'une chose est une vérité Lazi-1, nous venons donc de déduire ei en Lazi-1!

\section{Intérêt}

\subsection{Une informatique mathématisée}

Raisonner en termes d'objets mathématiques (fonctions, propriétés, vérité, déductions etc) apporte un paradigme à la fois simple et structurant.

L'expressivité des mathématiques n'a pas les limites des langages informatiques actuels. Une informatique ayant un cœur mathématique est une garantie d'absence de limitation dans les représentations, même les plus abstraites, par exemple :
\begin{itemize}
	\item Pouvoir utiliser des fonctions ayant des types en arguments.
	\item Exprimer toutes sortes de contraintes sur les éléments définis (et non se limiter aux types). Même des contraintes nécessitant des preuves peuvent être exprimer.
\end{itemize} 

\subsection{Une mathématique informatisée}

Raisonner en tant que processus en mathématique peut être fructueux, par exemple la vision de la fondation mathématique en tant que processus (comme ci-dessus) la rend énormément plus claire. 

L'informatique est sans cesse en contact et enrichie par la confrontation à la réalité, en unifiant les mathématiques à l'informatique alors les mathématiques pourrait bénéficier de ce contact permanent.

En intégrant complètement les mathématiques et l'informatique, le mathématicien peut alors bénéficier des facilités de l'informatique comme la vérification des preuves, le partage automatisé des théorèmes, la traductions des différents dialectes des langages mathématiques, et même dans la recherche de preuve.

\end{document}

