\documentclass[a4paper,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{array}
\usepackage{makeidx}
\usepackage{hyperref}
\usepackage{geometry}
\usepackage[autostyle=false, style=english]{csquotes}
\MakeOuterQuote{"}

% For definitions of terms
\makeindex

\parskip=1mm
\partopsep=1mm
\marginparwidth 1 cm
\geometry{vmargin=45pt}

\begin{document}
\title{Lazi / Les types informatiques / Présentation}
\author{Emmanuel Chantréau}
\maketitle
\tableofcontents


\section{Résumer}

Ce texte présente un nouveau système de typage qui présente de nombreux avantages par rapport à ceux actuels mais nécessite un langage de programmation mathématique. Nous allons voir les fonctionnalités des systèmes actuels et de celui défini en Lazi.

\section{Fonctionnalités des systèmes de types}

Définissons un minimum de vocabulaire :

\begin{description}
	\item[chose:] Toute entité matérielle ou imaginaire. En informatique une chose pourra donc être des données, une fonction, un type etc.
	\item[collection:] ensemble, dans le sens intuitif, de choses.
\end{description}

Un type est un identifiant d'une collection. Par exemple l'identifiant de la collection des entiers naturels sera le type "integer".


Identifier des collections permet diverses fonctionnalités comme :
\begin{itemize}
	\item \textbf{Associer} des choses à la collection (comme des fonctions ou des données dites statiques).
	\item \textbf{Construire} des types à partir d'autres, par le type "couple d'entiers" à partir du type "pair" et du type "integer".
	\item \textbf{Vérifier} qu'une chose se trouve dans la collection. Par exemple vérifier qu'une fonction est bien dans la collection des fonctions prenant un "char" en argument et retournant un "int".
	\item \textbf{Traduire} le code source (par exemple en code machine) grâce aux informations fournies par les déclarations de type.
\end{itemize}

\begin{paragraph}{Définition : } Un \textbf{type de base} est un type non construit à partir d'autres types.
\end{paragraph}

Nous allons voir comment sont définis les types de base et comment ces 3 premières fonctionnalités sont implémentées en C++; Haskell, théorie des ensembles et Lazi.

\subsection{Implémentation en C++}

\subsubsection{Types de base}

Ils sont prédéfinis dans le langage.

\subsubsection{Associer}

Les structures et les classes C++ permettent à la fois de définir un nouveau type par agglomération de données typées et d'y associer des fonctions et données dites statiques.

\subsubsection{Construire}

Plusieurs moyens :
\begin{itemize}
	\item Définition d'un alias (typedef)
	\item En spécifiant une structure ou une classe.
	\item En spécifiant le type de fonctions par les types des arguments et de la valeur retournée.
	\item Par la définition et l'utilisation de sortes de macro-fonctions prenant en arguments des types ou des données (les templates)
	
\end{itemize}

\subsubsection{Vérifier}

Pour chaque variable ou fonction utilisé, un type est déclaré (sauf si on utilise le pseudo type "auto"). Dans tous les cas le compilateur fait des vérifications de validité en utilisant les types.

\subsection{Implémentation en Haskell}

\subsubsection{Types de base}

Ils sont prédéfinis dans le langage.

\subsubsection{Associer}

Les classes permettent de créer une association entre un ou des types et des méthodes.

\subsubsection{Construire}

On peut construire de nouveaux types de données ou des types de fonctions en utilisant des constructeurs ayant en argument des types.

\subsubsection{Vérifier}

La déclaration des types des données et fonction est optionnelle, si elle n'est pas faite le compilateur infère les types manquant. Dans tous les cas le compilateur fait des vérifications de validité en utilisant les types.

\subsection{Implémentation en théorie des ensembles}

On compare ici les types en informatique avec \href{https://en.wikipedia.org/wiki/Mathematical_structure}{les structures en mathématique}.

\subsubsection{Types de base}

Il n'y a pas de type de base.

\subsubsection{Associer}

L'association se fait par des propriétés sur les éléments de la structure. Par exemple on énoncera que l'opérateur + doit être commutatif.

\subsubsection{Construire}

Il n'y a pas de contrainte de construction.

\subsubsection{Vérifier}

Comme ce n'est pas un langage informatique, il n'y a pas de vérification.

\subsection{Implémentation en Lazi}

\subsubsection{Types de base}

Il n'y a pas de type de base.

\subsubsection{Associer}

Un type est défini par une structure qui contient la fonction "domain", servant à reconnaître les choses de la collection que le type définit. Cette structure peut contenir aussi d'autres éléments comme des fonctions.

\subsubsection{Construire}

Il n'y a pas de contrainte sur les constructions de types. On peut par exemple construire le type d'une liste où le type du premier élément est fixé et où le type de l'élément suivant est déterminé en fonction de l'élément précédent.

\subsubsection{Vérifier}

La vérification du code source est vue en Lazi comme la vérification de la véracité de propriétés sur le code. On peut donc exprimer une contrainte traditionnelle sur les types, mais aussi d'autres contraintes. Vérifier la contrainte revient à rechercher une preuve. Si la preuve ne peut être produite automatiquement, il est alors possible de produire une aide à la preuve dans le code source (par exemple pour prouver que la fonction ne boucle pas indéfiniment).

\section{Lazi, ou l'unification totale}

Dans un langage comme le C++ les données, les fonctions et les types sont de différentes nature. 

En Haskell les données et les fonctions sont de même nature (le type d'une donnée et le type d'une fonction sans argument sont identiques). L'unification d'Haskell impose une contrainte qui est que les fonctions sont sans effet de bord. On s'aperçoit par la pratique que cette contrainte est en fait une bonne pratique.

En Lazi les données, les fonctions et les types sont de même nature (les "choses"). Cela amène des avantages comme :
\begin{itemize}
	\item Une simplicité du langage, par exemple il n'y a qu'une syntaxe de définition (\texttt{\$Def x = ...}), que ce soit pour les fonctions, données ou type.
	\item Une absence de limite dans l'expression des types, par exemple on peut utiliser des fonctions pour retourner un type. 
\end{itemize} 
Et cela amène des contraintes qui se révèlent être de bonnes pratiques comme :
\begin{itemize}
	\item Exprimer de manière souple les contraintes à vérifier sur le code, ce qui permet d'ajouter des vérifications et ainsi d'augmenter la fiabilité du code.
	\item Entièrement mathématiser le langage informatique ainsi que son interprétation (compilation ou interprétation). Ce qui permet une souplesse sans limite, par exemple un inconnu peut programmer une extension du langage et vous pouvez l'utiliser le lendemain car toutes les vérifications (dont la fiabilité et la sécurité) sont des preuves mathématiques.
\end{itemize} 





\end{document}

