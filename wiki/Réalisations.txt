Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-03-09T18:02:51+01:00

====== Réalisations ======

Cette partie documente les réalisation (mathématique,logiciels, syntaxe, concepts etc).

Pour accéder aux sources voir  "[[Organisation:Organisation des documents|Organisation des documents]] / Accès et modification des documents"

Les sous-sections :
* Sur le projet entier :
	* [[+1. Vision globale:Réalisations présentes et prévues|Présentations des réalisations présentes et prévues]]
	* [[+1. Vision globale:Principes fondateurs|Principes fondateurs et caractéristiques de Lazi]]
* Documentation sur des sous-parties :
	* [[+2. Mathématique|Mathématique]] : la base du projet Lazi est une fondation mathématique. 
	* [[+3. Informatique|Informatique]] : la fondation mathématique a des affinités et implications informatiques.
	* [[+4. Logiciels|Logiciels]] : différents logiciels permettent d'exécuter le langage informatique dérivé de Lazi.

[[https://lazi.bobu.eu/wiki_old/index.php/Main_Page|L'ancienne documentation]] n'est pas entièrement reportée ici.



@todo
