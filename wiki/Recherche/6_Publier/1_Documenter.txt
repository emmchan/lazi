Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-05-27T19:50:25+02:00

====== 1 Documenter ======


===== Description =====

Produire la documentation nécessaire à la publication.

===== Sous-tâches =====

* Documents dans "Réalisations" | en cours | 2020.
* Documenter le projet de compilateur mathématique | en cours | 2020.
* Principes fondateurs, sub-physique | en cours | 2020.
* [[+Principes fondateurs, sub-physique:Problème de la force d'une fonction de recherche|Problème de la force d'une fonction de recherche]] | en cours | 2020.
* Ajoute à translate la fonction de convertir en Lazi.0.0 et de calculer la profondeur de la formule obtenue | fait |2020 .
* Revoir la documentation du wiki des logiciels | à faire | 2020
* Revoir la documentation incluse dans le code source des logiciels | à faire | 2020
* Vérifier le code de everything.lazi et naturalNumber.lazi | à faire | 2020
