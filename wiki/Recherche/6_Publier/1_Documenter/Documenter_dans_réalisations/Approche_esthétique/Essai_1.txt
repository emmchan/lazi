Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2021-02-06T15:39:28+01:00

====== Essai 1 ======


Jeune, vous avez probablement eu l'impression que notre société était moderne, évoluée, sophistiquée et bien organisée n'est-ce pas ? Vous voyez mieu maintenant des problèmes énormes à tous les niveaux ? 
Je vous propose ici un exercice assez extraordinaire de par la méthode : à la place d'être sufoqué par tant de bazard nous allons aborder un point insignifiant que nous allons détricoter pour la retricoter correctement. Tout l'intérêt de l'exercice sera de vous prouver qu'en étant méticuleux et en réfléchissant logiquement on peut faire des miracles. Cette méthode, vous pourrez alors l'appliquer avec moi. Ça tombe bien, notre société a besoin d'être retricoté.

Nous allons prendre un point parmis plein d'autres, il pourrait être en économie, en mathématique, en physique etc, nous allons le prendre en informatique. Ce point est tout petit, c'est une simple maille, nous allons voir qu'elle est mal faite et nous allons la refaire. À partir de là nous allons retricoter autour. À la fin vous verrez que l'on a accompli un prodige juste par cette méthode logique et méticuleuse. 

Regardons ce point mal fait. Mais pour cela il faut utiliser la première règle de la méthode: 
	**Prendre de la distance et utiliser son bon sens.**
Notre bon sens nous dit que l'on peut donner un nom à tout chose (un lieu, une idée, un animal etc). On sait bien qu'il n'y a pas de limite à nomer les choses et qu'il suffit de se dire "je donne ce nom à cette chose". Par exemple je peux nommer "Ublu" la chaise sur laquelle je suis assis. Alors pourquoi en informatique, suivant ce que l'on veut nommer il faut utiliser une méthode dépendant ce que l'on nomme ? Par exemple pour une classe C++ il faudra passer par la syntaxe "Class //Nom// ..." et pour une variable qui est un entier il faudra passer par "int //Nom// ...". Je vous entends me dire "oui mais on s'en fou c'est pas grâve. C'est pourquoi je dois vous donner la deuxième règle de la méthode:
	**Les imperfections, même infimes, sont des paneaux indicateurs vers de grandes découvertes.**
Continuons, vous allez voir...
Si l'informatique était comme dans la vie, on devrait pouvoir tout nommer de la même manière, par une syntaxe qui signifie "donne ce nom à cette chose". Par exemple, que ce soit une classe, une variable ou n'importe quoi on écrirait "$Def //Nom// = ...". Ce n'est pas possible dans l'informatique actuelle car pour cela il faut que l'élément de droite de l'assignation (la [[https://en.wikipedia.org/wiki/Value_(computer_science)#R-values_and_addresses|r-value]]) soit une [[https://en.wikipedia.org/wiki/Value_(computer_science)|valeur]] et donc qu'elle ait un type. Hors, les types (int, une classe etc) n'ont pas de types, le mieux qui existe est Haskell où les types ont des [[https://wiki.haskell.org/Kind|kind]] mais même en Haskell les types ne peuvent être des valeurs. 
Dans la vie de tous les jours, comme en mathématique on peut parler/manipuler une chose sans obligatoirement définir son type car les choses sont ce qu'elles sont et les types sont des outils. Pour réussier à tout définir par "$Def", il nous faut accepter un langage qui ne déclare ni infère systématiquement les types. Bien sûr il devra permettre d'utiliser les types car c'est une manière de vérifier la validiter du code source et de définir des informations liées à une chose.
Mais alors comment gérer toutes ces choses (les tableaux, les fonctions etc) si les types ne sont pas obligatoire ?
Il semble que nous sommes confrontés à un problème pénible, mais la troisième règle de la méthode est là pour nous tirer d'affaire:
	**L'esthétique est toute puissante.**
Cela implique que d'un //certain// point de vue, tout peut paraître esthétique, peut importe le niveau d'esthétique que l'on souhaite. Bon c'est très bien mais comment l'appliquer ? Et bien nous sommes partis d'une idée esthétique (ne définir que par $Def), nous avons raisonner sans négliger des points, et nous arrivons à la conclusion que le langage que nous cherchons doit comporter à la fois des types et des valeurs sans types. Il doit donc y avoir une solution esthétique à notre problème. Si notre langage comportait de nombreuses définitions de mécanismes, ce serait lourd et inesthétique. Alors regardons plutôt vers un langage équivalent au [[https://fr.wikipedia.org/wiki/Logique_combinatoire|plus petit langage possible,]] c'est un langage sans variable, ne comportant que 4 mots et 5 règles de calcul que voici :
* Il n'y a que 4 mots : "if", "1", "0", "distribute"
* Les mots sont des formules
* pour x,y et z des formules quelconques :
	* "x (y)" est une formule.
	* "if (1) (x) (y)" se calcule en "x"
	* "if (0) (x) (y)" se calcule en "y"
	* "distribute (x) (y) (z)" se calcule en "x (z)(y (z))"
	* si x se calcule en y alors "x (z)" se calcule en "y (z)"
	* si x se calcule en y alors "if (x)" se calcule en "if (y)"
À partir de ce langage très simple, nous pouvons construire un langage ayant de nombreuses qualités, donc celle de tout pouvoir définir par $Def, d'utiliser des types de haut niveau (bien mieux que ceux actuels car dynamiques et regoursement logiques). Mais je ne résiste pas au plaisir de résoudre le premier paradoxe : 

Comment produire un programme avec des entrées/sorties à partir d'un langage sans entrée sortie ?
Avant de résoudre ce paradoxe, réjouissons nous ! Grâce à notre quatrième régle :
	**Les paradoxes sont toujours les panaux indicateurs vers la beauté.**
Pour résoudre celui-ci nous allons utiliser un point de tricot fort élégant : la maille à l'envers. Cela consiste à retourner toute chose. Appliqué à notre cas il s'agit de ne pas voir le programme comme une chose de notre univers mais plutôt à voir notre univers comme une chose du point de vue du programme. Notre programme peut lire son univers et le modifier.
