Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-03-06T10:45:15+01:00

====== Documenter les extensions mathématiques ======


===== Description =====

Documenter les extensions lazi déjà réalisées dans la partie "réalisation". Voir les docs existantes de l'ancien wiki.

===== Sous-tâches =====

* 01-events.ext | à faire | 2020.
* 02-story.ext-ev | à faire | 2020.
* 03-addSimpleRule.ext-ev | à faire | 2020.
* 04-goalSimpleRule.ext | à faire | 2020.
* 05-equality.deduc | à faire | 2020.
* 06-function.ext-nota | à faire | 2020.
* 07-definition.ext-ev | à faire | 2020.
* 08-wordRepr.ext-nota | à faire | 2020.
