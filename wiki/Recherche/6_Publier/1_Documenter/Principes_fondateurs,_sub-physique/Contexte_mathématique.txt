Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-01-17T08:33:03+01:00

====== Contexte mathématique ======


===== Contexte =====

Quand on a une activité mathématique (écrite, orale etc) il est nécessaire de préciser le contexte (ce sur quoi on s'appuie). C'est généralement implicite mais avec Lazi il faut pouvoir rendre formel l'expression du contexte.

===== Question =====

Comment formaliser et nommer l'expression du contexte ?

===== Réponse =====

On peut donner juste la mathématique définissant le contexte ou encore tout un chemin. C'est un peu comme si on demande en informatique "Dans quel répertoire on travaille ?". 

Vocabulaire : 
* une pré-mathématique : langage et logique floues, et donc non formellement défini, permettant néanmoins de définir une mathématique.
* le contexte mathématique : le cadre mathématique ou pré-mathématique dans lequel on se place
* chemin mathématique : liste de contextes mathématiques ou le suivant est défini dans le contexte du précédent

