Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-04-04T11:20:41+02:00

====== Recherche ======


===== Description =====

Cette partie présente la recherche sur Lazi passée, présente et prévue. Voir [[Organisation:Organisation des documents:Organisation de la partie recherche|l'organisation de cette partie]].


===== Sous-tâches =====

* [[+0 Logiciels|Logiciels]] | fait | 2018
* [[+1 lazi.0 et lazi.1.0|Définition de lazi.0 et lazi.1.0]] | en cours | 2019
* [[+2 Vérification des types|Vérification des types]]  | en cours | 2019
* [[+6 Publier|Publier]] | en cours | 2019
* [[+7 Compilateur|Compilateur]] | à faire | +2020
