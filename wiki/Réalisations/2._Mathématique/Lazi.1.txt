Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-03-12T05:39:38+01:00

====== Lazi.1 ======

Lazi.1 est un ensemble d'extension ajoutant ce qui est considéré comme la base des mathématiques: variables, quantificateurs, définitions etc. Lazi.0 est la fondation mathématique, et donc Lazi.1 n'en fait pas parti.
