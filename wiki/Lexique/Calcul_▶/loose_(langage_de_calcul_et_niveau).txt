Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-12-17T19:48:31+01:00

====== loose (langage de calcul et niveau) ======


===== Contexte =====

Il est nommé "libre" car il est possible de calculer n'importe quelle partie de la formule. Les calculs élémentaires fondamentaux sont les mêmes (if et distribute). Pour une même formule, le calcul libre pourra produire plus de résultats que le calcul univocal car on pourra calculer les sous-parties indifféremment. 


===== Définition du langage de calcul "loose" =====

Un calcul élémentaire "loose" est représenté par un [[Lexique:SFP ▶:NFSFP (Non Functional SFP) (de niveau n)|NFSFP]]. Le calcul élémentaire est le calcul élémentaire univocal à l'endroit désigné par ce NFSFP. 

Remarque: un calcul univocal est un calcul loose où les SFP sont tous les chemins désignant la formule globale.

===== Définition du niveau d'un calcul élémentaire "loose" =====

Soit s le NFSFP représentant un calcul élémentaire "loose", son niveau est [[Lexique:SFP ▶:NFSFP (Non Functional SFP) (de niveau n)|le niveau du NFSFP]] s.



