Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-05-07T11:45:03+02:00

====== calcul terminé ======

Pour un langage de calcul a et une formule x, on dit que **le calcul a de x est terminé** s'il n'existe pas de formule y telle que ''x ''→c[a]1'' y'' .

On dit que **le calcul de x est terminé** si le calcul de x est terminé dans [[univocal (langage de calcul)|univocal]].
