Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-01-23T18:22:59+01:00

====== calcul équivalent ======


===== Contexte =====

On définit, pour une formule donnée, l'ensemble des calculs produisant le même résultat.

===== Définition =====

Soit l un langage de calcul, x une formule et c un calcul valide sur x. On dit que le calcul d est équivalent à c sur x ssi :
* d est valide sur x
* ''calculate l c x = calculate l d x''


