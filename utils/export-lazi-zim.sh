#!/bin/bash
set -x
set -e
export LC_ALL="C"

HOME_PAGE="Accueil" # title of the home page (replace spaces by "_").
URL="https://lazi.bobu.eu/wiki/"
SSH_PATH="root@serveur1.bobu.eu:/var/www/lazi.bobu.eu/wiki/"
WIKI_DIR=~/perso/iggi/lazi/wiki
TEMPLATE_BASE=~/perso/iggi/lazi/utils/zim-template-flex1/lazi-wiki

T=$(mktemp -d) # Temporary directory to prepare the export
zim --export -r --root-url $URL --index-page=plan --format=html --template=$TEMPLATE_BASE/template.html --output=$T $WIKI_DIR
cp -a $TEMPLATE_BASE/resources/ $T/_resources
cp -a $T/${HOME_PAGE}.html $T/index.html
# We replace images of .svg with objects, so we can use links
find $T -type f -name "*.html" -print0|xargs -0 sed  -i 's%<img src="\([^"]*\)">%<object type="image/svg+xml" data="\1"></object>%g'
rsync -r -l -z -e ssh --no-owner -og --chown=www-data:www-data --delete --exclude "google*.html" --exclude ".htaccess" $T/ $SSH_PATH
[ -d $T ] && rm -rf $T
