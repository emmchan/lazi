///                                     function                                         
/*
Cette extension ajoute la notation "fonction". C'est la même notation que dans compute: une fonction à une variable.

Nous ajoutons un nouveau type de formule "functionT" représentant une fonction à une variable simple. Les variables des fonctions sont des mots de type "lvar".

Nous ajouterons les règles pour cette notation:
- Pour calculer l'application (paramètres: un mot x, des formules a et b):
  - Déduit "($F x → a) b = c" où c est a où x est remplacé par b sauf dans le corps des fonctions ayant x en variable.
- Une pour traduire la notation (paramètres: une formule x):
  - Déduit "x = y" où y est x où les notations de fonction sont traduites.
*/

///                          Composant de formule functionT                              

/*
Le type composant de formulaT représentant une fonction.
L'instance est :
- le nom de la variable
- le corps de la fonction

Le paramètre est:
- formulaT
*/
$Def functionT = 
  $Let parent = dynPairT ∪o $O($O[formulaT])[ t1 = wordT, t2f = $F var → formulaT .o'addLVar var ]
  ,
  parent ∪o
  $O($O[formulaT,t2f])[
    object = $F i → formulaT .o'object \ translateThis i
  ,
    mapFormulas = $F $T[var,body],f → $T[ var, f (t2f var) body ]
  ,
    // Surcharge, voir formulaT. On remplace dans le corps, sauf la variable, qui est protégée.
    replaceTypedWordsSubFormulas = $F $T[var,body],wt,dr → 
      $T[ 'function, var, t2f var .o'replaceTypedWords body wt \ if(wt =w 'lvar, dictReduce dr \ wordEqual var,dr) ]
  ,
    match = $F $T[var1,body1],vl,found,$T[var2,body2]  → 
      if(var1 =w var2, t2f var1 .o'match body1 vl found body2, nothing)
  ,
    translateThis = $F $T[var,body] → 
      // Nous traduisons d'abord la notation dans le corps.
      $Let body2@$T[type,j] = t2f var .o'translate body \ hasTypeName 'function,
      if( type =w 'apply 
      ;
        $Let $T[jf,ja]=j,
        $F[distribute] +f translateThis $T[var, jf] +f translateThis $T[var, ja]
      ;
        // body2 n'est ni un apply ni une fonction, donc c'est un word.
        $Let $T[wt, w] = j,
        if(wt =w 'lvar ∧b w =w var; $F[if 0b 1b]; $F[if 1b] +f body2)
      )
  ,
    mapFormulas =  $F i@$T[var,body],f →$T[var, f (t2f var) body]
  ,
    // Si le mot recherché est la variable de la fonction, alors il n'est pas considéré comme utilisé, car une variable est considérée comme utilisée que si elle est libre. 
    isWordUsed = $F $T[var,body],w@$T[wt,ww] →
      if( wt =w 'lvar ∧b var =w ww; 0b; t2f var .o'isWordUsed body w)
  ]
            

///                                Règle functionApplyT                                  

/*
Règle de déduction pour calculer le résultat de l'application d'un argument à une fonction.
- Instance: une formule qui est une application où la partie fonction est une fonction.
- Objet: aucune condition et en conclusion l'égalité où à droite le corps de la fonction où la variable est remplacée par l'argument de l'application.
- Traduction: la déduction de l'égalité similaire à l'objet mais où les notations fonctionnelles sont traduites.
*/
$Def functionApplyT =
  $O($O[deductionT])[
    formulaT = deductionT .o'formulaT
  ,
    // Le type du corps de la fonction (dépend de la variable).
    bodyFormulaT = $F var → formulaT .o'addLVar var
  ,
    domain = $F i@$T[type,$T[ft,_],a] → formulaT .o'domain i ∧b type =w 'apply  ∧b ft =w 'function
  ,
    equal = formulaT .o'equal
  ,
    object = $F i@$T[_,$T[_,var,body],arg] → $T[ ∅l, 
        $F[equal] +f i +f (bodyFormulaT var) .o'replaceTypedWords body 'lvar $L[ $T[var,arg] ]
      ]
  ,
    mapFormulas = $F i,f → f formulaT i
  ,
    /* La conclusion d'une déduction est de la forme "($F var → body) arg = body2" où body2 est body 
    où var est remplacé par arg. Il nous faudra donc prouver cette égalité, mais avec les notations des
    fonctions traduites. Pour cela nous allns calquer l'algorithme de traduction de "$F var → body" en
    produisant les preuves à chaque étape. Pour mieux comprendre on peut voir le cas le plus simple en premier:
    le dernier, celui du premier "else", quand la variable n'est pas utilisée.
    */ 
    translateThis = $F i@$T[apply,func@$T[_,var,body0],arg] →
      // On traduit d'abord body0 pour qu'il n'ait plus de notation de fonction interne.
      $Let body@$T[bodyt,bodyv] = bodyFormulaT var .o'translate body0 \ hasTypeName 'function,
      if
      (
        bodyFormulaT var .o'isWordUsed body $T['lvar,var]
      ;
        if
        (
          bodyt =w 'apply
        ;
          // Si le corps de la fonction est un apply, alors la fonction est traduite par "distribute ($F x → f) ($F x → a)".
          // Rappelons que "distribute x y z = (x z) (y z)".
          // Il nous faudra déjà les preuves pour les calculs de "($F var → f) arg" et "($F var → a) arg" (prooff et proofa).
          // Puis prouver que  "distribute ($F var → f) ($F var → a) arg = body2" où body2 est body où x est remplac par arg.
          $Let $T[jf,ja]=bodyv,
          $Let prooff = translateThis $T['apply,$T['function,var,jf],arg],
          $Let proofa = translateThis $T['apply,$T['function,var,ja],arg],
          // Des conclusions de ces deux preuves on récupère les valeurs (traduites) de "$F var → f", "$F var → a" ainsi que les résultats de l'application (les parties droites des égalités) et "arg" traduits.
          $Let $T[_apply,$T[_apply,_equal,_apply,prooffFunc,argTr],prooffRes] = ctruthToConc \ deductionT .o'object prooff,
          $Let $T[_,$T[_,_,_,proofaFunc,_],proofaRes] = ctruthToConc \ deductionT .o'object proofa,
          
          $T['proof,$L[
            prooff,
            proofa,
            $T['distribute, $D[x = prooffFunc, y = proofaFunc, z = argTr] ],
            $T['equalArgsFuncs, $D[ x1 = prooffFunc +f argTr, y1 = proofaFunc +f argTr , x2 = prooffRes, y2 = proofaRes] ],
            $T['equalTransitivity, $D[ x = $F[distribute] +f prooffFunc +f proofaFunc +f argTr,
                                        y = (prooffFunc +f argTr) +f (proofaFunc +f argTr),
                                        z = prooffRes +f proofaRes] ]
          ]]
        ;
          // Sinon c'est un mot, comme ce mot utilise la variable, c'est qu'il l'est. C'est donc la fonction identité. 
          // Il nous faut donc prouver que "if 0b 1b arg = arg" (la traduction de "$F v → v" est "if 0b 1b".
          $T[ 'ifOnZero, $D[ x = $F[1b], y = arg ] ]
         )
      ;
        // Si la variable n'est pas utilisée alors la fonction est traduite par "if 1b body", il nous faut donc prouver que 
        // "if 1b body arg = body".
        $T[ 'ifOnOne, $D[ x = body, y = arg ] ]
      )
  ]

  
///                             Règle functionTranslateT                                 

/*
Règle de traduction de la notation.
L'instance est une formule x.
La vérité produite est l'égalité x = y, où y est x où les notations de fonctions sont traduites.
Traduction de cette règle: si on traduit les notations, la rgle produit une vérité de la forme x=x. Nous traduirons donc une déduction par une déduction de réfléxivité.
*/
$Def functionTranslateT = 
  $O($O[deductionT])[
    formulaT = deductionT .o'formulaT
  ,
    domain = formulaT .o'domain
  ,
    equal = formulaT .o'equal
  ,
    // Retourne la ctruth sans condition et où la conclusion est l'égalité entre la fonction et un pas dans la traduction de la notation.
    object = $F i → $T[∅l, $F[equal] +f i +f formulaT .o'translate i \ hasTypeName 'function ]
  ,
    mapFormulas = $F i,f → f formulaT i
  ,
    // i sera traduit plus tard, voir "translateFunction" ci-dessous.
    translateThis = $F i → $T['equalReflexivity, $D[x = i]]
  ]
    
        
///                                                                                      
///                             Définition de l'extension                                
///                                                                                      

/*
La mathématique où l'on ajoute les deux règles de déduction functionTranslate et functionApply et le nouveau type consituant de formule "function". Au préalable on ajoute le nouveau type de mot "lvar" aux type formulaT de la mathématique.
*/
$Def lazi_1_6 = 
  (
    // On ajoute le fonction "addLVar" à la mathématique
    objMerge $O[ addLVar = $F v → objMod this 'formulaT ($F formulaT → formulaT .o'addLVar v) ] 
    \\
    // On ajoute le fonction "addLVar" à formulaT
    modObj 'formulaT \ objMerge 
      $O[ addLVar = $F var →  this.o=>'types .d->'word.o->'types .d->'lvar.o->'wordList =m= bwAddToList var ]
    \\
    // On ajoute le type de mot "lvar"
    lazi_1_5 .o'addWordType 'lvar \ wordExtT ∪od $D[ wordList = ∅l ]
  )
  .o'addFormulaType 'function functionT
  .o'addRule 'functionTranslate functionTranslateT 
  .o'addRule 'functionApply functionApplyT 
  
/*
Définit le domaine des instances des déductions.
*/
$Def domainFunction = lazi_1_6 .o'domain

/*
Fonction de traduction des instances (i) de déduction dans cette extension. On traduit les deux déductions, puis toutes les formules.
*/
$Def translateFunction = $F i → 
  perm2 (lazi_1_6 .o'mapFormulas) ($F formulaT,i → formulaT .o'translate i \ hasTypeName 'function) \
  perm2 (lazi_1_6 .o'translate) (hasTypeNameAmong $L['functionApply,'functionTranslate]) \
  i
