///                                  goalSimpleRule                                     

/*
En Lazi_1, appliquer une règle simple consiste à fournir le nom de la règle et les valeurs des variables. Cette extension permet de simplifier l'application de la règle simple en donnant, le plus souvant, le nom de la règle et la conclusion à déduire.

Cette extension ajoute une règle de déduction permetant d'appliquer une règle simple en donnant l'information suivante (qui est l'instance de la règle)
- le nom de la règle simple à utiliser
- la conclusion à déduire
- le dictionnaire (souvant vide) des valeurs des variables de la règle simple qui n'apparaissent pas dans la conclusion.

On utilise un triplet plutôt qu'un dictionnaire pour l'instance car cela est plus simple à écrire dans les preuves.
*/

$Def goalSimpleRuleT = 
  $Let parent = pairT ∪o $O($O[deductionT])[
      formulaT = deductionT .o'formulaT,
      t1 = wordExtT ∪od $D[ wordList = dictEntries \ deductionT .o'simpleRules ], 
      t2 = pairT ∪od $D[
          t1 = formulaT,
          t2 = uniDictT ∪od $D[ t = formulaT ]
        ]
    ]
  ,
  parent ∪o
  $O($O[deductionT,formulaT])[ 
    domain = $F i → i ∈t (parent ∪od $D[deductionT = deductionT]) ∧b isThing \ maybeTranslateOneStep i
  ,
    object = $F i → deductionT .o'object \ translateThis i
  ,
    mapFormulas = $F $T[name,goal,values],f → $T[ name, f formulaT goal, dictMap (f formulaT) values]
  ,
    // À partir d'une instance retourne maybe l'instance de deductionT correspondante, ou rien s'il n'y en a pas.
    maybeTranslateOneStep = $F $T[name,goal,values] → 
      listMapGetMaybeOne (
        $F $T[k,$D[vars,ctruth = $T[_,conc]]] → $Let res = formulaT .o'match goal vars values conc,
          if(
            name =w k ∧b dictEntries values ⊂w vars ∧b isThing res ∧b wordListEqual( vars , dictEntries \ getThing res) ; 
            just $T[k,getThing res] ; 
            nothing
          )
      ) \ deductionT .o'simpleRules
  ,
    // Version de maybeTranslateOneStep où l'on est sûr de pouvoir traduire : on retire le "just".
    translateThis = $F i → getThing \ maybeTranslateOneStep i
  ]
  
///                                                                                      
///                             Définition de l'extension                                
///                                                                                      
     
/*
On ajoute la nouvelle règle de déduction goalSimpleRuleT.
*/
$Def lazi_1_4 = lazi_1_3 .o'addRule 'goalSimpleRule goalSimpleRuleT


///                               domainGoalSimpleRule                                   

$Def domainGoalSimpleRule = lazi_1_4 .o'domain

///                              translateGoalSimpleRule                                 

// Il nous faut traduire les déduction "goalSimpleRule"
$Def translateGoalSimpleRule = $F i → lazi_1_4 .o'translate i \ hasTypeName 'goalSimpleRule
