///                                                                                      
///                                   Notations de base                                  
///                                                                                      

$Operator = infix 20 equal

///                                                                                      
///                                   Fonctions de base                                  
///                                                                                      

/*
Fonction `identité'
*/
$Def identF = if 0b 0b 

/*
Fonction constante (le premier argument est la constante), on aura constF x y=x
*/
$Def constF = if 1b

/*
Fonction constante ignorant 2 arguments.
*/
$Def const2F = $F c,x,y → c
  
/*
Composition de deux fonctions f et g.
*/
$Def functionCompose = $F f,g,x → f \ g x

// Opérateur correspondant. Caractère : ∘ U+2218, Nom : RING OPERATOR
$Operator ∘ infix 20 functionCompose

/*
Compose la fonction sur elle-même.
*/
$Def autoCompose = $F f → f ∘ f

/*
Modifie une fonction en appliquant l'argument à lui-même en premier.
*/
$Def selfx = $F f,x → f \ x x

/*
Si on définie x par récurrence, alors on a x = y où x est un nom et y une expression contenant x. Pour avoir une valeur pratique de x, il nous faut trouver une solution à cette équation. Remarquons que cette équation peut avoir de multiple solution, par exemple pour "x = x". Nous allons voir que l'on peut trouver une méthode pour trouver une solution quelque soit y.

Soit f telle que y = f x (où f n'utilise pas le nom "x"). L'égalité devient donc x = f x.
(Remarquons que x = f x = f \ f x = f \ f \ f x = f \ f \ f \ f x etc

Si on arrive à définir une fonction recurse telle que recurse f = f \ recurse f, alors recurse f est une solution à l'équation.
*/
$Def recurse = $F f → selfx f \ selfx f

/*
On a :
recurse f = 
selfx f \ selfx f =
f (selfx f \ selfx f ) =
f \ recurse f

On a donc bien recurse f = f \ recurse f
*/

/*
Du point de vue mathématique cette fonction ne fait rien d'intéressant puisqu'elle ne fait que retourner le dernier. Remarquons que nous pouvons remplacer n'importe quelle valeur x n'importe où par "trace opt test testComp label print x".

Cette fonction n'a d'autre utilité que le débogage. Un shortcut dans l'interpréteur permet de créer un effet de bord pour tracer une valeur (print) associé à une étiquette (label). Le label doit être un nom. opt est un mot servant à spécifier des options, les lettres de ce mots peuvent correspondre à des options (les autres sont ignorées):
- p : la sortie se fait à la console plutôt qu'en xml.
- c : print est calculé avant
- a : si l'option c est définie: les arguments sont aussi calculés.

"test" est une valeur calculée et syntaxiquement comparée à "testComp", la trace n'a lieu que si on a une égalité.

Si print est '_callstack, alors la valeur tracée est la pile d'appel Lazi. Voir les options dans command pour limiter la sortie.

*/
$Def trace = $F opt,label,test,testComp,print,value → value

/*
Une version simplifiée sans test 
*/
$Def autoTrace = $F opt,label,print,value → trace opt label 1b 1b print value

/*
Une version simplifiée sans test et où la valeur retournée est aussi la valeur tracée.
*/
$Def autoTraceThis = $F opt,label,value → autoTrace opt label value value

/*
Une version simplifiée sans test  sans option et où la valeur retournée est aussi la valeur tracée.
*/
$Def autoTraceThisNoOpt = $F label,value → autoTraceThis '_ label value

/*
Une version simplifiée servant juste à savoir s'il y a eu une évaluation, la valeur tracée est 'hello.
*/
$Def autoTraceHello = $F label,value → autoTrace '_ label 'hello value

/*
Pareil que autoTraceHello mais la sortie se fait sur la console texte.
*/
$Def autoTraceHelloTty = $F label,value → autoTrace 'p label 'hello value

///                                                                                      
///                                         Parmutations                                 
///                                                                                      
/* Nous définissons quelques fonction de permutation des arguments d'une fonction */

$Def perm2 = $F f,x,y → f y x

$Def perm3c1 = $F f,x,y,z → f y z x

$Def perm3c2 = $F f,x,y,z → f z x y

$Def perm3t13 = $F f,x,y,z → f z y x


///                                                                                      
///                                    Logique de base                                   
///                                                                                      

/*
"et" logique
*/
$Def boolAnd = $F x,y → if x y 0b

$Operator ∧b infix 10 boolAnd


/*
"ou" logique
*/
$Def boolOr = $F x,y → if x 1b y

$Operator ∨b infix 10 boolOr

/*
"non" logique
*/
$Def boolNeg = $F x → if x 0b 1b

$Operator ¬b prefix 10 boolNeg
 
/*
x est-il un booléen ? Le teste ne fonctionne que dans l'affirmative.
*/
$Def isBoolean = $F x → if x 1b 1b

// Remarque : Nous aurons une forme de tiers exclu qui nous permettra de déduire des vérités à partir 
// d'une vérité "isBoolean \ if x y z = 1b".

// Remarque : "isBoolean equal" n'a aucunne raison d'être un booléen.

/*
Deux booléens sont-ils égaux.
*/
$Def boolEqual = $F x,y → if x y ¬b y

// Remarque : "boolEqual" retourne un booléen si x et y le sont, alors que 0b = 0b  n'a aucunne raison d'être un booléen.
    
$Operator =b infix 10 boolEqual

/*
Deux booléens sont-ils inégaux.
*/
$Def boolNotEqual = $F x,y → ¬b boolEqual x y

$Operator ≠b infix 10 boolNotEqual

///                                                                                      
///                                         Appartenance                                 
///                                                                                      

/*
Cette définition n'a pas de valeur mathématique mais seulement pédagogique. "in" ne sert qu'à informer le lecteur que l'on s'attend à ce que "y x" retourne 0b ou 1b.
*/
$Def in = $F x,y → y x

$Operator ∈ infix 20 in
    
/*
Négation de l'appartenance.
*/
$Def notIn = $F x,y → ¬b y x

$Operator ∉ infix 20 notIn


///                                                                                      
///                                     Les couples                                      
///                                                                                      
   
/*
La fonction "couple de x et y".
*/
$Def pair = $F x,y,c → if c x y
    
/*
Retourne la premiere composante d'un couple.
*/
$Def pairFirst = $F x → x 1b
    
/*
Retourne la seconde composante d'un couple.
*/
$Def pairSecond = $F x → x 0b

// Remarque : Tout chose x peut-être vue comme une paire, ses deux éléments sont x 1b et x 0b.    

/*
La chose est-elle une paire \ Tout est une paire donc retourne toujours vrai.
*/
$Def isPair = constF 1b

// Notations pour les tuple
/*
Notation : variables :x,y
"$T[x,y]" → "(pair (x) (y))"

Un cas simple:

Notation : variables :x
"$T[x]" → "x"

Notation : variables :x, n>2
"$T[x1,...,xn]" → "$T[x1,$T[x2,...,xn]]"
*/
//                                                                                      
///                                       Maybe                                          
///                                                                                      

// Nous allons définir une représentation de "rien ou une chose x". Si m est un maybe, alors si m 1b = 0b on considère qu'il n'y a rien, si m 1b = 1b alors m 0b = x.

/*
représente "pas de chose", c'est la fonction qui retourne toujours 0b.
*/
$Def nothing = if 1b 0b

/*
Prend en argument x et signifie "la chose x"
*/
$Def just = $F x → $T[ 1b , x ]
    
/*
x est-il quelque chose ?
*/
$Def isThing = $F x → pairFirst x
    
/*
x est-il nothing ?
*/
$Def isNothing = $F x → ¬b isThing x

/*
x est un un "Just x" ou un "Nothing" ?
*/
$Def isMaybe = $F x → isBoolean \ isThing x
    
/*
Récupère la chose d'un "just x"
*/
$Def getThing = $F jx → pairSecond jx

/*
Si m est un just, retourne son contenu, sinon retourne l'autre valeur.
*/
$Def maybeElse = $F m,x → if ( isThing m ,  getThing m, x )

/*
Si m est un just, retourne f sur son contenu, sinon retourne l'autre valeur.
*/
$Def maybeMap = $F m,f,x → if ( isThing m ,  f \ getThing m, x )
   
/*
Gère l'application d'une fonction à un maybe, retourne un maybe qui est nothing si l'argument est nothing, sinon retourne just de l'application de la fonction à l'argument du just.
*/
$Def applyJust = $F f,mx → if ( isThing mx , just \ f \ getThing mx , nothing )
    
/*
Comme applyJust mais pour deux arguments.
*/
$Def apply2Just = $F f,x,y → 
  if ( isThing x ∧b isThing y , just \ f (getThing x, getThing y) , nothing )

/*
Remplace nothing sans toucher au just.
*/
$Def replaceNothing = $F m,x → if( isThing m; m; x)

///                                                                                      
///                                      Right / Left                                    
///                                                                                      

// Nous allons définir une représentation de ``une chose marquée left ou une chose marquée right''. Pour cela nous utilisons une paire $T[b,x] où x est la chose et b une valeur égal à 1b(left) ou 0b(right).

/*
Le LeftRight est-il un left.
*/
$Def isLeft = $F x → pairFirst x
    
/*
Le LeftRight est-il un right.
*/
$Def isRight = $F x → ¬b isLeft x
    
/*
Retourne la chose contenue dans le leftRight.
*/
$Def leftRightToThing = pairSecond
    
/*
Construction d'un left.
*/
$Def left = $F x → $T[ 1b , x ]

/*
Construction d'un right.
*/
$Def right = $F x → $T[ 0b , x ]
    
/*
L'argument est-il un leftRight ?
*/
$Def isLeftRight = $F x → isPair x ∧b isBoolean \ pairFirst x

///                                                                                      
///                                         Divers                                       
///                                                                                      
    
/*
L'application.
*/
$Def apply = $F f,a → f a
    
/*
Applique un argument à une fonction
*/
$Def applyArg = $F a,f → f a
    
/*
Fonction doublement constante (ignore deux arguments).
*/
$Def constF2 = $F c → constF \ constF c

///                                                                                      
///                                       Listes                                         
///                                                                                      

///                                  Fonctions de base                                   
/*
La liste implémentée ici n'a pas de sens particulier, c'est pour cela que l'on peut la nommer une "lazy-list". Par exemple si on ajoute les éléments par la fin alors il sera plus rapide ( O(1) vs O(n) ) d'accéder au derniers éléments, mais si on ajoute les éléments par le début alors il sera plus rapide d'accéder au premiers éléments ( O(1) vs O(n) ). La fonction la plus importante du point de vue de l'efficacité (fold) a un temps d'éxécusion similaire ou meilleurs que pour une structure (Maybe $T[ élément, reste ]). Par contre le coût est plus fort si on utilise une récurrence qui décompose la liste en élément/reste.

Une lazy-list prend en argument une fonction dite "fonction d'usage" et un argument dit "accumulateur", ce dernier a le même rôle que dans la fonction fold classique. Si l est la liste vide on aura l f r = r sinon l f r = f r p rl e où :
- p est la place de l'élément (début ou fin)
- rl est le reste de la liste
- e est l'élément de la liste à une extrémité.

On appelle "fonction de folding" une fonction réalisant une sorte (on en définira qui s'arrête etc) de fold sur une liste. Elle prend en argument :
  - dir : la direction du fold
  - f : l'opérateur de folding, qui prend en argument :
    - r : l'accumulateur
    - e : l'élément de la liste
  - r : l'accumulateur au départ
  - l : la liste
  
Par exemple pour le classique fold : 
listFold forward f r $L[a,b,c]  = f (f (f r a) b) c
listFold backward f r $L[a,b,c] = f (f (f r c) b) a

Pour chaque sorte de fold que nous définirons, nous définirons d'abord une fonction de traduction "...toUsage" prenant en argument :
  - dir : la direction du fold
  - f : l'opérateur de folding
et qui retournera la fonction d'usage associée.

De manière à de pas changer nos habitudes nous définirons des fonctions de folding pour cacher que la liste est la fonction principale, par exemple :
$Def listFold = $F dir, f, r, l → l (foldToUsage dir f) r

Ce format de liste étant proche de la fonction fold, on aura tendance à utiliser des fonctions de type "fold" pour faire des calculs sur les listes.
*/



/*
Une liste vide.
*/
$Def ∅l = $F f,r → r

/*
On définit forward signifiant "à l'avant" où "en marche avant" (c'est à dire un parcours du premier vers le dernier élément).
Et backward pour le sens opposé.
*/
$Def forward = 1b
$Def backward = 0b

/*
Ajoute un élément (elem) en tête ou queue à une liste (list) et retourne une liste.
f est dite "fonction d'usage" et r est l'accumulateur.
*/
$Def toListAdd = $F place,list,elem → 
  // La liste retournée :
  $F f,r → f r place list elem

$Def toListFwAdd = toListAdd forward
$Def toListBwAdd = toListAdd backward

$Operator +le infix 20 toListBwAdd

/*
Inversion des arguments de toListBwAdd, pour les map etc.
*/
$Def bwAddToList = $F last,init → toListBwAdd init last


/* 
Notation : 
variable :
$L[] → ∅l
*/


/* 
Notation : 
variables : x1 , ..., xn
condition : n>0
$L[x1,...,xn] → $L[x1,...,xn-1] +le xn
*/

// Remarque : $L[x2,...,x1]=$L[]

/*
Retourne la liste contenant l'unique élément x.
*/
$Def singletonList = $F x → ∅l +le x
  
///                                      Liste vide                                      

/*
La liste `l' est-elle vide.
*/
$Def isEmptyList = $F l → l ($F r,place,list,elem → 0b) 1b

/*
La liste `l' est-elle non vide.
*/
$Def notEmptyList = $F l → l ($F r,place,list,elem → 1b) 0b

///                                      Tête et queue                                   


/*
Retourne just le premier ou dernier élément de la liste ou nothing si elle est vide .
*/
$Def extremityListMaybe = $F place,l → 
  l 
  ($F r,ePlace,list,elem → 
    if( place =b ePlace ; just elem ; replaceNothing (extremityListMaybe place list , just elem) )
  ) 
  nothing

/*
Retourne le premier ou le dernier élément de la liste.
*/
$Def extremityList = $F place,l → getThing \ extremityListMaybe place l 
$Def listFirst = extremityList forward
$Def listLast = extremityList backward

/*
 Retourne just du premier élément rencontré ou bien nothing si la liste est vide.
*/
$Def maybeOneInList = $F l → l ($F r,ePlace,list,elem → just elem) nothing

/*
Version où l'on est sûr qu'il y a un élément.
*/
$Def oneInList = $F l → l ($F r,ePlace,list,elem → elem) nothing

/*
Retourne la liste moins l'élément se trouvant à l'endroit "place".
*/
$Def listRemainder = $F place,l → 
  l 
  ($F r,ePlace,list,elem → 
    if(place =b ePlace 
    ,
      list
    ,
      if(isEmptyList list; ∅l; toListAdd ePlace (listRemainder place list) elem)
    )
  ) 
  ∅l
  
/*
La liste privée de son premier élément.
*/
$Def listTail = listRemainder forward

/*
La liste privée de son dernier élément.
*/
$Def listInit = listRemainder backward


///                                                                                      
///                        Les fonctions de parcours de listes                           
///                                                                                      



///                                 Parcours de base                                     

/*
À partir d'une direction et d'un opérateur de folding, cré une fonction d'usage.
L'opérateur de folding prend en argument : r elem et retourne r. r est l'accumulateur.
*/
$Def foldToUsage = $F dir,f → 
  // optimisation légère de  
  // "$F dir,f → recurse $F g → $F r,place,list,elem → if( dir =b place ; list g (f r elem) ; f (list g r) elem )" 
  // car dir est fixé avant la récursion.
  if(dir
  ,
    // "$F r,place,list,elem → ..." est la fonction d'usage.
    recurse $F g → $F r,place,list,elem → if( place; list g (f r elem) ; f (list g r) elem )
  ,
    recurse $F g → $F r,place,list,elem → if( place; f (list g r) elem; list g (f r elem) )
  )

/*
Classique fold sur une liste.
*/
$Def listFold = $F dir, f, r, l → l (foldToUsage dir f) r
$Def fwListFold = listFold forward
$Def bwListFold = listFold backward

/* 
Pareil que foldToUsage mais l'accumulateur retourné par f est un Maybe et la récurrence s'arrête si il prend la valeur "nothing". L'accumulateur retourné est un Maybe. Le r donné à f est sans le Maybe. Le r de départ est sans le Maybe pour listFoldMaybe ci-dessous mais avec Maybe pour foldMaybeToUsage.
*/
$Def foldMaybeToUsage = $F dir,f → 
  // Comme le code est utilisé récursivement on optimise en mettant le "if(dir" en dehors de la récurrence. Voir foldToUsage pour une version plus didactique de cette optimisation.
  if( dir
  ,
    recurse $F g → $F r,place,list,elem →
      if(place
      ,
        if(isThing r; list g \ f (getThing r) elem; nothing)
      ,
        $Let newR = list g r;
        if(isThing newR; f (getThing newR) elem; nothing)
      )
  ,
    recurse $F g → $F r,place,list,elem →
      if(place
      ,
        $Let newR = list g r;
        if(isThing newR; f (getThing newR) elem; nothing)
      ,
        if(isThing r; list g \ f (getThing r) elem; nothing)
      )
  )


$Def listFoldMaybe = $F dir, f, r, l → l (foldMaybeToUsage dir f) \ just r
$Def fwListFoldMaybe = listFoldMaybe forward
$Def bwListFoldMaybe = listFoldMaybe backward

/* 
Pareil que foldMaybeToUsage mais s'arrête dans la situation inverse : quand l'accumulateur n'est pas un nothing. De ce fait à la place de l'opératuer de folding, puisque l'accumulateur vaut toujours nothing, on ne le passe pas en argument et c'est une fonction de recherche qui prend un seul argument qui est l'élément de la liste et retourne un Maybe.
*/
$Def searchToUsage  = $F dir,f → 
  // Voir foldMaybeToUsage pour la compréhension du code.
if( dir
,
    recurse $F g → $F r,place,list,elem → // r n'est pas utilisé
      if( place
      ,
        $Let newR = f elem; 
        if(isThing newR; newR ; list g nothing) // Le nothing est inutilisé
      , 
        $Let newR = list g nothing; 
        if(isThing newR ; newR ; f elem)
      )
  ,
    recurse $F g → $F r,place,list,elem →
      if( place
      ,
        $Let newR = list g nothing; 
        if(isThing newR ; newR ; f elem)
      , 
        $Let newR = f elem; 
        if(isThing newR; newR ; list g nothing) // Le nothing est inutilisé
      )
  )

/*
Recherche un élément dans la liste et retourne un Maybe: nothing si pas trouvé. f est une fonction de recherche (voir searchToUsage).
*/
$Def listSearch = $F dir, f, l → l (searchToUsage dir f) nothing
$Def fwListSearch = listSearch forward
$Def bwListSearch = listSearch backward
/* Version quand on est sûr de trouver l'élément. */
$Def listFind = $F dir, f, l → getThing \ l (searchToUsage dir f) nothing
$Def fwlistFind = listFind forward
$Def bwlistFind = listFind backward

/* 
Fournit la fonction d'usage pour remplacer un seul élément de la liste. La fonction f prend en argument un élément de la liste et retourne soit just y où y est la nouvelle valeur soit nothing si ce n'est pas l'élément à changer. Si aucun élément n'est changé retourne nothing, sinon just de la liste résultante.
L'élément changé est le premier possible rencontré, l'ordre est celui de déconstruction de la liste, il n'y a donc pas d'argument de direction.
*/
$Def mapOneToUsage  = $F f → 
  recurse $F g → $F r,place,list,x → // r est inutilisé
      $Let my = f x; 
      if(isThing my
      ,
        // Si c'est l'élément à changer, on remplace x.
        just \ toListAdd place list \ getThing my
      ,
        // Si on doit continuer, on fait le map sur list et si ce n'est pas nothing ou rajoute x où il était.
        applyJust ($F l → toListAdd place l x) (list g nothing)
      )
        
$Def maybeListMapOne = $F f,l → l (mapOneToUsage f) nothing
/*
Version où l'on revoie la liste telle quelle si il n'y a pas eu de mapping, sinon on retourne la liste modifiée.
*/
$Def listMapOne = $F f,l → maybeElse (maybeListMapOne f l)  l
/*
Version où si le maping ne se fait pas alors on ajoute un élément (x) en fin de liste.
*/
$Def listMapOneOrAdd = $F f,x,l → maybeElse (maybeListMapOne f l , toListBwAdd l x)


/*
Pareil que listFold mais pour 2 listes de même taille l et m. f prend donc deux éléments en argument (f r x y).
*/
$Def 2listFold = $F dir,f,r,l,m → 
  // On parcourt l avec comme accumulateur l'accumulateur à retourner plus la liste des éléments de m sous forme de couple $T[element, reste de la liste ]. On n'a pas besoin de marqueur de fin de liste pour cette structure car les deux listes font la même taille. 
  pairFirst \
    listFold 
      dir 
      ($F $T[r,$T[y,remainder]],x → $T[ f r x y, remainder ]
      ) 
      $T[r, listFold (¬b dir) ($F r,x → $T[x,r] ) nothing m] // le nothing n'est jamais lu
      l 

$Def fw2listFold = 2listFold forward
$Def bw2listFold = 2listFold backward
    
/*
Retourne la liste l traduite au format de liste "Maybe $T[elem,tail]" : nothing pour la liste vide et sinon l'élément  le reste de la liste.
*/
$Def listToData = $F dir,l → listFold dir ($F r,x → just $T[x,r] ) nothing l

/*
Pareil que 2listFold mais retourne nothing si une erreur est retournée, soit parce que les deux listes ont des tailles différentes, soit parce que l'opérateur de folding (qui retourne un maybe) retourne un nothing. 
*/
$Def 2listFoldMaybe = $F dir,f,r,l,m → 
  // Voir 2listFold pour le fonctionnement.
  // L'accumulateur de listFoldMaybe est un Maybe. f retourne un Maybe mais n'en prend pas un. mm est la partie de la liste à traiter, sous forme de donnée (un Maybe $T[elem, reste ], le Maybe est pour détecter la fin de liste (un nothing)).
  $Let res =
    listFoldMaybe 
      dir 
      ($F $T[r,mm],x → 
        if(isThing mm // la liste m n'est pas finie ?
        ,
          $Let $T[y,remainder] = getThing mm;
          $Let newR = f r x y;
          if( isThing newR; just $T[getThing newR,remainder]; nothing )
        , 
          nothing
        )
      ) 
      $T[r, listToData (¬b dir) m] 
      l 
  ;
  // Reste à vérifier que ce qui reste de la liste m est vide (si m était plus long on doit retourner nothing) et à retourner Maybe l'accumulateur.
  if(isThing res
  ,
    $Let $T[newR,mm] = getThing res;
    if(isThing mm; nothing; just newR)
  ,
    nothing
  )

$Def fw2listFoldMaybe = 2listFoldMaybe forward
$Def bw2listFoldMaybe = 2listFoldMaybe backward

  
  
///                                     Validité                                         

/*
`l' est-elle une liste. isList n'est pas une propriété.
*/
$Def isList = $F l → bwListFold ($F r,x→1b) 1b l
  


///                            Transformation simple de liste                            

/*
Retourne une liste : le début devient la fin
*/
$Def listReverse = $F l → bwListFold toListBwAdd ∅l l

/*
La liste constituée de l'application de `f' sur chaque élément de `l'.
*/
$Def listMap = $F f,l → fwListFold ( $F r,x → r +le f x ) ∅l l

///                                       Concaténation                                  

/*
La liste `x' à laquelle on a ajouté `y' en queue.
*/
$Def concatList = $F x,y → fwListFold toListBwAdd x y

/*
La liste `y' à laquelle on a ajouté `x' en queue.
*/
$Def concatToList = $F x,y → concatList y x

$Operator +ll infix 20 concatList

/*
Pour l une liste de listes, la concaténation des listes de l.
*/
$Def concatListList = $F l → fwListFold concatList ∅l l
    
/*
Combinaison de concatListList appliqué après un listMap.
*/
$Def concatListMap = $F f,l → concatListList \ listMap f l

///                              Opérations sur deux listes                              

/*
Comme listMap mais la fonction f prend deux arguments provenant de deux listes l et m devant être de la même longueur.
*/
$Def 2listMap = $F f,l,m → fw2listFold ( $F r,x,y → r +le f x y ) ∅l l m


///                              Combinaison de map et fold                              


/*
listFoldMap map une list en utilisant une accumulateur provenant d'un fold calculé en parallèle au map. On fournit deux fonctions : f pour le mapping et g pour l'accumulateur.
listFoldMap f g r $L[a,b,c] = $L[ f r a, f (g r a) b, f (g (g r a) b) c ].
*/
$Def listFoldMap = $F f,g,r,l → pairFirst \ fwListFold ( $F $T[rl, rAcc],x → $T[ rl +le f rAcc x , g rAcc x ] ) $T[∅l,r] l
    
///                                  "Pour tout" et "il existe"                          

/*
Pour ces fonctions nous allons définir des versions pour listes ordonnées et pour listes non ordonnées (version "Rev"). La version pour liste ordonnée est utile car il arrive que la propriété à tester ne puisse être calculée sur les éléments suivant celui qui arrête la recherche, cela peut être le cas quand on vérifie la validité d'une liste d'éléments.
D'autre part si on se contentait de la version ordonnée de ces fonctions on se passerait d'une optimisation puisque la version ordonnée a la tâche supplmentaire de remonter au premier élément de la liste avant de calculer.

*/

/*
Un "pour tout" sur une liste, prend en argument une liste `l' et une propriété `p' et exprime que tout élément de l est vérifié par `p'.
L'ordre importe car il est possible qu'un élément suivant celui provoquant l'arrêt ne soit pas calculables.
*/
$Def forAllInList = $F dir,l,p → isNothing \ listSearch dir ($F x → if(p x;nothing;just 0b)) l

$Def fwForAllInList = forAllInList forward
$Def bwForAllInList = forAllInList backward
    

/*
Notation : variables : p,x,m
Conditions : x est un word
∀l x/m; p → fwForAllInList m $F x → p

Remarque : un "L" minuscul est accolé au quantificateur.

Notation : variables : p,x,m
Conditions : x est un word
∀lr x/m; p → bwForAllInList m $F x → p

*/

/*
Applique "and" sur tous les éléments de la liste.
*/
$Def listAnd = $F l → ∀l x/l; x
      
/*
Un "il existe" sur une liste prend en argument une liste `l' et une propriété `p' et exprime qu'un élément de l est vérifié par `p'.
*/
$Def existsInList = $F dir,l,p → isThing \ listSearch dir ($F x → if(p x;just 0b;nothing)) l

$Def fwExistsInList = existsInList forward
$Def bwExistsInList = existsInList backward


/*
Notation : variables : p,x,m
Conditions : x est un word
∃l x/m; p → fwExistsInList m $F x → p

Notation : variables : p,x,m
Conditions : x est un word
∃lr x/m; p → bwExistsInList m $F x → p
*/
    
/*
Applique "or" sur tous les éléments de la liste.
*/
$Def listOr = $F l → ∃l x/l; x
    
///                                         Égalité                                      
    
/*
Retourne si les listes `l1' et `l2' sont égales suivant la fonction de comparaison `eq'.
*/
$Def listEqual = $F eq,l1,l2 → 
  // Le 1b des "just 1b" n'a aucunne d'importance.
  isThing \ fw2listFoldMaybe ($F r,x1,x2 → if(eq x1 x2; just 1b; nothing) ) (just 1b) l1 l2
 
   
///                                Appartenance à une liste                              

/*
Pour une comparaison `eq', la liste `l' contient-elle un élément comparable à x.
*/
$Def listOwns = $F eq,l,x → bwExistsInList l ( eq x )

///                                        Unicité                                       

/*
La liste ne contient pas de doublon ?
*/
$Def noDoubleInList = $F eq,l → isEmptyList l ∨b isThing \ 
  bwListFoldMaybe 
  ( 
    $F l,x → if ( listOwns eq l x , nothing , just \ listInit l ) ; 
    listInit l ; 
    l 
  )


///                                 filtrage de liste                                    


/*
Sélectionner les éléments de la liste `l' vérifiant la propriété p.
*/
$Def listFilter = $F p,l → fwListFold ($F res,x → if ( p x , res +le x , res )) ∅l l
    
/*
Comme listFilter mais en plus map les éléments sélectionnés.
*/
$Def listFilterMap = $F p,f,l → fwListFold ($F res,x → if ( p x , res +le f x , res )) ∅l l

/*
D'une liste de Maybe , ignore les nothing et retourne les choses dans les just, dans le même ordre.
*/
$Def listGetThings = $F l →
  fwListFold ($F r,m → if( isThing m, r +le getThing m, r) ) ∅l l
    
/*
Applique un maping retournant un Maybe sur une liste puis un listGetThings.
*/
$Def getThingsListMap = $F f,l → listGetThings \ listMap f l

/*
Applique un maping retournant un Maybe sur une liste puis retourne just le premier  élément rencontré ou nothing si la liste ne contient que des nothing.
*/
$Def listMapGetMaybeOne = $F f,l → maybeOneInList \ getThingsListMap f l

///                                      listRemoveList                                  
    
    
/*
La liste l1 moins les éléments se trouvant dans l2 suivant l'égalité eq.
*/
$Def listRemoveList = $F eq,l1,l2 → listFilter ( $F x → ¬b listOwns eq l2 x ) l1

///                                 Liste de booléens                                    

/*
l est-elle une liste binaire.
*/
$Def isBoolList = $F l → isList l ∧b ∀lr x/l; isBoolean x


///                Fonction définie par une liste (fonction par liste)                   
    
// Une liste de paires (argument,image) peut servir pour définir (en extension) une fonction.


/*
Retourne la liste des images de x.
*/
$Def listFuncToImages = $F eq,l,x → listFilterMap ( $F e → eq x \ pairFirst e ) pairSecond l
    
/*
Utilise une liste l comme une fonction définie en extension.
*/
$Def listFuncToFunc = $F eq,l,x → oneInList \ listFuncToImages eq l x


/*
Utilise une liste l comme une fonction définie en extension et retournant nothing si l'argument est hors domaine et sinon "just image".
*/
$Def listFuncToFuncMaybe = $F eq,l,x → maybeOneInList \ listFuncToImages eq l x
    
/*
Domaine d'une fonction par liste, retourné sous forme d'une liste.
*/
$Def listFuncDomain = $F l → listMap pairFirst l

/*
Teste si une chose est dans le domaine.
*/
$Def listFuncInDomain = $F eq,l,x → ∃lr a/l; eq x \ pairFirst a

/*
Codomaine d'une fonction par liste.
*/
$Def listFuncCodomain = $F l → listMap pairSecond l

/*
Teste si une chose est dans le codomaine.
*/
$Def listFuncInCodomain = $F eq,l,x → ∃lr a/l; eq x \ pairSecond a

/*
Réduit une fonction par un filtre p sur son domaine.
*/
$Def listFuncReduce = $F l,p → listFilter ( $F x → ¬b p ( pairFirst x ) ) l
     
    
/*
Ajoute le couple (a,i) à la liste par fonction.
*/
$Def listFuncAdd = $F f,a,i → f +le $T[ a , i ]

/*
Pareil que listFuncAdd mais la liste est en dernier.
*/
$Def addTolistFunc = $F a,i,f → f +le $T[ a , i ]
    

/*
Retourne l'union de deux fonctions, en cas de conflit l'image par g l'emporte. L'ordre dans la liste résultante est g suivi des éléments de f dans l'ordre inverse.
*/
$Def listFuncMerge = $F eq,f,g → bwListFold ( $F g,p@$T[x,_] → if (listFuncInDomain eq g x, g, g +le p) ) g f

/*
Renvoie une fonction similaire à f mais où la valeur "x" associée à "a" est remplacée par "g x", pour le premier "a" trouvé en commençant par la fin de la liste. L'ordre des éléments de la liste est concervé. Si "a" n'est pas trouvée alors la liste est inchangée.
*/
$Def listFuncMod = $F eq,f,a,g → listMapOne ( $F $T[x,y] → if(eq a x; just $T[a,g y];nothing) ) f

/*
Renvoie une fonction similaire à f mais où a est envoyé sur i, que a soit déjà dans le domaine de f ou non. L'ordre de la liste est respectée, si f comporte plusieurs éléments "a" dans le domaine un seul est modifié.
*/
$Def listFuncSet = $F eq,f,a,i → listMapOneOrAdd ( $F $T[x,y] → if(eq a x; just $T[a,i];nothing) ) $T[a,i] f 

/*
Comme map sur les listes, mais là ce sont les images de la fonction qui sont mappées.
*/
$Def listFuncMap = $F f,l → listMap ( $F $T[x,y] → $T[ x ,f y] ) l

/*
Définition informelle : super fonction :
Une super fonction est une fonction où les images sont considérées comme des fonctions. 
*/
  
///                                                                                      
///                                      Les mots                                        
///                                                                                      


///                                Définition des mots                                   

// Nous utiliserons des listes de booléens pour représenter les mots.

/*
Être un mot.
*/
$Def isWord = isBoolList
    
/*
Égalité entre deux mots.
*/
$Def wordEqual = listEqual boolEqual

$Operator =w infix 20 wordEqual

/*
Inégalité entre deux mots.
*/
$Def wordNotEqual = $F x,y → ¬b x =w y

$Operator ≠w infix 20 wordNotEqual

///                       Notation pour la représentation des mots                       

/*    
Nous pouvons utiliser n'importe quelle liste de 0 et 1 pour définir un mot. Il nous faut une notation pour faire correspondre ces listes binaires aux "word".

Nous utiliserons le codage informatique ``UTF-8'' qui permet de traduire chaque caractère en un nombre binaire entre 8 et 32 bits. Nous traduirons un mot par la concaténation des listes représentant les caractères de ce mot, le premier caractère étant en tête de liste.

Notation : l 
condition : l est un mot
'l → $L[ LB ]

où LB est la liste booléenne représentant le mot en UTF-8. Par exemple `AB' se traduit en 
$L [ 0,1,0,0,0,0,0,1,0,1,0,0,0,0,1,0 ] car en UTF-8 'A' a le code 65 et 'B' 66.
*/
