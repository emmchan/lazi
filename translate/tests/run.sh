#!/bin/bash
# Run all tests for lazi-translate.
# Configure the path of lazi-translate by exporting TRANSLATE_EXEC

# export CHEAT_MODE=1 : if the output is not equal to the model then ask to copy the output to the model.
if [ -z "$CHEAT_MODE" ]; then export CHEAT_MODE=0; fi

export DEFAULT_INC_OPTS="-i ../../mathematic/01-Lazi-1/01-tools-1.def.lazi -i ../../mathematic/01-Lazi-1/02-tools-2.def.lazi -i ../../mathematic/01-Lazi-1/03-types.def.lazi -i ../../mathematic/01-Lazi-1/04-formula.def.lazi"

if [ -z "$TRANSLATE_EXEC" ]; then
  export TRANSLATE_EXEC=$(which lazi-translate)
  if [ -z "$TRANSLATE_EXEC" ]; then
    if [ -f "../lazi-translate" ]; then
      export TRANSLATE_EXEC=../lazi-translate
    else
      echo "lazi-translate not found, set \$TRANSLATE_EXEC env. please."
    fi  
  fi
fi
if [ -z "$TMPDIR" ]; then
  export TMPDIR="/tmp"
fi

# Touch this file in test script if there is an error (so temporary files will not be deleted).
export TOUCH_IF_ERROR=$(mktemp -u $TMPDIR/lazi-test-XXXXXXXXX)

# To avoid to create these for each test:
export TMP_DIR=$(mktemp -d $TMPDIR/lazi-test-XXXXXXXXX)
export TMP_FILE=$(mktemp $TMPDIR/lazi-test-XXXXXXXXX)

function trans
{
  FAILURE="\e[1m\e[41m-=~_->\e[0m Failure : \e[1m$ID\e[0m"
  if ! $TRANSLATE_EXEC -f $FMT -x $IN -o $OUT $OPTS; then
    touch $TOUCH_IF_ERROR
    echo -e "$FAILURE"
    echo "lazi-translate error on $IN, see above."
    exit 1
  fi
  if ! cmp -s $OUT $GOOD; then
    touch $TOUCH_IF_ERROR
    echo -e "$FAILURE"
    echo "Wrong output file = $OUT"
    echo "Good output file  = $GOOD"
    if [ $CHEAT_MODE -eq 1 ]; then
      echo "Copy output to model ? [Y/n]"
      read -e answer
      if [ "$answer" = "Y" -o "$answer" = "y" -o -z "$answer" ]; then
        cp "$OUT" "$GOOD"
        echo "Copied."
      fi
    fi
    exit 1
  fi
}
export -f trans

run-parts --exit-on-error -v tests.d
if [ -f $TOUCH_IF_ERROR ]; then
  rm $TOUCH_IF_ERROR
  exit 1
else
  rm -r $TMP_DIR $TMP_FILE
  echo "Succes"
  exit 0
fi
