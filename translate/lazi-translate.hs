-- | Read an xml file and translate all formulas from one format to an other.
-- | <formula> tags are searched anywhere, so it's independant of the xml format file, except that formulas
-- | must be enclosed with the <formula format="..."></formula> tag.
-- | Supported formats: xml, sources, latex, unicode

module Main where

import Control.Monad hiding (when)
import Text.Show
import Data.Char
import Data.List
import Data.Either
import System.Environment
import System.Exit
import System.IO
import Control.Exception.Base hiding (try)
import Text.ParserCombinators.Parsec hiding (try)
import Text.ParserCombinators.Parsec.Char
import Text.Parsec.Prim
import Control.Monad.Error.Class
import System.FilePath.Posix

import LaziTranslate
import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.Formulas.ComputeFm.Definitions
import LaziTranslate.Sources
import qualified LaziTranslate.ParsRend.Language.Xml.Parser as XmlP
import qualified LaziTranslate.ParsRend.Language.Sources.Parser as SourcesP
import qualified LaziTranslate.ParsRend.Language.Xml.Renderer as XmlR
import qualified LaziTranslate.ParsRend.Language.Sources.Renderer as SourcesR
import qualified LaziTranslate.ParsRend.Language.SourcesComputeFm.Renderer as SourcesC

import LaziTranslate.ParsRend.CommonParser
import LaziTranslate.FormulaXML

version :: String
version="0.1"

data Options = Options
    {
      optOutputAll   :: Bool
    , optOutputLazi1  :: String
    , optOutput      :: String
    , optXmlFile     :: String
    , optIncludes    :: [String]
    , optFormat      :: Format 
    , optOptimise    :: Bool
    , optMessage     :: Maybe String -- store a message to return
    , optError       :: Bool -- bad option, we must return a failure code
    } deriving Show
    
defaultOptions    = Options
    {
      optOutputAll   = False
    , optOutputLazi1  = ""
    , optOutput      = ""
    , optXmlFile     = ""
    , optIncludes    = []
    , optFormat      = FormatNotDef
    , optOptimise    = False
    , optMessage     = Nothing
    , optError       = False
    }
    
-- | Read the list of argument,  return a Right Options or a Left String which is a message to print.
getOptions :: [ String ] -> Options -> Options
getOptions ("-a" : l) opts = getOptions l (opts { optOutputAll = True, optFormat = FormatXML } )
getOptions ("-a1" : name : l) opts = getOptions l (opts { optOutputLazi1 = name, optFormat = FormatComputeFm } )
getOptions ("-x" : file : l) opts = getOptions l (opts { optXmlFile = file } )
getOptions ("-i" : file : l) opts = getOptions l (opts { optIncludes = optIncludes opts ++ [file] } )
getOptions ("-o" : file : l) opts = getOptions l (opts { optOutput = file } )
getOptions ("-s" : l) opts = getOptions l (opts { optOptimise = True } )
getOptions ("-f" : "xml" : l) opts = getOptions l (opts { optFormat = FormatXML } )
getOptions ("-f" : "sources" : l) opts = getOptions l (opts { optFormat = FormatSources } )
getOptions ("-f" : "computeFm" : l) opts = getOptions l (opts { optFormat = FormatComputeFm } )
getOptions ("-f" : "lazi0a" : l) opts = getOptions l (opts { optFormat = FormatLazi_0_0 } )
getOptions ("-f" : "lazi0aDepth" : l) opts = getOptions l (opts { optFormat = FormatLazi_0_0Depth } )
getOptions("-v" : l) opts = opts { optMessage = Just ("lazi-translate V " ++ version) }
getOptions("-h" : l) opts = opts { optMessage = Just usage }
getOptions("--help" : l) opts = opts { optMessage = Just usage }
getOptions(x : l) opts = opts { optError = True, optMessage = Just ("lazi-translate : Unknown argument \"" ++ x ++ "\"\n\n" ++ usage)  }
getOptions [] opts = case optFormat opts of
                        FormatNotDef -> opts { optError = True, optMessage = Just ("lazi-translate : Option -f is mandatory.\n\n" ++ usage)  }
                        _ -> opts

usage = "lazi-translate  -f FORMAT [ -i FILE ] ... [ -i FILE ] -a [ -x FILE ] [ -o FILE ] [ -s ] [ -v ] [ -h ] [ --help ] \n\
\\n\
\-a : all definitions in lazi sources files specified by the -i option are output in the output file (see -o), the output format is XMLDefs with formulas in XML. Options -f  ans -x have then no effect.\n\
\-a1 name : like -a (and exclusive with -a), the output is for lazi.1: a defined variable (name) with a value containing all the sources.\n\
\-x FILE : xml file to translate, STDIN if the option is not given \n\
\-i FILE ... -i FILE : sources files to include.\n\
\-o FILE : ouput file, STDOUT if the option is not given \n\
\-f FORMAT : set the output format, can be : sources,xml,computeFm,lazi0a or lazi0aDepth \n\
\-s : Simplify code when it come from \"sources\" format. Usefull to optimise before compute, but not for a readable translation.\n\
\-h --help : print this usage message and exit. \n\
\-v : print version and exit. \n\
\ "



-- | Run a parser on a file content or stdin. Same as the parsec's function but use STDIN if the file name is empty.
-- | ParsecUS is the user stated needed to parse : operators informations and already defined names.
parseFromFileOrStd :: LazParsec a -> ParsecUS -> String -> IO (Either ParseError a)
parseFromFileOrStd p us fname = do
        input <- if fname == "" then getContents else readFile fname
        return (runP p us (if fname == "" then "STDIN" else fname) input)

-- | Parse the files to include and return a Sources containing the concatenation of all files contents.
parseIncludes :: LazParsec Sources -> [String] -> IO (Either ParseError Sources)
parseIncludes p lf = parseIncludesRec p lf []

parseIncludesRec :: LazParsec Sources -> [String] -> Sources -> IO (Either ParseError Sources)
parseIncludesRec p [] s = return $ Right s
parseIncludesRec p (f:l) s = do
  result <- parseFromFileOrStd p (getUS s) f
  case result of 
    Right new_s -> parseIncludesRec p l (s ++ new_s)
    err -> return err 
  

-- | Same as the System.IO's writeFile but can use stdout if the finename is empty
-- | Write a String in a file or stdout if the filepath is empty.
writeFileOrStd :: FilePath -> String -> IO ()
writeFileOrStd f txt = withFileOrStd f WriteMode (\ hdl -> hPutStr hdl txt)

-- | Same as the System.IO's withFile but can use stdout if the finename is empty
withFileOrStd :: FilePath -> IOMode -> (Handle -> IO r) -> IO r
withFileOrStd name mode = bracket (if name=="" then return stdout else openFile name mode) hClose

main = do
    args <- getArgs 
    let opts=getOptions args defaultOptions
    case optMessage opts of
      -- If we have just to print a message.
      Just mess -> 
        if optError opts 
          then do
          hPutStrLn stderr mess
          exitFailure
          else do
          putStrLn mess
          exitSuccess
      Nothing ->
        do
          -- Get the sources from the "includes" the list of sources files.
          result <- parseIncludes (SourcesP.sources $ optOptimise opts) (optIncludes opts)
          case result of 
            Left err -> do
              hPutStrLn stderr ( "lazi-translate error : " ++ (show err) )
              exitFailure
            Right s -> -- s is a Sources structure of all files to include
              -- Check for errors
              let err  = checkSources s in
              if not $ null err
                then
                  do
                    hPutStrLn stderr ( "lazi-translate error in sources : " ++ err )
                    exitFailure
                else
                  -- Do we have to translate all the includes in a XMLDefs/XML file ?
                  if optOutputAll opts
                  then
                    -- take all the definitions from the sources, translate to xml and write it.
                    writeFileOrStd (optOutput opts) $ definitionsToXml FormatXML (getUS s) (getDefinitions s)
                  else
                    -- Do we have to translate all the includes in a XMLDefs/XML Lazi1 file (all defs in one 1-formulas in a structure)?
                    if not (null $ optOutputLazi1 opts) 
                    then
                      writeFileOrStd (optOutput opts) $ definitionsToComputeFmTxt $ getDefinitions $ sourcesToLazi1 (optOutputLazi1 opts) s
                    else
                      do
                        -- Translate formula from a xml file (can be stdin).
                        result <- parseFromFileOrStd (parseXml (optOptimise opts) (optFormat opts)) (getUS s) (optXmlFile opts)
                        case result of 
                          Right st -> writeFileOrStd (optOutput opts) st 
                          Left err -> do
                              hPutStrLn stderr ( "lazi-translate error : " ++ (show err) )
                              exitFailure

definitionsToComputeFmTxt :: [Definition] -> String
definitionsToComputeFmTxt l = intercalate "\n\n" $ map definitionToComputeFmTxt l

definitionToComputeFmTxt :: Definition -> String
definitionToComputeFmTxt (Definition name val) = "$Def " ++ name ++ " = " ++ SourcesC.renderExpr val

  
