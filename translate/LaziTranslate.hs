module LaziTranslate where

import Data.Map

import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.Sources
import LaziTranslate.ParsRend.CommonParser
import qualified LaziTranslate.ParsRend.Language.Xml.Parser as XmlP
import qualified LaziTranslate.ParsRend.Language.Sources.Parser as SourcesP
import qualified LaziTranslate.ParsRend.Language.Xml.Renderer as XmlR
import qualified LaziTranslate.ParsRend.Language.SourcesComputeFm.Renderer as SourcesC
import qualified LaziTranslate.ParsRend.Language.Sources.Renderer as SourcesR
import qualified LaziTranslate.ParsRend.Language.Lazi_0_0.Renderer as Lazi_0_0R


data Format = FormatXML | 
               FormatSources | 
               FormatComputeFm | -- Like Sources, but without notations, just basic ComputeFm
               FormatLazi_0_0 | -- Lazi.0.a : just keywords & applications
               FormatLazi_0_0Depth | -- same as FormatLazi_0_0, used to print just the depths of formulas.
               FormatNotDef deriving Eq

-- | Return the corresponding renderer for this format
formatToRend :: Format -> ParsecUS -> ( ComputeFm -> String )
formatToRend f us  =
    case f of
         FormatXML -> XmlR.renderExpr
         FormatSources -> SourcesR.renderExpr (keysSet $ usDefinitions us) (usOperatorsInfos us)
         FormatComputeFm -> SourcesC.renderExpr
         FormatLazi_0_0 -> Lazi_0_0R.renderExpr $ usDefinitions us
         FormatLazi_0_0Depth -> Lazi_0_0R.renderExprDepth $ usDefinitions us

-- | Return the corresponding parser of formula for this format
-- | The first argument is the "simplify" global option (-s).
formatToPars :: Bool -> Format -> ( LazParsec ComputeFm )
formatToPars s f =
    case f of
         FormatXML -> XmlP.totalExpression
         FormatSources -> SourcesP.parseToComputeFm s
         FormatComputeFm -> SourcesP.parseToComputeFm s -- The Sources parser work with FormatComputeFm

instance Show Format where
    show FormatXML = "xml"
    show FormatSources = "sources"
    show FormatComputeFm = "sources computeFm"
    show FormatLazi_0_0 = "sources Lazi.0.a"
    show FormatLazi_0_0Depth = "sources Lazi.0.a/depth"
    show FormatNotDef = "not defined!"
