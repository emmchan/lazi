-- | In Lazi'n a variable can be a name but also a vector of names. We add a structure to support this feature.

module LaziTranslate.Formulas.SourcesFm.VarExp  where

import Data.Set hiding (map)
import Data.List (map,all)

import LaziTranslate.Formulas.ComputeFm.Word

-- | Represent a variable or a vector of variables.
data VarExp = 
  -- ^ Normal variable
  VWord String |
  -- ^ Assign a variable to a varexp
  VAssign String VarExp  |
  -- ^ Tuple
  VTuple [ VarExp ]  |
  -- ^ A dictionnary
  VDict [ (String , VarExp) ] |
  -- ^ An object
  VObj [ (String , VarExp) ] |
  -- ^ List 
  VList [ VarExp ] |
  -- ^ List add : init and last
  VListAdd VarExp VarExp
         deriving (Eq,Show)


-- | Get the variable(s) name(s) (as String) from a VarExp.
getVarNames :: VarExp -> Set String 

getVarNames (VWord v) = singleton v

getVarNames (VAssign v ve) = insert v $ getVarNames ve

getVarNames (VTuple l) = getListVarNames l

getVarNames (VDict l) = unions $ map ( \(s,ve) -> getVarNames ve ) l

getVarNames (VObj l) = unions $ map ( \(s,ve) -> getVarNames ve ) l

getVarNames (VList l) = getListVarNames l

getVarNames (VListAdd ve1 ve2) = union (getVarNames ve1) (getVarNames ve2)

-- | Same as getVarNames, but for a list of VarExp
getListVarNames :: [VarExp] -> Set String 
getListVarNames lv = unions $ map getVarNames lv


-- --------------------------------------------------------------------------------
-- | Try to concatenate "$L[a1 ... an ] :l b" to $L[a1,...,an,b]

concatVarExpList :: VarExp -> VarExp

concatVarExpList (VListAdd (VList l) la) = VList (l ++ [la])

concatVarExpList x = x

-- ---------------------------------------------------------------------------------
-- | Clean the varexp by simplification, for exemple empty list are moved.

cleanEmptyVE :: VarExp -> VarExp

cleanEmptyVE (VAssign v vl) | isEmptyVE vl = VWord v
cleanEmptyVE (VAssign v vl) = VAssign v (cleanEmptyVE vl)
cleanEmptyVE (VTuple l) = VTuple $ map cleanEmptyVE l
cleanEmptyVE (VDict l) = VDict $ map (\(k,v) -> (k,cleanEmptyVE v)) l
cleanEmptyVE (VObj l) = VObj $ map (\(k,v) -> (k,cleanEmptyVE v)) l
cleanEmptyVE (VList l) = VList $ map cleanEmptyVE l
cleanEmptyVE (VListAdd x y) = VListAdd (cleanEmptyVE x) (cleanEmptyVE y)
cleanEmptyVE x = x

-- ---------------------------------------------------------------------------------
-- | Is the varexp empty ?

isEmptyVE :: VarExp -> Bool

isEmptyVE (VTuple l) = all isEmptyVE l
isEmptyVE (VDict []) = True
isEmptyVE (VObj []) = True
isEmptyVE (VList l) = all isEmptyVE l
isEmptyVE (VListAdd v1 v2) = isEmptyVE v1 && isEmptyVE v2
isEmptyVE _ = False

--------------------------------------------------------
    
-- | Return Just the object's varExp from the readen varExp (the varexp $O(here)[...]), or Nothing if it's not a good varExp. The set is the set of keys of the dictionary. We check that the varExp don't collide with keys.
getObjVarExp :: Set String -> VarExp -> Maybe VarExp

-- $O(this)[...]
getObjVarExp ks (VWord this) = 
  checkObjVarExp ks $ VAssign (this) (VObj [])

-- $O($O[...])[...]
getObjVarExp ks (VObj dl) = 
  checkObjVarExp ks $ VAssign "this" (VObj dl)

-- $O(this@$O[...])[...] or error, no completion to do.
getObjVarExp ks ve = checkObjVarExp ks ve


-- | Check if a varExp of an object is valid. Ses getObjVarExp
checkObjVarExp :: Set String -> VarExp -> Maybe VarExp

checkObjVarExp ks v@(VAssign this (VObj _)) =
  if notMember this ks 
  then Just v
  else Nothing

checkObjVarExp ks _ = Nothing

