-- | Some utilities to find things in a ComputeFm

module LaziTranslate.Formulas.SourcesFm.FromCompute.VarExpUtilities  where

import qualified Data.Set as S

import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as C
import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.SourcesFm.VarExp



-- --------------------------------------------------------------------------------
--                        Main functions to recognise varexp
-- --------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------
-- | Recognise the structure access in arguments of functions. Return the identifiers list of the access (its type depends on the structure) and the rest of the expression. Arguments are:
-- | * The name of the variable for the structure
-- | * The function that recognise the expression and return the corresponding identifier
-- | * The expression to parse
-- | Return :
-- | ( The list of found identifier ,
-- | The rest of the expression )
recogniseArgs :: (C.ComputeFm -> Maybe t) -> C.ComputeFm -> ([t],C.ComputeFm)

recogniseArgs recog xy@(C.Apply x y) = 
  case recog y of
       Nothing -> ([],xy)
       Just i -> let (l,r)=recogniseArgs recog x in (l ++ [i],r)

recogniseArgs _ x = ([],x)



-- --------------------------------------------------------------------------------
-- | Given the result of recogniseArgs (execpt the expression is now a SourcesFm), try to find the varexp corresponding to the identifiers. 
-- | If success return :
-- | Just ( the list of ( identifier, varexp) , the rest of the expression (a SourcesFm) ).
-- | If failure return Nothing.
associateVarexp :: [t] -> SourcesFm -> Maybe ([(t,VarExp)],SourcesFm)

associateVarexp l (FunctionM vl f) = 
  let lengthL = length l
      lengthVl = length vl 
  in
    if lengthVl < lengthL
      then 
        Nothing
      else
        let (vl1,vl2) = splitAt lengthL vl in
          Just ( zip l vl1 , if null vl2 then f else (FunctionM vl2 f) )

associateVarexp _ _ = Nothing

-- --------------------------------------------------------------------------------
-- | Given :
-- | * the name of the variable corresponding to the structure,
-- | * the varexp
-- | * the rest of the expression as returned by associateVarexp
-- | Return the final expression (the overall convertion)
finalVarExpFunc :: String -> VarExp -> SourcesFm -> SourcesFm

finalVarExpFunc v ve f =
  FunctionM [(if varUsed v (FunctionM [ve] f) then (VAssign v) else id) ve] f
  



-- --------------------------------------------------------------------------------
--                        Secondary functions 
-- --------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------
-- | General function to walk in a formula and return a result. We give a variable because we must not go in functions having this variable. Argument:
-- - the function to apply to each part of the formula
-- - the variable to avoid
-- - compose 2 results

walkInFormula :: (C.ComputeFm -> t) -> String -> (t -> t -> t) -> C.ComputeFm ->  t

walkInFormula test var add f@(C.Apply x y) = 
  test f `add` walkInFormula test var add  x `add` walkInFormula test var add  y

walkInFormula test var add f@(C.Function v body) | v/=var =
  test f `add` walkInFormula test var add body

walkInFormula test var add f@(C.Dict l) = foldl (\r (_,e) -> r `add` walkInFormula test var add e) (test f) l
  
walkInFormula test var add f@(C.Obj this l) | this/=var = foldl (\r (_,e) -> r `add` walkInFormula test var add e) (test f) l

walkInFormula test var add f = test f


-- --------------------------------------------------------------------------------
-- | Specialization of walkInFormula when we search for an expression ((C.wd n) C.&/ (C.wv v)). n is the first argument, v the second.

exprIsUsed :: String -> String -> C.ComputeFm -> Bool

exprIsUsed n v f = walkInFormula ( (==) (C.wd n C.&/ C.wv v ))  v (||) f


-- --------------------------------------------------------------------------------
-- | Specialization of walkInFormula when we search entries looked up for a dictionnary v

lookedUpEntries :: String -> C.ComputeFm -> S.Set String

lookedUpEntries v f = walkInFormula getDictApplyEntry v S.union f
  where
    getDictApplyEntry (C.Apply (C.Apply (C.Word WtDVar "dictApply") (C.Word WtLVar v)) (C.NameRepr entry)) = S.singleton entry
    getDictApplyEntry f = S.empty


-- --------------------------------------------------------------------------------
-- | For a formula of the shape f (v .di `n1') ... (v .di `nn')
-- | return the list of n1,...nn entries names and the rest of the expression (f).
-- | The first argument is the variable name (v)

dictArgsToEntries :: String -> SourcesFm -> ([String],SourcesFm)

dictArgsToEntries v (Apply f (Apply (Apply (Word WtDVar "dictApply") (Word WtLVar w)) (NameRepr entry))) | w==v = 
  let (l,g) = dictArgsToEntries v g in (l ++ [entry],g)
      
dictArgsToEntries _ f = ([],f) -- if no match


-- --------------------------------------------------------------------------------
-- | For a formula of the shape $F a1,...,an -> f and a list of entries, make pairs of (entry,varExp). Also return the not consumed part (generaly f). It's not a problem if we don't empty the list of entries.

assembleEntriesVars :: [String] -> SourcesFm -> ([(String,VarExp)], SourcesFm)

assembleEntriesVars [] f = ([],f)

assembleEntriesVars (e:le) (FunctionM (v:lv) f) = 
  let (lev,g) = assembleEntriesVars le (FunctionM lv f) in ((e,v):lev,g)

assembleEntriesVars _ (FunctionM [] f) = ([],f)

assembleEntriesVars _ f = ([],f)


-- --------------------------------------------------------------------------------
-- | For a ComputeFm to convert to a varExp $D[] :
-- | Search entries (entry name, corresponding variable) in a formula already in the good shape : ($F a1, ..., an -> f) (v .di `n1') ... (v .di `nn'). Return also the rest (f).

dictEntriesAsVar :: String -> SourcesFm -> ([(String,VarExp)],SourcesFm)

dictEntriesAsVar v f =
  let (entries,f2) = dictArgsToEntries v f in assembleEntriesVars entries  f2

  
-- --------------------------------------------------------------------------------
-- | For entries of a dictionary which are accessed but not as a notation shape, replace the access by a variable to transform into the good shape.
-- | Arguments are :
-- | - the variable representing the dictionary
-- | - the set of entry access to replace
-- | - the formula to modifie
-- | - Return : ( the list of (entry,var) pairs, the rest of the formula (the body of the created function).

putDictEntriesAsVar :: String -> [String] -> SourcesFm -> ([(String,VarExp)],SourcesFm)

putDictEntriesAsVar v [] f = ([],f)

putDictEntriesAsVar v (e:le) f = 
  -- "a" is the new variable to replace the entry access, preferably the same name as the entry.
  let a = newVarFrom e f
      f2 = sourcesReplace (wd "dictApply" &/ wv v &/ NameRepr e) (S.singleton v) (wv a) f
      (lev,f3) = putDictEntriesAsVar v le f2
    in
      ((e,VWord a):lev , f3) -- order of the list does not count
      
