-- -------- | Translate a ComputeFm to a function argument to a varexp.

module LaziTranslate.Formulas.SourcesFm.FromCompute.VarExp (toVarExp) where
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as C
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.Formulas.SourcesFm.FromCompute.VarExpUtilities

-- --------------------------------------------------------------------------------
-- | Recognise a varExp
-- arguments:
-- * conv : the convertion function from SourcesFm to ComputeFm, used to recurse
-- * the formula to convert (a C.Function).

toVarExp :: (C.ComputeFm -> SourcesFm) -> C.ComputeFm -> SourcesFm

-- $T[...],  We don't recognise 1-tuple.
toVarExp conv f@(C.Function v body@(C.Apply _ (C.Apply (C.Word WtDVar "pairSecond") _))) =
  toVarExpGen conv (toVarExpDefault conv f) f recog isGoodArgs computeVe
  where
    recog :: C.ComputeFm -> Maybe Int
    recog (C.Apply (C.Word WtDVar "pairFirst") x) = 
      case recog' x of
        Just n -> Just $ n+1
        Nothing -> Nothing
    recog (C.Apply (C.Word WtDVar "pairSecond") x) = 
      case recog' x of
        Just n -> Just $ n+2
        Nothing -> Nothing
    recog _ = Nothing
    -- Count the number of embeded "pairSecond"
    recog' :: C.ComputeFm -> Maybe Int 
    recog' (C.Word WtLVar v2) |v2==v = Just 0
    recog' (C.Apply (C.Word WtDVar "pairSecond") x) =
      case recog' x of
        Just n -> Just $ n+1
        Nothing -> Nothing
    recog' _ = Nothing
    -- Recognise a well ordered args list
    isGoodArgs :: [Int] -> Bool
    isGoodArgs l = isGoodArgs' 1 l
    -- Recognise a well ordered args list starting at n
    isGoodArgs' :: Int -> [Int] -> Bool
    isGoodArgs' n [m]|m==n = True
    isGoodArgs' n (m:l)|m==n = isGoodArgs' (n+1) l
    isGoodArgs' _ _ = False -- Can be an empty list or bad value.
    -- Compute the VarExp corresponding to the tuple
    computeVe :: [(Int,VarExp)] -> VarExp
    computeVe l = VTuple $ snd $ unzip l

-- li :l la or $L[la]
-- We also try to concatenate "$L[a1, ..., an ] :l la" to $L[a1,...,an,la]
toVarExp conv f@(C.Function v body@(C.Apply _ (C.Apply (C.Word WtDVar gw ) _))) | (gw == "listLast" || gw == "listInit")  =
  toVarExpGen conv (toVarExpDefault conv f) f recog isGoodArgs computeVe
  where
    recog :: C.ComputeFm -> Maybe Int
    recog (C.Apply (C.Word WtDVar "listLast") (C.Word WtLVar v1)) | v1==v = Just 0
    recog (C.Apply (C.Word WtDVar "listInit") (C.Word WtLVar v1)) | v1==v = Just 1
    recog _ = Nothing
    isGoodArgs :: [Int] -> Bool
    isGoodArgs [0,1] = True
    isGoodArgs [0] = True
    isGoodArgs _ = False
    computeVe :: [(Int,VarExp)] -> VarExp
    computeVe [(0,la),(1,li)] = concatVarExpList $ VListAdd li la
    computeVe [(0,la)] = VList [la]
    
-- $D[ n1 = a1,..., nn = an ]
toVarExp conv f@(C.Function v body@(C.Apply _ (C.Apply (C.Apply (C.Word WtDVar "dictApply") (C.Word WtLVar _)) (C.NameRepr _)))) =
  toVarExpGen conv (toVarExpDefault conv f) f (recogDict v) isGoodArgsDict (VDict)


-- $O[ n1 = a1,..., nn = an ]:
toVarExp conv f1@(C.Function a (C.Apply f2@(C.Function v body@(C.Apply _ (C.Apply (C.Apply (C.Word WtDVar "dictApply") (C.Word WtLVar _)) (C.NameRepr _))))(C.Apply (C.Word WtLVar a1) (C.Word WtLVar a2)))) | a==a1 && a==a2  = 
  toVarExpGen conv  (toVarExpDefault conv f1) f2 (recogDict v) isGoodArgsDict (\l -> VAssign a (VObj l))

-- @ (when the VAssign is used on a VWord or an other VAssign, because the other cases are handled above)
toVarExp conv (C.Function v1 (C.Apply f2@(C.Function _ body) (C.Word WtLVar x))) | v1 == x = 
  let  (FunctionM (fv:lfv) body) = conv f2 in
    FunctionM ((VAssign v1 fv):lfv) body

-- If nothing special is recognised.
toVarExp conv f = toVarExpDefault conv f

-- Function for the dictionnary and object case.
recogDict :: String -> C.ComputeFm -> Maybe String
recogDict v (C.Apply (C.Apply (C.Word WtDVar "dictApply") (C.Word WtLVar v1)) (C.NameRepr entry)) | v1==v = Just entry
recogDict _ _ = Nothing
isGoodArgsDict :: [String] -> Bool
isGoodArgsDict (_:_) = True -- We accept multiple same names, all non-empty list is ok
isGoodArgsDict _ = False

-- --------------------------------------------------------------------------------
-- | Default toVarExp, when nothing is recognised.
-- | conv : the convertion function used to recurse
toVarExpDefault :: (C.ComputeFm -> SourcesFm) -> C.ComputeFm -> SourcesFm
toVarExpDefault conv (C.Function v body) = FunctionM [VWord v] $ conv body

-- --------------------------------------------------------------------------------
-- | Generic toVarExp, when the analyze can be described with the 3 functions (the 3 last arguments):
-- * recog :: C.ComputeFm -> Maybe t
--    Recognise an argument (or reject it) and return the corresponding identifier
-- * isGoodArgs :: [t] -> Bool
--    Return if the list of arguments is valid.
-- * computeVe :: [(t,VarExp)] -> VarExp
-- The 3 other arguments:
-- * conv : the general function to convert from ComputeFm to SourcesFm
-- * resErr : result if there is no match.
-- * f : the ComputeFm formula to convert
--    Compute the VarExp corresponding to the structure. The argument is the list of association between the identifier or the arg and the corresponding VarExp in the function.
toVarExpGen :: (C.ComputeFm -> SourcesFm) -> SourcesFm -> C.ComputeFm -> (C.ComputeFm -> Maybe t) -> ([t] -> Bool) -> ([(t,VarExp)] -> VarExp) -> SourcesFm
toVarExpGen conv resErr f@(C.Function v body) recog isGoodArgs computeVe =
  let (lArgs, rest1) = recogniseArgs recog body in
    if isGoodArgs lArgs
      then
        case associateVarexp lArgs (conv rest1) of
          Nothing -> resErr
          Just (lIdVe, rest2) ->
            finalVarExpFunc v (computeVe lIdVe) rest2
      else
        resErr
