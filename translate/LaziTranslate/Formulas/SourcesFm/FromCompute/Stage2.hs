--                 ComputeFm -> SourcesFm: Obj recognition and others in stage 2 of the convertion.
module LaziTranslate.Formulas.SourcesFm.FromCompute.Stage2 (computeToSrc2) where

import qualified Data.Set as S
import Data.Maybe
import Data.List

import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as C
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.Formulas.Common

-- --------------------------------------------------------------------------------
-- | After the convertion to a SourcesFm (except object, but with varExp objects), we recognize objects. 
-- | computeToSrc2 recognize patterns for objects then call computeToSrcObjIfMatch to finish the job.
-- | It also recognize CTruth structure.

-- An object is a dictionary inside a function when the argument of the function is meant to be an object. We don't have the type of the argument, so we try to guess.
computeToSrc2 :: SourcesFm -> SourcesFm
-- Patterns are sorted from ths most complete form to the less.

-- Object : "constF $D[...]" is probably an object.
computeToSrc2 (Apply (Word WtDVar "constF") di@(Dict dl@(_:_))) | not (varUsed "this" di) =
   Obj (VAssign "this" (VObj [])) dl

-- Object : "$F → Dict" : The varexp recognition is also done in the parser, we use getObjVarExp for this. This is not all cases for the computeFm recognition.
computeToSrc2 f@(FunctionM [ve] (Dict dl@(_:_))) = 
  case getObjVarExp (dictToKeys dl) ve of
       Nothing -> f
       Just (VAssign this (VObj vl)) -> 
          fromMaybe (FunctionM [ve] (computeToSrc2 (Dict dl))) (computeToSrcObjIfMatch this vl dl)

-- Recognize a CTruth
computeToSrc2 (Tuple [ List lcond , conc@(FormulaRepr _) ]) | all isFormulaRepr lcond = CTruth lcond conc
          
-- In other cases, no recognition, just recursion.
computeToSrc2 x = mapSourcesFm computeToSrc2 x

-- --------------------------------------------------------------------------------
-- | computeToSrc2 recognize patterns for object. This function (computeToSrcObjIfMatch) check the validity of the arguments and  if success return the object, else return Nothing.
-- | To finish the job : Replace "objApply this name" by accesses by variable, remove implicit keys in the varexp of the resulting object and recurse computeToSrc2 on values.
-- Arguments: 
-- * this : the variable name for the overall structure
-- * vl : The map (name,varexp) from the varexp
-- * dl : The map (name,formula) from the dictionnary

computeToSrcObjIfMatch :: String -> [ (String , VarExp) ]  -> [ (String , SourcesFm) ] -> Maybe SourcesFm
computeToSrcObjIfMatch this vl dl =
  -- Result if the dictionary is not recognised.
  let kdl = dictToKeys dl in
  if S.notMember this kdl &&
      -- Entries in the dictionary varexp must be simple (the varexp must be a VWord) on dictionary entries
      vWordForEntries vl kdl &&
      -- Free variables in the dictionary which are keys must be managed by the varExp. Else we guess that the varExp is not the one for the dictionary.
      S.isSubsetOf ( freeVars (Dict dl) `S.intersection` dictToKeys dl) (dictToKeys vl) 
    then
      Just $ Obj 
              (VAssign this (VObj $ removeImplicitEntries vl kdl)) 
              $ dictMap computeToSrc2 $ dictMap (useVarForKeys this (S.union (dictToKeys vl)kdl)) dl
    else 
      Nothing

-- --------------------------------------------------------------------------------
-- | Replace values accesses using "objApply" in dictionary by variables. Arguments are this name, entries, a value of the dictionnary to convert.
useVarForKeys :: String -> S.Set String -> SourcesFm -> SourcesFm
useVarForKeys this sv x = 
  S.foldl (\r y -> sourcesReplace (wd "objApply" &/ (wv this) &/ NameRepr y) (S.fromList [this,y]) (wv y) r) x sv

-- --------------------------------------------------------------------------------
-- | Check if entries in a VDict or VObj are simple (the varexp must be a VWord) on dictionary entries. Else we can't see it as a dictionnary, even dynamic. The set is the set of entries.

vWordForEntries :: [ (String , VarExp) ] -> (S.Set String) -> Bool
vWordForEntries vl kl = 
  all (\(k, ve) -> case ve of
                        (VWord _) -> True
                        _ -> S.notMember k kl
      )
      vl

-- --------------------------------------------------------------------------------
-- | Remove entries in a VObj which are implicit for a dictonnary ( key -> VWord key ).
-- | The first argument is the argument of the VObj, the second is the list of keys.

removeImplicitEntries :: [ (String , VarExp) ] -> (S.Set String) -> [ (String , VarExp) ]
removeImplicitEntries vl kl = filter  (\(k, ve) -> S.notMember k kl) vl
