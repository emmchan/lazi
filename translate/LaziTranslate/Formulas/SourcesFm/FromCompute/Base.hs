-- | Translate a ComputeFm to a SourcesFm
module LaziTranslate.Formulas.SourcesFm.FromCompute.Base (computeToSources) where

import qualified Data.Set as S
import qualified Data.Map as M
import Data.List

import  LaziTranslate.Formulas.SourcesFm.FromCompute.VarExp
import  LaziTranslate.Formulas.SourcesFm.FromCompute.Stage2
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as C
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.Formulas.Common


-- --------------------------------------------------------------------------------
-- | Translation from a SourcesFm to an ComputeFm. The argument is as is from the parsing.


-- --------------------------------------------------------------------------------
-- | Translate a ComputeFm to a SourcesFm. All except Obj. This is stage 1. The first argument is the set of dvar words.

computeToSources :: S.Set String -> C.ComputeFm -> SourcesFm

computeToSources defs f = wordsToUnknown defs $ computeToSrc2 $ computeToSrc1 f

-- --------------------------------------------------------------------------------
--                 ComputeFm -> SourcesFm, stage 1 (all except Obj)
-- --------------------------------------------------------------------------------

-- --------------------------------------------------------------------------------
-- | Translate a ComputeFm to a SourcesFm. All except Obj. This is stage 1.

computeToSrc1 :: C.ComputeFm -> SourcesFm

computeToSrc1 (C.Apply (C.Apply (C.Word WtDVar  "fwForAllInList") list) f@(C.Function _ _) ) =
  case toVarExp computeToSrc1 f of
       FunctionM [v] body -> ListQuantification Universal False v (computeToSrc1 list) body
       _ -> error "Bug in translate (FromCompute/Base.hs): toVarExp should return a FunctionM."

computeToSrc1 (C.Apply (C.Apply (C.Word WtDVar  "bwForAllInList") list) f@(C.Function _ _) ) =
  case toVarExp computeToSrc1 f of
       FunctionM [v] body -> ListQuantification Universal True v (computeToSrc1 list) body
       _ -> error "Bug in translate (FromCompute/Base.hs): toVarExp should return a FunctionM."

-- Recognise a \exists_l
computeToSrc1 (C.Apply (C.Apply (C.Word WtDVar "fwExistsInList") list) f@(C.Function _ _) ) =
  case toVarExp computeToSrc1 f of
       FunctionM [v] body -> ListQuantification Existential False v (computeToSrc1 list) body
       _ -> error "Bug in translate (FromCompute/Base.hs): toVarExp should return a FunctionM."

computeToSrc1 (C.Apply (C.Apply (C.Word WtDVar "bwExistsInList") list) f@(C.Function _ _) ) =
  case toVarExp computeToSrc1 f of
       FunctionM [v] body -> ListQuantification Existential True v (computeToSrc1 list) body
       _ -> error "Bug in translate (FromCompute/Base.hs): toVarExp should return a FunctionM."

-- Recognise a logical oparator
computeToSrc1 (C.Apply (C.Apply (C.Apply (C.Word WtKey0 "if") x) y) z) =
    if y == C.kw_0b && z == C.kw_1b 
    then (Apply (wd "boolNeg") (computeToSrc1 x))
    else
      if y == C.kw_1b
      then (Apply (Apply (wd "boolOr") (computeToSrc1 x)) (computeToSrc1 z))
      else
        if z == C.kw_0b 
          then wd "boolAnd" &/ computeToSrc1 x &/ computeToSrc1 y
        else
          wk "if" &/ computeToSrc1 x &/ computeToSrc1 y &/ computeToSrc1 z -- no notation

-- Recognise a Let expression
computeToSrc1 (C.Apply f@(C.Function _ _) a) = computeToLet (computeToSrc1 f) (computeToSrc1 a)

-- Recognise multi-variables functions and varExp
computeToSrc1 f@(C.Function _ _)  = toMultiFunc $  toVarExp computeToSrc1 f

-- Recognise a Tuple and then eventualla a formula representation.
computeToSrc1 (C.Apply (C.Apply (C.Word WtDVar "pair") x) y) = 
  let res = Tuple $ computeToSrc1 x : 
              (
                case computeToSrc1 y of
                  Tuple l -> l -- because Tuple [x,Tuple l] = Tuple x:l
                  a -> [a]
              ) 
  in
    -- reassemble formula representation (debug! to finish ?)
    case res of 
      Tuple [(NameRepr "apply"), (FormulaRepr x1), (FormulaRepr x2)] -> FormulaRepr (x1 &/ x2)
      _ -> res
      
-- Recognise a List or a Dict
computeToSrc1 (C.Dict l) = Dict $ dictMap computeToSrc1 l
computeToSrc1 (C.Word WtDVar "∅l") = List []
computeToSrc1 (C.Apply (C.Apply (C.Word WtDVar "toListBwAdd") l) x) | convertibleToList l = 
  let sx = computeToSrc1 x in
  case computeToSrc1 l of
    List [] -> case sx of
                 (Tuple ((NameRepr k) : v0 : vl)) -> Dict [(k,(if null vl then v0 else Tuple (v0 : vl)))]
                 _ -> List [sx]
    List m ->  List $ m ++ [sx] 
    Dict dl -> case sx of
                 (Tuple ((NameRepr k) : v0 : vl)) -> Dict $ dl ++ [(k,(if null vl then v0 else Tuple (v0 : vl)))]
                 _ -> List $ map (\(k,v) -> case v of -- Convert the Dict to a list, we must reconstruct tuples.
                                              Tuple lt -> Tuple $ NameRepr k : lt
                                              v2 -> Tuple [NameRepr k, v2]
                                 )
                                 dl
                                 ++ [sx]

-- Recognise an Obj when it's already a C.Obj (else it's done in the stage 2).
-- To do: try to recognise references to foreign entries accesses to put them in the VObj.
computeToSrc1 (C.Obj this l) = Obj (VAssign this (VObj [])) (dictMap computeToSrc1 l)

-- Recognise a constF x
computeToSrc1 (C.Apply (C.Apply (C.Word WtKey0 "if") (C.Word WtKey0 "1b")) x) = 
  wd "constF" &/ computeToSrc1 x

                                 
-- For the rest it's straightforward translation
computeToSrc1 (C.Apply x y)= Apply (computeToSrc1 x) (computeToSrc1 y)
-- We set WtUnknown at the end of the convertion process (see wordsToUnknownVars)
computeToSrc1 (C.Word t w) = Word t w
computeToSrc1 (C.NameRepr w) = NameRepr w
computeToSrc1 (C.FormulaRepr s) = FormulaRepr (computeToSrc1 s)


-- --------------------------------------------------------------------------------
-- | Return true if the formula can convert to a list
convertibleToList :: C.ComputeFm -> Bool
convertibleToList (C.Word WtDVar "∅l") = True
convertibleToList (C.Apply (C.Apply (C.Word WtDVar "toListBwAdd") l) _) = convertibleToList l
convertibleToList _ = False


-- --------------------------------------------------------------------------------
-- | Convert a Let expression. The 2 arguments are the function and the argument of the function.
computeToLet :: SourcesFm -> SourcesFm -> SourcesFm

-- If we recognise a recursive Let (the variable must be a simple VWord).
computeToLet (FunctionM (v1@(VWord _):lv1) body) (Apply (Word WtDVar "recurse") (FunctionM (v2:lv2) r)) | v1 == v2 =
    Let v1 (zeroVariableToBody $ FunctionM lv2 r) (zeroVariableToBody $ FunctionM lv1 body)

-- Non recursive Let
computeToLet (FunctionM (v:lv) body) r | S.null (S.intersection (getVarNames v)(freeVars r)) =
  Let v r (zeroVariableToBody $ FunctionM lv body)
  
-- Else we don't recognize.
computeToLet f g = f &/ g


-- --------------------------------------------------------------------------------
-- | Assemble nested functions in one.

toMultiFunc :: SourcesFm -> SourcesFm

toMultiFunc (FunctionM lv1 (FunctionM lv2 f)) =  
  toMultiFunc $ FunctionM (lv1++lv2) (toMultiFunc f)

toMultiFunc x = x


-- --------------------------------------------------------------------------------
--                 ComputeFm -> SourcesFm, stage 3 : set WtUnknown
-- --------------------------------------------------------------------------------
-- SourceFm must reflect the structure of the source code. The source have rules to infer the type of a word without type, this allow a lighter syntax.

-- | Use the most possible WtUnknown. The first argument is the set of dvar words.
wordsToUnknown :: S.Set String -> SourcesFm -> SourcesFm
wordsToUnknown defs f = wordsToUnknownVars defs S.empty False f

-- | Same as wordsToUnknown but vs (second argument) is the set of variable used when we are in a function and inRepr (third argument) tell if we are in a formula representation.
wordsToUnknownVars :: S.Set String -> S.Set String -> Bool -> SourcesFm -> SourcesFm

wordsToUnknownVars defs vs inRepr (Word WtKey0 w) | S.member w C.setKeywords = (Word WtUnknown w)

wordsToUnknownVars defs vs inRepr (Word WtLVar w) | S.member w vs && S.notMember w C.setKeywords = (Word WtUnknown w)

wordsToUnknownVars defs vs inRepr (Word WtDVar w) | S.notMember w vs && S.notMember w C.setKeywords = (Word WtUnknown w)

-- w is by default a gvar if w is in a formula representation and is nothing else (not a dvar, lvar or key0).
wordsToUnknownVars defs vs inRepr (Word WtGvar w) | inRepr && S.notMember w vs && S.notMember w defs && S.notMember w C.setKeywords = (Word WtUnknown w)

-- reset variable set and set the flag "in formula representation".
wordsToUnknownVars defs vs inRepr (FormulaRepr f) = FormulaRepr ( wordsToUnknownVars defs S.empty True  f )

wordsToUnknownVars defs vs inRepr (FunctionM lv body) = FunctionM lv $ wordsToUnknownVars defs (S.union vs $ getListVarNames lv) inRepr body

wordsToUnknownVars defs vs inRepr (Let var val body) = 
  let vs2 = S.union vs  $ getVarNames var in
      Let var (wordsToUnknownVars defs vs2 inRepr val) (wordsToUnknownVars defs vs2 inRepr body)

wordsToUnknownVars defs vs inRepr (Dict d) = Dict $ dictMap (wordsToUnknownVars defs (S.union vs (dictToKeys d)) inRepr) d

wordsToUnknownVars defs vs inRepr (Obj ve d) = Obj ve $ dictMap (wordsToUnknownVars defs (S.union vs $ S.union (getVarNames ve) (dictToKeys d)) inRepr) d

wordsToUnknownVars defs vs inRepr (ListQuantification q rev var list body) = 
    ListQuantification q rev var (wordsToUnknownVars defs vs inRepr list) (wordsToUnknownVars defs (S.union vs $ getVarNames var) inRepr body)

-- Other cases : basic mapping
wordsToUnknownVars defs vs inRepr f = mapSourcesFm (wordsToUnknownVars defs vs inRepr) f


