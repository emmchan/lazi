-- | Translation a SourcesFm to a ComputeFm.

module LaziTranslate.Formulas.SourcesFm.ToCompute.Base  ( sourcesToCompute ) where

import qualified Data.Set as S
import Data.Maybe

import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as C
import LaziTranslate.ParsRend
import LaziTranslate.ParsRend.Opparenth.OppExpression
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.Formulas.ComputeFm.Definitions
import LaziTranslate.Formulas.ComputeFm.BasicCompute
import LaziTranslate.Formulas.Common

import qualified Data.Map as M


-- --------------------------------------------------------------------------------
-- | Translation a SourcesFm to a ComputeFm. The first argument is the set of dvar words. The second argument is the "simplify" option. The SourcesFm argument is as is from the parsing.
sourcesToCompute :: S.Set String -> Bool -> SourcesFm -> C.ComputeFm

sourcesToCompute defs s f = (if s then basicCompute else id) $ sourcesToCompute' $ setWordType False S.empty f where

  -- --------------------------------------------------------------------------------
  -- | Translation from a SourcesFm to an ComputeFm. The argument is as is from the parsing.
  sourcesToCompute' :: SourcesFm -> C.ComputeFm

  sourcesToCompute' (Word wk wd) = C.Word wk wd

  sourcesToCompute' (Apply (ReverseArg f w) x) = (sourcesToCompute' f) C.&/ (sourcesToCompute' x) C.&/ C.NameRepr w

  sourcesToCompute' (Apply x y) = C.Apply (sourcesToCompute' x) (sourcesToCompute' y)

  sourcesToCompute' (NameRepr x) = C.NameRepr x

  sourcesToCompute' (FormulaRepr x) = C.FormulaRepr (sourcesToCompute' x)

  sourcesToCompute' f@(FunctionM _ _) = funcToOneVar f

  -- Special Let (when we simplify expressions): "$Let $T(v1,...,vn) = $T(a1,...an)", body : we translate in embeded $Let.
  sourcesToCompute' (Let (VTuple lv) (Tuple la) body) | s && length lv == length la = sourcesToCompute' $
    foldr (\(v,a) b -> Let v a b) body $ zip lv la

  -- If it's a recursive "$Let" definition.
  sourcesToCompute' (Let (var@(VWord v)) val body) | varUsed v val = sourcesToCompute' $
    (FunctionM [var] body) &/ ((wd "recurse") &/ (FunctionM [var] val))

  -- General "$Let", non recursive, not a "$T(v1,...,vn) = $T(a1,...an)"
  sourcesToCompute' (Let var val body) = sourcesToCompute' $ (FunctionM [var] body) &/ val

  sourcesToCompute' (Tuple [x]) = sourcesToCompute' x

  sourcesToCompute' (Tuple (x:l)) = 
    C.wd "pair" C.&/ (sourcesToCompute' x) C.&/ (sourcesToCompute' (Tuple l))

  sourcesToCompute' (List l) = 
    foldl (\r x -> C.wd "toListBwAdd" C.&/ r C.&/ (sourcesToCompute' x)) (C.wd "∅l") l

  sourcesToCompute' (Dict l) = C.Dict $ dictMap sourcesToCompute' l
  
  sourcesToCompute' (Obj (VAssign this (VObj vol)) dl) = 
    -- We replace all variables representing entries values by their values (which are all constructed from "this"). Theses variables are:
    -- * Variables corresponding to keys of the object.
    -- * Variables created by "vol".
    C.Obj this $ dictMap (transObjEntry  $ VAssign this $ constructObjVarExp vol $ dictToKeys dl) dl
  
  sourcesToCompute' (ListQuantification q rev var list f) = sourcesToCompute' $
    (wd $  (if rev then "bw" else "fw") ++ (if q == Universal then "ForAllInList" else "ExistsInList") ) &/
    list &/
    FunctionM [var] f
  
  sourcesToCompute' (CTruth conds conc) = sourcesToCompute' $ Tuple [ List conds, conc ]
  
  sourcesToCompute' x = error $ show x
  

  -- --------------------------------------------------------------------------------
  -- | Split a function with multiples variables into multiples functions.

  funcToOneVar :: SourcesFm -> C.ComputeFm

  funcToOneVar (FunctionM [] f) = sourcesToCompute' f

  funcToOneVar (FunctionM (VWord var : l) f) = C.Function var (sourcesToCompute' (FunctionM l f))

  -- simplifyVarExp is recursive by sourcesToCompute'
  funcToOneVar (FunctionM (v :l) f) = sourcesToCompute' $ simplifyVarExp v (FunctionM l f)


  -- ------------------------------------------------------------------------------------
  -- | Construct a varexp from the keys of an object and a varexp (see "sourcesToCompute' (Obj ...").
  -- | * l : the argument of the VObj
  -- | * ks: the set of keys of the object
  constructObjVarExp :: [ (String , VarExp) ] -> S.Set String -> VarExp
  
  constructObjVarExp l ks = 
    -- the set of keys to add to ve: ks less keys from ve
    let toAdd = S.difference ks $ dictToKeys l in
    VObj $ l ++ (map (\k -> (k, (VWord k))) $ S.toList toAdd)
  
  -- ------------------------------------------------------------------------------------
  -- | Translate a SourceFm value of an object entry to a ComputeFm one. We take the varexp corresponding to the object to replace variables .
  transObjEntry :: VarExp -> SourcesFm -> C.ComputeFm
  
  transObjEntry ve s = let (C.Function this body) = sourcesToCompute' $ FunctionM [ve] s in body
    

  -- --------------------------------------------------------------------------------
  -- | Replace the WordType WtUnknown by a known WordType.
  -- | Arguments:
  -- | - Are we in a representation of a formula ?
  -- | - The set of variables name (when the formula is in a function).
  -- | - The formula to process
  setWordType :: Bool -> (S.Set String) -> SourcesFm -> SourcesFm

  setWordType inRepr s (Word WtUnknown x) = Word
    (
      if S.member x s then WtLVar 
      else 
        if S.member x C.setKeywords 
        then WtKey0 
        else
          if (not inRepr) || S.member x defs
          then WtDVar 
          else WtGvar
    )
    x

  setWordType inRepr s (FunctionM vars body) = FunctionM vars $ setWordType inRepr (S.unions $ s : map getVarNames vars  ) body

  setWordType inRepr s (FormulaRepr x) = FormulaRepr $ setWordType True S.empty x

  setWordType inRepr s (Let var value body) = let s2 =S.union s (getVarNames var) in
    Let var ( setWordType inRepr s2 value ) ( setWordType inRepr s2 body )

  setWordType inRepr s (Obj ve l) = Obj ve $ dictMap (setWordType inRepr $ S.union s $ S.union (getVarNames ve) $ dictToKeys l) l

  setWordType inRepr s (ListQuantification qt rev v l p) = 
    ListQuantification qt rev v (setWordType inRepr s l) (setWordType inRepr (S.union (getVarNames v) s) p)

  -- In other cases we just recurse inside
  setWordType inRepr s f = mapSourcesFm (setWordType inRepr s) f


-- --------------------------------------------------------------------------------
-- | Replace VarExp ve with the value of the notation. f is the body of the function and ve the first varexp in the arguments list.

simplifyVarExp :: VarExp -> SourcesFm -> SourcesFm

simplifyVarExp ve f = simplifyVarExpV Nothing ve f


-- --------------------------------------------------------------------------------
-- | Same as simplifyVarExp but the first argument is Maybe String : String means "use v as variable representing the overall structure", Nothing means "there is no information on a variable to use to represent the overall structure".

simplifyVarExpV :: Maybe String -> VarExp -> SourcesFm -> SourcesFm

-- $T[ a ] without Vassign: this is a particular case, we can simplify because we access to all the structure.
simplifyVarExpV Nothing (VTuple [a] ) f = FunctionM [a] f

-- $T[ a1,...,an ]
simplifyVarExpV mv (VTuple l ) f =
  let g = FunctionM l f
      v = fromMaybe (newVar g) mv
  in
    FunctionM [ VWord v ] $ tupleVarExpListToArgs (length l) g (wv v)
    where
      tupleVarExpListToArgs :: Int -> SourcesFm -> SourcesFm -> SourcesFm
      tupleVarExpListToArgs 1 g arg = g &/ arg
      tupleVarExpListToArgs n g arg = tupleVarExpListToArgs (n-1) (g &/ (wd "pairFirst" &/ arg)) (wd "pairSecond" &/ arg)

-- $D[ ... ]
simplifyVarExpV mv (VDict la ) f =
  let freeVarsf = freeVars f
      cleanLa = filter (\(name, va) -> not $ S.null $ S.intersection freeVarsf $ S.insert name $ getVarNames va ) la -- la reduced to used entries in f.
      g = FunctionM (map (\(name, va) -> va) cleanLa) f
      v = fromMaybe (newVar g) mv
  in
    FunctionM [ VWord v ] $ 
      foldl 
        (\e (name, _) -> e &/ (wd "dictApply" &/ wv v &/ NameRepr name) )
        g
        cleanLa

-- $O[ ... ]
simplifyVarExpV mv (VObj la ) f =
  let freeVarsf = freeVars f
      cleanLa = filter (\(name, va) -> not $ S.null $ S.intersection freeVarsf $ S.insert name $ getVarNames va ) la -- la reduced to used 
      g = FunctionM (map (\(name, va) -> va) cleanLa) f
      v = fromMaybe (newVar g) mv
  in
    FunctionM [ VWord v ] $ 
      foldl 
        (\e (name, _) -> e &/ (wd "objApply" &/ wv v &/ NameRepr name) )
        g
        cleanLa

-- li +le la
simplifyVarExpV mv (VListAdd li la) f =
  let g = FunctionM [ la, li ] f
      v = fromMaybe (newVar g) mv
  in
    FunctionM [ VWord v ] $ g &/ (wd "listLast" &/ wv v) &/ (wd "listInit" &/ wv v)

-- $L [ ]
simplifyVarExpV mv (VList []) f = 
  let g = f
      v = fromMaybe (newVar g) mv
  in
    FunctionM [ VWord v ] g

-- $L [ a ]
simplifyVarExpV mv (VList [a]) f = 
  let g = FunctionM [ a ] f
      v = fromMaybe (newVar g) mv
  in
    FunctionM [ VWord v ] $ g &/ (wd "listLast" &/ wv v)

-- $L [ a1 , ..., an ] ( = $L [ a1 , ... a(n-1) ] +le an
simplifyVarExpV mv (VList l) f = 
  FunctionM [ 
              (if isJust mv then VAssign (fromJust mv) else id) 
                (VListAdd (VList $ init l) (last l) )
            ] 
            f

-- v@ve
simplifyVarExpV Nothing (VAssign v ve) f = simplifyVarExpV (Just v) ve f
-- v@ve when ve is not one of the structure above (it can be a VWord or a VAssign).
simplifyVarExpV (Just v) ve f =
  FunctionM [VWord v] $ (FunctionM [ve] f) &/ wv v

-- No other case : the case of the varExp being a VWord is already handled.
