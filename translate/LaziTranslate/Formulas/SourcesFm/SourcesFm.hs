-- | Structure of a SourcesFm, this is used to represent the source language.

module LaziTranslate.Formulas.SourcesFm.SourcesFm  where

import qualified Data.Map as M
import qualified Data.Set as S
import Data.List

import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E
import LaziTranslate.ParsRend.Opparenth.OppExpression
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.Formulas.Common


 -- | Kind of quantifier
data Quantifier = Universal | Existential deriving (Ord,Eq,Show)

 -- | SourcesFm 
data SourcesFm =
    -- ^ like in ComputeFm
    Word WordType String |
    -- ^ like in ComputeFm
    Apply SourcesFm SourcesFm |
    -- ^ like in ComputeFm
    NameRepr String |
    -- ^ like in ComputeFm
    FormulaRepr SourcesFm |
    -- ^ ReverseArg = $F f,y,x -> f x y. Used for notation op'word where op an infix operator.
    ReverseArg SourcesFm String |
    -- ^ multi-variables function
    FunctionM [VarExp] SourcesFm |
    -- ^ $Let var = value, body
    -- ^ Let can be recursive only if ve is a VWord. In other cases, variables used in ve are not free variables in val.
    Let VarExp SourcesFm SourcesFm |
    -- ^ A Tuple
    Tuple [SourcesFm] |
    -- ^ A list
    List [SourcesFm] |
    -- ^ A dictionary. We don't use a map to preserve order.
    Dict [(String,SourcesFm)] |
    -- ^ An object
    -- ^ Arg 1 : the varexp for the overall object, it's a (VAssign this (VObj dl)). dl don't need to contain the entries for keys in the object but it can (generaly to avorride the basic varexp for the entry).
    -- ^ Arg 2 : the list of (key,value).
    Obj VarExp [(String,SourcesFm)] | 
    -- ^ A quantifier on a list. Parameters : the quantifier, Reverse mode (True), the variable, the list, the property.
    ListQuantification Quantifier Bool VarExp SourcesFm SourcesFm | 
    -- ^ A CTruth : a pair of (list of formulas (the conditions), a formula (the conclusion)). The formulas are FormulaRepr.
    CTruth [SourcesFm] SourcesFm
     deriving (Eq,Show)

     
wv :: String  -> SourcesFm
wv s = Word WtLVar s
     
wd :: String  -> SourcesFm
wd s = Word WtDVar s

wk :: String  -> SourcesFm
wk s = Word WtKey0 s

(&/) :: SourcesFm -> SourcesFm -> SourcesFm
infixl 5 &/
x &/ y = Apply x y

-- precedence of || and |
bigSepPrec = 0
smallSepPrec = 15

-- | SourcesFm is an UnderExpr (see Opparenth).
instance UnderExpr SourcesFm where
    applyUnderExpr x y = Apply x y
    applyPrec = const 40
    sepsPrec = const [bigSepPrec,smallSepPrec]
    
kw_1b         = Word WtKey0 kws_1b
kw_0b        = Word WtKey0 kws_0b
kw_if          = Word WtKey0 kws_if
kw_equal       = Word WtKey0 kws_equal
kw_distribute  = Word WtKey0 kws_distribute

-- --------------------------------------------------------------------------------
-- | Is the formula a FormulaRepr.

isFormulaRepr :: SourcesFm -> Bool
isFormulaRepr (FormulaRepr _) = True
isFormulaRepr _ = False

-- --------------------------------------------------------------------------------
-- | Get the formula inside a FormulaRepr.

stripFormulaRepr :: SourcesFm -> SourcesFm
stripFormulaRepr (FormulaRepr x) = x

-- --------------------------------------------------------------------------------
-- | Return the set of free variables used in a formula.

freeVars :: SourcesFm -> S.Set String

freeVars (Word WtLVar v) = S.singleton v

freeVars (Apply x y) = S.union (freeVars x) (freeVars y)

freeVars (FunctionM ve body) = S.difference (freeVars body) (getListVarNames ve)

-- Let can be recursive only if ve is a VWord. In other cases, variables used in ve are not free variables in val.
freeVars (Let ve val body) = S.difference (S.union (freeVars val) (freeVars body)) (getVarNames ve)

freeVars (Tuple l) = S.unions $ map freeVars l

freeVars (List l) = S.unions $ map freeVars l

freeVars (Dict l) = freeVars (List $ snd $ unzip l)

freeVars (Obj ve l) = S.difference (freeVars $ Dict l) ( S.union (getVarNames ve) (dictToKeys l) )

freeVars (ListQuantification  _ _ ve l f) = S.union (freeVars l) (S.difference (freeVars f) (getVarNames ve))

freeVars  _ = S.empty

-- --------------------------------------------------------------------------------
-- | Return True if a free variable v is used in a formula f.

varUsed :: String -> SourcesFm -> Bool

varUsed v f = S.member v $ freeVars f


-- --------------------------------------------------------------------------------
-- | Find a String which is not used as a free variable in a formula.

newVar :: SourcesFm -> String
newVar f = newVarFrom "a" f

-- | Same, but start at s
newVarFrom :: String -> SourcesFm -> String

newVarFrom s f = if varUsed s f then  newVarFrom (incrementString s) f else s

-- --------------------------------------------------------------------------------
-- | map a SourcesFm, this is used to provide a default map for mapping functions that modifie just some constructors.

mapSourcesFm :: (SourcesFm -> SourcesFm) -> SourcesFm -> SourcesFm

mapSourcesFm f (Apply a b) = Apply (f a) (f b)
mapSourcesFm f (FormulaRepr g) = FormulaRepr (f g)
mapSourcesFm f (ReverseArg a s) = ReverseArg (f a) s
mapSourcesFm f (FunctionM l body) = FunctionM l (f body)
mapSourcesFm f (Let v x body) = Let v (f x) (f body)
mapSourcesFm f (Tuple l) = Tuple (map f l)
mapSourcesFm f (List l) = List (map f l)
mapSourcesFm f (Dict l) = Dict (dictMap f l)
mapSourcesFm f (Obj v l) = Obj v (dictMap f l)
mapSourcesFm f (ListQuantification qt b v l body) = ListQuantification qt b v (f l) (f body)
mapSourcesFm f (CTruth l g) = CTruth (map f l) (f g)
-- Other constructors have no sub-formula
mapSourcesFm f x = x

-- --------------------------------------------------------------------------------
-- | Simplify a zero variable function

zeroVariableToBody :: SourcesFm -> SourcesFm
zeroVariableToBody (FunctionM [] body) = body
zeroVariableToBody x = x

-- --------------------------------------------------------------------------------
-- | Map a formula with an accumulator to also retreive an accumulator result. This can be used, for example, to replace a kind of subformula and retreive the set of accountered name associated with the replacement.
-- | Arguments:
-- | -the map function: get a formula and the accumulator, return Nothing or Just (replacement formula, new accumulator)
-- | -

-- --------------------------------------------------------------------------------
-- | Replace an expression by an other in a formula. The set of variables used in the replacemenet expression is needed to avoid to go in functions using one of these variables .
-- | Arguments:
-- | -the expression to replace (x)
-- | -the set of variables used in this expression (lv)
-- | -the replacement expression (y)
-- | -the formule to search&replace (f)
sourcesReplace :: SourcesFm -> S.Set String -> SourcesFm -> SourcesFm -> SourcesFm

sourcesReplace x lv y f | x==f = y

sourcesReplace x lv y f@(FormulaRepr _) = f

sourcesReplace x lv y (FunctionM lve body) | S.null $ S.intersection (getListVarNames lve)  lv =
  FunctionM lve $ sourcesReplace x lv y body

sourcesReplace x lv y (Let v a b) | S.null $ S.intersection (getVarNames v)  lv =
  Let v (sourcesReplace x lv y a) (sourcesReplace x lv y b)

sourcesReplace x lv y (Obj ve l) | S.null $ S.intersection lv $ S.union (getVarNames ve) (dictToKeys l) = 
  Obj ve $ dictMap (sourcesReplace x lv y) l

sourcesReplace x lv y (ListQuantification qt rev v l f) =
  ListQuantification qt rev v (sourcesReplace x lv y l) $
    if S.null $ S.intersection lv $ getVarNames v
       then
        sourcesReplace x lv y f
       else
        f
        
-- In other cases we just recurse inside
sourcesReplace x lv y f = mapSourcesFm (sourcesReplace x lv y) f
