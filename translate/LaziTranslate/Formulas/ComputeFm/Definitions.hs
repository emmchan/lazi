

module LaziTranslate.Formulas.ComputeFm.Definitions where

import qualified Data.Map as M
import qualified Data.Set as S
import Data.List

import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.Formulas.ComputeFm.ComputeFm
-- import Lazi.Maths.Deduction

data Definition = Definition String ComputeFm

type DefsList = [Definition]

type DefsMap = M.Map String ComputeFm

-- | The map of all Foundation definitions.
definitionsMap :: DefsList -> DefsMap
definitionsMap dl = M.fromList (map (\(Definition n v) -> (n, v) ) dl)

-- | Given a list of definitions, return Nothing if there is no error, else return Just(name,an error message) where name is the defined name where there is an error.
checkDefinitions :: [Definition] -> Maybe (String,String)
-- foldl's accumulator is (set of already defined names, string of error message).
checkDefinitions dl = 
  case foldl func (Right S.empty) dl of
    Left (n,err) -> Just (n,err)
    _ -> Nothing
  where
    func :: Either (String,String) (S.Set String) -> Definition -> Either (String,String) (S.Set String)
    func (Left err) _ = Left err
    func (Right ns) (Definition n f) = 
      if S.member n ns
        then
          Left(n,"Name is already defined.")
        else
          let err=checkFormula ns f in
            if null err then Right (S.insert n ns) else Left (n,err)
          

-- --------------------------------------------------------------------------------
-- | Return an error message if the formula has a free variable or use and undefined global const. Arguments are: the set of defined global const names, the formula. If there is no error return an empty string.

checkFormula :: S.Set String -> ComputeFm -> String

checkFormula ns f =
  let freeV = S.toAscList $ freeVars f
      notDefGlobs = S.toAscList $ S.difference (globUsed f) ns
  in
    if not $ null freeV -- debug!
      then
        "Free variables: " ++ intercalate ", " freeV
      else
        if not $ null notDefGlobs
          then
            "Undefined global constant: " ++ intercalate ", " notDefGlobs
          else
            ""
