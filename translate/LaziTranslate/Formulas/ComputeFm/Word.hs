-- | Base module, define a data structure to store a Lazi' word. A Lazi' word is a list of bits, and some of them are equivalent to strings. Generaly we use strings, but sometimes we need a Lazi' word wich is not translatable to a String.
module LaziTranslate.Formulas.ComputeFm.Word  where

import Data.Char

import Data.Word
import Data.Bits
import Data.Set

data WordType = 
    -- ^ A variable of a function
    WtLVar |
    -- ^ A global constant name
    WtDVar |
    -- ^ A basic keyword ("if", "distribute" etc)
    WtKey0 |
     -- ^ A global variable in Lazi-1 (so just in formula representation).
    WtGvar |
   -- ^ Unknown word kind, this is used for reading and writing the source. See node "Représentation du langage".
    WtUnknown deriving (Ord,Eq)

instance Show WordType where
  show WtLVar = "lvar"
  show WtDVar = "dvar"
  show WtKey0 = "key0"
  show WtGvar = "gvar"
  show WtUnknown = "unknown"

-- The list of all word types
wtList = [WtLVar,WtDVar,WtKey0,WtGvar]
-- The list of all pairs (word type name, word type)
wtPairs = Prelude.map (\wt -> (show wt,wt)) wtList
-- The list of all names of word types.
wtNames = Prelude.map show wtList


   -- | Keywords, as String
-- Lazi' language
kws_1b        = "1b"
kws_0b       = "0b"
kws_if          = "if"
kws_equal       = "equal"
kws_distribute  = "distribute"

    
