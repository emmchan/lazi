-- | Define ComputeFm data type and functions for this type. ComputeFm is the type of formula used by the compute software.
module LaziTranslate.Formulas.ComputeFm.ComputeFm where
      
import GHC.Exts
import qualified Data.Set as S
import Data.Char
import Data.List


import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.Formulas.Common


-- --------------------------------------------------------------------------------
-- | ComputeFm is the type of formula used by the compute software.
data ComputeFm =
  -- ^ A Word
  Word WordType String|
  -- ^ Application of a function to an argument
  Apply ComputeFm ComputeFm |
  -- ^ A function definition with the variable and the body.
  Function String ComputeFm |
  -- ^ A representation of a Lazi' Word (representable by String).
  NameRepr String |
  -- ^ A representation of a Lazi Word (representable by String).
  FormulaRepr ComputeFm | 
  -- ^ A dictionary.  We don't use a map to preserve order.
  Dict [(String,ComputeFm)] |
  -- ^ An object
  -- ^ Arg 1 : the variable name representing the input object.
  -- ^ Arg 2 : the list of (key,value).
  Obj String [(String,ComputeFm)]
  deriving (Ord,Eq,Show)


-- --------------------------------------------------------------------------------
(&/) :: ComputeFm -> ComputeFm -> ComputeFm
infixl 5 &/
x &/ y = Apply x y

     
wv :: String  -> ComputeFm
wv s = Word WtLVar s
     
wd :: String  -> ComputeFm
wd s = Word WtDVar s

    
-- --------------------------------------------------------------------------------
-- The list of keywords
keywords=[kws_1b, kws_0b, kws_if, kws_equal, kws_distribute]

setKeywords = S.fromList keywords


-- --------------------------------------------------------------------------------
-- | Keywords as Expressions, haskell variable, usefull to optimize this program (memory and probably time) and to avoid typo
-- Lazi'1
kw_1b          = Word WtKey0 kws_1b
kw_0b          = Word WtKey0 kws_0b
kw_if          = Word WtKey0 kws_if
kw_equal       = Word WtKey0 kws_equal
kw_distribute  = Word WtKey0 kws_distribute


-- --------------------------------------------------------------------------------
--Logical operators
opEq x y = kw_equal &/ x &/ y
opImply x y = kw_if &/ x &/ y &/ kw_1b
opAnd x y = kw_if &/ x &/ y &/ kw_0b
opOr x y = kw_if &/ x &/ kw_1b &/ y
opNeg x  = kw_if &/ x &/ kw_0b &/ kw_1b


-- --------------------------------------------------------------------------------
-- | Check if all words of "WtDVar" are really defined. The set is the set of defined names. Return the list of undefined names.
allGlobConst :: S.Set String -> ComputeFm -> [String]

allGlobConst s (Apply f a) =   allGlobConst s f ++   allGlobConst s a
allGlobConst s (Word WtDVar x) = if S.member x s then [] else [x]
allGlobConst s (Function _ body) =   allGlobConst s body 
allGlobConst s (Dict l) =  concat $ map (allGlobConst s) (snd $ unzip l)
allGlobConst s (Obj var l) =  concat $ map (allGlobConst s) (snd $ unzip l)
allGlobConst _ _ = []


-- --------------------------------------------------------------------------------
-- | Count the number or times a free variable is used in a formula.

varUsage :: String -> ComputeFm -> Int

varUsage v (Word WtLVar w) = if v==w then 1 else 0

varUsage v (Apply x y) = varUsage v x + varUsage v y

varUsage v (Function w body) = if v==w then 0 else varUsage v body

varUsage v (Dict l) = sum $ map (varUsage v) (snd $ unzip l)

varUsage v (Obj w l) = if v==w then 0 else varUsage v $ Dict l

varUsage  _ _ = 0

-- --------------------------------------------------------------------------------
-- | Return the set of free variables used in a formula.

freeVars :: ComputeFm -> S.Set String

freeVars (Word WtLVar v) = S.singleton v

freeVars (Apply x y) = S.union (freeVars x) (freeVars y)

freeVars (Function w body) = S.delete w $ freeVars body

freeVars (Dict l) = S.unions  $ map freeVars (snd $ unzip l)

freeVars (Obj v l) = S.delete v $ freeVars (Dict l)

freeVars  _ = S.empty

-- --------------------------------------------------------------------------------
-- | Return the set of dvar used in a formula.

globUsed :: ComputeFm -> S.Set String

globUsed (Word WtDVar v) = S.singleton v

globUsed (Apply x y) = S.union (globUsed x) (globUsed y)

globUsed (Function w body) = globUsed body

-- We suppose that dvar used to construct a Dict are defined (bwAddToList etc), so we don't add them.
globUsed (Dict l) = S.unions $ map globUsed (snd $ unzip l)

globUsed (Obj v l) = globUsed (Dict l)

globUsed  _ = S.empty

-- --------------------------------------------------------------------------------
-- | Return the set of variables used in a formula.

allVars :: ComputeFm -> S.Set String

allVars (Word WtLVar v) = S.singleton v

allVars (Apply x y) = S.union (allVars x) (allVars y)

allVars (Function w body) = allVars body

allVars (Dict l) = S.unions $ map allVars (snd $ unzip l)

allVars (Obj v l) = allVars (Dict l)

allVars  _ = S.empty

-- --------------------------------------------------------------------------------
-- | Return True if a free variable is used in a formula.

varUsed :: String -> ComputeFm -> Bool

varUsed v f = S.member v $ freeVars f

varUnused :: String -> ComputeFm -> Bool
varUnused v f = not $ varUsed v f

-- --------------------------------------------------------------------------------
-- | Replace a sub-expression by an other in a formula.

replaceSub :: ComputeFm -> ComputeFm -> ComputeFm -> ComputeFm

replaceSub s r f | s==f = r

replaceSub s r (Apply x y) = Apply (replaceSub s r x) (replaceSub s r y)

replaceSub s r (Function v b) | varUnused v s  =
  Function v (replaceSub s r b)

replaceSub s r (Dict l)  = Dict $ dictMap (replaceSub s r) l

replaceSub s r (Obj v l) | varUnused v s = Obj v $ dictMap (replaceSub s r) l

replaceSub _ _ f = f -- Other cases : nothing to replace

-- --------------------------------------------------------------------------------
-- | Replace a variable by a formula in a body

replaceVar :: String -> ComputeFm -> ComputeFm -> ComputeFm 

replaceVar w val f = replaceSub (Word WtLVar w) val f

-- --------------------------------------------------------------------------------
-- | Find a String which is not used as a variable in a formula.

newVar :: ComputeFm -> String
newVar f = newVar' (allVars f) "a"

-- | Same, but start at s and have the set of used vars
newVar' :: S.Set String -> String -> String

newVar' vars s = if S.member s vars then newVar' vars (incrementString s) else s 
