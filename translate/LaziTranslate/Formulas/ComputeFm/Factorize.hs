-- | Factorize sub-expressions in a formula to optimize the computation time. For example "f (g . x y , h . x y)" become "($F a -> f (g a, h a)) . x y" ("x y" is factorized). See node "Factoriser le code".

module LaziTranslate.Formulas.ComputeFm.Factorize (factorize) where
      
import Data.Ord
import Data.Char
import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.Formulas.Common
import LaziTranslate.Formulas.ComputeFm.Word 

-- --------------------------------------------------------------------------------
-- | We associate the depth of the formula with the formula, hence we can easaly order formula with sub-formula < formula. This is used to get the biggest factor first, which avoid to factorize a sub-factors.

data DepthComputeFm = DepthComputeFm Int ComputeFm

instance Eq DepthComputeFm where
  (==) (DepthComputeFm n f) (DepthComputeFm m g) = (n == m) && (f == g)

instance Ord DepthComputeFm where
  compare (DepthComputeFm n f) (DepthComputeFm m g) =
    if n == m then compare f g else compare n m

-- --------------------------------------------------------------------------------
-- | The tree corresponding to a formula, it mirrors the formula adding informations about it:
-- | * [DepthComputeFm] is the ordered list of all sub-expressions (the expression itself included) which could be factorized. So there is just Apply expression, because we factorize to avoid double computation. And if there is no Apply there is no computation (global constants can be computed but the computation is already shared).
-- | * [FactorsTree] is the list of direct children (2 for apply, 1 for a function, 0 for others) for as FactorsTree structure.

data FactorsTree = FactorsTree ComputeFm [DepthComputeFm] [FactorsTree]


-- --------------------------------------------------------------------------------
-- | Return the list of factors.

subList :: FactorsTree -> [DepthComputeFm]
subList (FactorsTree _ l _) = l
  
-- --------------------------------------------------------------------------------
-- | Translate a computeFm to a FactorsTree.

computeFmToTree :: ComputeFm -> FactorsTree

computeFmToTree f@(Apply x y) =
  let fx = computeFmToTree x
      fy = computeFmToTree y
      lx = subList fx
      ly = subList fy
  in
    FactorsTree f (DepthComputeFm (max (maxDepth lx) (maxDepth ly) + 1) f : (mergeLists lx ly)) [ fx , fy ]

computeFmToTree f@(Function v body) =
  let fb = computeFmToTree body
      l = subList fb
  in
    FactorsTree f (removeVar v l) [ fb ]

computeFmToTree f = FactorsTree f [] []


-- --------------------------------------------------------------------------------
-- | Get the max depth of a [DepthComputeFm], zero if empty

maxDepth :: [DepthComputeFm] -> Int

maxDepth [] = 0
maxDepth ((DepthComputeFm n _):_) = n

-- --------------------------------------------------------------------------------
-- | Merge two [DepthComputeFm] returning an ordered list.

mergeLists :: [DepthComputeFm] -> [DepthComputeFm] -> [DepthComputeFm]

mergeLists [] [] = []

mergeLists l [] = l

mergeLists [] l = l

mergeLists ll@(x:l) mm@(y:m) = 
  case compare x y of
    GT -> x : mergeLists l mm 
    LT -> y : mergeLists ll m
    EQ -> x : mergeLists l m

  
-- --------------------------------------------------------------------------------
-- | Remove expressions containing a variable from a [DepthComputeFm].

removeVar :: String -> [DepthComputeFm] -> [DepthComputeFm]

removeVar v l = filter (\(DepthComputeFm _ f) -> varUnused v f  ) l


-- --------------------------------------------------------------------------------
-- | Translate a FactorsTree to a computeFm.

treeToComputeFm :: FactorsTree -> ComputeFm

treeToComputeFm (FactorsTree f _ _) = f


-- --------------------------------------------------------------------------------
-- | Maybe find a common factor between two lists.

commonFact :: [DepthComputeFm] -> [DepthComputeFm] -> Maybe ComputeFm

commonFact _ [] = Nothing

commonFact [] _ = Nothing

commonFact ll@(x:l) mm@(y:m) = 
  case compare x y of
       GT -> commonFact l mm -- suppress the strict maximum  : x
       LT -> commonFact ll m -- suppress the strict maximum  : y
       EQ -> let (DepthComputeFm _ f) = x in Just f


-- --------------------------------------------------------------------------------
-- | Replace a formula containing multiples factors by a factorized formula.

replaceFactor :: ComputeFm -> ComputeFm -> ComputeFm

replaceFactor fac f =
  let v = newVar f in
      Function v (replaceSub fac (Word WtLVar v) f) &/ fac
      

-- --------------------------------------------------------------------------------
-- | Factorize all possible factors in a FactorsTree, return the resulting ComputeFm

factorizeTree :: FactorsTree -> ComputeFm

-- If we find a factor we drop all the tree to recreate a new one. But this is rare comparing to the tree depth, so it's ok. This is simpler because a multiple factor can create multiples sub-factors.
factorizeTree (FactorsTree f@(Apply _ _) _ [fx,fy]) =
  case commonFact (subList fx) (subList fy) of
    Nothing -> Apply (factorizeTree fx) (factorizeTree fy)
    Just fac -> factorize $ replaceFactor fac f

factorizeTree (FactorsTree (Function v _) _ [tb]) = Function v (factorizeTree tb)

factorizeTree (FactorsTree f _ _) = f

      

-- --------------------------------------------------------------------------------
-- | Main function : factorize a formula

factorize :: ComputeFm -> ComputeFm

factorize f = factorizeTree $ computeFmToTree f
