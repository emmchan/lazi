-- | Translation a ComputeFm to a Lazi_0_0Fm.
module LaziTranslate.Formulas.ComputeFm.ToLazi_0_0Fm.Base where

import Data.Char

import Data.Word
import Data.Bits
import qualified Data.Map as M
 
import LaziTranslate.Formulas.ComputeFm.Definitions 
import LaziTranslate.Formulas.ComputeFm.ComputeFm 
import LaziTranslate.Formulas.ComputeFm.Word 
import qualified LaziTranslate.Formulas.Lazi_0_0Fm.Lazi_0_0Fm as L 



-- --------------------------------------------------------------------------------
-- | Translate a ComputeFm to a Lazi_0_0Fm. The first argument is the set of définitions. 
sourcesToCompute :: DefsMap -> ComputeFm -> L.Lazi_0_0Fm

sourcesToCompute defm f = simplComputeFmToLazi_0_0 (internalTr f) where
  
  -- | Translate internaly in CompteFm, because we can return formulas with variables.
  internalTr :: ComputeFm -> ComputeFm
  internalTr (Word WtDVar v) = internalTr $ M.findWithDefault (Word WtKey0 ("Unknown DVar " ++ v)) v defm 
    
  internalTr (Apply x y) = Apply (internalTr x) (internalTr y)
  
  internalTr (Function v (Word WtLVar w)) | v == w =  kw_if &/ kw_0b &/ kw_0b -- idFunc
  internalTr (Function v f) | varUnused v f =  kw_if &/ kw_1b &/ (internalTr f) -- constF f
  internalTr (Function v (Apply f (Word WtLVar w)))  | varUnused v f && v == w = (internalTr f)
  internalTr (Function v (Apply f g)) = kw_distribute &/ (internalTr (Function v f)) &/ (internalTr (Function v g)) 
  internalTr (Function v f) = internalTr (Function v (internalTr f))

  internalTr (NameRepr w) = internalTr $ simplifyWordRepr w
  
  internalTr (FormulaRepr f) = internalTr $ simplifyTextRepr (internalTr f)
 
  internalTr (Dict l) = internalTr $ foldr (\(k,v) r -> (wd "toListBwAdd") &/ r &/((wd "pair") &/ (NameRepr k) &/ v)) (wd "∅l") l
  
  internalTr (Obj v l) = internalTr $ Function v (Dict l)
  
  internalTr f = f -- For words (like Word WtKey0) which doesn't need a translation.

-- | Convert a ComputeFm containing just Apply and Word WtKey0 to a Lazi_0_0Fm
simplComputeFmToLazi_0_0 :: ComputeFm -> L.Lazi_0_0Fm
  
simplComputeFmToLazi_0_0 (Word WtKey0 v) = L.Word v
simplComputeFmToLazi_0_0 (Apply x y) = L.Apply (simplComputeFmToLazi_0_0 x) (simplComputeFmToLazi_0_0 y)

--------------------------------------------------------------------------------
-- Function to symplify a TextRepr

-- | Simplify a TextRepr (a compDeduct function). The content is already simplyfied, so it just contains words and applications.
simplifyTextRepr :: ComputeFm ->  ComputeFm

-- show convert a WordType to the name used in Lazi
simplifyTextRepr  (Word t w) = (wd "wordToFormula") &/ (NameRepr $ show t) &/ (NameRepr w)

simplifyTextRepr (Apply e1 e2) = 
    (wd "formulaApply") &/ (FormulaRepr e1) &/ (FormulaRepr e2)


--------------------------------------------------------------------------------
-- Function to symplify a WordRepr

-- | Simplify a WordRepr 
simplifyWordRepr :: String ->  ComputeFm

simplifyWordRepr  w = boolListToExpression (stringWordToList w)


-- | Convert a String to a [Bool]
stringWordToList :: String -> [Bool]

stringWordToList [] = []
stringWordToList (a:l) = (uintToBools 32 (fromIntegral (ord a))) ++ (stringWordToList l)

   -- | Convert an unsigned 32 bits integer i to a list of n bools
uintToBools :: Int -> Word32 -> [Bool]

uintToBools 0 _ = []
uintToBools n i = (testBit i (n-1)) : uintToBools (n-1) i

-- | Convert a Haskell boolean to dw_true/dw_false

boolToKw :: Bool -> ComputeFm
boolToKw b = if b then kw_1b else kw_0b

-- | Convert a boolean list to a Lazi' list of true/false
boolListToExpression :: [Bool] ->  ComputeFm

boolListToExpression l  = foldl (\r x -> (wd "toListBwAdd") &/ r &/ x) (wd "∅l") (map boolToKw l)



