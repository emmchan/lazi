-- | Do basic computations. The notations can produce fat and stupid code, so it's good to make some simplifications.
module LaziTranslate.Formulas.ComputeFm.BasicCompute (basicCompute) where

import LaziTranslate.Formulas.ComputeFm.Word 
import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.Formulas.Common

-- | Do basic computations. The notations can produce fat and stupid code, so it's good to make some simplifications.

basicCompute :: ComputeFm -> ComputeFm 

basicCompute f = basicComputeInside f

-- --------------------------------------------------------------------------------
-- Simplifications must start inside the formula, because there is more simplifications outside if the inside is already simplified (like unused variable). This is the job of basicComputeInside.

basicComputeInside :: ComputeFm -> ComputeFm 

basicComputeInside (Apply x y) = basicComputePattern $ Apply (basicComputeInside x) (basicComputeInside y)
basicComputeInside (Function v body) = basicComputePattern $ (Function v (basicComputeInside body))
basicComputeInside (Dict l) = basicComputePattern $ (Dict $ dictMap basicComputeInside l)
basicComputeInside (Obj v l) = basicComputePattern $ (Obj v $ dictMap basicComputeInside l)

basicComputeInside f = f

-- --------------------------------------------------------------------------------
-- | Do basic computations by patterns. The notations can produce fat and stupid code, so it's good to make some simplifications.
-- | - "$F x -> y x" -> "y" (où x n'apparait pas dans y)
-- | - "$F x -> body" -> constF body (où x n'apparait pas dans y). C'est util car 2 shortcuts existent dans compute pour ce cas ("constF f x" et "recurse constF f").
-- | - constF body x -> "body"
-- | - if body use 'x' one time and if there is no collision of variable then we replace x by y in body (see varNoCollision below) : "($F x -> body) y" -> "body2" with body2 = body where x is replaced by y
-- | - if body don't use f : "recurse $F f -> body" -> f

basicComputePattern :: ComputeFm -> ComputeFm 

basicComputePattern (Function v (Apply y (Word WtLVar x))) | v==x && varUnused v y  = y

basicComputePattern (Apply (Apply (Word WtDVar "constF") body) x) = body

basicComputePattern (Apply (Function v body) _) | varUnused v body  = (Word WtDVar "constF") &/ body

basicComputePattern (Function v body) | varUnused v body  = (Word WtDVar "constF") &/ body

-- Applies the value to the function if:
-- * the variable appears only once or
-- * the calculation of the argument is fast (like objApply) or shared (like a dvar)
-- For the argument to be applied it is necessary that the argument does not contain a free variable of the same name.
basicComputePattern (Apply (Function x body) y) 
  | ((varUsage x body == 1) || (fastCompute y)) && (varNoCollision x y body)
    = basicComputeInside $ replaceVar x y body -- new formula, so we re-do simplifications

basicComputePattern (Apply (Word WtDVar "recurse") (Apply (Word WtDVar "constF") body)) = body

basicComputePattern f = f

-- --------------------------------------------------------------------------------
-- | Check if an argument of a function worth to be applied, even if he is duplicated (see above).

fastCompute :: ComputeFm  -> Bool

fastCompute (Word _ _) = True
fastCompute (Function _ _) = True
fastCompute (NameRepr _) = True
fastCompute (FormulaRepr _) = True
fastCompute (Dict _) = True
fastCompute (Obj _ _) = True
fastCompute (Apply (Word WtDVar "pairFirst") (Word _ _)) = True
fastCompute (Apply (Word WtDVar "pairSecond") (Word _ _)) = True
fastCompute (Apply (Apply (Word WtDVar "objApply") (NameRepr _)) (Word _ _)) = True
fastCompute (Apply (Apply (Word WtDVar "dictApply") (NameRepr _)) (Word _ _)) = True
fastCompute _ = False

-- --------------------------------------------------------------------------------
-- | varNoCollision var y body check if we can replace a variable var by y in body without collision. A collision occurs when var is in a function of variable v where v is a free variable in y.

varNoCollision :: String -> ComputeFm -> ComputeFm  -> Bool

varNoCollision var y (Apply a b) = varNoCollision var y a && varNoCollision var y b

varNoCollision var y x@(Function v a) | varUsed var x  = not(varUsed v y) && varNoCollision var y a

varNoCollision var y (Dict l) = and $ map (varNoCollision var y) (snd (unzip l))

varNoCollision var y x@(Obj v l) | varUsed var x = not(varUsed v y) && varNoCollision var y (Dict l)

varNoCollision _ _ _ = True
