-- | Common to all formulas
module LaziTranslate.Formulas.Common where

import Data.Char
import qualified Data.Set as S

-- | Increment a string. The string must contain only "a-z" characters.
incrementString :: String -> String
incrementString s =
  case incrementString' s of
       (True,res) -> 'a' : res
       (False,res) -> res

-- | Increment a string, return True if we must increment the up character (like with numbers)
incrementString' :: String -> (Bool,String)

incrementString' (c:l) = 
  case incrementString' l of
    (True,l2) -> if c == 'z' then (True,'a':l2) else (False,chr (ord c + 1) : l2)
    (False,l2) -> (False,c:l2)

incrementString' [] = (True,[]) -- [] is like zero (in fact, we should write zero as an empty number).
    
-- --------------------------------------------------------------------------------
-- | Return all keys (as a Set) of a dictionary.
dictToKeys :: (Ord a) => [(a,b)] -> S.Set a
dictToKeys l = S.fromList $ fst $ unzip l 

-- --------------------------------------------------------------------------------
-- | Return all values (as a Set) of a dictionary.
dictToValues :: (Ord b) =>  [(a,b)] -> S.Set b
dictToValues l = S.fromList $ snd $ unzip l 

-- --------------------------------------------------------------------------------
-- | Map values of a dictionary.
dictMap ::  (b -> c) -> [(a,b)] -> [(a,c)]
dictMap f l = map ( \(k,v) -> (k,f v) ) l
