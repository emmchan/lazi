-- | Define Lazi_0_0Fm data type and functions for this type. Lazi_0_0Fm is the type of lazi.0.a formula (just apply and keywords).
module LaziTranslate.Formulas.Lazi_0_0Fm.Lazi_0_0Fm where

data Lazi_0_0Fm = 
  -- ^ A Word
  Word String|
  -- ^ Application of a function to an argument
  Apply Lazi_0_0Fm Lazi_0_0Fm
  deriving (Ord,Eq,Show)


depth :: Lazi_0_0Fm -> Int

depth (Word _) = 0
depth (Apply x y) = 1 + max (depth x) (depth y)
