module LaziTranslate.Sources where

import LaziTranslate.Formulas.ComputeFm.Definitions
import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.ParsRend.OpInfos





data SourcesItem = SourceDefFunc Definition |
                   SourceDefNota OpInfos

type Sources = [SourcesItem]


-- | Parser user state
-- | Informations from source needed to parse or render. For example, when we parse, we need to know operators already defined.
data ParsecUS = ParsecUS { 
  usOperatorsInfos :: OperatorsInfos , -- operators found
  usDefinitions :: DefsMap -- definition found
}

-- | Filter the sources to get just definitions.
getDefinitions :: Sources -> [Definition]
getDefinitions l = concatMap  getDefinition l

getDefinition :: SourcesItem -> [Definition]
getDefinition (SourceDefFunc d)=[d]
getDefinition _ = []

-- | Filter the sources to get just OpInfos.
getOperatorsInfos :: Sources -> OperatorsInfos
getOperatorsInfos l = concatMap  getOpInfos l

getOpInfos :: SourcesItem -> OperatorsInfos
getOpInfos (SourceDefNota d)=[d]
getOpInfos _ = []

-- | Filter the sources to get the user state of the parser
getUS :: Sources -> ParsecUS
getUS s = ParsecUS { usOperatorsInfos = getOperatorsInfos s , usDefinitions = definitionsMap $ getDefinitions s }

-- | Check for errros and return an error string (empty if no error).
checkSources :: Sources -> String
checkSources s = 
  case checkDefinitions (getDefinitions s) of
       Just (n,err) -> "Error in the definition of " ++ n ++ ": " ++ err
       Nothing -> ""

       
-- Convert a Sources in a lazi.1 formula sources.
sourcesToLazi1 :: String -> Sources -> Sources
sourcesToLazi1 name s = [ SourceDefFunc $ Definition name $
    Dict [
      (
        "defs",
        Dict $ map (\(Definition name val)-> (name,FormulaRepr val)) (getDefinitions s)
      )]
  ]
