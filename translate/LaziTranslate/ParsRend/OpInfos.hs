-- | Define the data structure "OpInfos" and some utilites to use it. A "OpInfos" give informations about an operator.
module LaziTranslate.ParsRend.OpInfos  where

import Text.ParserCombinators.Parsec

import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.ParsRend.Opparenth.OppExpression
import LaziTranslate.Formulas.SourcesFm.SourcesFm


-- Informations about operators: name, type of item (cf Opparenth.OppExpression), 
-- the string used to render the item, the SourcesFm to represent the item.
data OpInfos = OpInfos OperatorType Precedence String SourcesFm deriving (Eq, Show)

type OperatorsInfos = [OpInfos]

-- --------------------------------------------------------------------
-- | Used to recognise an operator from an expression.
-- | Search for an operator with n (1 or 2) arguments corresponding to a function given as an expression (the third argument). If nothing is found then return Nothing.
searchOpInfos :: OperatorsInfos -> Int -> SourcesFm -> Maybe OpInfos

searchOpInfos [] _ _ = Nothing

searchOpInfos (i:l) n exp =
    let found =
                case n of
                  1 ->
                    case i of
                      OpInfos Prefix _ _ e -> e==exp
                      OpInfos Postfix _ _ e -> e==exp
                      _  -> False
                  2 ->
                    case i of
                      OpInfos Infix _ _ e -> e==exp
                      _ -> False
    in
      if found then Just i else searchOpInfos l n exp

-- ------------------------------------------------
-- | Same as searchOpInfos but return an Element as needed by the Opparenth librayry.
searchOpp :: OperatorsInfos -> Int -> SourcesFm -> Maybe (Element SourcesFm)

searchOpp table args expr =
      case searchOpInfos table args expr of
          Just (OpInfos opty prec _ _) -> Just (Item (Operator prec opty) expr)
          Nothing -> Nothing

-- --------------------------------------------------------------------
-- Selecting in the list
      
-- | Reduce the list of items infos by filtering on the OperatorType
filterOpInfos :: ( OperatorType -> Bool) -> OperatorsInfos -> OperatorsInfos
filterOpInfos p l = filter (\ii@(OpInfos ty _ _ _) -> p ty) l

filterPrefixOp :: OperatorType -> Bool
filterPostfixOp :: OperatorType -> Bool
filterInfixOp :: OperatorType -> Bool

filterPrefixOp Prefix = True
filterPrefixOp _ = False

filterPostfixOp Postfix = True
filterPostfixOp _ = False

filterInfixOp  Infix = True
filterInfixOp _ = False

