-- | The data structure used to represent the result of the operators assembling.
{-# LANGUAGE ScopedTypeVariables #-}
module LaziTranslate.ParsRend.Opparenth.OppExpression where


-- | A UnderExpr structure stand for an item of the expression that we analyse or render.
-- |  "apply" is the function to assemble a function with its argument.
class (Show a) => UnderExpr a where
    -- | apply : apply a function AST to an argument AST
    applyUnderExpr :: a -> a -> a
    -- | The priority between 2 non-operators. The argument is just here to bind the type with the function.
    applyPrec :: a -> Precedence
    -- | The priorities of separators. The argument is just here to bind the type with the function.
    sepsPrec :: a -> [Precedence]

type Precedence = Int

-- | An item is an element of the expression to parse, it can be an operator a separator or a parentheses. "i" is the type of "operator information", it is user data but we must be able to test if two operators are equals for associativity.
data Element t =
  Item ItemType t | -- ^ An operator or an opaque item.
  Separator Precedence | -- ^ A separator
  Parentheses [ Element t ] -- ^ An expression in parentheses
    deriving (Eq,Show)

        
        
-- | Type of an item (see Item).
data ItemType =
    NonOp ProtKind| -- ^ if the item is not an operator
    Operator Precedence OperatorType -- ^ for operator: precedence, type and user-data
      deriving (Eq,Show)

-- | What kind of protection this item need
data ProtKind =
    NoProt | -- no protection
    ProtRight -- if there is something on the right, then enclose the item in parentheses
        deriving (Eq,Show)

-- | See "data Operator "
data OperatorType  = Prefix | Postfix | Infix deriving (Eq,Show)

-- | An OppExpression is an expression easily managed for Opparenth and which represent an
-- |  expression: list of items.

type OppExpression t = [ Element t ]

  
-- ----------------------------------------------------------------------------------
-- | Return the precedence on the left of the element which can't be a separatator.
precOnLeft :: forall t . (UnderExpr t) => Element t -> Precedence

precOnLeft (Item (NonOp _) _) = (applyPrec (undefined :: t))
precOnLeft (Item (Operator prec Prefix) _) = (applyPrec (undefined :: t))
precOnLeft (Item (Operator prec _) _) = prec
precOnLeft (Parentheses _) = (applyPrec (undefined :: t))

-- ----------------------------------------------------------------------------------
-- | Return the precedence on the right of the element which can't be a separatator.
precOnRight :: forall t . (UnderExpr t) => Element t -> Precedence

precOnRight (Item (NonOp _) _) = (applyPrec (undefined :: t))
precOnRight (Item (Operator prec Postfix) _) = (applyPrec (undefined :: t))
precOnRight (Item (Operator prec _) _) = prec
precOnRight (Parentheses _) = (applyPrec (undefined :: t))

-- ----------------------------------------------------------------------------------
-- | Return the precedence, the weakest of the left or right

getPrecedence :: (UnderExpr t) => Element t -> Precedence

getPrecedence (Separator prec) = prec
getPrecedence x = min (precOnLeft x) (precOnRight x)


-- ----------------------------------------------------------------------------------
-- | Return the weakest precedence in a list

weakestPrec :: UnderExpr t => OppExpression t -> Precedence

weakestPrec l = minimum $ map getPrecedence l

-- ----------------------------------------------------------------------------------------
-- | Return if the elemeent is an infix operator or a separator
isInfixOrSep :: (UnderExpr t) => Element t -> Bool

isInfixOrSep (Item (Operator _ Infix) _)=True
isInfixOrSep (Separator _) = True
isInfixOrSep _ = False

-- --------------------------------------------------------
isOperator :: (UnderExpr t) =>Element t -> Bool
isOperator (Item  (Operator _ _) _) = True
isOperator _ = False

-- --------------------------------------------------------
isSeparator :: (UnderExpr t) =>Element t -> Bool
isSeparator (Separator _) = True
isSeparator _ = False

-- --------------------------------------------------------
isInfix :: (UnderExpr t) =>Element t -> Bool
isInfix (Item  (Operator _ Infix) _) = True
isInfix _ = False

-- --------------------------------------------------------
isPrefix :: (UnderExpr t) =>Element t -> Bool
isPrefix (Item  (Operator _ Prefix) _) = True
isPrefix _ = False

-- --------------------------------------------------------
isPostfix :: (UnderExpr t) =>Element t -> Bool
isPostfix (Item  (Operator _ Postfix) _) = True
isPostfix _ = False

-- ----------------------------------------------------------------
-- | Tell if the element need a protection on right.

isProtRight :: (UnderExpr t) => Element t -> Bool
isProtRight (Item (NonOp ProtRight) _) = True
isProtRight _ = False

-- ----------------------------------------------------------------
-- | Tell if a list of elements need a protection on right (yes if the last element need it).
isListProtRight :: (UnderExpr t) => OppExpression t -> Bool
isListProtRight l = isProtRight $ last l
