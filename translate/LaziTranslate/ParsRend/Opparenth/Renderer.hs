-- | Opparenth.Renderer is an independant library used to compute where to put parentheses in an expression with operators in the aim of having a minimal number of parentheses.
{-# LANGUAGE ScopedTypeVariables #-}


module LaziTranslate.ParsRend.Opparenth.Renderer ( reduceParenth ) where

import Data.Maybe

import LaziTranslate.ParsRend.Opparenth.OppExpression


-- ----------------------------------------------------------------
-- | Take an expression OppExpression and return the expression with less parentheses as possible. The expression must be non empty. reduceParenth is called also in recurrence with an argument which is protected (by surrounding parentheses).

reduceParenth :: UnderExpr t => OppExpression t -> OppExpression t

-- Special simple case when the expression has just one item.
reduceParenth [ i@(Item _ _) ] = [ i ]

-- Special simple case when the expression has overall parentheses.
reduceParenth [ Parentheses ly ] = reduceParenth ly

-- When there is more than one element. reduceParenth is called by reduceParenthStart and reduceParenthAfter. So insertSeparators is called deeply first.
reduceParenth l = insertSeparators $ reduceParenthAfter $ reduceParenthStart l


-- ----------------------------------------------------------------
-- | Reduce parentheses of the first element of the list.
reduceParenthStart :: UnderExpr t => OppExpression t -> OppExpression t

reduceParenthStart l@( (Parentheses ly) : lr) | isListProtRight ly = Parentheses (reduceParenth ly) : lr

-- An element alone in the parenthese which doesn't need protection.
reduceParenthStart ( (Parentheses [x]) : l ) = x:l

reduceParenthStart ( (Parentheses ly) : zl@(z : _) ) =
    -- if we can remove the parenthese
    if weakestPrec ly >= precOnLeft z 
      then 
        reduceParenthStart ( ly ++ zl )
      else
        (Parentheses (reduceParenth ly)) : zl -- Else just recurse.

-- If the first element is not a Parentheses.
reduceParenthStart l = l

-- ----------------------------------------------------------------
-- | Reduce parentheses of all elements of the list except the first.
reduceParenthAfter :: UnderExpr t => OppExpression t -> OppExpression t

-- If it's the last element
reduceParenthAfter [ x, (Parentheses ly) ] =
    -- if we can remove the parenthese
    if length ly == 1 || precOnRight x < weakestPrec ly || startWithPrefix ly
      then 
        reduceParenthAfter (x : ly)
      else
        [ x , Parentheses (reduceParenth ly) ] -- Else just recurse.

reduceParenthAfter (x : (l@( (Parentheses ly) : lr))) | isListProtRight ly = 
  x : reduceParenthAfter (Parentheses (reduceParenth ly) : lr)

-- An element alone in the parenthese which doesn't need protection.
reduceParenthAfter (x : ( (Parentheses [y]) : lr )) = x:reduceParenthAfter (y:lr)

reduceParenthAfter (x : ( (Parentheses ly) : zl@(z : _) )) =
  let precLy = weakestPrec ly -- Weakest precedence in ly.
  in
    -- if we can remove the parenthese
    if (precOnRight x < precLy || startWithPrefix ly)  && precLy >= precOnLeft z 
      then 
        reduceParenthAfter ( x : (ly ++ zl) )
      else
        x : reduceParenthAfter ((Parentheses (reduceParenth ly)) : zl) -- Else just recurse.

-- If there is no parentheses, go to the next element. We have "lenght lr >= 2".
reduceParenthAfter (x : lr@(_:_:_)) = x : reduceParenthAfter lr

-- Else it was the last element.
reduceParenthAfter l = l

-- ----------------------------------------------------------------
-- | Try to insert separators. We don't try to insert a lot of separators, because it would be too much, for exemple "f ( x y , z t )" is better than "f | x y || z t". We just try to replace :
-- | - "( expr1 )( expr2 )" by " expr1 sep expr2 "
-- | - " f ( expr1 )" by " f sep expr1 "

insertSeparators :: forall t . UnderExpr t => OppExpression t -> OppExpression t

insertSeparators [(Parentheses lx), (Parentheses ly)] =
    let mprec = getInfPrec (sepsPrec (undefined :: t)) (min (weakestPrec lx) (weakestPrec ly)) in
      if isJust mprec
        then
          insertSeparators ( lx ++ (Separator (fromJust mprec) : ly ) )
        else
          [(Parentheses (insertSeparators lx)) , (Parentheses (insertSeparators ly))]

insertSeparators [x, (Parentheses ly)] =
    let mprec = getInfPrec (sepsPrec (undefined :: t)) (min (precOnRight x) (weakestPrec ly)) in
      if isJust mprec
        then
          insertSeparatorsRec $ x : Separator (fromJust mprec) : ly 
        else
          [x, (Parentheses (insertSeparators ly))]

-- else just recurse
insertSeparators l = insertSeparatorsRec l

-- ----------------------------------------------------------------
-- | Maybe return a precedence (from a list of precedence) inferior or equal to prec.
getInfPrec :: [Precedence] -> Precedence -> Maybe Precedence
getInfPrec l prec = 
  let lf = filter (\x -> x <= prec) l in
    if null lf then Nothing else Just $ maximum lf

-- ----------------------------------------------------------------
-- | Recursion in parentheses for insertSeparators

insertSeparatorsRec :: UnderExpr t => OppExpression t -> OppExpression t

insertSeparatorsRec ((Parentheses lx) : lr) = (Parentheses (insertSeparators lx)) : insertSeparatorsRec lr

insertSeparatorsRec (x:lr) = x : insertSeparatorsRec lr

insertSeparatorsRec [] = []

-- ----------------------------------------------------------------
-- | The OppExpression start with a prefix ?

startWithPrefix :: UnderExpr t => OppExpression t -> Bool

startWithPrefix ((Item (Operator _ Prefix) _):_) = True
startWithPrefix _ = False
