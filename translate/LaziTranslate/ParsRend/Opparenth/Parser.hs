-- | Opparenth.Parser is an independant library used to assemble operator and arguments using precedence.
-- | Parser translate an OppExpression t to t.
-- | Read the documentation to understand the algorithme : http://lazi.bobu.eu/wiki/index.php/Études-Mathématique/Syntaxe_et_précédence
{-# LANGUAGE ScopedTypeVariables #-}

module LaziTranslate.ParsRend.Opparenth.Parser ( assemble )  where

import Debug.Trace
import LaziTranslate.ParsRend.Opparenth.OppExpression

-- -----------------------------------------------------------------
-- | Main function: Get an expression and return the result: an UnderExpr object or an error.
assemble :: UnderExpr t => OppExpression t -> t

assemble e = replaceApply  $ replaceInfix  $ replacePostfix  $ replacePrefix $ replaceSeps $ replaceParenths e
 
-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- | Replace parentheses : replace parentheses by the corresponding item, using assemble.

replaceParenths :: UnderExpr t => OppExpression t -> OppExpression t

replaceParenths = map parenthsToItems where

  parenthsToItems :: UnderExpr t => Element t -> Element t 

  parenthsToItems (Parentheses l) = Item (NonOp NoProt) (assemble l)
  parenthsToItems x = x

-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- | Replace separators 

replaceSeps :: UnderExpr t => OppExpression t -> OppExpression t

replaceSeps ( (Separator prec) : lr ) = replaceSeps $ replaceASep prec lr
replaceSeps (x:lr) = x : replaceSeps lr
replaceSeps []=[]
-- ----------------------------------------------------------------
-- | Given the list after a separator, return the list where the part after the separator is replaced by just an item. prec is a precedence of the separator.
replaceASep :: UnderExpr t => Precedence -> OppExpression t -> OppExpression t
replaceASep prec l = let (lSep,lr)=sepSplitToEnd prec l in Item (NonOp NoProt) (assemble lSep) : lr
                       
-- ----------------------------------------------------------------
-- | Given the list after a separator, return the list to the end of the separator area and the list of the rest
sepSplitToEnd  :: UnderExpr t => Precedence -> OppExpression t -> (OppExpression t,OppExpression t)

-- Cases where we stop.
sepSplitToEnd prec [] = ([],[])

sepSplitToEnd prec l@((Separator prec2):_) | prec2 < prec = ([],l)

sepSplitToEnd prec l@((Item (Operator prec2 Postfix) _):_) | prec2 < prec = ([],l)

sepSplitToEnd prec l@((Item (Operator prec2 Infix) _):_) | prec2 < prec = ([],l)

-- Else we continue.
sepSplitToEnd prec (x:l)= let (ls,lr)=sepSplitToEnd prec l in (x:ls,lr)
 
-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- | Replace prefixs

replacePrefix:: UnderExpr t => OppExpression t -> OppExpression t

replacePrefix ((Item (Operator prec Prefix) op):lr) = replacePrefix $ replaceAPrefix op prec lr
replacePrefix (x:lr) = x : replacePrefix lr
replacePrefix [] =  []
-- ----------------------------------------------------------------
-- | Given the list after a prefix , return the list where the prefix part is replaced by just an item.
replaceAPrefix :: UnderExpr t => t -> Precedence -> OppExpression t -> OppExpression t

replaceAPrefix op prec l = 
  let (larg,lr)=prefixSplitArg prec True l in 
    Item (NonOp NoProt) (applyUnderExpr op (assemble larg)) : lr

-- ----------------------------------------------------------------
-- | Given the list after a prefix, return the list of the arg and the list of the rest. The boolean tell :
-- | True -> we must get the next element blindly, this is the case at start or after an infix.
-- | False -> the last element of the argument was a non-op.
prefixSplitArg  :: forall t . UnderExpr t => Precedence -> Bool -> OppExpression t -> (OppExpression t,OppExpression t)

-- If we reach the end.
prefixSplitArg _ _ [] = ([],[])

-- If we accounter an other prefix in the argument list, then we replace it by its value, and we continue.
prefixSplitArg prec waitArg ((Item (Operator prec2 Prefix) pref):lr) = prefixSplitArg prec waitArg $ replaceAPrefix pref prec2 lr

prefixSplitArg prec _ (op@(Item (Operator prec2 Postfix) _):lr) | prec2>prec = 
  let (lArg,lr2)=prefixSplitArg prec False lr in (op:lArg,lr2)

prefixSplitArg prec _ (op@(Item (Operator prec2 Infix) _):lr) | prec2>prec = 
  let (lArg,lr2)=prefixSplitArg prec True lr in (op:lArg,lr2)
      
-- If we wait an argument, because prefix case is already handled, it can only be a nonOp.
prefixSplitArg prec True (nonOp:lr) = 
  let (lArg,lr2)=prefixSplitArg prec False lr in (nonOp:lArg,lr2)

-- If it's not after an operator, because we handled operators cases before, we have just to handle a nonOp.
prefixSplitArg prec False (nonOp@(Item (NonOp _) _):lr) | (applyPrec (undefined :: t))>prec = 
  let (lArg,lr2)=prefixSplitArg prec False lr in (nonOp:lArg,lr2)

-- For the other cases, we stop to take the arguments.
prefixSplitArg _ _ l =  ([],l)

-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- | Replace all postfixs.
-- We split the list at the first postfix, all we need is in the first chunk. We reverse it and we get all with a precedence superior or equal.
replacePostfix:: UnderExpr t => OppExpression t -> OppExpression t

replacePostfix l = 
  let (ls,postfix_lr) = break isPostfix l in
    if null postfix_lr -- If there is no prefix.
      then
        l
      else
        let ((Item (Operator prec Postfix) postfix):lr) = postfix_lr
            (lArg,lr2)= postfixSplitArg prec True (reverse ls)
        in
          -- We reconstruct l with the replacement of the postfix and its argument, then we recurse to replace other postfixs.
          replacePostfix $ reverse lr2 ++ ( Item (NonOp NoProt) (applyUnderExpr postfix (assemble $ reverse lArg)) : lr)


-- ----------------------------------------------------------------
-- | Given the list after before a postfix, in a **reverse order**, return the list of the arg and the list of the rest. The boolean tell :
-- | True -> we must get the next element blindly, this is the case at start or after an infix.
-- | False -> the last element of the argument was a non-op.
-- | We can't accounter a postfix (because we took the first), so we just have to handle infix and non-op.
postfixSplitArg  :: forall t . UnderExpr t => Precedence -> Bool -> OppExpression t -> (OppExpression t,OppExpression t)

-- If we reach the end.
postfixSplitArg _ _ [] = ([],[])

postfixSplitArg prec _ (op@(Item (Operator prec2 Infix) _):lr) | prec2>=prec = 
  let (lArg,lr2)=postfixSplitArg prec True lr in (op:lArg,lr2)
      
-- If we wait an argument, it can only be a nonOp.
postfixSplitArg prec True (nonOp:lr) = 
  let (lArg,lr2)=postfixSplitArg prec False lr in (nonOp:lArg,lr2)

-- If it's not after an operator, because we handled operators cases before, we have just to handle a nonOp.
postfixSplitArg prec False (nonOp@(Item (NonOp _) _):lr) | (applyPrec (undefined :: t))>=prec = 
  let (lArg,lr2)=postfixSplitArg prec False lr in (nonOp:lArg,lr2)

-- For the other cases, we stop to take the arguments.
postfixSplitArg _ _ l =  ([],l)

-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- | Replace all infixs.
-- We do like replacePostfix and for getting arguments we use the functions for a prefix and a postfix
replaceInfix :: UnderExpr t => OppExpression t -> OppExpression t

replaceInfix l = 
  let (ls,infix_lr) = break isInfix l in
    if null infix_lr -- If there is no infix.
      then
        l
      else
        let ((Item (Operator prec Infix) infx):lr) = infix_lr
            (lArg,lr2)= postfixSplitArg prec True (reverse ls)
            op = applyUnderExpr infx (assemble $ reverse lArg) -- The infix with its first argument.
        in
          -- We reconstruct l with the replacement of the infix and its arguments, then we recurse to replace other infixs.
          replaceInfix $ reverse lr2 ++ ( replaceAPrefix op prec lr )


-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- | Replace all applications. 
-- At this stage, there is just application of non-op.

replaceApply :: UnderExpr t => OppExpression t -> t

replaceApply l = foldl1 applyUnderExpr $ map (\(Item (NonOp _) x)->x) l
