-- Convert abstract syntax tree to expression (string).
module LaziTranslate.ParsRend.CommonRenderer(formulaNotaToOpp,toListNotation)  where

import Data.List
import Data.Char(isAlphaNum)
import Data.Maybe


import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.ParsRend
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.ParsRend.Opparenth.OppExpression
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E



-- | Convert a Boolean list to a String of 1/0
boolListToString :: [Bool] -> String
boolListToString l= map (\b -> if b then '1' else '0') l



-- --------------------------------------

-- Translation a SourcesFm to an Element SourcesFm


-- | Translate a SourcesFm to a (Element SourcesFm), exceeding parentheses will be removed later.
formulaNotaToOpp :: 
  -- | A function to recognise operators expression and to convert it to a Element.
  (Int -> SourcesFm -> Maybe (Element SourcesFm) ) 
  -- | The expression to translate.
  -> SourcesFm 
  -- | The result.
  -> Element SourcesFm

formulaNotaToOpp tr (Apply opx@(Apply f x) y)=
    case tr 2 f of
        Just op -> Parentheses [ formulaNotaToOpp tr x, op, formulaNotaToOpp tr y ]
        Nothing -> Parentheses [ formulaNotaToOpp tr opx, formulaNotaToOpp tr y ]

formulaNotaToOpp tr (Apply f x) =
    case tr 1 f of
        Just op@(Item (Operator _ Postfix) _) -> Parentheses [ formulaNotaToOpp tr x , op ]
        Just op -> Parentheses [ op, formulaNotaToOpp tr x ]
        Nothing -> Parentheses [ formulaNotaToOpp tr f , formulaNotaToOpp tr x ]

formulaNotaToOpp tr fw@(ReverseArg f w)  = 
    case tr 2 f of
        Just (Item (Operator prec Infix) _) -> Item (Operator prec Postfix) fw
        Nothing -> error $ "In formulaNotaToOpp. ReverseArg without corresponding infix operator:\n" ++ show fw
    
formulaNotaToOpp _ e | needRightProtect e = Parentheses [ Item (NonOp ProtRight) e ]

formulaNotaToOpp _ e = Item (NonOp NoProt) e

-- ---------------------------------------------------------------------------
-- | Recognise a list constructed with emptyList and bwAddToList or toListBwAdd and translate to the notation.

toListNotation :: SourcesFm -> SourcesFm

toListNotation a@(Apply x y) = toListNotation' (toListNotation x &/ toListNotation y)
toListNotation x = x


toListNotation' :: SourcesFm -> SourcesFm

toListNotation' (Apply (Apply (Word WtDVar "bwAddToList") x) (Word WtDVar "∅l")) = List [x]

toListNotation' (Apply (Apply (Word WtDVar "toListBwAdd") (Word WtDVar "∅l")) x) = List [x]

toListNotation' (Apply (Apply (Word WtDVar "bwAddToList") x) (List l)) = List (l ++ [x])

toListNotation' (Apply (Apply (Word WtDVar "toListBwAdd") (List l)) x) = List (l ++ [x])

toListNotation' x = x




