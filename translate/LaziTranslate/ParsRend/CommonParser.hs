-- | Some useful functions for parsers in "Language" directory.

module LaziTranslate.ParsRend.CommonParser where

import Text.ParserCombinators.Parsec
import Text.Parsec.Prim hiding (try)
import Text.ParserCombinators.Parsec.Char
import Data.List
import Data.Map (member,insert)
import qualified Data.Set as S
import Data.Char



import LaziTranslate.Sources
import LaziTranslate.Formulas.ComputeFm.Definitions
import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.ParsRend.Opparenth.OppExpression
import LaziTranslate.ParsRend.Opparenth.Parser as Op
-- import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.ParsRend
import LaziTranslate.Formulas.ComputeFm.ComputeFm
import LaziTranslate.Formulas.ComputeFm.Word

type LazParsec = Parsec String ParsecUS 




-- | Parse a string when the value can be in a finite list of strings. Argument is:
-- | - a list of pair (string,result) where string is the expected attribute value and result is the returned value.
stringEnum :: [(String,a)] -> LazParsec a
stringEnum l = choice $ map (\(str,res) -> do { try ( string str ); return res } ) l
    
-- | Parse an integer
decimal :: LazParsec Int
decimal = do
    digits <- many1 digit
    let n = foldl (\x d -> 10*x + digitToInt d) 0 digits
    seq n (return n)



-- | What is a Lazi' Word
wordParser = wordParserNoLetterStart <|> wordParserLetterStart <?> "name"

wordParserNoLetterStart = do
    start <- many1 nameSymbolsNoAlphaNum
    rest <- many nameSymbols
    return (start ++ rest)
    

wordParserLetterStart = many1 nameSymbolsAlphaNum

   
------------------------------------------------------
-- Basic Parsing Utilities

-- | Any symbol in a name or operator
nameSymbols :: LazParsec Char
nameSymbols = nameSymbolsAlphaNum <|> nameSymbolsNoAlphaNum

-- | Parse the first symbol of a Lazi' name.
nameSymbolsNoAlphaNum :: LazParsec Char
nameSymbolsNoAlphaNum = satisfy charOkNoAlphaNum

-- | Parse a symbol which we can accounter inside a Lazi' name. 
-- | Actually the first symbol and others are in the same set.
nameSymbolsAlphaNum :: LazParsec Char
nameSymbolsAlphaNum = satisfy charOkAlphaNum

-- | Unicode categories ok at the start of the name (several of these symbols can be at start).
okInNoAlphaNumUnicodeCat = S.fromList [MathSymbol,CurrencySymbol,ModifierSymbol,OtherSymbol,ConnectorPunctuation,DashPunctuation,DashPunctuation,OtherPunctuation]

-- | Unicode categories ok inside the name
okAlphaNumUnicodeCat = S.fromList [UppercaseLetter,LowercaseLetter,TitlecaseLetter,OtherLetter,DecimalNumber,LetterNumber,OtherNumber]


-- | Return if a Char is autorised in a Lazi name at first place
charOkNoAlphaNum :: Char -> Bool
charOkNoAlphaNum c = S.member (generalCategory c) okInNoAlphaNumUnicodeCat && S.notMember c (S.fromList "∃∀@|\\,;:'\"$£€")
       

-- | Return alpha-num type authorized in names (with "_" added).
charOkAlphaNum :: Char -> Bool
charOkAlphaNum c = S.member (generalCategory c) okAlphaNumUnicodeCat || c=='_'
       
-- | A comma and a semicolon has the same function, the writer can use semicolon to emphasize the separation.
comma :: LazParsec Char
comma = satisfy (\c -> c == ',' || c == ';')




