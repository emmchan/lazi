-- | Parsing of basic elements, like blanks etc
module LaziTranslate.ParsRend.Language.Sources.ParserBasics  where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char
import Text.Parsec.Char
import Text.Parsec.Prim hiding (try)
import Control.Monad.Trans hiding (try)
import Data.Functor.Identity
import Data.List
import Data.Set (member,notMember,fromList)
import Data.Char

import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E
import LaziTranslate.ParsRend
import LaziTranslate.ParsRend.CommonParser
import LaziTranslate.Formulas.ComputeFm.ComputeFm

-- Parser to parse a sources file. It return a list of sourcesItems.

-- ---------------------  blank things ---------------------------- 

-- default value for the "blank separator" parser
blank = do { space <|> tab <|> newline <?> "space"; return () }

-- Parser for a many Sources blank space, possibly zero
blanks= skipMany blank

-- Parser for a many Sources blank space, at least one
blanks1= skipMany1 blank

-- default value for the "blank separator" parser
oneBlankCom = do { blanks1 <|> comment; return () }
                 
comment = lineComment <|> longComment <?> "comment"

blankCom = do
    skipMany oneBlankCom
    return ()

blankCom1 = do
    skipMany1 oneBlankCom
    return ()

lineComment = do
    try ( string "//" )
    skipMany (noneOf "\n")
    (do { newline; return () } ) <|> eof

longComment  :: LazParsec ()
longComment = do
    try (string "/*")
    longCommentContentAndEnd
 
longCommentContentAndEnd :: LazParsec ()
longCommentContentAndEnd = do
    skipMany (noneOf "*/")
    try ( do { string "*/" ; return () } ) <|> 
     ( do { longComment ;  longCommentContentAndEnd } ) <|> 
     ( do { anyChar ; longCommentContentAndEnd } ) 
     <?> "*//"
    
    

-- ---------------------  end of blank things ---------------------------- 

quotedStringParser :: LazParsec String
quotedStringParser =  many1 $ choice [
    do
        char '\\'
        anyChar 
    ,
    do
        noneOf "\""
   ]
   
