-- | Define "parseSources", all the other functions are part of the definition of parseSources.
module LaziTranslate.ParsRend.Language.Sources.Parser (sources , parseToComputeFm) where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char
import Text.Parsec.Char
import Text.Parsec.Prim hiding (try)
import Control.Monad.Trans hiding (try)
import Data.Functor.Identity
import Data.List
import qualified Data.Set as S
import qualified Data.Map as M
import Data.Char

import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E
import LaziTranslate.Formulas.ComputeFm.Definitions
import LaziTranslate.Sources
import LaziTranslate.ParsRend
import LaziTranslate.ParsRend.CommonParser
import LaziTranslate.ParsRend.Opparenth.Parser
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.SourcesFm.ToCompute.Base
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.ParsRend.Opparenth.OppExpression
import LaziTranslate.ParsRend.Language.Sources.ParserBasics
import LaziTranslate.ParsRend.Language.Sources.VarExp.Parser
import LaziTranslate.ParsRend.OpInfos
import LaziTranslate.Formulas.Common

-- Parser to parse a sources file. It return a list of sourcesItems.

-- | State of the parsing for the argument point of vue.
data ArgBefore = 
  -- ^ The last parsed element is a regular expression (like a parenthesed expression, or a keyword).
  ArgOnLeft |
  -- ^ This is the start of the expression
  NothingOnLeft | 
  -- ^ The last parsed element is an infox or prefix operator.
  OperatorOnLeft
    deriving (Eq,Show)

-- --------------------------------------------------------------------------------
-- --------------------------------- parse a sources file -------------------------

-- |  The argument is the "simplify" global option (-s).
sources :: Bool -> LazParsec Sources

sources s = do
    blankCom; 
    li <- sepEndBy (sourceItem s) blankCom
    eof
    return li

sourceItem s = oPNotaDef <|> globConstDef s <?> "$Operator or $Def"

-- ----------------------- Parse an operator notation definition.

-- | $Nota : notation to define an operator notation. The notation is stored in the state and returned.
oPNotaDef :: LazParsec SourcesItem
oPNotaDef = do
    try ( do { string "$Operator" ; blanks1 } )
    name <- wordParser
    blanks1
    opType <- opTypeParser name
    blanks1
    prior <- decimal
    blanks1
    value <- parseToSourcesFm
    let opInfos = OpInfos opType prior name value
    -- Check the error "Operator already defined."
    us <- getState
    if not $ null $ filter (\(OpInfos _ _ n _) -> name == n) (usOperatorsInfos us)
      then
        parserFail $ "Operator \"" ++ name ++ "\" already defined."
      else
        do
          --
          modifyState $ (\us -> us{usOperatorsInfos = opInfos : usOperatorsInfos us })
          return $ SourceDefNota opInfos
      
opTypeParser :: String -> LazParsec OperatorType
opTypeParser name = 
  do
  {
    try $ stringEnum  [("prefix",Prefix),("postfix",Postfix),("infix",Infix)]
  }
    <?> "operator type"

    
-- ----------------------- Parse a definition of a global const name

-- | $Def : define a symbol. The argument is the "simplify" global option (-s).
globConstDef :: Bool -> LazParsec SourcesItem
globConstDef s = do
    try ( do { string "$Def" ; blanks1 } )
    name <- wordParser
    blanks
    char '='
    blankCom
    fn <- parseToSourcesFm
    us <- getState
    let value = sourcesToCompute ( M.keysSet (usDefinitions us) ) s $ 
                  if M.member "recurse" (usDefinitions us)
                    then (wd "recurse") &/ (FunctionM [(VWord name)] fn)
                    else fn
    -- We always use the recursive form because basicCompute can simplify it.
    setState $ us { usDefinitions = M.insert name value (usDefinitions us) }
    return $ SourceDefFunc $ Definition name value
    
   
-- ---------------------------------------------------------------------------------------
-- ---------- Parse a formula to a E.ComputeFm   -----------------

-- ---------------------------------------------------------------------------------------
-- -------------------------------------- parse a formula to a E.ComputeFm -----------------
-- Free words (not keywords or variables) are set as global constant

-- | Parse an expression and return a ComputeFm. The argument is the "simplify" global option (-s).
parseToComputeFm :: Bool -> LazParsec E.ComputeFm
parseToComputeFm s =  do
  f <- parseToSourcesFm
  eof
  us <- getState
  return $ sourcesToCompute (M.keysSet (usDefinitions us)) s f

-- ---------------------------------------------------------------------------------------
-- -------------------------------------- parse a formula to a SourcesFm ---------------
   
-- | Same as "expression" but use Opparenth to return the SourcesFm (so without parentheses).

parseToSourcesFm :: LazParsec SourcesFm
parseToSourcesFm = do
    e <- oppExpression
    return (assemble e)


-- ---------------------------------------------------------------------------------------
-- ---------------------- parse a formula to a OppExpression SourcesFm -----------------

-- | Parse an expression
oppExpression :: LazParsec (OppExpression SourcesFm)
oppExpression =  do
    blankCom
    e1 <- parseOneElement NothingOnLeft
    blankCom
    e <- sepEndBy (parseOneElement ArgOnLeft) blankCom
    return (e1 ++ concat e)

-- | Parse one element of an expression. The ArgBefore (see definition on top) tell if what kind of expression is parsed before.
parseOneElement :: ArgBefore -> LazParsec (OppExpression SourcesFm)

parseOneElement NothingOnLeft =
  parenthesizedExpr <|> prefixOpExpr <|> 
    toMulti basicName <|> toMulti specialExpr <?> "formula"

parseOneElement ArgOnLeft = 
  parenthesizedExpr <|> prefixOpExpr <|> infixOpExpr <|> separatorExpr <|> 
    toMulti postfixOpExpr <|> toMulti basicName <|> toMulti specialExpr <?> "formula"

parseOneElement OperatorOnLeft =
  parenthesizedExpr <|> prefixOpExpr <|> separatorExpr <|> 
    toMulti basicName <|> toMulti specialExpr <?> "formula"


-- ----------- parse expression with operators


parserPrefix = parserFromOperatorsInfos $ filterOpInfos filterPrefixOp 
parserPostfix = parserFromOperatorsInfos $ filterOpInfos filterPostfixOp 
parserInfix = parserFromOperatorsInfos $ filterOpInfos filterInfixOp 

-- | Given a filter on OpInfos list, create the corresponding parser.
parserFromOperatorsInfos :: ( OperatorsInfos -> OperatorsInfos ) -> LazParsec (Element SourcesFm)
parserFromOperatorsInfos filt =  do
    us <- getState
    choice  $ map (\ii -> parserFromOpInfos ii) (filt $ usOperatorsInfos us)

-- | Given an OpInfos create a parser for the string that return the corresponding item.
parserFromOpInfos :: OpInfos -> LazParsec (Element SourcesFm)
parserFromOpInfos ( OpInfos ot prec st expr ) =
    try (do
            string st
            notFollowedBy nameSymbols
            return (Item (Operator prec ot) expr)
        )

prefixOpExpr :: LazParsec (OppExpression SourcesFm)
prefixOpExpr =  do
    op <- try parserPrefix
    blankCom1
    -- After a prefix operator we need a "true" element (not just an operator), this can be an other prefix operator followed by a non operator for example.
    f <- parseOneElement OperatorOnLeft <?> "formula after prefix operator"
    return $ op:f

postfixOpExpr :: LazParsec (Element SourcesFm)
postfixOpExpr = parserPostfix


infixOpExpr :: LazParsec (OppExpression SourcesFm)
infixOpExpr = do
    op@(Item (Operator prec Infix) fop) <- parserInfix
    do -- case op'word notation
    {
      char '\'';
      w <- wordParser;
      return [ Item (Operator prec Postfix) (ReverseArg fop w) ]
    }
     <|> 
      do -- Normal case: an argument must follow the operator
      {
        blankCom1;
        -- "True" because we need an element as in a starting expression after an infix.
        f <- parseOneElement OperatorOnLeft <?> "formula after infix operator" ; 
        return $ op:f
      }

-- | Parse a separator
separatorExpr ::  LazParsec (OppExpression SourcesFm)
separatorExpr = do
  char separatorChar
  prec <- do{char separatorChar;return bigSepPrec} <|> return smallSepPrec;
  blankCom ;
  -- The separator is like an open parenthesis, so after it it's like a new expression.
  f <- parseOneElement NothingOnLeft <?> "formula after a separator (a " ++ simpleSep ++ " or " ++ doubleSep ++ " )" ;
  return $ Separator prec : f

-- ----------- parse ( xd fe , eo fe , ef ze )
    
parenthesizedExpr :: LazParsec (OppExpression SourcesFm)
parenthesizedExpr = do
    try( char '(' )
    exprList <- ( sepBy1 oppExpression (do { blankCom ; comma; blankCom } ))
    char ')'
    return $ map Parentheses exprList
    
basicName :: LazParsec (Element SourcesFm)
basicName = do
    -- get the word-type
    wt <- do 
          {
            char ':';
            wt <- stringEnum wtPairs <?> "Word type : " ++ intercalate ", " wtNames;
            char ':';
            return wt
          }
            <|> return WtUnknown
    name <- wordParser
    return (Item (NonOp NoProt) (Word wt name))

specialExpr ::  LazParsec (Element SourcesFm)
specialExpr = quantifExpr  <|> someOtherSourcesExpr


-- --------------------------------------------------------------------------------
-- -------------------------------------- simple Items ----------------------------

quantifExpr :: LazParsec (Element SourcesFm)
quantifExpr =
    do
        expr <- quantifForAll <|> quantifExists
        return (Item (NonOp ProtRight) expr)


someOtherSourcesExpr :: LazParsec (Element SourcesFm)
someOtherSourcesExpr =
    do
        expr <- choiceSomeOtherSourcesExpr
        return (Item (NonOp (if needRightProtect expr then ProtRight else NoProt)) expr)

quantifForAll :: LazParsec SourcesFm
quantifForAll = do
  char '∀'
  char 'l'
  rev <- (do { char 'r'; return True } <|> return False);
  blanks1
  var <- varExp
  blanks
  char '/'
  listoe <- parseToSourcesFm
  char ';'
  p <- parseToSourcesFm
  return $ ListQuantification Universal rev var listoe p
 
quantifExists :: LazParsec SourcesFm
quantifExists = do
  char '∃'
  char 'l'
  rev <- (do { char 'r'; return True } <|> return False);
  blanks1
  var <- varExp
  blanks
  char '/'
  listoe <- parseToSourcesFm
  char ';'
  p <- parseToSourcesFm
  return $ ListQuantification Existential rev var listoe p
       
choiceSomeOtherSourcesExpr :: LazParsec SourcesFm
choiceSomeOtherSourcesExpr = choice [
    do
        try( do { string "$F" ; blanks1 } )
        vars <- varExpList
        (string "->" <|> string"→")
        expr <- parseToSourcesFm
        return (FunctionM vars expr)
    ,
    do
        try( do {string "$Let"; blanks1 } )
        var <- varExp
        blanks
        string "="
        value <- parseToSourcesFm
        comma
        body <- parseToSourcesFm
        -- We can have recursivity only on single variable
        if
          case var of
            VWord _ -> True
            _ -> S.null $ S.intersection (getVarNames var)(freeVars value)
        then
          return $ Let var value body
        else
          parserFail "A \"$Let\" can only be recursive with single variable."
    ,
    do
        try(char '\'')
        w <- wordParser
        return (NameRepr w)
    ,
    do
        try( string "$F[" )
        expr <- parseToSourcesFm
        char ']'
        return (FormulaRepr expr)
    ,
    do
        try( string "$L[" )
        exprList <- expressionList
        char ']'
        return (List exprList)
    ,
    do -- Dictionary: $D[ a1 = x1 , ...., an = xn ]
        try( string "$D[" )
        l <- sepBy assignmentWordParser (comma ) 
        char ']'
        return $ Dict l
    ,
    do -- Object: $O[ a1 = x1 , ...., an = xn ]
        try( string "$O[" )
        l <- sepBy assignmentWordParser (comma ) 
        char ']'
        if S.member "this" (dictToKeys l)
          then  
            parserFail $ "The object name (this) can't be a key."
          else
            return $ Obj (VAssign "this" (VObj [])) l
    ,
    do -- Object: $O(varExp)[ a1 = x1 , ...., an = xn ]
        try( string "$O(" )
        blankCom
        ve <- varExp
        blankCom
        char ')'
        blankCom
        char '[' 
        l <- sepBy assignmentWordParser (comma ) 
        char ']'
        case getObjVarExp (dictToKeys l) ve of
          Just dve -> return $ Obj dve l
          Nothing ->  parserFail "Wrong var-exp (format or variable collision) in $O(... <- error here)[]"
    ,
    do
        try( string "$T[" )
        exprList <- expressionList1
        char ']'
        return (Tuple exprList)
    ,
    do
        try( string "$CT[" )
        blankCom
        char '('
        conds <- expressionList
        char ')'
        blankCom
        (string "⊢" <|> string "|-")
        blankCom
        conc <- parseToSourcesFm
        blankCom
        char ']'
        return (CTruth (map FormulaRepr conds) (FormulaRepr conc))
    ]


-- --------------------------------------------------------------------------------
-- --------------------------------------- Lowest level functions ------------------------------

    
-- | Parse a list of expression.
expressionList :: LazParsec [ SourcesFm ]
expressionList = do
    blankCom
    r <- sepBy parseToSourcesFm (comma )
    return r
    
-- | Parse a list of expression with at least one element.
expressionList1 :: LazParsec [ SourcesFm ]
expressionList1 = do
    blankCom
    r <- sepBy1 parseToSourcesFm (comma )
    return r

-- | Parse a list of n expression, n must be >0   
expressionCountedArgs :: Int -> LazParsec [ SourcesFm ]
expressionCountedArgs n =
    do
        e <- parseToSourcesFm
        if n == 1 
            then
            return [ e ]
            else
            do
                comma
                l <- expressionCountedArgs (n-1)
                return (e:l)


-- | Parse an assignment, keywords are ok.
assignmentWordParser :: LazParsec (String,SourcesFm)
assignmentWordParser =
  do
    blankCom
    w <- wordParser
    blanks
    char '='
    expr <- parseToSourcesFm
    return (w,expr)

-- | Translate a parser for one expression to a parser returning a list of expressions.
toMulti  :: LazParsec (Element SourcesFm) -> LazParsec (OppExpression SourcesFm)
toMulti parser = do
  r <- parser
  return [r]
