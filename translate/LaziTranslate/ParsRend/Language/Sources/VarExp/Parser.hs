-- | Define "parseSources", all the other functions are part of the definition of parseSources.
module LaziTranslate.ParsRend.Language.Sources.VarExp.Parser (varExpList , varList, varExp, varParser) where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char
import Text.Parsec.Char
import Text.Parsec.Prim hiding (try)
import Control.Monad.Trans hiding (try)
import Data.Functor.Identity
import Data.List
import Data.Set (member)
import Data.Char

import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E
import LaziTranslate.ParsRend
import LaziTranslate.ParsRend.CommonParser
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.ParsRend.Language.Sources.ParserBasics


varExpList :: LazParsec [VarExp]
varExpList = do
    blanks
    r <- sepBy varExp (try( do { blanks; comma; blanks }) )
    blanks
    return r
    

varExpList1 :: LazParsec [VarExp]
varExpList1 = do
    blanks
    r <- sepBy1 varExp (try( do { blanks; comma; blanks }) )
    blanks
    return r
    
varList :: LazParsec [String]
varList = do
    blanks
    r <- sepBy varParser (try( do { blanks; comma; blanks }) )
    blanks
    return r
    
varExp :: LazParsec VarExp
varExp = varExpOp <|> varExpNonOp <?> "variable expression"

varExpOp :: LazParsec VarExp
varExpOp = vassign <|> vlistAdd

varExpNonOp :: LazParsec VarExp
varExpNonOp = vword <|> vtuple <|> vlist <|> vdictObj <|> vparenthesed

-- ---------------- @

vassign :: LazParsec VarExp
vassign = do
  var <- try ( do
      var <- varParser
      blanks
      char '@'
      return var
    )
  blanks
  val <- varExp
  return $ VAssign var val
  
-- --------------- :l

vlistAdd :: LazParsec VarExp
vlistAdd = 
  try( do
      linit <- varExpNonOp
      blanks
      vlistAddRec linit
  )
  
vlistAddRec :: VarExp -> LazParsec VarExp
vlistAddRec linit = do
  try( string "+le" )
  blanks
  llast <- varExpNonOp
  blanks
  (vlistAddRec $ VListAdd linit llast) <|> (return $ VListAdd linit llast )
  
-- ----------------- variable


varParser :: LazParsec String
varParser = wordParser

-- ---------------------- vword

vword :: LazParsec VarExp
vword = do
  var <- varParser
  return $ VWord var

-- ---------------------- $T[  ]

vtuple :: LazParsec VarExp
vtuple = do
  try ( string "$T[" )
  l <- varExpList1
  char ']'
  return $ VTuple l

-- ---------------------- $L[   ]

vlist :: LazParsec VarExp
vlist = do
  try ( string "$L[" )
  l <- varExpList
  char ']'
  return $ VList l


-- ---------------------- $D[   ]


vdictObj :: LazParsec VarExp
vdictObj = do
  dictObj <- try ( do{ char '$'; dictObj <- (do{char 'D';return VDict;} <|> do{char 'O';return VObj;}); char '['; return dictObj;} )
  blanks
  l <- vlistDictAssign
  blanks
  char ']'
  return $ dictObj l

vlistDictAssign :: LazParsec [ (String , VarExp) ]
vlistDictAssign = do
    r <- sepBy dictAssign (try( do { blanks; comma; blanks }) )
    return r

dictAssign :: LazParsec (String , VarExp)
dictAssign = dictAssignExpr <|> dictAssignSelf

dictAssignExpr :: LazParsec (String , VarExp)
dictAssignExpr = do
  name <- try( do
      name <- wordParser 
      blanks
      char '='
      return name
    )
  blanks
  val <- varExp
  return ( name, val )

dictAssignSelf :: LazParsec (String , VarExp)
dictAssignSelf = do
  name <- wordParser 
  return ( name, VWord name )

-- --------------------------- ( ... )

vparenthesed :: LazParsec VarExp
vparenthesed = do
  char '('
  blanks
  ve <- varExp
  blanks
  char ')'
  return ve
