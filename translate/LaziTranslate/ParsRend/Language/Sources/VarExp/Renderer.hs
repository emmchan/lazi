-- Translate a VarExp to a String
module LaziTranslate.ParsRend.Language.Sources.VarExp.Renderer  where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char
import Data.List
import Data.Char
import Data.Maybe

import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E
import LaziTranslate.ParsRend
import LaziTranslate.ParsRend.CommonRenderer
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.ParsRend.PrintText


-- | Print a Var
printVar :: String -> String
printVar n = n


-- | Print a list of variables
printVarList :: [String] -> PrintText
printVarList l = TextList (map (normalText . printVar) l) [ SepComma,Close,Pack ]


-- | Print a list of VarExp (like in function definition variables).
printVarExpList :: [VarExp] -> PrintText
printVarExpList l = TextList (map printVarExp l) [ SepComma,Close,Pack ]

-- | Print a bracketed list of VarExp (like in function definition variables).
printVarExpListBrack :: [VarExp] -> PrintText
printVarExpListBrack l = TextList (map printVarExp l) [ SepComma, EncBracket,Close,Pack ]

-- | Print a VarExp

printVarExp :: VarExp -> PrintText

printVarExp (VWord v) = normalText $ printVar v

printVarExp (VAssign var val) = 
  TextList [ 
    normalText $ printVar var ,
    normalText "@",
    printVarExp val
  ] [ Pack ]
  
printVarExp (VTuple l) =
  TextList [ 
    normalText "$T", 
    ( TextList (map printVarExp l) [ SepComma, EncBracket ] )
  ] [ Pack ]

printVarExp (VDict l) =
  TextList [ 
    normalText "$D", 
    ( TextList (map printDictAssign l) [ SepComma, EncBracket ] )
  ] [ Pack ]

printVarExp (VObj l) =
  TextList [ 
    normalText "$O", 
    ( TextList (map printDictAssign l) [ SepComma, EncBracket ] )
  ] [ Pack ]

printVarExp (VList l) =
  TextList [ 
    normalText "$L", 
    ( TextList (map printVarExp l) [ SepComma, EncBracket ] )
  ] [ Pack ]

printVarExp (VListAdd init last) =
  TextList [ 
    printProtectVarExpLeft init,
    normalText "+le", 
    printProtectVarExpRight last
  ]  [ SepSpace, Pack ]

-- ---------------------- 

-- | Print a directory assignment
printDictAssign :: (String , VarExp) -> PrintText

-- if the entry has the same name as the variable: we simplify
printDictAssign (var,VWord x) | x == var = normalText var
  

printDictAssign (var,val) = 
  TextList [ 
    normalText var ,
    normalText "=",
    printVarExp val
  ] [ SepSpace, Pack ]

-- ---------------------- 

varExpUseOp :: VarExp -> Bool
varExpUseOp (VAssign _ _) = True
varExpUseOp (VListAdd _ _) = True
varExpUseOp _ = False

varExpUseAssign :: VarExp -> Bool
varExpUseAssign (VAssign _ _) = True
varExpUseAssign _ = False

printProtectVarExpLeft :: VarExp -> PrintText

printProtectVarExpLeft v = 
  if varExpUseAssign v 
     then
      TextList [ printVarExp v ] [ EncParenthese ]
     else
      printVarExp v

    
printProtectVarExpRight :: VarExp -> PrintText

printProtectVarExpRight v = 
  if varExpUseOp v 
     then
      TextList [ printVarExp v ] [ EncParenthese ]
     else
      printVarExp v

    
