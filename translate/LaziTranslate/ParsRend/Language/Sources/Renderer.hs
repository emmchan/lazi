-- Translate an ComputeFm to a String. Use Opparenth to remove useless parentheses. 
-- We have this property after reduceParenth : parentheses to render are only those expressed by the constructor "Parentheses". Except SourcesFm inside Item, these SourcesFm are not inside by an Apply but by other expressions which protect them, like "NameRepr".
module LaziTranslate.ParsRend.Language.Sources.Renderer (renderExpr)  where

-- import Text.ParserCombinators.Parsec
-- import Text.ParserCombinators.Parsec.Char
import Data.List
import Data.Maybe
import qualified Data.Map as M
import qualified Data.Set as S

import LaziTranslate.Formulas.ComputeFm.Word
import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as E
import LaziTranslate.ParsRend
import LaziTranslate.ParsRend.CommonRenderer
import LaziTranslate.Formulas.SourcesFm.SourcesFm
import LaziTranslate.Formulas.SourcesFm.VarExp
import LaziTranslate.ParsRend.OpInfos
import LaziTranslate.ParsRend.Opparenth.OppExpression
import LaziTranslate.ParsRend.Opparenth.Renderer
import LaziTranslate.ParsRend.PrintText
import LaziTranslate.ParsRend.Language.Sources.VarExp.Renderer
import LaziTranslate.Formulas.SourcesFm.FromCompute.Base


type OppNota = OppExpression SourcesFm

--  | Translate a E.ComputeFm to a String, with elimination of unnecessary parenthesis.
--  Arguments:
--  * the set of dvar words
--  * The opperators list
--  * the ComputeFm to translate
renderExpr :: S.Set String -> OperatorsInfos -> E.ComputeFm -> String

renderExpr defs opperatorsList e = printText ""  $ renderFormulaNota $ toListNotation $ computeToSources defs e  where


  -- | Translate a SourcesFm to a String, with elimination of unnecessary parenthesis.
  renderFormulaNota :: SourcesFm -> PrintText
  renderFormulaNota e = renderOppExpr $ reduceParenth [ formulaNotaToOpp (searchOpp opperatorsList) e]

  -- ------------------------------------------------------------
  -- | Translate an OppNota to a String. Here we consider separators as operators.

  renderOppExpr :: OppNota -> PrintText
  renderOppExpr l =  if useComma l then renderOppExprComma l else renderOppExprNormal l


  renderOppExprNormal :: OppNota -> PrintText
  renderOppExprNormal l = 
      case weakestOpPrecedence l of 
          Just pr ->
              TextList ( map renderOppOrList ( splitByWeakestOp pr l)) [ SepSpace,OpInList ]
          Nothing ->
              TextList (map renderItem l) [ SepSpace ]

  -- | Render Either the operator (an Element) or a OppNota. This is used to render the result of splitByWeakestOp.
  renderOppOrList :: Either (Element SourcesFm) OppNota -> PrintText
  renderOppOrList (Left x) = 
      let (TextString s al) = renderItem x in
          TextString s (OpInList:al)
  renderOppOrList (Right l) = renderOppExpr l
              
  -- | Split a list of items at the weakest infix operators (pr is the weakest priority). The infix operators are alone in a Left and the other elements in a Right list. For examole [ a b op c op d e f ] -> [ Right [ a b ] , Left op , Right [ c ] , Left op , Right [ d e f ] ]
  splitByWeakestOp :: Int -> OppNota -> [Either (Element SourcesFm) OppNota]
  splitByWeakestOp pr l =
      case splitByWeakestOpOneChunk pr l of
          (ls,[]) -> [Right l]
          (ls,lt) -> ls : splitByWeakestOp pr lt

  -- | Do the job of splitByWeakestOp for one time. Return the first chunk and the rest of the list. The rest can be empty if no operator is found.
  splitByWeakestOpOneChunk :: Int -> OppNota -> (Either (Element SourcesFm) OppNota,OppNota)
  splitByWeakestOpOneChunk _ [] = (Right [],[])
  splitByWeakestOpOneChunk pr (o@(Item (Operator pri Infix) _):l) | pr==pri = 
      (Left o,l)
  splitByWeakestOpOneChunk pr (x:o@(Item (Operator pri Infix) _):l) | pr==pri = 
      (Right [x],o:l)
  -- 2 same cases but with separators
  splitByWeakestOpOneChunk pr (o@(Separator pri):l) | pr==pri = 
      (Left o,l)
  splitByWeakestOpOneChunk pr (x:o@(Separator pri):l) | pr==pri = 
      (Right [x],o:l)
  -- If there is an element after x then it's not a matching operator. So we can continue the same chunk.
  splitByWeakestOpOneChunk pr (x:l) = 
      let (Right ls,le)=splitByWeakestOpOneChunk pr l in
          (Right (x:ls),le)
          

  -- | Use comma between parenthesed expression. If the first item is a function, word or global constant, then use comma for all arguments, else start at first and end at last parenthesed.
  renderOppExprComma :: OppNota -> PrintText
  renderOppExprComma l@(v:_) = 
      if isLikeFunction v then renderOppExprCommaFunc l else renderOppExprCommaNormal l 

  isLikeFunction :: Element SourcesFm -> Bool
  isLikeFunction (Item (NonOp _) (FunctionM _ _)) = True
  isLikeFunction (Item (NonOp _) (Word _ _)) = True
  isLikeFunction _ = False

  -- | Render an operator expression list in a function style
  renderOppExprCommaFunc :: OppNota -> PrintText
  renderOppExprCommaFunc (v:l) =
      TextList [renderItem v, renderOppExprCommaAll l] [ SepSpace ]

  -- | We use comma, but not for the non parenthesed items at start and end. So we split the list in 3 parts. (list non parenthesed start, list parenthesed, list non parenthesed end).
  renderOppExprCommaNormal :: OppNota -> PrintText
  renderOppExprCommaNormal l =
      let (lnps,lp,lpne)= splitMiddlePar l in
      case (lnps,lpne) of
          ([], []) -> renderOppExprCommaAll lp
          (_:_ , []) -> 
              TextList [ renderOppExprNormal lnps, renderOppExprCommaAll lp
                      ] [ SepSpace ]
          ( [], _:_ ) -> 
              TextList [ renderOppExprCommaAll lp,renderOppExprNormal lpne
                      ] [ SepSpace ]
          _ -> TextList [ renderOppExprNormal lnps, renderOppExprCommaAll lp,renderOppExprNormal lpne
                      ] [ SepSpace ]

  -- | see renderOppExprCommaNormal above
  splitMiddlePar :: OppNota -> (OppNota, OppNota,OppNota)
  splitMiddlePar l =
      let (ls,r) = break isParenthesed l in
          let (le,lm) = break isParenthesed (reverse r) in 
              (ls,reverse lm,reverse le)

  -- | Render an operator expression list using ( x1 , x2 ... , xn ) style
  renderOppExprCommaAll :: OppNota -> PrintText 
  renderOppExprCommaAll l = TextList (map renderItemUnpar l) [ EncParenthese, SepComma ]

  -- | Render an Item but if he is parenthesed then remove them and render the list.
  renderItemUnpar :: Element SourcesFm -> PrintText
  renderItemUnpar (Parentheses l) = renderOppExpr l
  renderItemUnpar x = renderItem x

  -- | Decide if we use a comma for all items of the list : true if more than 45% is parenthesed.
  useComma :: OppNota -> Bool
  useComma l =
      if (length l<2 || or (map (\x -> isOperator x || isSeparator x) l)) 
        then
          False
        else
          fromIntegral (length (filter isParenthesed l)) / fromIntegral (length l) > 0.45

  -- | Is it a Parentheses of two elements ?
  is2Parenthesed :: Element SourcesFm -> Bool
  is2Parenthesed (Parentheses [_,_]) = True
  is2Parenthesed _ = False
          
  isParenthesed :: Element SourcesFm -> Bool
  isParenthesed (Parentheses _) = True
  isParenthesed _ = False

  -- ---------------------------------------------
  -- | Render an Item 

  renderItem :: Element SourcesFm -> PrintText

  renderItem e@(Parentheses l) = 
      let (TextList tl al) = renderOppExpr l in
          TextList tl (EncParenthese : al)

  renderItem (Item (NonOp NoProt) e) = printFormulaNoPar e

  renderItem (Item (Operator _ Infix) e) =
      case searchOpInfos opperatorsList 2 e of
        Just (OpInfos _ _ s _) -> normalText s
        Nothing -> printFormulaNoPar e -- An operator can come from somewhere else than opperatorsList. Oh ! ? Always true ? Dead code ?
    
  -- If it's the special operator for the notation infixOp'word :
  renderItem (Item (Operator _ Postfix) (ReverseArg fop w)) =
    case searchOpInfos opperatorsList 2 fop of
        Just (OpInfos _ _ s _) -> normalText $ s ++ "'" ++ w
        Nothing -> error $ "In renderItem. ReverseArg without corresponding infix operator:\n" ++ show fop
        
  -- Then it's a 1 argument operator
  renderItem (Item _ e) =
    case searchOpInfos opperatorsList 1 e of
      Just (OpInfos _ _ s _) -> normalText s
      Nothing -> printFormulaNoPar e -- An operator can come from somewhere else than opperatorsList. Oh ! ? Always true ? Dead code ?

  renderItem (Separator prec) = normalText (if prec == bigSepPrec then doubleSep else simpleSep)
  -- ---------------------------------------------

  -- | Translate a SourcesFm to a String, without the problem of eliminating unnecessary parenthesis.
  printFormulaNoPar :: SourcesFm -> PrintText

  printFormulaNoPar (NameRepr w) = normalText $ "'" ++ w

  printFormulaNoPar (FormulaRepr e) = 
      TextList [ normalText "$F", 
          ( TextList [renderFormulaNota e] [ EncBracket ] )
          ] [ Pack ]
 
  printFormulaNoPar (Word WtUnknown w) = normalText w
  printFormulaNoPar (Word WtLVar w) = normalText $ ":lvar:" ++ w
  printFormulaNoPar (Word WtDVar w) = normalText $ ":dvar:" ++  w
  printFormulaNoPar (Word WtKey0 w) = normalText $ ":key0:" ++  w
  printFormulaNoPar (Word WtGvar w) = normalText $ ":gvar:" ++  w
         
  -- Let
  printFormulaNoPar (Let v r body) =
      TextList [
          TextList [ 
              normalText "$Let" ,
              printVarExp v,
              normalText "=",
              renderFormulaNota r
            ] [ SepSpace, Pack ],
          renderFormulaNota body
      ]  [ SepComma,NoShift ]

  -- normals functions
  printFormulaNoPar (FunctionM lv rf) =
      TextList [ 
          TextList [ normalText "$F" ,
              printVarExpList lv,
              normalText "→"
              ] [ SepSpace, Pack ],
          renderFormulaNota rf
          ] [ SepSpace, Shift ]
  
  -- $T[...]
  printFormulaNoPar (Tuple l) =
      TextList [ normalText "$T", 
          ( TextList (map renderFormulaNota l) [ SepComma, EncBracket ] )
          ] [ Pack ]
  
  -- $CT[ ( ∅l , cond2 ... ) ⊢ conc ]
  printFormulaNoPar (CTruth [] conc) =
      TextList [ normalText "$CT", 
          ( TextList [
              normalText "()", 
              normalText "⊢",
              renderFormulaNota (stripFormulaRepr conc)
            ] [ EncBracket,SepSpace ] )
            ] [ Pack ]
  
  -- $CT[ ( cond1 , cond2 ... ) ⊢ conc ]
  printFormulaNoPar (CTruth conds conc) =
      TextList [ normalText "$CT", 
          ( TextList [
              ( TextList (map (renderFormulaNota . stripFormulaRepr) conds) [ SepComma, EncParenthese ] ),
              normalText "⊢",
              renderFormulaNota (stripFormulaRepr conc)
            ] [ EncBracket,SepSpace ] )
            ] [ Pack ]
 
  printFormulaNoPar f@(List []) = normalText "∅l"

  -- Dict
  printFormulaNoPar (Dict l) =
      TextList [ normalText "$D" , 
          ( TextList (map renderDictEqual l) [ SepComma, EncBracket ] )
          ] [ IsDictionary, Pack, NoShift ]

  -- Obj $O[...]
  printFormulaNoPar (Obj (VAssign "this" (VObj [])) l) =
      TextList [ normalText ("$O") , 
          ( TextList (map renderDictEqual l) [ SepComma, EncBracket ] )
          ] [ IsDictionary, Pack, NoShift ]

  -- Obj $O(...)[...]
  printFormulaNoPar (Obj v l) =
      TextList [ 
          TextList [ normalText ("$O" ++ "("),
            printVarExp (cleanEmptyVE v),
            normalText ")"
          ] [ Close,Pack ],
          ( TextList (map renderDictEqual l) [ SepComma, EncBracket ] )
          ] [ IsDictionary, Pack, NoShift ]

  printFormulaNoPar f@(List l) =
      TextList [ normalText "$L", 
          ( TextList (map renderFormulaNota l) [ SepComma, EncBracket ] )
          ] [ Pack ]

  printFormulaNoPar (ListQuantification quant rev v listExpr p) =
      TextList [
        TextList [
            (normalText (printQuantifier quant ++ "l" ++ (if rev then "r" else "") ++ " ")),
            (printVarExp v),
            (normalText "/"),
            (renderFormulaNota listExpr),
            normalText ";"] 
            [ Close,Pack ],
        renderFormulaNota p
      ]  [ NoShift ]

  printFormulaNoPar x = error $ show x
          
  -- | Render one dictionnary equality
  renderDictEqual :: (String,SourcesFm) -> PrintText
  renderDictEqual (x,y) =
      TextList [ normalText x , normalText "=", renderFormulaNota y ] [SepSpace,Pack]



---------------------------------------------------
-- | Return Just the lowest precedence of infix operators or separators in an OppExpression t, else return Nothing
printQuantifier Universal = "∀"
printQuantifier Existential = "∃"

---------------------------------------------------
-- | Return Just the lowest precedence of infix operators or separators in an OppExpression t, else return Nothing
weakestOpPrecedence :: (UnderExpr t) => OppExpression t -> Maybe Precedence

weakestOpPrecedence l = 
  let lp = map getPrecedence (filter isInfixOrSep l) in
      if null lp then Nothing else Just (minimum lp)
