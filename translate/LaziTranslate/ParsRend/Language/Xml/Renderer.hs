-- Translate an ComputeFm to a String. 
module LaziTranslate.ParsRend.Language.Xml.Renderer (renderExpr)  where

import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.Formulas.ComputeFm.ComputeFm

-- | Print a ComputeFm to an XML string format
renderExpr :: ComputeFm -> String

renderExpr (Word wt s) = "<word type=\"" ++ show wt ++ "\" name=\"" ++ s ++ "\"/>"

renderExpr (Apply x y) = "<apply><f>" ++ renderExpr x ++ "</f><a>" ++ renderExpr y ++ "</a></apply>"

renderExpr (Function v e) = "<function var=\"" ++ v ++ "\">" ++ renderExpr e ++ "</function>"

renderExpr (Dict l) = "<dict>" ++ concatMap renderEntry l ++ "</dict>"

renderExpr (Obj v l) = "<obj var=\"" ++ v ++ "\">" ++ concatMap renderEntry l ++ "</obj>"

renderExpr (NameRepr x) = "<nameRepr name=\"" ++ x ++ "\"/>"

renderExpr (FormulaRepr x) = "<formulaRepr>" ++ renderExpr x ++ "</formulaRepr>"

-- | Print an entry of a dictionnary or an object.
renderEntry :: (String,ComputeFm) -> String

renderEntry (key,value) = "<entry name=\"" ++ key ++ "\">" ++ renderExpr value ++ "</entry>"
