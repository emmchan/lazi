-- | Parse an xml formula
module LaziTranslate.ParsRend.Language.Xml.Parser (totalExpression) where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char
import Data.List
import Data.Char

import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.ParsRend.CommonParser hiding (function)
import LaziTranslate.Formulas.ComputeFm.ComputeFm



-- LazParsec to parse a unicode formula. If there is no error we return Right ComputeFm which is the corresponding Lazi'nh formula.

-- | Parse an formula which end with "end of file": all is parsed.
totalExpression :: LazParsec ComputeFm
totalExpression =  do
    e <- formula
    return e

-- | Parse an formula
formula :: LazParsec ComputeFm
formula =  do
    blankSeps
    e <- ( word <|> apply <|> function <|> dict <|> obj <|> nameRepr <|> formulaRepr ) 
    blankSeps
    return e

-- | What is a Lazi' Word
word = do
    try (string "<word ")
    blankSeps
    t <- attributeEnum "type" [("key0",WtKey0), ("lvar",WtLVar), ("dvar",WtDVar), ("gvar",WtGvar)]
    n <- attribute "name"
    string "/>"
    return $ Word t n

apply :: LazParsec ComputeFm
apply = do 
    try (tago "apply")
    f <- inTags "f" formula
    a <- inTags "a" formula
    tagc "apply"
    return (Apply f a)
    
function = do
    try (string "<function")
    var <- attribute "var"
    char '>'
    body <- formula
    tagc "function"
    return (Function var body)
    
dict = do
    try (string "<dict>")
    blankSeps
    l <- many1 dictEntry
    tagc "dict"
    return (Dict l)
 
obj = do
    try (string "<obj")
    var <- attribute "var"
    char '>'
    blankSeps
    l <- many1 dictEntry
    tagc "obj"
    return (Obj var l)

dictEntry = do
  try (string "<entry")
  name <- attribute "name"
  char '>'
  value <- formula
  tagc "entry"
  blankSeps
  return (name,value)

nameRepr = do
    try (string "<nameRepr")
    name <- attribute "name"
    string "/>"
    return (NameRepr name)
    
formulaRepr = do
    e <- inTags "formulaRepr" formula
    return (FormulaRepr e)


-- --------------------------------------------------------------------------------
-- ------------------------------- basic parsers ----------------------------------

-- | open (o) and close tags
tago :: String -> LazParsec ()
tago s = do
    blankSeps
    string ( "<" ++ s ++ ">" )
    return ()

tagc :: String -> LazParsec ()
tagc s = do
    blankSeps
    string ( "</" ++ s ++ ">" )
    return ()

-- | Parse something using "pars" in a simple open/close tags
inTags :: String -> LazParsec x -> LazParsec x
inTags s pars = do
    try (tago s)
    r <- pars
    tagc s
    return r

-- | parse an attribute "s" and return the value
attribute s = do
    blankSeps
    string s
    string "=\""
    v <- many (noneOf "\"")
    char '"'
    blankSeps
    return v

-- | parse an attribute when the value can be in a finite list of strings. Arguments are:
-- | - the attribute name
-- | - a list of pair (string,result) where string is the expected attribute value and result is the returned value.
attributeEnum s l = do
    blankSeps
    string s
    string "=\""
    r <- stringEnum l
    char '"'
    blankSeps
    return r
    
-- --------------------------------------------------------------------------------
-- -------------------------- Lowest level functions ------------------------------

-- default value for the "blank separator" parser
blankSepBase = do { space <|> tab <|> newline; return () }

-- LazParsec for a Unicode blank space
blankSep = blankSepBase


-- LazParsec for a many Unicode blank space, possibly zero
blankSeps :: GenParser Char st ()
blankSeps= skipMany blankSep
