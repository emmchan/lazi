-- Translate an Lazi_0_0Fp to a String or to the depth or the formula.
module LaziTranslate.ParsRend.Language.Lazi_0_0.Renderer (renderExpr,renderExprDepth)  where

import qualified LaziTranslate.Formulas.ComputeFm.ComputeFm as C
import LaziTranslate.Formulas.Lazi_0_0Fm.Lazi_0_0Fm
import LaziTranslate.Formulas.ComputeFm.Definitions 
import LaziTranslate.Formulas.ComputeFm.ToLazi_0_0Fm.Base

-- | Convert a ComputeFm to a Lazi_0_0Fm and print it.
renderExpr :: DefsMap -> C.ComputeFm -> String

renderExpr df f = renderExpr' $ sourcesToCompute df f

-- | Print a Lazi_0_0Fm to a string format
renderExpr' :: Lazi_0_0Fm -> String

renderExpr' (Word s) = s

renderExpr' (Apply (Word x) (Word y)) = x ++ " " ++ y

renderExpr' (Apply (Word x) y) = x ++ " ( " ++ renderExpr' y ++ " )"

renderExpr' (Apply x (Word y)) = "( " ++ renderExpr' x ++ " ) " ++ y

renderExpr' (Apply x y) = "( " ++ renderExpr' x ++ " )( " ++ renderExpr' y ++ " )"

-- | Convert a ComputeFm to a Lazi_0_0Fm and print its depth.
renderExprDepth :: DefsMap -> C.ComputeFm -> String

renderExprDepth df f = renderExprDepth' $ sourcesToCompute df f


-- |  Print the depth of the formula
renderExprDepth' :: Lazi_0_0Fm -> String

renderExprDepth' f = show $ depth f
