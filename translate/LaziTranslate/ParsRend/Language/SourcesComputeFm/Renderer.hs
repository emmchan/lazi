-- Translate an ComputeFm to a String, as a Source, the file can be read by "Sources" parser.. 
module LaziTranslate.ParsRend.Language.SourcesComputeFm.Renderer (renderExpr)  where

import Data.List

import LaziTranslate.Formulas.ComputeFm.Word
import LaziTranslate.Formulas.ComputeFm.ComputeFm

-- | Print a ComputeFm to an XML string format
renderExpr :: ComputeFm -> String

renderExpr (Word wt s) = ":" ++ show wt ++ ":" ++ s

renderExpr (Apply x y) = "(" ++ renderExpr x ++ " " ++ renderExpr y ++ ")"

renderExpr (Function v e) = "($F " ++ v ++ " → " ++ renderExpr e ++ ")"

renderExpr (Dict l) = "$D[ " ++ renderEntries l ++ " ]"

renderExpr (Obj v l) = "$O(v)[ " ++ renderEntries l ++ " ]"

renderExpr (NameRepr x) = "'" ++ x

renderExpr (FormulaRepr x) = "$F[ " ++ renderExpr x ++ " ]"

-- | Print an entry of a dictionnary or an object.
renderEntry :: (String,ComputeFm) -> String

renderEntry (key,value) = key ++ " = " ++ renderExpr value

-- | Print all entries of a dictionnary or an object.
renderEntries :: [(String,ComputeFm)] -> String

renderEntries l = intercalate " ,\n" $ map renderEntry l
