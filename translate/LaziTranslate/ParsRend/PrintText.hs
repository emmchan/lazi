-- | Library to represent source code as text and to convert it to a String. Except printPaddedText convert functions don't put spaces at start (even padding), so we are more free to use the result anywhere.

module LaziTranslate.ParsRend.PrintText  (
    printText,PrintText(..),TextAttribs,TextAttrib(..),basicTextList,normalText,addToTextList
    ) 
    where

import Data.List
import Data.Char

-- | A text to print
data PrintText =
    TextList 
        [ PrintText ] -- Elements of the lists
        TextAttribs -- Attributes to configure the printing
    |
    TextString 
        String -- The string to print
        TextAttribs -- Attributes to configure the printing
     deriving Show

type TextAttribs = [ TextAttrib ]       
        
data TextAttrib = 
    OpInList | -- ^ operator or separator in a list (to break line with it)
    IsDictionary | -- ^ It's a list of assignments
    EncBracket | -- ^ Enclose the list by [ ]
    EncParenthese | -- ^ Enclose the list by ( )
    SepComma | -- ^ Use a comma as separator
    SepSpace | -- ^ Use a space as separator
    Close | -- ^ Remove as much space as possible
    NoShift | -- ^ Don't add more padding
    Shift | -- ^ Add more padding
    Pack  -- ^ If the list dont fit, don't put one element by line, but pack as much as possible.
     deriving (Eq,Show)

minMaxWidth :: Int
maxMaxWidth :: Int
minMaxWidth = 80
maxMaxWidth = 120
     
tabSpace :: String
tabSpace = "  "

--------------------------------------------------------------------------   
-- | Main function 
printText :: String ->  PrintText -> String
printText pad (TextString s _) = s

-- debug only
printText _ t@(TextList _ _ ) | isSomewhereEmpty t =
    error $ "Empty TextList in \n" ++ show t

printText pad tl = 
    (if isTooBig pad tl then printBigList else printSmallList) pad tl 


-- | Same as printText but with padding (add spaces at the head of each line)
printPaddedText :: String -> PrintText -> String

printPaddedText pad i = pad ++ printText pad i

--------------------------------------------------------------------------   
-- | Debug : check if there is an empty TextList

isSomewhereEmpty :: PrintText -> Bool

isSomewhereEmpty (TextString _ _) = False

isSomewhereEmpty (TextList [] _) = True

isSomewhereEmpty (TextList l _) = or (map isSomewhereEmpty l)

--------------------------------------------------------------------------   
-- | Compute if the PrintText is too big (in this case we use \n).

-- | Compute the maxWidth, using the pad
maxWidth :: String -> Int
maxWidth pad = max minMaxWidth (maxMaxWidth - length pad)

isTooBig :: String -> PrintText -> Bool
isTooBig pad t = isTooBig' (maxWidth pad) t < 0

-- | Internal for "isTooBig" : compute what space is left after printing the text, stop if <0
isTooBig' :: Int -> PrintText -> Int
isTooBig' n (TextString s _) = n - length s
isTooBig' n (TextList l _) = 
    ( foldl (\n' x -> if n'<0 then n' else isTooBig' n' x) (n - length l) l ) 

--------------------------------------------------------------------------   
-- | Print a small list (fit in a line)

printSmallList :: String -> PrintText -> String
printSmallList pad t@(TextList _ al) =
    let inside = printSmallListIntern  pad t in
    if EncBracket `elem` al || EncParenthese `elem` al
       then
        let (start,stop) = if EncBracket `elem` al then ("[","]") else ("(",")") in
        let space=(
                if  SepSpace `elem` al || SepComma `elem` al 
                    then 
                        if Close `elem` al then "" else " " 
                    else 
                        "" )
            in
          start ++ space ++ inside ++ space ++ stop
       else
        inside
        
printSmallListIntern :: String -> PrintText ->  String
printSmallListIntern pad t@(TextList l al) =
    intercalate (getSep pad al False) (map (printText pad) l)


--------------------------------------------------------------------------   
-- | Print a big list (doesn't fit in a line)

-- String : padding (spaces at start)
-- PrintText : text-list to print
-- pad is the padding of the caller
printBigList :: String -> PrintText -> String
printBigList pad t@(TextList l al) = 
    let newPad=pad++(if NoShift `elem` al then "" else tabSpace) in
    if EncBracket `elem` al || EncParenthese `elem` al
    then
        let (start,stop) = if EncBracket `elem` al then ("[","]") else ("(",")") in
        let (interStart,interStop)= 
             if Close `elem` al 
               then 
                ("","")
               else 
                if Pack `elem` al then (" "," ") else ("\n" ++ newPad, "\n" ++ pad)
          in
          start ++ interStart ++ 
            printBigListIntern newPad pad t ++ interStop ++ stop
    else
        printBigListIntern (if Shift `elem` al then newPad else pad) pad t

-- First pad is for elements, second is for the quote
printBigListIntern :: String -> String -> PrintText -> String
printBigListIntern pad qpad t@(TextList l al)|Pack `elem` al =
    printBigListInternPacked (maxWidth pad) False pad (getSep qpad al False) (getSep qpad al True) ( rearrange t )

printBigListIntern pad qpad t@(TextList l al) =
    printBigListInternNotPacked pad (getSep qpad al True) ( rearrange t )

-- String : padding (spaces at start)
-- String : padding for the separator
-- String : separator 
-- PrintText : text-list to print
printBigListInternNotPacked :: String -> String -> PrintText -> String

printBigListInternNotPacked pad sep tl@(TextList (i:l) al ) =
    printText pad i ++ 
    if null l then "" else sep ++ pad ++ 
        printBigListInternNotPacked pad sep (TextList l al )

printBigListInternNotPacked _ _ (TextList [] _ ) = ""    
    
-- | Same as printBigListInternNotPacked but try to put more than one element on the line. The first argument is the space left. The Bool is to tell if the line is started. sep is the separator on a line, sepcr is the multi-line separator.
printBigListInternPacked :: Int -> Bool -> String -> String -> String -> PrintText -> String
printBigListInternPacked left started pad sep sepcr tl@(TextList (i:l) al ) =
    let t = printText pad i in
     let (multi,len)=elementSize t in
        ( if started 
            then
                if len < left 
                then
                    sep ++ t
                else
                    sepcr ++ pad ++ t
            else -- we are on a new line
                t
        ) ++    
        if null l -- nothing after
           then 
            "" 
         else
            if multi 
             then
                sepcr ++ pad ++ printBigListInternPacked (maxWidth pad) False  pad sep sepcr (TextList l al)
             else 
                printBigListInternPacked (left-len) True  pad sep sepcr (TextList l al )

-- | For a printed element, tell if it is multi-line and give the length first line
elementSize :: String -> (Bool,Int)
elementSize t = 
    if '\n' `elem` t 
        then 
            (True, length $ head (lines t))
        else 
            (False,length t)

--------------------------------------------------------------------------   
-- | Rearrange a big list before printing
rearrange :: PrintText -> PrintText

-- If it's a list with operators or a dictionnary
rearrange (TextList l al)|OpInList `elem` al = 
    TextList (rearrangeOpp l) al

rearrange (TextList [d, TextList l sal] al)|IsDictionary `elem` al && Pack `notElem` sal = 
    TextList [d, TextList (rearrangeDict l) sal] al

rearrange x = x

-- | Put the operator at the end of the line
rearrangeOpp :: [ PrintText ] -> [ PrintText ]

rearrangeOpp [ ] = [ ]

rearrangeOpp [ x ] = [ x ]

rearrangeOpp (tl@(TextList li al):op@(TextString _ al2):l)|OpInList `elem` al2  = 
    TextList [ tl, op ] [ SepSpace ] : rearrangeOpp l

rearrangeOpp (i:l) = i : rearrangeOpp l

-- | Add spaces to allign equalty
rearrangeDict :: [ PrintText ] -> [ PrintText ]

rearrangeDict l = map (rearrangeDictEntry (maxDictEntry l)) l

rearrangeDictEntry m tl@(TextList ((TextString n als):lr) al) =
    (TextList ((TextString (n ++ replicate (m - length n) ' ') als):lr) al)

rearrangeDictEntry _ x = error (show x)

-- | Find the max size of the dictionnaries entries.
maxDictEntry :: [ PrintText ] -> Int
maxDictEntry  = foldl ( \m (TextList ((TextString n _):_) _) -> max m (length n)) 0 


    
--------------------------------------------------------------------------   
-- | Compute the separator to use. Arguments are: pad, element's attributes, in big list.

getSep :: String  ->TextAttribs -> Bool -> String

-- one line
getSep pad al False|Close `elem`  al && SepComma `elem`  al = ","
getSep pad al False|SepComma `elem`  al = " , "

getSep pad al False|SepSpace `elem`  al = " "
getSep pad _ False = ""

-- multi-line
getSep pad al True|SepComma `elem` al = "\n" ++ pad ++ ";\n"
getSep pad al True = "\n"


--------------------------------------------------------------------------   
-- | Utilities

-- | Create a normal, space separeted list.
basicTextList :: [ String ] -> PrintText
basicTextList l = 
    TextList ( map (\s -> TextString s  []) l) [SepSpace]

    
-- | Add an element at the end of a TextList
addToTextList :: PrintText -> PrintText -> PrintText
addToTextList i (TextList l al) =
    TextList (l ++ [i]) al

-- | A normal TextString
normalText :: String -> PrintText
normalText s = TextString s []
