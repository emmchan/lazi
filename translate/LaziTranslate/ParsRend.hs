-- | Base module, define interface with extern program
module LaziTranslate.ParsRend  where

import LaziTranslate.Formulas.SourcesFm.SourcesFm

-- | Precedence is from 0 to 9
maxPrecedence = 10::Int

separatorChar = '\\'
simpleSep = [separatorChar]
doubleSep = [separatorChar,separatorChar]

-- -----------------------------------------------------
-- | Tell if a formula need to be protected on right side (by parentheses).
needRightProtect ::  SourcesFm -> Bool

needRightProtect (FunctionM _ _)  = True
needRightProtect (Let _ _ _)  = True
needRightProtect (ListQuantification _ _ _ _ _)  = True
needRightProtect _ = False
