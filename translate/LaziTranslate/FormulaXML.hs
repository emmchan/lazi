-- Parse a XML file, whatever dtd, just the tag "<formula>" is processed, the content is translated. Also produce the XML file of definitions (see definitionsToXml).
module LaziTranslate.FormulaXML (definitionsToXml,parseXml) where

import Text.ParserCombinators.Parsec hiding (try)
import Text.ParserCombinators.Parsec.Char
import Text.Parsec.Prim
import qualified Data.Map as M

import LaziTranslate.Sources
import LaziTranslate.ParsRend.CommonParser
import qualified LaziTranslate.ParsRend.Language.Xml.Parser as XmlP
import qualified LaziTranslate.ParsRend.Language.Sources.Parser as SourcesP
import qualified LaziTranslate.ParsRend.Language.Xml.Renderer as XmlR
import qualified LaziTranslate.ParsRend.Language.Sources.Renderer as SourcesR
import LaziTranslate.Formulas.ComputeFm.Definitions
import LaziTranslate
import LaziTranslate.Formulas.ComputeFm.ComputeFm

-- | The complete xml file content of all definitions. The formula are rendered in Format.
definitionsToXml :: Format -> ParsecUS -> [Definition] -> String
definitionsToXml format us ld =
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
\<definitions>\n" ++ concatMap (definitionToXml format us) ld ++ "</definitions>\n"

definitionToXml :: Format -> ParsecUS -> Definition -> String
definitionToXml format us (Definition n f) =
    "<definition name=\"" ++ n ++ "\">" ++ formula_to_xml format us f ++ "</definition>\n"

-- | Parse the content of an xml document replacing all <formula> tags by  <formula> tags where the content is a Lazi formula in Format. 
-- | The first argument is the "simplify" global option (-s).
parseXml :: Bool -> Format -> LazParsec String                     
parseXml s format = do
    l <- many ( (noTag format) <|> (formula s format) <|> (tag format) )
    eof
    return (concat l)

noTag :: Format -> LazParsec String         
noTag format = many1 ( noneOf "<" )

-- | The first argument is the "simplify" global option (-s).
formula :: Bool -> Format -> LazParsec String         
formula simpl format = do
    try (string "<formula ")
    spaces
    string "format=\""
    (readFormat,simpl2) <- choice [ (string "xml" >> return (FormatXML,simpl)),
                ( try(string "sourcesNoOp") >> return (FormatSources,False)),
                ( try(string "sources") >> return (FormatSources,simpl))  ]  
    char '"'
    spaces
    char '>'
    us <- getState
    res <- if readFormat == FormatXML
            then
                do
                    f <- XmlP.totalExpression
                    return $ formula_to_xml format us f 
            else
                do
                    s <- many ( noneOf "<" ) -- get the expression to parse
                    let res = runP  (formatToPars simpl2 readFormat) us "" (decode_escapes_xml s)
                    case res of 
                      Right f -> 
                        let err = checkFormula (M.keysSet (usDefinitions us)) f in
                          if null err
                            then
                              return $ formula_to_xml format us f
                            else
                              unexpected $ "Error in an xml formula: " ++ err
                      Left err -> unexpected (show err)
    string "</formula>"
    return res
                    
formula_to_xml :: Format -> ParsecUS -> ComputeFm -> String
formula_to_xml format us formula =
    let enc = (if format == FormatXML then id else encode_escapes_xml) in -- do we escape xml special chars ?
        "<formula format=\"" ++ show format ++ "\">" ++ 
        (enc ((formatToRend format us) formula) ) ++
        "</formula>"
   
                    
tag :: Format -> LazParsec String         
tag format = do
    char '<'
    s <- many ( noneOf ">" )
    char '>'
    return ("<" ++ s ++ ">")

encode_escapes_xml :: String -> String

encode_escapes_xml ('<' : l) = "&lt;" ++ (encode_escapes_xml l)
encode_escapes_xml ('&' : l) = "&amp;" ++ (encode_escapes_xml l)
encode_escapes_xml (x : l) = x : (encode_escapes_xml l)
encode_escapes_xml "" = ""

decode_escapes_xml :: String -> String
decode_escapes_xml ('&' : 'l' : 't' : ';' : l) = '<' : ( decode_escapes_xml l)
decode_escapes_xml ('&' : 'g' : 't' : ';' : l) = '>' : ( decode_escapes_xml l)
decode_escapes_xml ('&' : 'a' : 'm' : 'p' : ';' : l) = '&' : ( decode_escapes_xml l)
decode_escapes_xml ('&' : 'q' : 'u' : 'o' : 't' : ';' : l) = '\"' : ( decode_escapes_xml l)
decode_escapes_xml ('&' : '#' : '3' : '9' : ';' : l) = '\'' : ( decode_escapes_xml l)
decode_escapes_xml (x:l) = x : (decode_escapes_xml l)
decode_escapes_xml "" = ""


